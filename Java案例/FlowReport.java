package MM;

import Tools.UrlRequest;
import javafx.scene.Cursor;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.Context;


/**
 * 流量报告
 * Created by helloklf on 2016/8/5.
 */
public class FlowReport {
    static String Api = "";

    public String  Test(String r){
        String txt = new FlowReport.BaseDao().decryptString(r);
        System.out.println("DecodeResult:"+txt);
        return txt;
    }

    public static class DataFormat
    {
        public static String byte2String(byte[] paramArrayOfByte, String paramString)
        {
            Object localObject = null;
            try
            {
                return new String(paramArrayOfByte, paramString);
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                return null;
            }
        }

        public static int byte2int(byte[] paramArrayOfByte, int paramInt)
        {
            int j = 0;
            int i = 0;
            while (true)
            {
                if (i >= 4)
                    return j;
                j = j << 8 | paramArrayOfByte[(i + paramInt)] & 0xFF;
                i += 1;
            }
        }

        public static String bytesToHexString(byte[] paramArrayOfByte)
        {
            StringBuilder localStringBuilder = new StringBuilder("");
            if ((paramArrayOfByte == null) || (paramArrayOfByte.length <= 0))
                return null;
            int i = 0;
            while (true)
            {
                if (i >= paramArrayOfByte.length)
                    return localStringBuilder.toString();
                String str = Integer.toHexString(paramArrayOfByte[i] & 0xFF);
                if (str.length() < 2)
                    localStringBuilder.append(0);
                localStringBuilder.append(str);
                i += 1;
            }
        }

        public static String formatSize(float paramFloat)
        {
            String str = null;
            if (paramFloat >= 1024.0F)
            {
                str = "KB";
                float f = paramFloat / 1024.0F;
                paramFloat = f;
                if (f >= 1024.0F)
                {
                    str = "MB";
                    paramFloat = f / 1024.0F;
                }
                f = paramFloat;
                if (paramFloat >= 1024.0F)
                    str = "GB";
            }
            for (float f = paramFloat / 1024.0F; ; f = paramFloat)
            {
                StringBuilder localStringBuilder = new StringBuilder(new DecimalFormat("#0.00").format(f));
                if (str != null)
                    localStringBuilder.append(str);
                return localStringBuilder.toString();
            }
        }

        public static byte[] hex2Byte(String paramString)
        {
            int j = paramString.length() / 2;
            byte[] arrayOfByte = new byte[j];
            int i = 0;
            while (true)
            {
                if (i >= j)
                    return arrayOfByte;
                arrayOfByte[i] = Integer.valueOf(paramString.substring(i * 2, i * 2 + 2), 16).byteValue();
                i += 1;
            }
        }

        public static byte[] int2byte(int paramInt)
        {
            return new byte[] { (byte)(paramInt >> 24), (byte)(paramInt >> 16), (byte)(paramInt >> 8), (byte)paramInt };
        }

        public static byte[] mergeBytes(byte[][] paramArrayOfByte)
        {
            int k = paramArrayOfByte.length;
            int j = 0;
            int m = paramArrayOfByte.length;
            int i = 0;
            byte[] arrayOfByte = new byte[0];
            if (i >= m)
            {
                arrayOfByte = new byte[j];
                j = 0;
                i = 0;
            }
            while (true)
            {
                if (i >= k)
                {
                    return arrayOfByte;
                    //j += paramArrayOfByte[i].length;
                    //i += 1;
                    //break;
                }
                System.arraycopy(paramArrayOfByte[i], 0, arrayOfByte, j, paramArrayOfByte[i].length);
                j += paramArrayOfByte[i].length;
                i += 1;
            }
        }
    }

    public static class TrafficInfo
            implements Serializable
    {
        private static final long serialVersionUID = 3373093047944833658L;
        public int _id;
        public int appId = -100;
        public String appName;
        public long mobileReceive;
        public long mobileSend = 0L;
        public String packageName;
        public long wifiReceive = 0L;
        public long wifiSend = 0L;

        public static TrafficInfo from(AppTrafficInfo paramAppTrafficInfo)
        {
            TrafficInfo localTrafficInfo = new TrafficInfo();
            localTrafficInfo.appId = paramAppTrafficInfo.appId;
            localTrafficInfo.packageName = paramAppTrafficInfo.packageName;
            localTrafficInfo.appName = paramAppTrafficInfo.appName;
            return localTrafficInfo;
        }

        private String getTrafficInK(long paramLong1, long paramLong2)
        {
            float f = (float)(paramLong1 + paramLong2) / 1024.0F;
            if ((f < 1.0F) && (f > 0.0F))
                return "1";
            if (f <= 0.0F)
                return "0";
            if ((f > 1048576.0F) && (f < 2097152.0F))
                return "1.1";
            if ((f >= 2097152.0F) && (f < 3145728.0F))
                return "2.2";
            if ((f >= 3145728.0F) && (f < 4194304.0F))
                return "3.3";
            if ((f >= 4194304.0F) && (f < 5242880.0F))
                return "4.4";
            if (f >= 5242880.0F)
                return "5.5";
            return Integer.toString( Math.round(f) );
        }

        public String getFormatedMobileReceive()
        {
            if (this.mobileReceive <= 0L)
                return "0KB";
            return DataFormat.formatSize((float)this.mobileReceive);
        }

        public String getFormatedMobileSend()
        {
            if (this.mobileSend <= 0L)
                return "0KB";
            return DataFormat.formatSize((float)this.mobileSend);
        }

        public String getFormatedWifiReceive()
        {
            if (this.wifiReceive <= 0L)
                return "0KB";
            return DataFormat.formatSize((float)this.wifiReceive);
        }

        public String getFormatedWifiSend()
        {
            if (this.wifiSend <= 0L)
                return "0KB";
            return DataFormat.formatSize((float)this.wifiSend);
        }

        public String getMobileTrafficInK()
        {
            return getTrafficInK(this.mobileSend, this.mobileReceive);
        }

        public String getWifiTrafficInK()
        {
            return getTrafficInK(this.wifiSend, this.wifiReceive);
        }

        public boolean isZeroTraffic()
        {
            return (this.mobileSend <= 0L) && (this.mobileReceive <= 0L) && (this.wifiSend <= 0L) && (this.wifiReceive <= 0L);
        }

        public String toString()
        {
            return "appid=" + this.appId + " appName=" + this.appName + " mobileSend=" + this.mobileSend + " mobileReceive=" + this.mobileReceive + " wifiSend=" + this.wifiSend + " wifiReceive=" + this.wifiReceive + " packageName=" + this.packageName;
        }
    }

    public class TrafficDailyInfo extends TrafficInfo
    {
        private static final long serialVersionUID = 3373093047944833658L;
        public String CAID;
        public String GUID;
        public String SHACode;
        public long activeTime;
        public long apkSize;
        public String dataId;
        public long installTime;
        public String installTime2;
        public int isActivated = 0;
        public String scramble;
        public String trafficDate;
        public String userId;
        public String version;

        public String getTrafficDate()
        {
            return this.trafficDate;
        }

        public String toString()
        {
            return this.userId + "@" + "trafficDate:" + this.trafficDate + " " + "installTime2:" + this.installTime2 + " " + "scramble:" + this.scramble + " " + "isActivated:" + this.isActivated + " " + super.toString();
        }

        public float totalReceive()
        {
            return (float)(this.wifiReceive + this.wifiSend);
        }

        public float totalSend()
        {
            return (float)(this.wifiSend + this.mobileSend);
        }
    }


    public class AppTrafficInfo
    {
        public int appId;
        public String appName;
        public String packageName;
        public long totalReceive = 0L;
        public long totalSend = 0L;

        public String getFormatedTotalReceive()
        {
            if (this.totalReceive <= 0L)
                return "0KB";
            return DataFormat.formatSize((float)this.totalReceive);
        }

        public String getFormatedTotalSend()
        {
            if (this.totalSend <= 0L)
                return "0KB";
            return DataFormat.formatSize((float)this.totalSend);
        }

        public boolean isZeroTraffic()
        {
            return (this.totalSend <= 0L) && (this.totalReceive <= 0L);
        }

        public String toString()
        {
            return "appName=" + this.appName + " appid=" + this.appId + " totalSend= " + this.totalSend + " totalReceive= " + this.totalReceive;
        }
    }


    public class BaseDao
    {
        private String mmKey;

        public  BaseDao(Context context){

        }

        public BaseDao()
        {
            //8c1674580000bbc0
            this.mmKey = "8c1674580000bbc0"; // new MMKey().getKey();
        }

        public String decryptString(String paramString)
        {
            try
            {
                paramString = AESForNodejs.decrypt(paramString, this.mmKey).split("\\|\\|")[0];
                return paramString;
            }
            catch (Exception e)
            {
                //e.printStackTrace();
            }
            return null;
        }

        public long decryptTrafficInfo(String paramString)
        {
            try
            {
                long l = Long.valueOf(AESForNodejs.decrypt(paramString, this.mmKey).split("\\|\\|")[0]).longValue();
                return l;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return 0L;
        }

        public String encryptString(String paramString)
        {
            try
            {
                //8c1c9824000023c0
                //paramString = AESForNodejs.encrypt(paramString + "||" + SDKKeysManager.getDatasParams(), this.mmKey);
                paramString = AESForNodejs.encrypt(paramString + "||" + "8c1c9824000023c0", this.mmKey);
                return paramString;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return "";
        }

        public String encryptTrafficInfo(long paramLong)
        {
            try
            {
                //8c1c9824000023c0
                //String str = AESForNodejs.encrypt(paramLong + "||" + SDKKeysManager.getDatasParams(), this.mmKey);
                String str = AESForNodejs.encrypt(paramLong + "||" + "8c1c9824000023c0", this.mmKey);
                return str;
            }
            catch (Exception localException)
            {
                localException.printStackTrace();
            }
            return "";
        }
    }


    static class AESForNodejs
    {
        public static final String DEFAULT_CODING = "utf-8";

        public static String decrypt(String paramString1, String paramString2)
                throws Exception
        {
            Cipher localCipher = Cipher.getInstance("AES");
            localCipher.init(2,  new SecretKeySpec(MessageDigest.getInstance("MD5").digest(paramString2.getBytes("utf-8")), "AES"));
            return new String(localCipher.doFinal(toByte(paramString1)));
        }

        public static String encrypt(String paramString1, String paramString2)
                throws Exception
        {
            byte[] paramString1_byte = paramString1.getBytes("utf-8");
            Object localObject = new SecretKeySpec(MessageDigest.getInstance("MD5").digest(paramString2.getBytes("utf-8")), "AES");
            Cipher paramString2_Cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            paramString2_Cipher.init(1, (Key)localObject);
            localObject = new byte[paramString2_Cipher.getOutputSize(paramString1_byte.length)];
            int i = paramString2_Cipher.update(paramString1_byte, 0, paramString1_byte.length, (byte[])localObject, 0);
            paramString2_Cipher.doFinal((byte[])localObject, i);
            return parseByte2HexStr((byte[])localObject);
        }

        public static void main(String[] paramArrayOfString)
                throws Exception
        {
            System.out.println(encrypt("fsadfsdafsdafsdafsadfsadfsadf", "12345678"));
            System.out.println(decrypt("9a4e2668242fe2491b845d9dfaade382db7b912714546c4de67f7f7efcdf08f5", "12345678"));
        }

        public static String parseByte2HexStr(byte[] paramArrayOfByte)
        {
            StringBuffer localStringBuffer = new StringBuffer();
            int i = 0;
            while (true)
            {
                if (i >= paramArrayOfByte.length)
                    return localStringBuffer.toString();
                String str2 = Integer.toHexString(paramArrayOfByte[i] & 0xFF);
                String str1 = str2;
                if (str2.length() == 1)
                    str1 = '0' + str2;
                localStringBuffer.append(str1);
                i += 1;
            }
        }

        private static byte[] toByte(String paramString)
        {
            int j = paramString.length() / 2;
            byte[] arrayOfByte = new byte[j];
            int i = 0;
            while (true)
            {
                if (i >= j)
                    return arrayOfByte;
                arrayOfByte[i] = Integer.valueOf(paramString.substring(i * 2, i * 2 + 2), 16).byteValue();
                i += 1;
            }
        }
    }





    public class TrafficDailyDao2 extends BaseDao
    {

        //protected final SQLiteDatabase db;
        Statement db = null;

        private static final String C_ACTIVE_TIME = "column18";
        private static final String C_APK_SIZE = "column11";
        private static final String C_APP_ID = "column2";
        private static final String C_APP_NAME = "column7";
        private static final String C_CAID = "column16";
        private static final String C_DATA_ID = "column14";
        private static final String C_GUID = "column15";
        private static final String C_INSTALL_TIME = "column17";
        private static final String C_MOBILE_RECEIVE = "column4";
        private static final String C_MOBILE_SEND = "column3";
        private static final String C_PACKAGE_NAME = "column8";
        private static final String C_SHACODE = "column10";
        private static final String C_TRAFFIC_DATE = "column9";
        private static final String C_USER_ID = "column13";
        private static final String C_VERSION = "column12";
        private static final String C_WIFI_RECEIVE = "column6";
        private static final String C_WIFI_SEND = "column5";
        private static final String TABLE_NAME = "table_traffic_daily";
        //private static final String TAG = TrafficDailyDao2.class.getSimpleName();
        private static final String _ID = "column1";
        //private static TrafficDailyDao2 instance;

        public TrafficDailyDao2(Context paramContext) throws SQLException {
            super(paramContext);

            Connection connection = null;
            connection = DriverManager.getConnection("jdbc:sqlite:R:/dbs/triffi_stat3.db");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            this.db = statement;

            //ResultSet rs = statement.executeQuery("select * from table_traffic_daily  LIMIT 5  offset 0");

        }

        //private ArrayList<TrafficDailyInfo> convert(Cursor paramCursor)
        private ArrayList<TrafficDailyInfo> convert(ResultSet paramCursor) throws SQLException {
            ArrayList localArrayList = new ArrayList();
            if (paramCursor != null);
            while (true)
            {
                //if (!paramCursor.moveToNext())
                if (!paramCursor.next())
                {
                    paramCursor.close();
                    return localArrayList;
                }
                TrafficDailyInfo localTrafficDailyInfo = new TrafficDailyInfo();
                localTrafficDailyInfo.appId = Integer.valueOf(decryptString(paramCursor.getString("column2"))).intValue();
                localTrafficDailyInfo.mobileSend = decryptTrafficInfo(paramCursor.getString("column3"));
                localTrafficDailyInfo.mobileReceive = decryptTrafficInfo(paramCursor.getString("column4"));
                localTrafficDailyInfo.wifiSend = decryptTrafficInfo(paramCursor.getString("column5"));
                localTrafficDailyInfo.wifiReceive = decryptTrafficInfo(paramCursor.getString("column6"));
                localTrafficDailyInfo.appName = decryptString(paramCursor.getString("column7"));
                localTrafficDailyInfo.packageName = decryptString(paramCursor.getString("column8"));
                localTrafficDailyInfo.trafficDate = decryptString(paramCursor.getString("column9"));
                localTrafficDailyInfo.SHACode = paramCursor.getString("column10");
                localTrafficDailyInfo.apkSize = decryptTrafficInfo(paramCursor.getString("column11"));
                localTrafficDailyInfo.version = decryptString(paramCursor.getString("column12"));
                localTrafficDailyInfo.userId = decryptString(paramCursor.getString("column13"));
                localTrafficDailyInfo.dataId = decryptString(paramCursor.getString("column14"));
                localTrafficDailyInfo.GUID = decryptString(paramCursor.getString("column15"));
                localTrafficDailyInfo.CAID = decryptString(paramCursor.getString("column16"));
                localTrafficDailyInfo.installTime = paramCursor.getLong("column17");
                localTrafficDailyInfo.activeTime = paramCursor.getLong("column18");
                localTrafficDailyInfo._id = paramCursor.getInt("column1");
                localTrafficDailyInfo.scramble = paramCursor.getString("column20");
                localTrafficDailyInfo.installTime2 = decryptString(paramCursor.getString("column19"));
                localTrafficDailyInfo.isActivated = Integer.valueOf(decryptString(paramCursor.getString("column21"))).intValue();
                localArrayList.add(localTrafficDailyInfo);
            }
        }

        /*
        public TrafficDailyDao2 getInstance(Context paramContext)
        {
            if (instance == null)
                instance = new TrafficDailyDao2(paramContext);
            return instance;
        }*/

        public List<TrafficDailyInfo> selectUnreportTraffic() throws ParseException, SQLException {
            new ArrayList();
            //DebugLog.d(TAG, "【sql】" + "SELECT * FROM table_traffic_daily");

            //ArrayList localArrayList1 = convert(this.db.rawQuery("SELECT * FROM table_traffic_daily", null));
            ArrayList localArrayList1 = convert(this.db.executeQuery("SELECT * FROM table_traffic_daily"));
            Date localDate = new SimpleDateFormat(new SimpleDateFormat("yyyy-MM-dd").format(new Date())).parse("yyyy-MM-dd");//DateTimeUtil.getDateFromStr(new SimpleDateFormat("yyyy-MM-dd").format(new Date()), "yyyy-MM-dd");
            ArrayList localArrayList2 = new ArrayList();
            int i = 0;
            while (true)
            {
                if (i >= localArrayList1.size())
                    return localArrayList2;
                TrafficDailyInfo localTrafficDailyInfo = (TrafficDailyInfo)localArrayList1.get(i);

                //if (DateTimeUtil.getDateFromStr(localTrafficDailyInfo.getTrafficDate(), "yyyy-MM-dd").before(localDate))
                if (new SimpleDateFormat(localTrafficDailyInfo.getTrafficDate()).parse("yyyy-MM-dd").before(localDate))
                    localArrayList2.add(localTrafficDailyInfo);
                i += 1;
            }
        }
    }


    public class ReportTask
    {
        private static final int MAX_TRIES = 3;
        //private static final String TAG = ReportTask.class.getSimpleName();
        private Context context;

        public ReportTask(Context paramContext)
        {
            this.context = paramContext;
        }

        private boolean tryReport2(List<TrafficDailyInfo> paramList, int paramInt)
        {
            long l = System.currentTimeMillis(); //DateTimeUtil.currentServerTimeMillis();
            String str;
            try
            {
                boolean bool = new ServerApi().requestReport2(this.context, paramList, paramInt, l).isSuccess(); //ServerApi.getInstance().requestReport2(this.context, paramList, paramInt, l).isSuccess();
                return bool;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public void report(String paramString) throws ParseException, SQLException {
            List<TrafficDailyInfo> paramString_arr;

            //DebugLog.d(TAG, "准备上传流量数据...", true);

            paramString_arr = new TrafficDailyDao2(this.context).selectUnreportTraffic();//TrafficDailyDao2.getInstance(this.context).selectUnreportTraffic();
            //if ((paramString == null) || (paramString.size() <= 0))
            //    DebugLog.i(TAG, "没有流量记录可以上传！返回，不调用后台上传接口..", true);

            //paramString = devide(paramString);

            int i = 0;
            //DebugLog.d(TAG, "上传流量数据,size=" + localList.size(), true);
            if ((paramString_arr != null) && (paramString_arr.size() > 0))
            {
                if (!tryReport2(paramString_arr, 1))
                    return;
                //DebugLog.d(TAG, "new上传流量数据成功@page" + i, true);
                //TrafficDailyDao2.getInstance(this.context).delTraffics(localList);
                //DebugLog.d(TAG, "new上传流量数据成功后删除数据库数据处理结束...@page" + i, true);
            }

        }
    }


    public abstract class MD5Coder
    {
        public static final String MD5(String paramString)
        {
            char[] arrayOfChar = new char[16];
            char[] tmp8_6 = arrayOfChar;
            tmp8_6[0] = 48;
            char[] tmp13_8 = tmp8_6;
            tmp13_8[1] = 49;
            char[] tmp18_13 = tmp13_8;
            tmp18_13[2] = 50;
            char[] tmp23_18 = tmp18_13;
            tmp23_18[3] = 51;
            char[] tmp28_23 = tmp23_18;
            tmp28_23[4] = 52;
            char[] tmp33_28 = tmp28_23;
            tmp33_28[5] = 53;
            char[] tmp38_33 = tmp33_28;
            tmp38_33[6] = 54;
            char[] tmp44_38 = tmp38_33;
            tmp44_38[7] = 55;
            char[] tmp50_44 = tmp44_38;
            tmp50_44[8] = 56;
            char[] tmp56_50 = tmp50_44;
            tmp56_50[9] = 57;
            char[] tmp62_56 = tmp56_50;
            tmp62_56[10] = 65;
            char[] tmp68_62 = tmp62_56;
            tmp68_62[11] = 66;
            char[] tmp74_68 = tmp68_62;
            tmp74_68[12] = 67;
            char[] tmp80_74 = tmp74_68;
            tmp80_74[13] = 68;
            char[] tmp86_80 = tmp80_74;
            tmp86_80[14] = 69;
            char[] tmp92_86 = tmp86_80;
            tmp92_86[15] = 70;
            try
            {
                byte[] paramStringBytes = paramString.getBytes();
                MessageDigest localObject = MessageDigest.getInstance("MD5");
                ((MessageDigest)localObject).update(paramStringBytes);
                paramStringBytes = ((MessageDigest)localObject).digest();
                int k = paramStringBytes.length;
                localObject = new char[k * 2];
                int i = 0;
                int j = 0;
                while (true)
                {
                    if (i >= k)
                        return new String((char[])localObject);
                    int m = paramStringBytes[i];
                    int n = j + 1;
                    localObject[j] = arrayOfChar[(m >>> 4 & 0xF)];
                    j = n + 1;
                    localObject[n] = arrayOfChar[(m & 0xF)];
                    i += 1;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        public static String encodeMD5(File paramFile)
                throws Exception
        {
            return DataFormat.bytesToHexString(encodeMD5(new FileInputStream(paramFile)));
        }

        public static byte[] encodeMD5(InputStream paramInputStream)
                throws IOException
        {
            return DigestUtils.md5(paramInputStream);
        }

        public static byte[] encodeMD5(String paramString)
                throws Exception
        {
            return DigestUtils.md5(paramString);
        }

        public static String encodeMD5Hex(String paramString)
                throws Exception
        {
            return DigestUtils.md5Hex(paramString);
        }
    }


    public class SecurityManager
    {
        //private static final String TAG = SecurityManager.class.getSimpleName();


        public String encrypt(String paramString)
        {
            //return AESForNodejs.encrypt(paramString, SDKKeysManager.getAESKey());
            return "RichinfoDevelopedTwoMMarketAESKEY";
        }

        public String encryptByNewKey(String paramString)
        {
            //
            //return AESForNodejs.encrypt(paramString, new MMKey().getKey2());
            return "nobesbkbnzbmt3gv";
        }

        public String genVerificationKey(String paramString, long paramLong)
        {
            //tokenKey  RichinfoDevelopedTwoMMarketMD5KEY

            String str = "RichinfoDevelopedTwoMMarketMD5KEY";//SDKKeysManager.getTokenVerificationKey();
            try
            {
                paramString = MD5Coder.MD5(paramString + paramLong + str);
                return paramString;
            }
            catch (Exception e)
            {
                //DebugLog.e(TAG, paramString.getMessage(), true);
            }
            return "";
            //return "D1E2BF551FE9DD150D5D4D531EE2D200";
        }

        //phoneId:c27674f3e6cb19f4d09ade2ec10242cd
        //timestamp:1471227341399
        //versionName:2.0.2
        public String genVerificationKey(String paramString1, long paramLong, String paramString2)
        {
            String str = "RichinfoDevelopedTwoMMarketMD5KEY";//SDKKeysManager.getTokenVerificationKey();
            try
            {
                paramString1 = MD5Coder.MD5(paramString1 + paramLong + paramString2 + str);
                return paramString1;
            }
            catch (Exception e)
            {
                //DebugLog.e(TAG, paramString1.getMessage(), true);
            }
            return "";
        }

        public String zipAndEncrypt(String paramString)
                throws Exception
        {
            return encrypt(ZipUtil2.gzip(paramString));
        }

        public String zipAndEncryptByNewKey(String paramString)
                throws Exception
        {
            return encryptByNewKey(ZipUtil2.gzip(paramString));
        }
    }



    //public PhoneInfo createPhoneInfo(Context paramContext)
    public PhoneInfo createPhoneInfo()
    {
        PhoneInfo localPhoneInfo = new PhoneInfo();

        DeviceInfo d = VitualDataGet.GetDeviceInfo();
        //paramContext = SystemUtil.getInstance(paramContext);
        localPhoneInfo.imei = VitualDataGet.getIMEI();//paramContext.getIMEI();
        localPhoneInfo.brand = d.client_brand; //paramContext.getPhoneBrand();
        localPhoneInfo.model = d.client_model;//paramContext.getDeviceModel();
        localPhoneInfo.osVersion = "4.4.4";//paramContext.getDeviceOSVersion();
        localPhoneInfo.dataMemorySize = 2772;//(paramContext.getTotalInternalMemorySize() / 1048576L);
        localPhoneInfo.dataAvailableMemorySize = 1231;//(paramContext.getAvailableInternalMemorySize() / 1048576L);
        localPhoneInfo.sdcardMemorySize = 23871;//(paramContext.getTotalExternalMemorySize() / 1048576L);
        localPhoneInfo.sdcardAvailableMemorySize = 12145;//(paramContext.getAvailableExternalMemorySize() / 1048576L);
        localPhoneInfo.phoneNumber = "13178638515";//paramContext.getPhoneNumber();
        localPhoneInfo.imsi = VitualDataGet.getImsi();//paramContext.getIMSI();
        localPhoneInfo.deviceId = "267353d493db38f1";//paramContext.getDeviceId();
        localPhoneInfo.totalRam = 3072;//paramContext.getTotalRam();
        return localPhoneInfo;
    }


    public PhoneInfo createPhoneInfo2(Context paramContext)
    {
        if (paramContext == null)
            System.out.println("context空指针异常");

        DeviceInfo d = VitualDataGet.GetDeviceInfo();
        System.out.println(paramContext);
        PhoneInfo localPhoneInfo = new PhoneInfo();
        //SystemUtil localSystemUtil = SystemUtil.getInstance(paramContext);
        //Object localObject = (TelephonyManager)paramContext.getSystemService("phone");
        String str1 =  d.client_model; //Build.MODEL;
        String str2 = d.client_brand; //Build.BRAND;
        //localObject = ((TelephonyManager)localObject).getLine1Number();
        String str3 = VitualDataGet.getIMEI();//new GetImei(paramContext).getImei();
        String str4 = VitualDataGet.getImsi();//GetImsi.getImsi(paramContext);
        //paramContext = localSystemUtil.getVersionName(paramContext);
        localPhoneInfo.brand = str2;
        localPhoneInfo.model = str1;
        localPhoneInfo.osVersion = "v23";//localSystemUtil.getDeviceOSVersion();
        localPhoneInfo.versionCode = "4.4.1";//paramContext;
        localPhoneInfo.phoneNumber = "13178638515";//((String)localObject);
        localPhoneInfo.imsi = str4;
        localPhoneInfo.deviceId = str3;
        localPhoneInfo.imei = str3;
        return localPhoneInfo;
        //phoneid c27674f3e6cb19f4d09ade2ec10242cd
    }

    public class PhoneInfo
    {
        private static final int UNIT = 1048576;
        public String brand;
        public long dataAvailableMemorySize;
        public long dataMemorySize;
        private String deviceId;
        public String imei;
        public String imsi;
        public String model;
        public String osVersion;
        public String phoneNumber;
        public long sdcardAvailableMemorySize;
        public long sdcardMemorySize;
        public long totalRam;
        public String versionCode;

        public String genPhoneId()
        {
            String str1 = this.imei;
            if ((this.imei == null) || ("".equals(this.imei.trim())))
                str1 = this.deviceId;
            try
            {
                String str2 = new SecurityManager().encrypt(str1);
                return str2;
            }
            catch (Exception localException)
            {
                localException.printStackTrace();
            }
            return str1;
        }
    }


    public class DataBuilder
    {
        private static final char DEVIDER = '|';
        private static final String END = "\r\n";
        private StringBuffer sb = new StringBuffer();

        public DataBuilder add(Object paramObject)
        {
            return add(paramObject.toString());
        }

        public DataBuilder add(String paramString)
        {
            this.sb.append(paramString).append('|');
            return this;
        }

        public DataBuilder append(String paramString)
        {
            this.sb.append(paramString);
            return this;
        }

        public void clear()
        {
            this.sb.delete(0, this.sb.length());
        }

        public String end()
        {
            if ((this.sb.length() >= 1) && (this.sb.charAt(this.sb.length() - 1) == '|'))
                this.sb.deleteCharAt(this.sb.length() - 1);
            this.sb.append("\r\n");
            return this.sb.toString();
        }

        public String value()
        {
            return this.sb.toString();
        }
    }


    public class ServerApi
    {
        //private static final String TAG = ServerApi.class.getSimpleName();
        //private static ServerApi instance = null;
        private String configURL = this.host + "fusionUpPhone.do";
        private String host = "http://pcapi.mmarket.com:8081";//GlobalCfg.adminServer;
        private String reportURL = this.host + "fusionUpFlow.do";
        private String reportURL2 = this.host + "/fusionUpFlowReal.do";
        private String reportURL3 = this.host + "/fusionUpFlowNew.do";

        private String buildArgInfo(Context paramContext, PhoneInfo paramPhoneInfo, long paramLong)
        {
            String str = new SecurityManager().genVerificationKey(paramPhoneInfo.genPhoneId(), paramLong, paramPhoneInfo.versionCode);
            return new DataBuilder().add(paramContext).add(Long.valueOf(paramLong)).add(paramPhoneInfo).add(str).end();
        }

        private String buildConfigDataInfo(Context paramContext)
        {
            DataBuilder localDataBuilder = new DataBuilder();
            PhoneInfo phoneInfo = createPhoneInfo();//PhoneInfo.create(paramContext);
            return localDataBuilder.add(phoneInfo.genPhoneId()).add(phoneInfo.brand).add(phoneInfo.model).add(phoneInfo.phoneNumber).add(phoneInfo.imsi).add(phoneInfo.osVersion).add(Long.valueOf(phoneInfo.totalRam)).add(phoneInfo.dataMemorySize).add(Long.valueOf(phoneInfo.dataAvailableMemorySize)).add(Long.valueOf(phoneInfo.sdcardMemorySize)).add(Long.valueOf(phoneInfo.sdcardAvailableMemorySize)).end();
        }

        private String buildReportDataInfo(Context paramContext, List<TrafficDailyInfo> paramList, PhoneInfo paramPhoneInfo)
                throws Exception
        {
            String str1 = paramPhoneInfo.brand;
            String str2 = paramPhoneInfo.model;
            String str3 = paramPhoneInfo.phoneNumber;
            String str4 = paramPhoneInfo.imei;
            String str5 = paramPhoneInfo.imei;//SharedPreferencesUtil.getInstance(paramContext).get("last_imei", "");
            //if ((!TextUtils.isEmpty(str5)) && (!str5.equals(str4)))
            //    LogDao.getInstance(paramContext).insert(paramPhoneInfo, "imei change,last_imei:" + str5 + ",current_imei:" + str4, DateTimeUtil.getNowDateTime());
            str4 = paramPhoneInfo.imsi;
            str5 = paramPhoneInfo.genPhoneId();
            //paramContext = InstallManager.getInstance(paramContext);
            DataBuilder localDataBuilder1 = new DataBuilder();
            String str6 = new SimpleDateFormat("yyyyMMdd").format(Long.valueOf(System.currentTimeMillis()));//DateTimeUtil.convertDateTime2(DateTimeUtil.currentServerTimeMillis());



            for (TrafficDailyInfo item : paramList) {
                TrafficDailyInfo localTrafficDailyInfo = item;//(TrafficDailyInfo)paramList.next();
                DataBuilder localDataBuilder2 = new DataBuilder();
                String str7 = localTrafficDailyInfo.packageName;
                //Object localObject = paramContext.queryForFirst2(str7);

                String str8 =  new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat( "yyyy-MM-dd").parse(localTrafficDailyInfo.trafficDate));
                //DateTimeUtil.getFormatDateTime(localTrafficDailyInfo.trafficDate, "yyyy-MM-dd", "yyyyMMdd");
                //if (localObject != null)
                //{
                //String str9 = ((InstallInfo)localObject).installType;
                String str9 = "1";
                //String str10 = ((InstallInfo)localObject).clientPlatform;
                String str10 = "android_pad";
                //String str11 = ((InstallInfo)localObject).client_versionCode;
                String str11 = "2.2.1";//((InstallInfo)localObject).client_versionCode;
                String str12 = "0";//((InstallInfo)localObject).scramble;
                //localObject = ((InstallInfo)localObject).installTime2;
                //localDataBuilder1.append(localDataBuilder2.add(str5).add(str6).add(localTrafficDailyInfo.GUID).add(localTrafficDailyInfo.userId).add(localTrafficDailyInfo.dataId).add(localTrafficDailyInfo.CAID).add(str7).add(localTrafficDailyInfo.appName).add(localTrafficDailyInfo.version).add(Long.valueOf(localTrafficDailyInfo.apkSize)).add(localTrafficDailyInfo.getMobileTrafficInK()).add(localTrafficDailyInfo.getWifiTrafficInK()).add(str8).add((String)localObject).add(str9).add(str10).add(paramPhoneInfo.versionCode).add(str11).add(str4).add(str3).add(str1).add(str2).add(Integer.valueOf(localTrafficDailyInfo.isActivated)).add(str12).end());
                localDataBuilder1.append(localDataBuilder2.add(str5).add(str6).add(localTrafficDailyInfo.GUID).add(localTrafficDailyInfo.userId).add(localTrafficDailyInfo.dataId).add(localTrafficDailyInfo.CAID).add(str7).add(localTrafficDailyInfo.appName).add(localTrafficDailyInfo.version).add(Long.valueOf(localTrafficDailyInfo.apkSize)).add(localTrafficDailyInfo.getMobileTrafficInK()).add(localTrafficDailyInfo.getWifiTrafficInK()).add(str8).add("20160815084035").add(str9).add(str10).add(paramPhoneInfo.versionCode).add(str11).add(str4).add(str3).add(str1).add(str2).add(Integer.valueOf(localTrafficDailyInfo.isActivated)).add(str12).end());
                //}
            }
            return localDataBuilder1.value();

            /*
            paramList = paramList.iterator();
            while (true)
            {
                if (!paramList.hasNext())
                    return localDataBuilder1.value();
                TrafficDailyInfo localTrafficDailyInfo = (TrafficDailyInfo)paramList.next();
                DataBuilder localDataBuilder2 = new DataBuilder();
                String str7 = localTrafficDailyInfo.packageName;
                //Object localObject = paramContext.queryForFirst2(str7);

                String str8 =  new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat( "yyyy-MM-dd").parse(localTrafficDailyInfo.trafficDate));
                //DateTimeUtil.getFormatDateTime(localTrafficDailyInfo.trafficDate, "yyyy-MM-dd", "yyyyMMdd");
                //if (localObject != null)
                //{
                    String str9 = ((InstallInfo)localObject).installType;
                    String str10 = ((InstallInfo)localObject).clientPlatform;
                    String str11 = ((InstallInfo)localObject).client_versionCode;
                    String str12 = ((InstallInfo)localObject).scramble;
                    localObject = ((InstallInfo)localObject).installTime2;
                    localDataBuilder1.append(localDataBuilder2.add(str5).add(str6).add(localTrafficDailyInfo.GUID).add(localTrafficDailyInfo.userId).add(localTrafficDailyInfo.dataId).add(localTrafficDailyInfo.CAID).add(str7).add(localTrafficDailyInfo.appName).add(localTrafficDailyInfo.version).add(Long.valueOf(localTrafficDailyInfo.apkSize)).add(localTrafficDailyInfo.getMobileTrafficInK()).add(localTrafficDailyInfo.getWifiTrafficInK()).add(str8).add((String)localObject).add(str9).add(str10).add(paramPhoneInfo.versionCode).add(str11).add(str4).add(str3).add(str1).add(str2).add(Integer.valueOf(localTrafficDailyInfo.isActivated)).add(str12).end());
                //}
            }*/
        }

        /*public static ServerApi getInstance()
        {
            if (instance == null)
                instance = new ServerApi();
            return instance;
        }*/

        private String request(String paramString1, String paramString2, String paramString3)
                throws Exception
        {
            //DebugLog.d(TAG, "服务器地址：" + paramString1, true);
            //ArrayList localArrayList = new ArrayList();
            //paramString2 = new BasicNameValuePair("Info", URLEncoder.encode(paramString2, "utf-8"));
            //paramString3 = new BasicNameValuePair("Data", URLEncoder.encode(paramString3, "utf-8"));
            //localArrayList.add(paramString2);
            //localArrayList.add(paramString3);
            String datas = "";
            datas+=("Info="+URLEncoder.encode(paramString2, "utf-8")+"\n");
            datas+=("Data="+URLEncoder.encode(paramString3, "utf-8"));

            //return new HttpClientRequest().post(paramString1, localArrayList).asString();
            return UrlRequest.POST(paramString1,datas);
        }

        public void printHostUrl()
        {
            //DebugLog.i(TAG, "config = " + this.configURL, true);
            //DebugLog.i(TAG, "report = " + this.reportURL, true);
        }

        public ReportResultInfo requestReport2(Context paramContext, List<TrafficDailyInfo> paramList, int paramInt, long paramLong)
                throws Exception
        {
            ReportResultInfo localReportResultInfo = new ReportResultInfo();
            Object localObject = localReportResultInfo;
            String str1="";
            String str2="";
            String str3="";
            String url ="";
            if (paramList != null)
            {
                localObject = localReportResultInfo;
                if (paramList.size() > 0)
                {
                    localObject = createPhoneInfo2(paramContext);//PhoneInfo.create2(paramContext);
                    str1 = buildArgInfo(paramContext, (PhoneInfo)localObject, paramLong);
                    str2 = buildReportDataInfo(paramContext, paramList, (PhoneInfo)localObject);
                    str3 = SecurityManager.zipAndEncryptByNewKey(str2);
                    localObject = localReportResultInfo;
                    if (str2!=null && str2.trim()!="")
                    {
                        //paramContext = "";
                        url = this.reportURL3;
                        if (paramInt == 2)
                        {
                            url = this.reportURL2;
                            //paramContext = "reportURL2**上报应用激活接口";
                        }
                        //paramContext = "reportURL3**New上报流量接口";
                    }
                }
            }

            /*
            while (true)
            {
                //DebugLog.d(TAG, "thread_id:" + Thread.currentThread().getId() + "**" + paramContext + "new请求配置信息Info参数：" + str1, true);
                //DebugLog.d(TAG, "thread_id:" + Thread.currentThread().getId() + "**" + paramContext + "new请求配置信息Data参数(原文)：" + str2, true);
                //DebugLog.d(TAG, "thread_id:" + Thread.currentThread().getId() + "**" + paramContext + "new请求配置信息Data参数(加密)" + str3, true);
                paramContext = request(paramList, str1, str3);
                //DebugLog.d(TAG, "上传接口返回：" + paramContext, true);
                localObject = ReportResultInfo.fromJson(paramContext);
                return localObject;
            }
            */
            String r = request(url, str1, str3);
            System.out.println(r);
            return  null;
        }

        public void setHost(String paramString)
        {
            this.host = paramString;
            if (this.host.lastIndexOf("/") == this.host.length() - 1)
            {
                this.configURL = (paramString + "fusionUpPhone.do");
                this.reportURL = (paramString + "fusionUpFlow.do");
                return;
            }
            this.configURL = (paramString + "/fusionUpPhone.do");
            this.reportURL = (paramString + "/fusionUpFlow.do");
        }
    }



    public ReportResultInfo ReportResultInfo_fromJson(String paramString)
    {
        ReportResultInfo localReportResultInfo = new ReportResultInfo();
        localReportResultInfo.respCode = 1;//Integer.valueOf(new JSONObject(paramString).getString("respCode")).intValue();
        return localReportResultInfo;
    }

    public class ReportResultInfo
    {
        private static final int SUCCESS = 200;
        public int respCode = -1;


        public boolean isSuccess()
        {
            return this.respCode == 200;
        }

        public String toString()
        {
            return "respCode=" + this.respCode;
        }
    }

    //报告信息
    class  FlowReportInfo{
        public  FlowReportInfo(){

        }

        //获取用于提交报告的字符串
        public String GetString(){
            return "";
        }
    }

    //发送报告
    public void  Report(){

    }
}
