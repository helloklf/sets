package MM;

import Tools.UrlRequest;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by helloklf on 2016/7/29.
 */
public class Install{

    //安装日志提交API
    static String Api = "http://pcweb.mmarket.com/si/appInstallLog.do";


    public synchronized boolean postInstall(String postData,LoginResult result) {
        try {
            String installResult = UrlRequest.POST(Api, postData);
            if(installResult != null) {
                try {
                    String decryptedInstallResult = Install.decode(installResult, result.KeyOne);
                    if(decryptedInstallResult.equals("200")) {
                        return true;
                    } else {
                        System.out.println(decryptedInstallResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public  void  Test(LoginResult result) throws Exception {
        AppConfigReader r = new AppConfigReader("R:\\appconfig.txt");
        r.readAll();
        AppInfo[] appInfos = r.getAppMap().values().toArray(new AppInfo[0]);

        for (int i = 0; i < appInfos.length; i++) {
            InstallRecord record = getNewRecord(
                    "2015112816464597330588", "13468053714",
                    "e0:70:7c:48:a8:f2", "386802701102788", "HTC",
                    "M8T", "1", "0", "192.168.199.78", "20150520131400",
                    null,appInfos[i]);
            if (postInstall(record.getPostStr(result.UserID,result.KeyOne,result.KeyTwo),result)) {
                System.out.println("提交安装报告成功: " + appInfos[i].strResourceName);
                ;
            } else {
                System.out.println("安装报告提交失败...");
            }
        }
    }

    class AppConfigReader {

        public int strPackageIdIndex;
        public int nPackageVersionIndex;
        public int nActivateStatusIndex;
        public int strResourceIdIndex;
        public int nFlagIndex;
        public int strResourceNameIndex;
        public int strResourcePackageNameIndex;
        public int strResourceVersionIndex;
        public int nColumnIdIndex;
        public int nScoreIndex;
        public int nExperienceIdIndex;
        public int strCaidIndex;
        public int strMd5Index;
        public int nTypeIndex;
        public int appSizeIndex;
        public int nPreColumnIdIndex;

        BufferedReader bufferedReader;
        Map<String, AppInfo> appMap;

        public AppConfigReader(String path) throws Exception {
            FileReader reader = new FileReader(path);
            bufferedReader = new BufferedReader(reader);
        }

        public AppInfo getById(String id) {
            if(appMap.containsKey(id)) {
                return appMap.get(id);
            }
            return null;
        }

        public Map<String, AppInfo> getAppMap() {
            return appMap;
        }

        public Map<String, AppInfo> readAll() throws IOException {
            String headersLine = bufferedReader.readLine();
            String[] headers = headersLine.split("\t");
            for(int i = 0; i < headers.length; i++) {
                switch(headers[i]) {
                    case "strPackageId":
                        strPackageIdIndex = i;
                        break;
                    case "nPackageVersion":
                        nPackageVersionIndex = i;
                        break;
                    case "nActivateStatus":
                        nActivateStatusIndex = i;
                        break;
                    case "strResourceId":
                        strResourceIdIndex = i;
                        break;
                    case "nFlag":
                        nFlagIndex = i;
                        break;
                    case "strResourceName":
                        strResourceNameIndex = i;
                        break;
                    case "strResourcePackageName":
                        strResourcePackageNameIndex = i;
                        break;
                    case "strResourceVersion":
                        strResourceVersionIndex = i;
                        break;
                    case "nColumnId":
                        nColumnIdIndex = i;
                        break;
                    case "nScore":
                        nScoreIndex = i;
                        break;
                    case "nExperienceId":
                        nExperienceIdIndex = i;
                        break;
                    case "strCaid":
                        strCaidIndex = i;
                        break;
                    case "strMd5":
                        strMd5Index = i;
                        break;
                    case "nType":
                        nTypeIndex = i;
                        break;
                    case "appSize":
                        appSizeIndex = i;
                        break;
                    case "nPreColumnId":
                        nPreColumnIdIndex = i;
                        break;
                    default:
                        break;
                }
            }

            appMap = new HashMap<String, AppInfo>();
            String line;
            String[] words;
            AppInfo appInfo;
            while ((line = bufferedReader.readLine()) != null) {
                words = line.split("\t");
                appInfo = new AppInfo();
                appInfo.strResourceId = words[strResourceIdIndex];
                if(appMap.containsKey(appInfo.strResourceId)) {
                    continue;
                }
                appInfo.strPackageId = words[strPackageIdIndex];
                appInfo.nPackageVersion = words[nPackageVersionIndex];
                appInfo.nActivateStatus = words[nActivateStatusIndex];
                appInfo.nFlag = words[nFlagIndex];
                appInfo.strResourceName = words[strResourceNameIndex];
                appInfo.strResourcePackageName = words[strResourcePackageNameIndex];
                appInfo.strResourceVersion = words[strResourceVersionIndex];
                appInfo.nColumnId = words[nColumnIdIndex];
                appInfo.nScore = words[nScoreIndex];
                appInfo.nExperienceId = words[nExperienceIdIndex];
                appInfo.strCaid = words[strCaidIndex];
                appInfo.strMd5 = words[strMd5Index];
                appInfo.nType = words[nTypeIndex];
                appInfo.appSize = words[appSizeIndex];
                appInfo.nPreColumnId = words[nPreColumnIdIndex];
                appMap.put(appInfo.strResourceId, appInfo);
            }

            return appMap;
        }

    }


    public static String digest(String toDigest) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return b(toDigest);
    }

    public static String decode(String paramString1, String paramString2)
            throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeyException, UnsupportedEncodingException,
            BadPaddingException, IllegalBlockSizeException {
        SecretKeySpec localSecretKeySpec = new SecretKeySpec(
                paramString2.getBytes(), "AES");
        Cipher localCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        localCipher.init(2, localSecretKeySpec);
        return new String(localCipher.doFinal(a(paramString1)));
    }

    public static String encode(String paramString1, String paramString2) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, ShortBufferException, IllegalBlockSizeException, BadPaddingException
    {
        byte[] arrayOfByte1 = paramString1.getBytes("utf-8");
        SecretKeySpec localSecretKeySpec = new SecretKeySpec(paramString2.getBytes(), "AES");
        Cipher localCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        localCipher.init(1, localSecretKeySpec);
        byte[] arrayOfByte2 = new byte[localCipher.getOutputSize(arrayOfByte1.length)];
        int i = localCipher.update(arrayOfByte1, 0, arrayOfByte1.length, arrayOfByte2, 0);
        localCipher.doFinal(arrayOfByte2, i);
        return a(arrayOfByte2);
    }

    private static String a(byte[] paramArrayOfByte) {
        StringBuffer localStringBuffer = new StringBuffer();
        for (int i = 0; i < paramArrayOfByte.length; i++) {
            String str = Integer.toHexString(0xFF & paramArrayOfByte[i]);
            if (str.length() == 1)
                str = '0' + str;
            localStringBuffer.append(str);
        }
        return localStringBuffer.toString();
    }

    private static byte[] a(String paramString) {
        int i = paramString.length() / 2;
        byte[] arrayOfByte = new byte[i];
        for (int j = 0; j < i; j++)
            arrayOfByte[j] = Integer.valueOf(
                    paramString.substring(j * 2, 2 + j * 2), 16).byteValue();
        return arrayOfByte;
    }


    public static String b(String paramString) throws UnsupportedEncodingException, NoSuchAlgorithmException
    {
        Object localObject = null;
        try
        {
            String str = deal(MessageDigest.getInstance("SHA-1").digest(paramString.getBytes("utf-8")));
            return str;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw e;
        }
    }

    public static String deal(byte[] paramArrayOfByte)
    {
        String str1 = "";
        String str2 = "";
        int i = 0;
        while (true)
        {
            if(i < paramArrayOfByte.length) {
                str2 = Integer.toHexString(0xFF & paramArrayOfByte[i]);
                if(str2.length() == 1) {
                    str1 = str1 + "0" + str2;
                } else {
                    str1 = str1 + str2;
                }
            }
            else {
                break;
            }
            i++;
        }
        return str1.toLowerCase();
    }


    //APP信息
    class AppInfo {
        //
        public String strPackageId;
        //
        public String nPackageVersion;
        //
        public String nActivateStatus;
        //
        public String strResourceId;
        //
        public String nFlag;
        //
        public String strResourceName;
        //
        public String strResourcePackageName;
        //
        public String strResourceVersion;
        //
        public String nColumnId;
        //
        public String nScore;
        //
        public String nExperienceId;
        //
        public String strCaid;
        //
        public String strMd5;
        //
        public String nType;
        //
        public String appSize;
        //
        public String nPreColumnId;

        @Override
        public String toString() {
            return "AppInfo [strPackageId=" + strPackageId + ", nPackageVersion=" + nPackageVersion + ", nActivateStatus="
                    + nActivateStatus + ", strResourceId=" + strResourceId + ", nFlag=" + nFlag + ", strResourceName="
                    + strResourceName + ", strResourcePackageName=" + strResourcePackageName + ", strResourceVersion="
                    + strResourceVersion + ", nColumnId=" + nColumnId + ", nScore=" + nScore + ", nExperienceId="
                    + nExperienceId + ", strCaid=" + strCaid + ", strMd5=" + strMd5 + ", nType=" + nType + ", appSize="
                    + appSize + ", nPreColumnId=" + nPreColumnId + "]";
        }


    }

    class DeviceInfo{
        //品牌
        String client_brand;
        //imei
        String client_imei;
        //imsi
        String client_imsi;
        //mac
        String client_mac;
        //型号
        String client_model;
        //手机号
        String client_phone_num;
    }

    class InstallRecord{
        public InstallRecord(){

        }
        public InstallRecord(AppInfo appInfo,DeviceInfo deviceInfo,LoginResult result){

        }

        //public String tiCreateTime;
        public String strCreateUserId;
        public String strPhone;
        public String strMac;
        public String strIMEI;
        //品牌
        public String strMobileBrand;
        public String strMobileType;
        public String strPackageId = "-1";
        public String nPackageVersion = "0";
        public String nActivateStatus = "0";
        public String strResourceId;
        //public String nFlag;
        //public String strPackageGuid;
        public String strErrorMessage;
        public String strResourceName;
        public String nReinstall;
        public String nInstallState;
        public String strResourcePackageName;
        public String strResourceVersion;
        public String strClientIP;
        public String strClientVersion = "Ultimate Edition";
        //public String strIMSI;
        public String strClientOSName = "Microsoft Windows 7";
        public String strClientOSBits = "64";
        public String strMMVersion = "3.2.3.1";
        public String nColumnId = "0";
        public String nScore = "0";
        public String strInstallType = "1";
        public String nExperienceId = "1";
        public String strCaid = ""; //现在没有这个数据
        public String strGuid;
        public String strPhoneId;
        //public String strTimestamp;
        //public String strMd5; //apk的Md5
        //public String strclientPlatform;
        //public String nType;
        //public String strCheckSum;
        //public String nClientMode;
        public String strInstallTime; //20150521141006
        //public String strUploadTime;
        //public String nUploadStatus;
        public String appSize; //app的大小，这个不适合留空
        public String nPreColumnId = "-1";
        public String nPhoneConnType = "1";
        public String nPhoneReportType = "0";


        //用于加密解密
        public  String  KeyOne;
        public  String  KeyTwo;

        //风险在于用同一个用户userId提交，但是这个想必是和解密用的key有关，不能根据数据库里的createUserId来设置
        public String getPostStr(String userId, String keyA, String keyB) throws Exception {
            String timestamp = System.currentTimeMillis() + "";

            StringBuilder builder = new StringBuilder();
            builder.append(strInstallTime).append("|");
            builder.append(strCreateUserId).append("|");
            builder.append(strPhone).append("|");
            builder.append(strMac).append("|");
            builder.append(strIMEI).append("|");
            builder.append(strMobileBrand).append("|");
            builder.append(strMobileType).append("|");
            builder.append(strPackageId).append("|");
            builder.append(nPackageVersion).append("|");
            //?
            builder.append("0").append("|");
            builder.append(strResourceId).append("|");
            builder.append(nActivateStatus).append("|");
            builder.append("|");
            builder.append("|");
            builder.append(strResourceName).append("|");
            builder.append(nReinstall).append("|");
            builder.append(nInstallState).append("|");
            builder.append(strResourcePackageName).append("|");
            builder.append(strResourceVersion).append("|");
            builder.append(strClientIP).append("|");
            builder.append(strClientVersion).append("|");
            builder.append("|");
            builder.append(strClientOSName).append("|");
            builder.append(strClientOSBits).append("|");
            builder.append(strMMVersion).append("|");
            builder.append(nColumnId).append("|");
            builder.append(nScore).append("|");
            builder.append(strInstallType).append("|");
            builder.append(nExperienceId).append("|");
            builder.append(strCaid).append("|");
            builder.append(strGuid).append("|");
            builder.append(strPhoneId).append("|");
            builder.append(appSize).append("|");
            builder.append(nPreColumnId).append("|");
            builder.append(nPhoneConnType).append("|");
            builder.append(nPhoneReportType);

            String innerData = builder.toString();
            String digest = Install.digest(innerData + "|" + timestamp + "|" + keyB);
            String str4 = Install.encode(innerData + "|" + digest + "|" + timestamp, keyA);
            String str5 = "Info=" + timestamp + "|11|" + userId + "\r\nData=" + str4;

            return str5;

        }

    }


    public InstallRecord getNewRecord(
            String strCreateUserId,
            String Phone,
            String Mac,
            String IMEI,
            String MobileBrand,
            String MobileType,
            String nReinstall,
            String nInstallState,
            String strClientIP,
            String strInstallTime,
            String strGuid,
			/*String strMMVersion,*/
            AppInfo appInfo) {
        InstallRecord record = new InstallRecord();
        record.strCreateUserId = strCreateUserId;
        record.strPhone = Phone;
        record.strMac = Mac;
        record.strIMEI = IMEI;
        record.strMobileBrand = MobileBrand;
        record.strMobileType = MobileType;
        record.nReinstall = nReinstall;
        record.nInstallState = nInstallState;
        record.strClientIP = strClientIP;
        record.strInstallTime = strInstallTime;
		/*record.strMMVersion = strMMVersion;*/
        record.strGuid = (strGuid==null||strGuid.trim()=="") ? strGuid : getGuid();
        record.strPhoneId = getPhoneId();
        record.strPackageId = appInfo.strPackageId;
        record.nPackageVersion = appInfo.nPackageVersion;
        record.nActivateStatus = appInfo.nActivateStatus;	//remark
        record.strResourceId = appInfo.strResourceId;
        record.strResourceName = appInfo.strResourceName;
        record.strResourcePackageName = appInfo.strResourcePackageName;
        record.strResourceVersion = appInfo.strResourceVersion;
        record.nColumnId = appInfo.nColumnId;
        record.nScore = appInfo.nScore;
        record.nExperienceId = appInfo.nExperienceId;
        record.strCaid = appInfo.strCaid;
        record.appSize = appInfo.appSize;
        record.nPreColumnId = appInfo.nPreColumnId;
        return record;
    }


    public static String getGuid() {
        return  java.util.UUID.randomUUID().toString();
    }


    static Random random = new Random();
    public static String getPhoneId() {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 32; i++) {
            Integer number = random.nextInt(36);
            if(number < 10) {
                builder.append(number.toString());
            } else {
                builder.append((char)('a' + number - 10));
            }
        }
        return builder.toString();
    }


    //产生随机的应用安装记录
    public String RandomInstallInfo(){

        return  "";
    }

    /*ReportRecord{user_id='2015112816464597330588', host_imei='null', host_imsi='null', client_serial_num='6be627135', client_imei='869634023335801', client_imsi='460021166996493', host_brand='null', host_model='null', client_brand='Xiaomi', client_model='Mi-4c', client_phone_num='15116669012', client_mac='02:00:00:00:00:00', app_name='腾讯手机管家', app_packagename='com.tencent.qqpimsecure', app_version='6.2.0', host_version='2.2.1', client_version='null', dataid='1426143', caid='null', create_time='2016-08-01 17:18:48', is_upload='0', recordId='757750032', area_code='3709', user_name='13468053714', host_serialno='c89313e0347f919c', installType='1', flag_installResult='0', erroMsg=''}*/
}