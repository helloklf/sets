import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
import sun.misc.IOUtils;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by helloklf on 2016/8/15.
 */
public class GzipTest {
	public static void main(String[] args) throws IOException {
		String queryString = "c27674f3e6cb19f4d09ade2ec10242cd|20160815|{f24b8042-77f2-492f-8feb-000bf3cd989a}|2015112816464597330588|1426139|300009630200|com.tencent.mtt|QQ浏览器|6.6.0.2300|20659075|0|2263|20160815|20160815100937|1|21|2.0.2|2.2.1|460021166996493|15116669012|Xiaomi|Mi-4c|1|c50d787cbf80993e94733cbcafb06fb2d774f9ef";
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		Base64OutputStream b64os = new Base64OutputStream(bos);
		GZIPOutputStream gout = new GZIPOutputStream(b64os);
		gout.write(queryString.getBytes("UTF-8"));
		gout.close();
		b64os.close();

		byte b1[] = bos.toByteArray();

		System.out.println("Encode:" + new String(b1));

		InputStream bais = new ByteArrayInputStream(b1);
		Base64InputStream b64io = new Base64InputStream(bais);
		GZIPInputStream gin = new GZIPInputStream(b64io);

		System.out.println(IOUtils.toString(gin,"UTF-8"));
	}
}
