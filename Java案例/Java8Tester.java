import java.util.List;
import java.util.ArrayList;

public class Java8Tester {
	public static void Do(Object text){
		if(text!=null)
			System.out.print("Is Not Null");
	}
	
   public static void main(String args[]){
      List names = new ArrayList();
		
      names.add("Google");
      names.add("Runoob");
      names.add("Taobao");
      names.add("Baidu");
      names.add("Sina");
	  
      names.forEach(Java8Tester::Do);
	  
		System.out.print("\r\n");
			
	  Tools obj = new Tools();
      names.forEach(obj::Do);
   }
   
}
 class Tools{
	
	public void Do(Object text){
		if(text!=null)
			System.out.print("Is Not Null");
	}
}