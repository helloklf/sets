create database NewsDB
go

create table NewsTable
(
	TypeId int identity(1,1) primary key,	--类型编号
	TypeName nvarchar(50) not null unique	--类型名称
)

go

create table News
(
	IndexId int identity(1,1) primary key,	--记录索引
	TypeId int not null,					--新闻类型 编号
	NewsDate datetime default(getdate()),	--时间
	NewsFrom nvarchar(100),					--来源
	NewsAuthor nvarchar(50),				--作者
	NewsTitle nvarchar(100) not null,		--新闻标题
	NewsContent NText not null				--新闻内容
)

go

