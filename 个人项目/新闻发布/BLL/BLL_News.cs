﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{

    /// <summary>
    /// 查询News(新闻条目)表的对应BLL类
    /// </summary>
    public class BLL_News
    {
        static DAL.DAL_News dNews = new DAL.DAL_News(); //DAL层中的对应操作类

        /// <summary>
        /// 查询某个类型的新闻
        /// </summary>
        /// <param name="typeid"></param>
        /// <returns></returns>
        public static List<Model.M_News> GetNews(int typeid = 0)
        {
            return dNews.GetNews(typeid);
        }
        
        
        /// <summary>
        /// 更新新闻
        /// </summary>
        /// <param name="news"></param>
        /// <returns></returns>
        public static int UpdateNews(Model.M_News news) { return dNews.UpdateNews(news); }

        /// <summary>
        /// 删除新闻
        /// </summary>
        /// <param name="news"></param>
        /// <returns></returns>
        public static int DeleteNews(Model.M_News news) { return dNews.DeleteNews(news); }

        /// <summary>
        /// 查询某条新闻
        /// </summary>
        /// <param name="typeid"></param>
        /// <returns></returns>
        public static List<Model.M_News> GetNewsByID(int newsid)
        {
            return dNews.GetNewsByID(newsid);
        }

        /// <summary>
        /// 添加新闻
        /// </summary>
        /// <returns></returns>
        public static int AddNews(Model.M_News news) 
        {
            return dNews.AddNews(news);
        }
    }
}
