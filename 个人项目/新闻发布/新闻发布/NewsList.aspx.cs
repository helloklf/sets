﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace 新闻发布
{
    public partial class NewsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Model.M_News> news = BLL.BLL_News.GetNews();
            Asp_NewsList.DataSource = news; Asp_NewsList.DataBind();
        }
    }
}