﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MyWebSite.Messages.REFiles
{
    /// <summary>
    /// AjaxFileUpload 的摘要说明
    /// </summary>
    public class AjaxFileUpload : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string savapath = "/DataSources/NewsImgSource/" + Guid.NewGuid().ToString() + context.Request.Params["type"];
                context.Request.Files[0].SaveAs(context.Server.MapPath("~"+savapath));
                context.Response.Write("../.."+savapath);
            }
            catch(Exception ex)
            {
                context.Response.Write(ex.Message);
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}