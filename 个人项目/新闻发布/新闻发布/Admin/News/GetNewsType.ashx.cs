﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace 新闻发布.Admin.News
{
    /// <summary>
    /// GetNewsType 的摘要说明
    /// </summary>
    public class GetNewsType : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                JavaScriptSerializer jss = new JavaScriptSerializer();
                context.Response.Write(jss.Serialize(BLL.BLL_NewsType.GetNewsType()));
            }
            catch { context.Response.Write("servererror"); }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}