﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace 新闻发布.Admin
{
    public partial class NewsType : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) ReBind();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewsTypeList_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            BLL.BLL_NewsType.DeleteNewsType(new Model.M_NewsType() { TypeId = (int)e.Keys[0] });
            //Response.Write("<script>alert('" + e.Keys[0] + "')</script>");
            ReBind();
        }

        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewsTypeList_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            var value = (e.Item.FindControl("TypeNameTextBox") as TextBox).Text;
            if (value.Trim().Length > 0)
            {
                BLL.BLL_NewsType.AddtNewsType( new Model.M_NewsType() { TypeName = value } );
                ReBind();
            }
            else
            {
                Response.Write("<script>alert('不能设置的类型名称！')</script>");
            }
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewsTypeList_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            NewsTypeList.EditIndex = e.NewEditIndex;
            ReBind();
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewsTypeList_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            //Response.Write("<script>alert('" + e.NewValues[0] + "')</script>");
            var value = e.NewValues[0].ToString();
            var index = (int)e.Keys[0];
            if (value.Trim().Length > 0)
            {
                BLL.BLL_NewsType.UpdatetNewsType(new Model.M_NewsType() { TypeName = value, TypeId = index });
                NewsTypeList.EditIndex = -1;
                ReBind();
            }
            else
            {
                Response.Write("<script>alert('不能设置的类型名称！')</script>");
            }
        }

        /// <summary>
        /// 重新绑定
        /// </summary>
        void ReBind() 
        {
            NewsTypeList.DataSource = BLL.BLL_NewsType.GetNewsType();
            NewsTypeList.DataBind();
        }
    }
}