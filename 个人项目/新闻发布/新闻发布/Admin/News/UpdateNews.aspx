﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateNews.aspx.cs" Inherits="新闻发布.Admin.NewsUpdate" MasterPageFile="~/Frameworks/News.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<div style="width:100%; margin:0px; padding:0px; height:100%;"><script src="nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            var myNicEditor = new nicEditor();
            myNicEditor.setPanel('myNicPanel');
            myNicEditor.addInstance('myInstance1');
        });
    </script>
    <div style="position:absolute; left:30px; right:30px;width:auto">
        新闻标题：<input type="text" id="NewsTitle"/><br />
        <%--新闻索引：--%><input id="IndexId" readonly="true" style="display:none; visibility:collapse;"/><%--<br />--%>
        新闻作者：<input id="NewsAuthor"/><br />
        新闻来源：<input id="NewsFrom"/><br />
        发布时间：<input id="NewsDate" disabled="disabled" readonly="true"/><br />
        新闻类型：<select onchange='document.getElementById("NewsType").value= value' id="NewsTypeSelect" style="width: 260px; font-size: 18px; vertical-align: middle; padding: 5px"></select>
                <input type="text" id="NewsType" value="" readonly="true" style="width:40px; " disabled="disabled" />
        <br />
        内容简述：<textarea id="GeneralizeTitle" style="height:50px; width:100%"></textarea><br />
        
        <div id="myNicPanel" style="width: 100%;"></div>
        <div id="myInstance1" style="font-size: 16px; background-color: #ccc; padding: 3px; min-height: 400px; width: 100%;">
            新闻正文...
        </div>
        <input type="button" value="完成" onclick="AjaxUpdateNews()"/>
    </div>
</div>
<script src="../../JavaScript/AjaxGetData.js"></script>
<script>
    function $$(id) { return document.getElementById(id); }
    var newstype = 1; //全局变量，用户保存原始新闻id
    var newsid = 0; //全局变量，用户保存原始新闻id
    var NewsVessel = "REPdoc"; //显示新闻的内联框架id
    //处理新闻内容
    function GetNewsPostBack(r) {
        try {
            var js = JSON.parse(r);
            for (var i in js) {
                newstype = js[i].NewsType;
                $$("NewsTitle").value = js[i].NewsTitle;
                $$("IndexId").value = js[i].IndexId;
                $$("NewsAuthor").value = js[i].NewsAuthor;
                $$("NewsFrom").value = js[i].NewsFrom;
                $$("NewsDate").value = js[i].NewsDateText;
                $$("GeneralizeTitle").textContent = js[i].GeneralizeTitle;
                myInstance1.innerHTML = js[i].NewsContent;
                //IFrameEnableEdit(js[i].NewsContent, NewsVessel);
            }
            AjaxGetNewsType();
        }
        catch (e) { IFrameEnableEdit("数据解析错误！", NewsVessel); }
    }

    //获取新闻内容
    function AjaxGetNewsContent(newsid, _postback) {
        var xml = new XMLHttpRequest();
        xml.onreadystatechange = function () {
            if (xml.readyState == 4) {
                if (xml.status == 200) {
                    var r = xml.response;
                    if (r != "servererror") _postback(r);
                    else IFrameEnableEdit("没有找到相关新闻！", NewsVessel);
                }
                else { IFrameEnableEdit("您的网络不给力！", NewsVessel); }
            }
        }
        var f = new FormData(); f.append("newsid", newsid);
        xml.open("POST", "../../GetNewsContent.ashx", true);
        xml.send(f);
    }
    //Ajax获取新闻类型的回送
    function GetNewsTypePostBack(response) 
    {
        if (response == "serverror") { throw Error("服务器出现错误"); }
        else {
            var json = JSON.parse(response);
            if (json)
            {
                AddNewsTypeToSelect(json, 'NewsTypeSelect');
                document.getElementById("NewsType").value = document.getElementById("NewsTypeSelect").value;
            }
            else { alert("新闻类型数据转换失败，请联系开发者!"); }
        }
    }
    //将新闻类型添加到Select中 需要提供json集合对象和Select容器ID
    function AddNewsTypeToSelect(json, _selectId) {
        var select = document.getElementById(_selectId);
        for (var i in json) {
            var op = document.createElement("option");
            op.value = json[i].TypeId;
            op.textContent = json[i].TypeName;
            select.appendChild(op);
            if (json[i].TypeId == newstype)
            {
                document.getElementById("NewsType").value = newstype; op.selected = true;
            }
        }
    }
    //将新闻类型添加到Ul中 需要提供json集合对象和Ul容器ID
    function AddNewsTypeToUl(json, _ulId) {
        var ul=document.getElementById(_ulId);
        for (var i in json) {
            var li = document.createElement("li");
            li.textContent = json[i].TypeName; li.value = json[i].TypeId;
            ul.appendChild(li);
        }
    }
    //Ajax 获取新闻类型
    function AjaxGetNewsType() {
        AjaxGet("GetNewsType.ashx", GetNewsTypePostBack);
    }

    //添加新闻操作回送
    function AddNewsPostBack(response) {
        if (response == "complete") { window.location.href = "../NewsManagement.aspx"; }
        else if (response == "servererror") { alert("服务器错误，无法发布新闻，请联系开发者！"); }
    }
    //生成可以用Ajax提交的表单数据
    function CreateFormData() {
        var fd = new FormData();
        fd.append("NewsContent", encodeURI(myInstance1.innerHTML));
        fd.append("NewsTitle", encodeURI($Id("NewsTitle").value));
        fd.append("NewsType", $Id("NewsType").value);
        fd.append("NewsAuthor", encodeURI($Id("NewsAuthor").value));
        fd.append("NewsFrom", encodeURI($Id("NewsFrom").value));
        fd.append("GeneralizeTitle", encodeURI($Id("GeneralizeTitle").textContent));
        return fd;
    }

    function AjaxUpdateNews() {
        var fd =  CreateFormData(); fd.append("IndexId",newsid);
        AjaxPost("UpdateNews.ashx",fd, AddNewsPostBack)
    }

    //截取newsid(新闻ID)参数
    if (location.href.indexOf('?') > 0) {
        var href = location.href;
        href = href.substring(href.indexOf('?newsid=') + 8, href.length);
        if (href.indexOf('&') > 0) href = href.substring(0, href.indexOf('&')); newsid = href;
        AjaxGetNewsContent(href, GetNewsPostBack); 
    }
</script>


</asp:Content>