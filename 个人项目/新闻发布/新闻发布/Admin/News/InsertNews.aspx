﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertNews.aspx.cs" Inherits="新闻发布.Admin.AddNews" MasterPageFile="~/Frameworks/News.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script src="nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            var myNicEditor = new nicEditor();
            myNicEditor.setPanel('myNicPanel');
            myNicEditor.addInstance('myInstance1');
        });
    </script>
    <style>
        #newsaddForm {
            list-style:none;
        }
        #newsaddForm li {
            padding:5px 0px;
        }
    </style>
    <div>
    <ul id="newstypelist"></ul>
        <ul id="newsaddForm">
            <li>标题：<input type="text" id="NewsTitle" value="请输入新闻标题" />
            </li>
            <li>类型：<select onchange='document.getElementById("NewsType").value= value' id="NewsTypeSelect" style="width: 260px; font-size: 18px; vertical-align: middle; padding: 5px"></select>
                <input type="text" id="NewsType" value="" readonly="true" hidden="hidden" style="display: none" />
            </li>
            <li>来源：<input type="text" id="NewsFrom" value="科瑞网络" />
            </li>
            <li>作者：<input type="text" id="NewsAuthor" value="科瑞网络编辑" />
            </li>
            <li>简介：<textarea id="GeneralizeTitle" style="width: 100%; height: 50px">请在这里书写新闻的简要信息</textarea>
            </li>
        </ul>
        <div id="myNicPanel" style="width: 100%;"></div>
        <div id="myInstance1" style="font-size: 16px; background-color: #ccc; padding: 3px; min-height: 400px; width: 100%;">
            新闻正文...
        </div>
        <input type="button" onclick="AjaxAddNews()" value="完成"/>
    </div>
    
    <script src="../../JavaScript/AjaxGetData.js"></script>
    <script>
        var newstype=1;
        //Ajax获取新闻类型的回送
        function GetNewsTypePostBack(response) {
            if (response == "serverror") { throw Error("服务器出现错误"); }
            else {
                var json = JSON.parse(response);
                if (json) {
                    AddNewsTypeToSelect(json, 'NewsTypeSelect');
                    //alert(newstype);
                    document.getElementById("NewsType").value = document.getElementById("NewsTypeSelect").value;
                }
                else { alert("新闻类型数据转换失败，请联系开发者!"); }
            }
        }
        //将新闻类型添加到Select中 需要提供json集合对象和Select容器ID
        function AddNewsTypeToSelect(json, _selectId) {
            var select = document.getElementById(_selectId);
            for (var i in json) {
                var op = document.createElement("option");
                op.value = json[i].TypeId;
                op.textContent = json[i].TypeName;
                select.appendChild(op);
                if (json[i].TypeId == newstype) {
                    document.getElementById("NewsType").value = newstype; op.selected = true;
                }
            }
        }
        //Ajax 获取新闻类型
        function AjaxGetNewsType() {
            AjaxGet("GetNewsType.ashx", GetNewsTypePostBack);
        }
        //添加新闻操作回送
        function AddNewsPostBack(response) {
            if (response == "complete") { window.location.href = "../NewsManagement.aspx"; }
            else if (response == "servererror") { alert("服务器错误，无法发布新闻，请联系开发者！"); }
        }
        //生成可以用Ajax提交的表单数据
        function CreateFormData() {
            var fd = new FormData();
            fd.append("NewsContent", encodeURI( myInstance1.innerHTML ));
            fd.append("NewsTitle", encodeURI($Id("NewsTitle").value));
            fd.append("NewsType", $Id("NewsType").value);
            fd.append("NewsAuthor", encodeURI($Id("NewsAuthor").value));
            fd.append("NewsFrom", encodeURI($Id("NewsFrom").value));
            fd.append("GeneralizeTitle", encodeURI($Id("GeneralizeTitle").textContent));
            return fd;
        }
        //通过Ajax提交新闻
        function AjaxAddNews() {
            AjaxPost("AddNews.ashx", CreateFormData(), AddNewsPostBack)
        }
        AjaxGetNewsType();
    </script>
</asp:Content>