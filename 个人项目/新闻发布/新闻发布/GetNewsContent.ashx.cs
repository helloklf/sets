﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Script.Serialization;

namespace 新闻发布
{
    /// <summary>
    /// GetNewsContent 的摘要说明
    /// </summary>
    public class GetNewsContent : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                if (context.Request.Params["newsid"] != null)
                {
                    int id = int.Parse(context.Request.Params["newsid"]);
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    context.Response.Write(jss.Serialize(BLL.BLL_News.GetNewsByID(id)));
                }
            }
            catch { context.Response.Write("servererror"); }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}