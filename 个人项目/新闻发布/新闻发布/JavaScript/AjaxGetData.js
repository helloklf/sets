﻿function $Id(id) { return document.getElementById(id); }
function AjaxGet(url, _postback) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                _postback(xml.response);
            }
        }
    }
    xml.open("GET", url, true);
    xml.send(null);
}
function AjaxPost(url, data, _postback) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                _postback(xml.response);
            }
        }
    }
    xml.open("POST", url, true);
    xml.send(data);
}