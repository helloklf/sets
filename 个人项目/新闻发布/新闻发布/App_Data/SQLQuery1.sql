create database NewsDB
go

create table NewsType
(
	TypeId int identity(1,1) primary key,	--类型编号
	TypeName nvarchar(50) not null unique,	--类型名称
	IsValid bit default(1)					--是否有效
)

go


create table News
(
	IndexId int identity(1,1) primary key,	--记录索引
	NewsType int check(NewsType>0) not null,--新闻类型 编号
	NewsDate datetime default(getdate()),	--时间
	NewsFrom nvarchar(100),				--来源
	NewsAuthor nvarchar(50),				--作者
	NewsTitle nvarchar(100) not null,		--新闻标题
	GeneralizeTitle nvarchar(200),			--推广标题
	NewsContent NText not null,			--新闻内容
	IsValid bit default(1)					--是否有效
)



go

insert into NewsType( TypeName ) values ( N'周边' )
insert into NewsType( TypeName ) values ( N'行业' )
insert into NewsType( TypeName ) values ( N'服务' )

update NewsType set IsValid = case when IsValid = 1 then 0 else 1 end

select typeid,TypeName,case IsValid when 1 then 'True' else 'False' end from NewsType

go

select * from NewsType

go

insert into News( NewsType, NewsTitle, NewsFrom, NewsAuthor,GeneralizeTitle, NewsContent)
values( 3,N'双十一背后的故事2',N'科瑞网络',N'小编1号',N'双十一千万销量背后的故事，你知道吗？',N'<html><div><h1>内容</h1><div></html>' )
insert into News( NewsType, NewsTitle, NewsFrom, NewsAuthor,GeneralizeTitle, NewsContent)
values( 2,N'电商行业最新报告',N'科瑞网络',N'小编2号',N'电商行业最新最及时的动态，你想知道吗？',N'新闻正文' )

go

select * from News where IsValid = 1

select * from News join NewsType on News.NewsType = NewsType.TypeId where News.IsValid = 1 and NewsType=1
