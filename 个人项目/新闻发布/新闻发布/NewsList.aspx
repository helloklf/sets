﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsList.aspx.cs" Inherits="新闻发布.NewsList" MasterPageFile="~/Frameworks/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div>
        <style>
            ul {
                list-style:decimal;
            }
            li {
                padding:15px 5px; font-size:12px; font-weight:600;
            }
            li div {
                padding:3px 0px;
            }
            li a {
                font-weight:900;
            }
            a {
                text-decoration:none;color:#0094ff;
            }
            a:hover {
                color:#507CD1;
            }
            .newsTtile {
                font-size:25px; padding:5px 0px
            }
        </style>
        <asp:Repeater runat="server" ID="Asp_NewsList">
            <ItemTemplate>
                <a class="newsTtile" href="<%# "NewsDetails.aspx?newsid="+ Eval("IndexId") %>"><%# Eval("NewsTitle") %> </a>
                <br />
                <span>类型：<%# Eval("TypeName") %> </span>
                <br />
                <span>简述：<%# Eval("GeneralizeTitle") %> </h3>
                <hr />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
