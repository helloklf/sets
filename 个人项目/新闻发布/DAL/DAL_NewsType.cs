﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// 查询NewsType(新闻类型)表的对应DAL类
    /// </summary>
    public class DAL_NewsType
    {
        /// <summary>
        /// 获取所有新闻类型
        /// </summary>
        /// <returns></returns>
        public List<Model.M_NewsType> GetNewsType(bool? IsValid = null)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    string cmd = "";
                    switch (IsValid)
                    {
                        case true: { cmd = " where IsValid = 1"; break; }
                        case false: { cmd = " where IsValid = 0"; break; }
                        default: break;
                    }
                    DbDataReader dr = db.ExecuteReader("select * from NewsType" + cmd);
                    return ReadNewsTypeData(dr);
                }
            }
            catch { throw new Exception("查找新闻类型出错"); }
        }

        /// <summary>
        /// 添加一种新闻类型
        /// </summary>
        /// <param name="newstype"></param>
        /// <returns></returns>
        public int AddtNewsType(Model.M_NewsType newstype)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    return db.ExecuteNonQuery("insert into NewsType(TypeName) values(@TypeName)", new SqlParameter[]{
                        new SqlParameter("@TypeName",newstype.TypeName)
                    });
                }
            }
            catch { throw new Exception("添加新闻类型错误"); }
        }

        /// <summary>
        /// 修改一种新闻类型
        /// </summary>
        /// <param name="newstype"></param>
        /// <returns></returns>
        public int UpdatetNewsType(Model.M_NewsType newstype)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    return db.ExecuteNonQuery("update NewsType set TypeName=@TypeName where TypeId = @TypeId", new SqlParameter[]{
                        new SqlParameter("@TypeName",newstype.TypeName),
                        new SqlParameter("@TypeId",newstype.TypeId)
                    });
                }
            }
            catch { throw new Exception("修改新闻类型错误"); }
        }

        /// <summary>
        /// 删除或恢复一种新闻类型
        /// </summary>
        /// <param name="newstype"></param>
        /// <returns></returns>
        public int DeleteNewsType(Model.M_NewsType newstype)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    //update NewsType set IsValid=0 where TypeId = @TypeId
                    //update NewsType set IsValid = case when IsValid = 1 then 0 else 1 end
                    return db.ExecuteNonQuery("update NewsType set IsValid = case when IsValid = 1 then 0 else 1 end where TypeId = @TypeId", new SqlParameter[]{
                        new SqlParameter("@TypeId",newstype.TypeId)
                    });
                }
            }
            catch { throw new Exception("修改新闻类型错误"); }
        }

        /// <summary>
        /// 从DbDataReader读取数据到泛型集合 适用于M_NewsType（新闻类型）
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public List<Model.M_NewsType> ReadNewsTypeData(DbDataReader dr)
        {
            try
            {
                List<Model.M_NewsType> newstlist = new List<Model.M_NewsType>();
                Model.M_NewsType newst;
                while (dr.Read())
                {
                    newst = new Model.M_NewsType();
                    newst.IsValid = (bool?)dr["IsValid"];
                    newst.TypeId = (int)dr["TypeId"];
                    newst.TypeName = dr["TypeName"].ToString();
                    newstlist.Add(newst);
                }
                dr.Dispose();
                return newstlist;
            }
            catch { throw new Exception("读取新闻类型条目出错"); }
        }
    }

}
