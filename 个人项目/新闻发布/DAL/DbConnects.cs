﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DbConnects
    {
        internal static SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["News"].ConnectionString);//SQL数据库使用的连接方式

        /// <summary>
        /// 创建Sql连接实例(DBHelper是SQLDBHelper的父类，如果你要使用其它数据库，也可改成DBHelper的其它子类)
        /// </summary>
        /// <returns></returns>
        internal static DBHelper GetDbHelper()
        {
            return new SQLDBHelper(DbConnects.sqlConn);
        }
    }
}
