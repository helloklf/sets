﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class M_NewsType
    {
        int typeId;
        /// <summary>
        /// 类型编号
        /// </summary>
        public int TypeId
        {
            get { return typeId; }
            set { typeId = value; }
        }


        string typeName;
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        bool? isValid;
        /// <summary>
        /// 有效性
        /// </summary>
        public bool? IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }
    }
}
