﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 反射的性能测试
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //反射
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var obj = System.Reflection.Assembly.Load("反射的性能测试");
            List<Model1cs> m1 = new List<Model1cs>();
            Model1cs m;
            System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
            st.Start();
            for (int i = 0; i < 7654321; i++)
            {
                m1.Add(obj.CreateInstance("Model1cs") as Model1cs);
            }
            st.Stop();
            T2.Text = st.ElapsedTicks.ToString();
        }

        //正常
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            List<Model1cs> m1 = new List<Model1cs>();
            System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
            st.Start();
            for (int i = 0; i < 7654321; i++)
            {
                m1.Add(new Model1cs() {  Name="名称" });
            }
            st.Stop();
            T1.Text = st.ElapsedTicks.ToString();
        }

        //测试
        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            List<Class1> cl = new List<Class1>();
            State.Text = "开始测试...";
            pj.Text = "";
            await System.Threading.Tasks.Task.Run(() =>
            {
                long one = 0;
                long two = 0;
                for (int j = 0; j < 20; j++)
                {
                    Dispatcher.Invoke(() => { State.Text = "正在进行第" + j+"次测试..."; });
                    Class1 c = new Class1();
                    List<Model1cs> m1 = new List<Model1cs>();
                    System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
                    st.Start();
                    for (int i = 0; i < 7654321; i++)
                    {
                        m1.Add(new Model1cs() );
                    }
                    st.Stop();
                    c.one = st.ElapsedTicks;
                    one += st.ElapsedTicks;

                    var obj = System.Reflection.Assembly.Load("反射的性能测试");
                    List<Model1cs> m2 = new List<Model1cs>();
                    System.Diagnostics.Stopwatch st2 = new System.Diagnostics.Stopwatch();
                    st2.Start();
                    for (int i = 0; i < 7654321; i++)
                    {
                        m1.Add((obj.CreateInstance("Model1cs") as Model1cs));
                    }
                    st2.Stop();
                    c.two = st2.ElapsedTicks;
                    two += st2.ElapsedTicks;

                    cl.Add(c);
                    Dispatcher.Invoke(() =>
                    {
                        L1.ItemsSource = null;
                        L1.ItemsSource = cl;
                        pj.Text = "平均："+(two * 1.0 / one);
                    });
                }
            }); 
            State.Text = "测试完成";
        }

    }
}
