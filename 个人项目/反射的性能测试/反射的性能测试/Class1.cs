﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 反射的性能测试
{
    class Class1
    {
        public long one { get; set; }
        public long two { get; set; }
        public double count { get { return two*1.0 / one; } }
    }
}
