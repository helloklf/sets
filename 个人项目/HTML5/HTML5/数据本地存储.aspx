﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="数据本地存储.aspx.cs" Inherits="HTML5.数据本地存储" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="float: left; width: 40%">
                账号：<input id="Html_email" type="email" value="klf@live.com" style="padding: 5px; width: 200px; margin:5px;" />
                <br />
                密码：<input id="Html_pwd" type="password" value="123456" style="padding: 5px; width: 200px;margin:5px;" />
                <br />
                <input type="button" value="存储" onclick="LocalStorageTest()" />
            </div>
            <div>
                <div id="Html_LSList"></div>
            </div>
        </div>
    </form>
</body>
    <script>
        function LocalStorageTest() {
            if (window.localStorage) {
                var value =document.getElementById("Html_email").value;;
                localStorage.email = value;
                //或写作 
                    //2:    localStorage[email] = value;
                    //3:    localStorage.setItem("email",value);
                localStorage.pwd = document.getElementById("Html_pwd").value;

                ReferenceList();//显示结果
            }
            else {
                alert("你的浏览器不支持本地存储！");
            }
        }
        function ReferenceList() {
            //单项取值
            //var value = localStorage.getItem("email");
            //var value = localStorage.email;
            //var value = localStorage["getItem"];

            var listPanel = document.getElementById("Html_LSList");
            listPanel.innerHTML = "";

            //遍历
            for (var i = 0; i < localStorage.length; i++) {
                listPanel.innerHTML += localStorage.key(i) +"："+ localStorage.getItem(localStorage.key(i))  + "<br/>";
            }
        }
    </script>
</html>
