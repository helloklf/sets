﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SqlDataBase.aspx.cs" Inherits="HTML5.SqlDataBase" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script>
        var db;
        function Test() {
            //此功能在IE（IE11）上仍不支持

            //连接或创建数据库：数据库名，数据库版本，描述信息，大小（字节）
            if (window.openDatabase) {
                db = window.openDatabase("MyTestDB", "1.0", "描述信息", 1024 * 1024);
                if (db)
                    alert("数据连接成功！");
                else {
                    alert("数据库连接失败！");
                    return;
                }
                
            }
            else
            {
                alert("不支持此浏览器！");
            }
        }
        function CreateTable() {
            if (db) {
                //开始事务
                db.transaction(function (e) {
                    e.executeSql("create table T1( name nvarchar(50),remark nvarchar(500) )");//执行Sql语句
                });
            }
        }
        function Insert() {
            if (db) {
                //开始事务
                db.transaction(function (e) {
                    e.executeSql("insert into T1(name,remark) values('你好','大家好')");//执行Sql语句

                //或者这样写
                //e.executeSql("insert into T1(name,remark) values(?,?)",["你好","大家好"]);//执行Sql语句
                });
            }
        }
        function Select() {
            if (db) {
                //开始事务
                db.transaction(function (e) {
                    e.executeSql("select * from T1",[],function (tx, result) {
                        var tab = document.getElementById("Html_DataList");
                        tab.innerHTML = "";
                        for (var i = 0; i < result.rows.length; i++) {
                            tab.innerHTML += "<tr><td>" + result.rows.item(i)['name'] + "</td><td>"+result.rows.item(i)['remark']+"</td></tr>";
                        }
                    },
                    function (e) {
                        alert("出错了！");
                    });//执行Sql语句
                });
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%--WebDataBase并不属于HTML5规范的组成部分，而是单独的规范，是通过一套API来操作客户端的数据库--%> 
        <input onclick="Test()" type="button" value="连接数据库"/>
        <input type="button" value="创建数据表" onclick
            ="CreateTable()"/>
        <input type="button" value="插入数据行" onclick="Insert()"/>
        <input type="button" value="查询数据库" onclick="Select()"/>
    </div>
        <div>
            <table id="Html_DataList" style="width:500px"></table>
        </div>
    </form>
</body>
</html>
