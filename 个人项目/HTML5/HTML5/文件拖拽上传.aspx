﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="文件拖拽上传.aspx.cs" Inherits="HTML5.文件拖拽上传" %>

<!DOCTYPE html>

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script>
        //开始拖放
        function CanvasDrop() {
            event.preventDefault();//覆盖浏览器默认的拖放事件
            document.getElementById("Html_LoadState").textContent += "CanvasDrop　　";
        }
        //放开鼠标按钮
        function CanvasDrop() {
            event.preventDefault();//覆盖浏览器默认的拖放事件
            document.getElementById("Html_LoadState").textContent += "CanvasDrop　　";
            if (event.dataTransfer.files.length < 1) {
                alert("无有效文件！");
            }
            else {
                var _files = event.dataTransfer.files;//文件列表
                for (var i = 0; i < _files.length; i++)
                {
                    document.getElementById("Html_Files").innerHTML+="<li>文件名："+_files[i].name+ "<br/>　大小："+_files[i].size+"</li>";
                }
            }
        }
        //拖放进入容器
        function CanvasDragEnter() {
            event.preventDefault();//覆盖浏览器默认的拖放事件
            document.getElementById("Html_LoadState").textContent += "CanvasDragEnter　　";
        }
        //拖动内容在容器上移动
        function CanvasDragOver() {
            event.preventDefault();//覆盖浏览器默认的拖放事件
            document.getElementById("Html_LoadState").textContent += "CanvasDragOver　　";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="float:left;width:38%">
         <canvas ondragenter="CanvasDragEnter()" ondrop="CanvasDrop()" ondragover="CanvasDragOver()" ondrag="CancasDrag()" height="300"  style="background:#ff6a00; width:100%">
             <label>拖动文件到此区域上传文件</label>
         </canvas>
        </div>
        <div style="float:left;width:30%">
            <label id="Html_LoadState"></label>
        </div>
        <div style="float:left;width:30%">
            <ul id="Html_Files">

            </ul>
        </div>
    </div>
    </form>
</body>
