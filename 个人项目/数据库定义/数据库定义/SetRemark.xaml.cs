﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace 数据库查询工具
{
    /// <summary>
    /// SetRemark.xaml 的交互逻辑
    /// </summary>
    public partial class SetRemark : Window
    {
        public SetRemark()
        {
            InitializeComponent();
        }

        public bool Save = false;

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Save = true;
            Close();
        }
    }
}
