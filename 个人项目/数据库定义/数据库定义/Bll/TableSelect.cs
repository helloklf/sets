﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace 数据库查询工具
{
    public partial class TableSelect : Window
    {
        /// <summary>
        /// 获取数据库中的所有数据表
        /// </summary>
        /// <returns></returns>
        async Task<List<TabBasicInfo>> Db_GetTables()
        {
            List<TabBasicInfo> tabinfos = new List<TabBasicInfo>();
            await Task.Run(() =>
            {
                var tabs = DbGet.GetTables();
                foreach (var item in tabs)
                {
                    tabinfos.Add(new TabBasicInfo() { TableName = item });
                }
            });
            return tabinfos;
        }

        async void Db_UpdateFieldsRemark(DataTable dt,string tabName)
        {
            await Task.Run(() =>
            {
                foreach (DataRow item in dt.Rows)
                {
                    var remark = item["中文名"].ToString();
                    var name = item["列名"].ToString();
                    DbGet.SP_SetExtendedProperty(remark, tabName, name);
                }
            });
        }


        /// <summary>
        /// 获取表结构
        /// </summary>
        /// <param name="tabName"></param>
        /// <returns></returns>
        async Task<DataTable> Db_GetTableSchema(string tabName)
        {
            DataTable dt = null;
            await Task.Run(() => 
            {
                dt = DbGet.GetTableDetails(tabName);
            });
            return dt;
        }
    }


    public partial class TableSelect : Window
    {
        /// <summary>
        /// 数据表集合
        /// </summary>
        List<TabBasicInfo> DbTables = new List<TabBasicInfo>();

        /// <summary>
        /// 搜索数据表
        /// </summary>
        /// <param name="keyWord">关键字</param>
        /// <param name="mode">搜索模式</param>
        /// <returns></returns>
        List<TabBasicInfo> SearchTables(string keyWord,TableSearchMode mode)
        {
            if (mode == TableSearchMode.None || string.IsNullOrWhiteSpace(keyWord)) //如果无关键字或搜索模式为无
                return DbTables;
            else if (mode == TableSearchMode.TableNameHas)
                return DbTables.Where(item => item.TableName.ToLower().Contains(keyWord) || item.Remark.ToLower().Contains(keyWord)).ToList();
            else
                return DbTables.Where(item => item.TableName.ToLower() == keyWord || item.Remark.ToLower() == keyWord).ToList();
        }

        /// <summary>
        /// 搜索包含字段内容的数据表
        /// </summary>
        /// <param name="tabs">要搜索的表</param>
        /// <param name="keyWord">关键字</param>
        /// <param name="mode">搜索模式</param>
        /// <returns></returns>
        List<TabBasicInfo> SearchTableField(List<TabBasicInfo> tabs, string keyWord, FieldSearchMode mode)
        {
            if (mode == FieldSearchMode.None || string.IsNullOrWhiteSpace(keyWord)) //如果无关键字或搜索模式为无
                return tabs;
            else if (mode == FieldSearchMode.None)
                return tabs;

            var filterOver = new List<TabBasicInfo>();
            foreach (var tab in tabs)
            {
                if (SearchTableField(tab,keyWord,mode))
                    filterOver.Add(tab);
            }
            return filterOver;
        }

        /// <summary>
        /// 表搜索：搜索包含某个字段，或包含某个值得表
        /// </summary>
        /// <param name="tab">表名</param>
        /// <param name="keyWord">关键字</param>
        /// <param name="mode">搜索模式</param>
        /// <returns></returns>
        bool SearchTableField(TabBasicInfo tab, string keyWord, FieldSearchMode mode)
        {
            if (mode == FieldSearchMode.None || string.IsNullOrWhiteSpace(keyWord)) //如果无关键字或搜索模式为无
                return true;

            var key = keyWord;
            #region 使用列名对比
            if (mode == FieldSearchMode.FieldNameEquals || mode == FieldSearchMode.FieldNameHas)
            {
                DataTable tabFieldsInfo = DbGet.GetTableDetails(tab.TableName);
                #region 遍历字段
                foreach (DataRow tfiRow in tabFieldsInfo.Rows)
                {
                    var cName = ((tfiRow["中文名"] ?? "").ToString()).ToLower(); //中文名
                    var fName = ((tfiRow["列名"] ?? "").ToString()); //字段名
                    #region 列名比较
                    switch (mode)
                    {
                        case FieldSearchMode.FieldNameHas:
                            {
                                if (cName.Contains(key) || fName.Contains(key))
                                    return true;
                                break;
                            }
                        case FieldSearchMode.FieldNameEquals:
                            {
                                if (cName == (key) || fName == (key))
                                    return true;
                                break;
                            }
                    }
                    #endregion
                }
                #endregion
            }
            #endregion
            #region 使用列值对比
            else if (mode == FieldSearchMode.FieldValueEquals || mode == FieldSearchMode.FieldValueHas)
            {
                var dt = DbGet.GetTableAllData(tab.TableName); //获取单张表的所有数据行
                var colCount = dt.Columns.Count; //计算列数

                #region 遍历数据行
                foreach (DataRow dr in dt.Rows)
                {
                    #region 循环列值
                    for (int i = 0; i < colCount ; i++)
                    {
                        var xx = ((dr[i] ?? "").ToString()).ToLower();
                        #region 比较列值
                        switch (mode)
                        {
                            case FieldSearchMode.FieldValueHas:
                                {
                                    if (xx.Contains(key))
                                        return true;
                                    break;
                                }
                            case FieldSearchMode.FieldValueEquals:
                                {
                                    if (xx == key)
                                        return true;
                                    break;
                                }
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            #endregion
            return false;
        }

        /// <summary>
        /// 字段搜索：搜索名称包含某个关键字，或值包含某个关键字的字段
        /// </summary>
        /// <param name="tab"></param>
        /// <param name="keyWord"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        DataTable SearchFields(TabBasicInfo tab, string keyWord, FieldSearchMode mode)
        {
            var allData = DbGet.GetTableAllData(tab.TableName); //获取单张表的所有数据行

            if (mode == FieldSearchMode.None || string.IsNullOrWhiteSpace(keyWord)) //如果无关键字或搜索模式为无
            {
                DataTable allFields = DbGet.GetTableDetails(tab.TableName);//获取单张表的所有字段

                var colsCount = allFields.Rows.Count;
                var cols = new Dictionary<string, string>();
                foreach (DataRow item in allFields.Rows)
                {
                    cols[item["列名"].ToString()] = (item["中文名"]??"").ToString();   
                }
                foreach (DataColumn col in allData.Columns)
                {
                    if (cols.ContainsKey(col.ColumnName))
                    {
                        col.ColumnName = col.ColumnName + "\r\n"+ cols[col.ColumnName];
                    }
                }
                return allData;
            }

            var key = keyWord;
            #region 使用列名对比
            if (mode == FieldSearchMode.FieldNameEquals || mode == FieldSearchMode.FieldNameHas)
            {
                DataTable allFields = DbGet.GetTableDetails(tab.TableName);//获取单张表的所有字段
                var filterOverFields = new List<FiledBasicInfo>();//存放搜索结果
                #region 遍历字段
                foreach (DataRow tfiRow in allFields.Rows)
                {
                    var cName = ((tfiRow["中文名"] ?? "").ToString()).ToLower(); //中文名
                    var fName = ((tfiRow["列名"] ?? "").ToString()); //字段名
                    #region 列名比较
                    switch (mode)
                    {
                        case FieldSearchMode.FieldNameHas:
                            {
                                if (cName.Contains(key) || fName.Contains(key))
                                    filterOverFields.Add(new FiledBasicInfo() { FieldName = fName, Remark = cName });
                                break;
                            }
                        case FieldSearchMode.FieldNameEquals:
                            {
                                if (cName == (key) || fName == (key))
                                    filterOverFields.Add(new FiledBasicInfo() { FieldName = fName, Remark = cName });
                                break;
                            }
                    }
                    #endregion
                }
                #endregion
                var filterOverDatas = new DataTable();
                //创建表结构
                foreach (var item in filterOverFields)
                {
                    filterOverDatas.Columns.Add( new DataColumn( item.FieldName +"\r\n"+ item.Remark ) );
                }
                foreach (DataRow row in allData.Rows)
                {
                    var newrow = filterOverDatas.NewRow();
                    foreach (var field in filterOverFields)
                    {
                        newrow[field.FieldName + "\r\n" + field.Remark] = row[field.FieldName];
                    }
                    filterOverDatas.Rows.Add(newrow);
                }
                return filterOverDatas;
            }
            #endregion
            #region 使用列值对比
            else if (mode == FieldSearchMode.FieldValueEquals || mode == FieldSearchMode.FieldValueHas)
            {
                var colCount = allData.Columns.Count; //计算列数

                var filterOverDatas = new DataTable();
                foreach (DataColumn item in allData.Columns)
                {
                    filterOverDatas.Columns.Add( item.ColumnName );
                }
                #region 遍历数据行
                foreach (DataRow dr in allData.Rows)
                {
                    bool isHas = false;
                    #region 循环列值
                    for (int i = 0; i < colCount; i++)
                    {
                        var xx = ((dr[i] ?? "").ToString()).ToLower();
                        #region 比较列值
                        switch (mode)
                        {
                            case FieldSearchMode.FieldValueHas:
                                {
                                    if (xx.Contains(key))
                                    {
                                        isHas = true;
                                    }
                                    break;
                                }
                            case FieldSearchMode.FieldValueEquals:
                                {
                                    if (xx == key)
                                    {
                                        isHas = true;
                                    }
                                    break;
                                }
                        }
                        #endregion
                    }
                    if (isHas)
                    {
                        var newrow = filterOverDatas.NewRow();
                        foreach (DataColumn item in allData.Columns)
                        {
                            newrow[item.ColumnName] = dr[item.ColumnName];
                        }
                        filterOverDatas.Rows.Add(newrow);
                    }
                    #endregion
                }
                return filterOverDatas;
                #endregion
            }
            #endregion
            return null;
        }
    }
}
