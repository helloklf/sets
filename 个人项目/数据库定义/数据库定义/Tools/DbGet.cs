﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 数据库查询工具
{
    /// <summary>
    /// 数据库操作
    /// </summary>
    public static partial class DbGet
    {
        public static System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ConnectionString);

        /// <summary>
        /// 获取数据库所有存储过程
        /// </summary>
        /// <returns></returns>
        public static List<string> GetProcedures()
        {
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select name from sys.procedures", conn);
            List<string> procs = new List<string>();
            using (cmd)
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        procs.Add(reader[0].ToString());
                    }
                }
            }
            return procs;
        }


        /// <summary>
        /// 获取存储过程定义详情
        /// </summary>
        /// <param name="procName"></param>
        /// <returns></returns>
        public static string GetProceductDetails(string procName)
        {
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("sp_helptext", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@objname", procName));
            List<string> procs = new List<string>();
            using (cmd)
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        procs.Add(reader[0].ToString());
                    }
                }
            }
            var Text = "";
            foreach (var item in procs)
            {
                Text += item;
            }
            return Text;
        }

        /// <summary>
        /// 更新存储过程
        /// </summary>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static int UpdateProc(string procAlert)
        {
            var cmd = new System.Data.SqlClient.SqlCommand(procAlert, conn);
            using (cmd)
            {
                var count = cmd.ExecuteNonQuery();
                return count;
            }
        }

        /// <summary>
        /// 获取系统所有数据表
        /// </summary>
        /// <returns></returns>
        public static List<string> GetTables()
        {
            SqlCommand cmd = new SqlCommand("select * from sys.tables",conn);
            List<string> tabs = new List<string>();
            using (cmd)
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tabs.Add(reader["name"].ToString());
                    }
                }
            }
            return tabs;
        }


        /// <summary>
        /// 获取数据表定义详细
        /// </summary>
        /// <param name="tableName">表名称</param>
        /// <returns></returns>
        public static DataTable GetTableDetails(string tableName)
        {
            //o.object_id as '对象编号',
            var sql = @"select 
p.value as '中文名',
o.name as '列名' ,
o.column_id as '编号' ,--字段在数据表中处于第几列？（从1开始）
o.max_length as '最大长度',
o.is_identity as '自增长',
o.is_nullable as '是否可空',
df.definition as '默认值',
types.name as '类型'
from 
	sys.all_columns o --获取所有的列
		left join 
	sys.extended_properties p  --连接到属性拓展信息表，以获得表的描述信息
		on o.object_id = p.major_id and p.minor_id= o.column_id
		left join 
	sys.types types --连接到系统数据类型表，以获得列的类型
		on o.user_type_id = types.user_type_id 
		left join
	sys.default_constraints df
		on o.default_object_id = df.object_id
where o.object_id =  ( select object_id from sys.objects where  type = 'U' and name = '" + tableName + "')";
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, conn);
            List<string> table = new List<string>();
            using (cmd)
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                var dataset = new DataSet();
                sda.Fill(dataset);
                return dataset.Tables[0];
            }
        }

        /// <summary>
        /// 获取数据表的所有字段的备注信息
        /// </summary>
        /// <returns></returns>
        public static List<string> GetTableFieldsRemark(string tableName)
        {
            var sql = @"select 
'中文名' = case when p.value is not null then p.value else o.name end
from 
	sys.all_columns o --获取所有的列
		left join 
	sys.extended_properties p  --连接到属性拓展信息表，以获得表的描述信息
		on o.object_id = p.major_id and p.minor_id= o.column_id 
		left join 
	sys.types types --连接到系统数据类型表，以获得列的类型
		on o.system_type_id = types.system_type_id 
		left join
	sys.default_constraints df
		on o.default_object_id = df.object_id
where o.object_id =  ( select object_id from sys.objects where  type = 'U' and name = '" + tableName + "')";
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, conn);
            List<string> tabs = new List<string>();
            using (cmd)
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tabs.Add(reader[0].ToString());
                    }
                }
            }
            return tabs;
        }

        /// <summary>
        /// 查询数据表中的所有数据行和列
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DataTable GetTableAllData(string tableName)
        {
            var sql = "select * from "+ tableName;
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, conn);
            List<string> table = new List<string>();
            using (cmd)
            {
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                var dataset = new DataSet();
                sda.Fill(dataset);
                return dataset.Tables[0];
            }
        }

        
        /// <summary>
        /// 获取指定数据表的名称
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static string GetTableReamarkName(string tableName){
            var sql = "select value from sys.extended_properties where major_id = ( select object_id from sys.objects where  type = 'U' and name = '"+ tableName +"')  and minor_id = 0";
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, conn);
            List<string> table = new List<string>();
            using (cmd)
            {
                return (cmd.ExecuteScalar()??"").ToString();
            }
        }


        /// <summary>
        /// 为表或表的字段设置描述信息
        /// </summary>
        /// <param name="remark">描述信息</param>
        /// <param name="tableName">表名</param>
        /// <param name="fieldName">字段名（为空时表示是设置表描述）</param>
        /// <returns></returns>
        public static void SP_SetExtendedProperty(string remark,string tableName,string fieldName = null)
        {
            if (string.IsNullOrWhiteSpace(fieldName))
            {
                setTableRemark(remark, tableName);
            }
            else
            {
                setTablePropertyRemark(remark, tableName, fieldName);
            }
        }

        /// <summary>
        /// 设置表的描述信息
        /// </summary>
        /// <param name="remark">备注信息</param>
        /// <param name="tableName">表名</param>
        static void setTableRemark(string remark, string tableName)
        {
            var sql = @"if(exists(select * from sys.extended_properties where major_id= (select object_id from sys.tables where type ='U' and name ='{0}' and minor_id =0)))
begin
	EXECUTE   sp_updateextendedproperty   N'MS_Description', '{1}',   N'user',   N'dbo',   N'table',   N'{0}',  NULL,   NULL  
end
else
begin
	EXECUTE   sp_addextendedproperty   N'MS_Description', '{1}',   N'user',   N'dbo',   N'table',   N'{0}',  NULL,   NULL  
end";

            sql = string.Format(sql, tableName, remark);
            SqlCommand cmd = new SqlCommand(sql,conn);
            using (cmd)
            {
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// 设置数据表中某一字段的备注信息
        /// </summary>
        /// <param name="remark">备注信息</param>
        /// <param name="tableName">表名</param>
        /// <param name="fieldName">字段名</param>
        static void setTablePropertyRemark(string remark, string tableName, string fieldName)
        {
            var sql = @" if(exists(select * from sys.extended_properties p join (select object_id,column_id from sys.columns where object_id in ( select object_id from sys.tables where type = 'U' and name = '{0}' ) and name = '{1}') field on p.major_id = field.object_id and p.minor_id = field.column_id ))
begin
	EXECUTE   sp_updateextendedproperty   N'MS_Description', '{2}',   N'user',   N'dbo',   N'table',   N'{0}',  'column',   '{1}'  
end
else
begin
	EXECUTE   sp_addextendedproperty   N'MS_Description', '{2}',   N'user',   N'dbo',   N'table',   N'{0}',  'column',    '{1}'  
end";

            sql = string.Format(sql, tableName, fieldName, remark);
            SqlCommand cmd = new SqlCommand(sql, conn);
            using (cmd)
            {
                cmd.ExecuteNonQuery();
            }
        }
    }
}
