﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace 数据库查询工具
{
    public static partial class DbGet
    {
        /// <summary>
        /// 获取所有视图
        /// </summary>
        /// <returns>视图信息集合</returns>
        public static List<ViewBasicInfo> GetViews()
        {
            var sql = "select name,[object_id] from sys.views";
            var cmd = new SqlCommand(sql, conn);
            using (cmd)
            {
                var dr = cmd.ExecuteReader(); 
                List<ViewBasicInfo> views = new List<ViewBasicInfo>();
                using (dr)
                {
                    while (dr.Read())
                    {
                        ViewBasicInfo vbi = new ViewBasicInfo();
                        vbi.ViewName = (dr["name"] ?? "").ToString();
                        vbi.ObjectId = int.Parse( (dr["object_id"] ?? "0").ToString() );
                        views.Add(vbi);
                    }
                }
                return views;
            }
        }

        /// <summary>
        /// 获取视图的定义
        /// </summary>
        /// <param name="viewName">视图名称</param>
        /// <returns></returns>
        public static string GetViewDetails(string viewName)
        {
            var sql = "execute sp_helpText '"+ viewName +"'";
            var cmd = new SqlCommand(sql,conn);
            using (cmd)
            {
                using ( var dr = cmd.ExecuteReader())
                {
                    List<string> codes = new List<string>();
                    while (dr.Read())
                    {
                        codes.Add((dr[0] ?? "").ToString());
                    }
                    StringBuilder code = new StringBuilder();
                    foreach (var item in codes)
                    {
                        code.Append(item);
                    }
                    return code.ToString();
                }
            }
        }
    }
}
