﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace 数据库查询工具
{
    /// <summary>
    /// Menu.xaml 的交互逻辑
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
            SetConn setconn = new SetConn();
            setconn.ShowDialog();
            if (setconn.isSave == false)
            {
                Close();
                return;
            }

            System.Windows.Forms.MenuItem menuItem0 = new System.Windows.Forms.MenuItem() { Text="退出" };
            menuItem0.Click += menuItem0_Click;
            var menu = new System.Windows.Forms.ContextMenu(new System.Windows.Forms.MenuItem[]{ menuItem0 });
            notifyico.ContextMenu = menu;
            notifyico.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            notifyico.Click += notifyico_Click;
        }


        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void menuItem0_Click(object sender, EventArgs e)
        {
            notifyico.Visible = false;
            Application.Current.Shutdown();
        }

        private void Border_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void Proc_Click(object sender, RoutedEventArgs e)
        {
            OpenWindow<Proc>();
        }

        private void TableView_Click(object sender, RoutedEventArgs e)
        {
            OpenWindow<TableSelect>();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            Xaml_HideItem0.Visibility = Visibility.Visible;
        }

        private void GetMore_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("当前版本已经是最新版本！");
        }

        private void ConnSet_Click(object sender, RoutedEventArgs e)
        {
            OpenWindow<SetConn>();
        }

        private void View_Click(object sender, RoutedEventArgs e)
        {
            OpenWindow<View>();
        }


        void OpenWindow<T>() where T:Window,new()
        {
            var newWindow = new T();
            var type = newWindow.GetType();
            foreach (Window item in Application.Current.Windows)
            {
                if (item.GetType() == type)
                {
                    if (item.IsLoaded)
                        item.Activate();
                    else
                        item.Show();
                    return;
                }
            }
            newWindow.Show();
        }


        System.Windows.Forms.NotifyIcon notifyico = new System.Windows.Forms.NotifyIcon();

        /// <summary>
        /// 托盘图标点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void notifyico_Click(object sender, EventArgs e)
        {
            this.Visibility = Visibility.Visible;
            this.ShowInTaskbar = true;
            this.Activate();
            notifyico.Visible = false;
            //throw new NotImplementedException();
        }

        private void Window_Min(object sender, RoutedEventArgs e)
        {
            notifyico.Icon = new System.Drawing.Icon("App.ico"); //new System.Drawing.Icon(System.Windows.Forms.Application.StartupPath + "\\\\wp.ico");
            notifyico.Visible = true;
            this.ShowInTaskbar = false;
            this.Visibility = Visibility.Collapsed;
        }

        private void Window_Max(object sender, RoutedEventArgs e)
        {
            this.WindowState = this.WindowState == System.Windows.WindowState.Maximized ? System.Windows.WindowState.Normal : System.Windows.WindowState.Maximized;
        }
    }
}
