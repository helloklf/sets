﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace 数据库查询工具
{
    /// <summary>
    /// View.xaml 的交互逻辑
    /// </summary>
    public partial class View : Window
    {
        public View()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetList(GetAllView());
        }

        /// <summary>
        /// 刷新
        /// </summary>
        void Reload()
        {
            SetList(GetAllView());
        }

        /// <summary>
        /// 获取选中项的名称
        /// </summary>
        /// <returns></returns>
        string selectItem()
        {
            return (Xaml_ViewList.SelectedItem as ViewBasicInfo).ViewName;
        }

        /// <summary>
        /// 获取所有视图
        /// </summary>
        /// <returns></returns>
        List<ViewBasicInfo> GetAllView()
        {
            return DbGet.GetViews();
        }

        /// <summary>
        /// 获取视图的定义代码
        /// </summary>
        /// <param name="viewName"></param>
        /// <returns></returns>
        string GetViewDetails(string viewName)
        {
            return DbGet.GetViewDetails(viewName);
        }

        /// <summary>
        /// 设置视图列表
        /// </summary>
        /// <param name="list"></param>
        void SetList(List<ViewBasicInfo> list)
        {
            Xaml_ViewList.ItemsSource = list;
        }
        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_ViewList_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SetCodes();
        }

        /// <summary>
        /// 显示视图定义代码
        /// </summary>
        void SetCodes()
        {
            if (Xaml_ViewList.SelectedItem != null)
            {
                var vName = selectItem();
                TextRange tr = new TextRange(Xaml_ViewCodes.Document.ContentStart, Xaml_ViewCodes.Document.ContentEnd);
                tr.Text = DbGet.GetViewDetails(vName);
            }
        }


        #region 窗口管理
        private void Window_Close(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Max(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState == System.Windows.WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void Window_Min(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Window_Move(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == System.Windows.WindowState.Maximized)
                {
                    WindowState = System.Windows.WindowState.Normal;
                    return;
                }
                DragMove();
            }
        }

        private void WindowsTitle_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState == System.Windows.WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }
        #endregion

        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reload_Click(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        /// <summary>
        /// 设置连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeConn_Click(object sender, RoutedEventArgs e)
        {
            SetConn setconn = new SetConn();
            setconn.ShowDialog();
        }

    }

    /// <summary>
    /// 视图基本信息
    /// </summary>
    public class ViewBasicInfo : INotifyPropertyChanged
    {
        public static ViewBasicInfo GetItem(long object_id)
        {
            return new ViewBasicInfo();
        }

        string viewName;

        long objectId;

        string displayName;

        public string ViewName
        {
            get { return viewName; }
            set { viewName = value; }
        }
        public long ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set
            {
                displayName = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
