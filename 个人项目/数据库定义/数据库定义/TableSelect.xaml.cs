﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace 数据库查询工具
{
    public partial class TableSelect : Window
    {
        public TableSelect()
        {
            InitializeComponent();
            #region 初始化
            try
            {
                Ui_SetFieldSearchModes();
                Ui_SetTableSearchModes();
                Init();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "界面初始化失败");
                Close();
            }
            #endregion
        }

        #region 获取和设置搜索模式

        /// <summary>
        /// 设置字段搜索模式选项
        /// </summary>
        /// <returns></returns>
        void Ui_SetFieldSearchModes()
        {
            this.Xaml_Filter_Field.ItemsSource =
            new[]{ 
                new{ Text="列名包含", Mode = FieldSearchMode.FieldNameHas },
                new{ Text="列名等于", Mode = FieldSearchMode.FieldNameEquals },
                new{ Text="列值包含", Mode = FieldSearchMode.FieldValueHas },
                new{ Text="列值等于", Mode = FieldSearchMode.FieldValueEquals }
            };
        }

        /// <summary>
        /// 获取当前选中的字段搜索模式
        /// </summary>
        /// <returns></returns>
        FieldSearchMode Ui_GetFieldSearchMode()
        {
            FieldSearchMode mode = FieldSearchMode.None;
            Dispatcher.Invoke(new Action(() =>
            {
                try
                {
                    if (Xaml_Filter_Field.SelectedValue == null || Xaml_Filter_Field.SelectedIndex < 0)
                        mode = FieldSearchMode.None;
                    else
                    {
                        mode = (FieldSearchMode)Xaml_Filter_Field.SelectedValue;
                    }
                }
                catch
                {
                    mode = FieldSearchMode.None;
                }
            }));
            return mode;
        }

        /// <summary>
        /// 获取字段搜索关键字
        /// </summary>
        /// <returns></returns>
        string Ui_GetFieldSearchModeKeyWord()
        {
            var keyWord = "";
            Dispatcher.Invoke(new Action(
                () =>
                {
                    keyWord = this.Xaml_Search_ColValue.Text.Trim().ToLower();
                }
                ));
            return keyWord;
        }

        /// <summary>
        /// 设置表名搜索模式选项
        /// </summary>
        void Ui_SetTableSearchModes()
        {
            this.Xaml_Filter_Table.ItemsSource = new[] { 
                new { Text="表名包含", Mode = TableSearchMode.TableNameHas },
                new { Text="表名等于", Mode = TableSearchMode.TableNameEquals }
            };
        }

        /// <summary>
        /// 获取表名搜索模式
        /// </summary>
        /// <returns></returns>
        TableSearchMode Ui_GetTableSearchMode()
        {
            TableSearchMode mode = TableSearchMode.None;
            Dispatcher.Invoke(new Action(() =>
            {
                try
                {
                    if (Xaml_Filter_Table.SelectedValue == null || Xaml_Filter_Table.SelectedIndex < 0)
                        mode = TableSearchMode.None;
                    else
                    {
                        mode = (TableSearchMode)Xaml_Filter_Table.SelectedValue;
                    }
                }
                catch
                {
                    mode = TableSearchMode.None;
                }
            }));
            return mode;
        }

        /// <summary>
        /// 获取表名搜索关键字
        /// </summary>
        /// <returns></returns>
        string Ui_GetTableSearchKeyWord()
        {
            var keyWord = "";
            Dispatcher.Invoke(new Action(
                () =>
                {
                    keyWord = this.Xaml_Search_TabName.Text.Trim().ToLower();
                }
                ));
            return keyWord;
        }

        #endregion


        #region 左侧列表UI操作

        /// <summary>
        /// 双击表名：根据字段搜索条件过滤并显示当前选中表的数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_DataTableList_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Xaml_DataTableList.SelectedItem != null)
            {
                Ui_SetDataGrid(SearchFields(Ui_GetTableListSelected(), Ui_GetFieldSearchModeKeyWord(), Ui_GetFieldSearchMode()));
            }
        }

        /// <summary>
        /// 表菜单：搜索选中表中的数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchDatas_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_DataTableList.SelectedItem != null)
            {
                Ui_SetDataGrid(SearchFields(Ui_GetTableListSelected(), Ui_GetFieldSearchModeKeyWord(), Ui_GetFieldSearchMode()));
            }
        }

        /// <summary>
        /// 表菜单：显示选中表的所有数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_SearchAllData_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_DataTableList.SelectedItem != null)
            {
                Ui_SetDataGrid(SearchFields(Ui_GetTableListSelected(), "", FieldSearchMode.None));
            }
        }

        /// <summary>
        /// 表菜单：显示表结构
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_SchemaView_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_DataTableList.SelectedItem != null)
            {
                Ui_DisplaySchema(Ui_GetTableListSelected().TableName);
            }
        }

        /// <summary>
        /// 表菜单：复制表名
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_CopyTableName_Click(object sender, RoutedEventArgs e)
        {
            if (this.Xaml_DataTableList.SelectedItem != null)
            {
                Clipboard.SetText(Ui_GetTableListSelected().TableName);
            }
        }


        /// <summary>
        /// 设置数据表的备注信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_SetRemark_Click(object sender, RoutedEventArgs e)
        {
            if (this.Xaml_DataTableList.SelectedItem != null)
            {
                var tab = Ui_GetTableListSelected();
                SetRemark sr = new SetRemark() { Title = tab.TableName };
                sr.Xaml_InputRemark.Text = tab.Remark;
                sr.ShowDialog();
                if (sr.Save)
                {
                    var newRemark = sr.Xaml_InputRemark.Text.Trim();
                    if (newRemark == tab.Remark)
                        return;
                    else
                    {
                        try
                        {
                            DbGet.SP_SetExtendedProperty(newRemark, tab.TableName);
                            tab.Remark = newRemark;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("修改失败！" + ex.Message);
                        }
                    }
                }
            }
        }

        #endregion

        #region 搜索字段

        /// <summary>
        /// 列值搜索框按键事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Ui_Search();
            }
        }

        /// <summary>
        /// 点击搜索按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            Ui_Search();
        }

        #endregion


        /// <summary>
        /// 表过滤
        /// </summary>
        async void Ui_Search()
        {
            try
            {
                if (SetWait(true))
                {
                    await Task.Run(() =>
                    {
                        var tabs = SearchTables(Ui_GetTableSearchKeyWord(), Ui_GetTableSearchMode());
                        tabs = SearchTableField(tabs, Ui_GetFieldSearchModeKeyWord(), Ui_GetFieldSearchMode());
                        Ui_ClearDataGrid();
                        Ui_DisplayTableList(tabs);
                        MessageBox.Show("包含关键字的数据表有：" + tabs.Count + "张");
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SetWait(false);
            }
        }

        #region 进度条

        /// <summary>
        /// 指示是否在等待
        /// </summary>
        bool _isOnWait
        {
            get { return this.Xaml_OnLoad.IsIndeterminate; }
            set
            {
                this.Xaml_OnLoad.IsIndeterminate = value;
            }
        }

        /// <summary>
        /// 获取是否处于等待状态
        /// </summary>
        /// <returns></returns>
        bool IsOnWait()
        {
            if (_isOnWait)
            {
                MessageBox.Show("正在进行前一个操作，请稍后再进行其它操作！");
            }
            return _isOnWait;
        }

        /// <summary>
        /// 设置等待状态
        /// </summary>
        /// <param name="wait">状态</param>
        /// <returns>指示是否操作成功</returns>
        bool SetWait(bool wait)
        {
            if (wait && IsOnWait())
                return false;
            else
            {
                _isOnWait = wait;
                return true;
            }
        }

        #endregion

        /// <summary>
        /// 初始化
        /// </summary>
        async void Init()
        {
            try
            {
                if (SetWait(true))
                {
                    //初始化-清空Table列表
                    Ui_ClearTableList();
                    //初始化-清空DataGrid
                    Ui_ClearDataGrid();
                    //初始化-获取所有Table
                    DbTables = await Db_GetTables();
                    //初始化-显示所有Table
                    Ui_DisplayTableList(DbTables);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SetWait(false);
            }
        }

        #region 数据列表操作

        /// <summary>
        /// 显示数据表列表
        /// </summary>
        /// <param name="tabList"></param>
        void Ui_DisplayTableList(List<TabBasicInfo> tabList)
        {
            //显示数据列表-情况Table列表
            Ui_ClearTableList();
            Dispatcher.Invoke(new Action(() =>
            {
                this.Xaml_DataTableList.ItemsSource = tabList; ;
            }));
        }

        /// <summary>
        /// 清空表选择列表显示
        /// </summary>
        void Ui_ClearTableList()
        {
            Dispatcher.Invoke(new Action(() =>
            {
                this.Xaml_DataTableList.ItemsSource = "";
            }));
        }

        /// <summary>
        /// 清空DataGrid中的数据
        /// </summary>
        void Ui_ClearDataGrid()
        {
            Dispatcher.Invoke(new Action(() =>
            {
                Xaml_TableDatas.DataContext = "";
            }));
        }

        #endregion

        /// <summary>
        /// 获取当前选中的表名
        /// </summary>
        /// <returns></returns>
        TabBasicInfo Ui_GetTableListSelected()
        {
            TabBasicInfo selectedItem = null;
            Dispatcher.Invoke(new Action(() =>
            {
                selectedItem = Xaml_DataTableList.SelectedItem as TabBasicInfo;
            }));
            return selectedItem;
        }

        /// <summary>
        /// 将DataTable显示到DataGrid上
        /// </summary>
        /// <param name="dt"></param>
        void Ui_SetDataGrid(DataTable dt)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                Xaml_TableDatas.DataContext = dt;
            }));
        }

        #region 表结构操作
        /// <summary>
        /// 显示指定名称的数据表的表结构
        /// </summary>
        /// <param name="tabName"></param>
        async void Ui_DisplaySchema(string tabName)
        {
            if (SetWait(true))
            {
                try
                {
                    DataTable tab = await Db_GetTableSchema(tabName);
                    tab.TableName = tabName;
                    Ui_SetDataGrid(tab);
                    this.Xaml_SaveFieldRemark.Visibility = Visibility.Visible;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    SetWait(false);
                }
            }
        }

        /// <summary>
        /// 更新列描述信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveFieldsRemark_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SetWait(true))
                {
                    var dt = this.Xaml_TableDatas.DataContext as DataTable; //获取修改后的内容
                    var tabName = dt.TableName;  //获取表名
                    if (MessageBox.Show("你正企图更新表：" + tabName + " 的字段描述信息，确定？", "操作确认", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Db_UpdateFieldsRemark(dt, tabName);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SetWait(false);
            }
        }

        #endregion

        #region 页面顶部菜单

        /// <summary>
        /// 重新加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reload_Click(object sender, RoutedEventArgs e)
        {
            Init();
        }

        /// <summary>
        /// 设置数据库连接方式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeConn_Click(object sender, RoutedEventArgs e)
        {
            SetConn setconn = new SetConn();
            setconn.ShowDialog();
        }

        #endregion

        #region 窗口控制
        private void Window_Close(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Max(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState == System.Windows.WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void Window_Min(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Window_Move(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == System.Windows.WindowState.Maximized)
                {
                    WindowState = System.Windows.WindowState.Normal;
                    return;
                }
                DragMove();
            }
        }

        private void WindowsTitle_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState == System.Windows.WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }
        #endregion


        /// <summary>
        /// 创建C#模型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateModel_For_CSharp_Click(object sender, RoutedEventArgs e)
        {
            var tabinfo = Ui_GetTableListSelected();
            if (tabinfo != null)
            {
                var fieldsInfo = DbGet.GetTableDetails(tabinfo.TableName);
                var dialog = new Microsoft.Win32.SaveFileDialog();
                dialog.FileName = tabinfo.TableName + ".cs";
                dialog.Filter = "C#类文件|*.cs";
                if (dialog.ShowDialog()==true)
                {
                    var doc = System.IO.File.CreateText(dialog.FileName);
                    doc.WriteLine("using System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\nusing System.Threading.Tasks;\r\n");
                    doc.WriteLine("namespace Models");
                    doc.WriteLine("{");
                    doc.WriteLine("\tpublic class " + tabinfo.TableName);
                    doc.WriteLine("\t{");
                    foreach (DataRow item in fieldsInfo.Rows)
                    {
                        try
                        {
                            doc.WriteLine("\t\t /// <summary>");
                            if (item["自增长"] != null && (bool)item["自增长"] == true)
                            {
                                doc.WriteLine("\t\t /// " + item["中文名"]+"（自增长列）");
                            }
                            else
                            {
                                doc.WriteLine("\t\t /// " + item["中文名"]);
                            }
                            doc.WriteLine("\t\t /// <summary>");
                            doc.Write("\t\t public string ");
                            doc.Write(item["列名"]);
                            doc.Write(" { get; set; }");
                            //doc.WriteLine(" //" +item["中文名"]);
                            doc.Write("\r\n\r\n");
                        }
                        catch
                        {
                        }
                    }
                    doc.WriteLine("\t}");
                    doc.WriteLine("}");
                    doc.Flush();
                    doc.Close();
                }
            }
        }
    }

    /// <summary>
    /// 表搜索模式
    /// </summary>
    public enum TableSearchMode
    {
        /// <summary>
        /// 无
        /// </summary>
        None = 0,

        /// <summary>
        /// 表名包含
        /// </summary>
        TableNameHas = 1,

        /// <summary>
        /// 表名对比
        /// </summary>
        TableNameEquals = 2,
    }

    /// <summary>
    /// 字段搜索模式
    /// </summary>
    public enum FieldSearchMode
    {
        /// <summary>
        /// 无
        /// </summary>
        None = 0 ,

        /// <summary>
        /// 字段名包含
        /// </summary>
        FieldNameHas = 1 ,

        /// <summary>
        /// 字段名对比
        /// </summary>
        FieldNameEquals = 2 ,
        
        /// <summary>
        /// 字段值包含
        /// </summary>
        FieldValueHas = 3 ,
        
        /// <summary>
        /// 字段值等于
        /// </summary>
        FieldValueEquals = 4
    }

    /// <summary>
    /// 数据表基本信息
    /// </summary>
    public class TabBasicInfo : INotifyPropertyChanged
    {
        string tableName;
        public string TableName
        {
            get { return tableName; }
            set
            {
                tableName = value;
                try
                {
                    var cName = DbGet.GetTableReamarkName(value);
                    this.Remark = string.IsNullOrWhiteSpace(cName) ? tableName : cName;
                }
                catch { }
            }
        }
        string remark;

        public string Remark
        {
            get { return remark; }
            set
            {
                remark = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Remark"));
            }
        }

        List<FiledBasicInfo> fields = new List<FiledBasicInfo>();
        
        /// <summary>
        /// 字段信息
        /// </summary>
        public List<FiledBasicInfo> Fields
        {
            get { return fields; }
            set { fields = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }


    /// <summary>
    /// 数据字段基本信息
    /// </summary>
    public class FiledBasicInfo : INotifyPropertyChanged
    {
        string fieldName;
        string remark;
        string dataType;
        string defaultValue;

        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; }
        }

        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }


        /// <summary>
        /// 数据类型
        /// </summary>
        public string DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }


        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultValue
        {
            get { return defaultValue; }
            set { defaultValue = value; }
        }

        bool is_nullable;

        /// <summary>
        /// 是否可空
        /// </summary>
        public bool Is_nullable
        {
            get { return is_nullable; }
            set { is_nullable = value; }
        }

        bool is_identity;
        /// <summary>
        /// 是否是自增长
        /// </summary>
        public bool Is_identity
        {
            get { return is_identity; }
            set { is_identity = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
