﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace 数据库查询工具
{
    /// <summary>
    /// SetConn.xaml 的交互逻辑
    /// </summary>
    public partial class SetConn : Window
    {
        public SetConn()
        {
            InitializeComponent();
            this.Xaml_ConnectString.Text = System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ConnectionString; 
        }

        public bool isSave = false;
        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            save();
        }

        void save()
        {
            try
            {
                var connText = this.Xaml_ConnectString.Text;
                System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connText);
                conn.Open();
                isSave = true;
                DbGet.conn = conn;
                if (SetAsDefault.IsChecked == true)
                {
                    var configFile = "数据库定义.exe.config";
                    var doc = XDocument.Load(configFile);
                    XElement xe = doc.Root;
                    var connectionString = xe.Elements("connectionStrings");
                    if (connectionString.Count() > 0)
                    {
                        var nodes = connectionString.First();
                        var defaultConn = nodes.Elements("add").Where(item => item.Attribute("name").Value == "Default");
                        if (defaultConn.Count() > 0)
                        {
                            defaultConn.First().SetAttributeValue("connectionString", connText);
                        }
                    }
                    doc.Save(configFile);
                }
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("设置连接失败！" + ex.Message);
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void Xaml_ConnectString_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                save();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// 双击项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnItemClick(object sender, MouseButtonEventArgs e)
        {
            var item = ((sender as ListBoxItem).Tag).ToString();
            var connText = System.Configuration.ConfigurationManager.ConnectionStrings[item].ConnectionString; 
            try
            {
                System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connText);
                conn.Open();
                isSave = true;
                DbGet.conn = conn;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("设置连接失败！" + ex.Message);
            }
        }
    }
}
