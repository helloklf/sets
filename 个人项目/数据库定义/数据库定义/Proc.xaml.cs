﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace 数据库查询工具
{
    /// <summary>
    /// Proc.xaml 的交互逻辑
    /// </summary>
    public partial class Proc : Window
    {
        public Proc()
        {
            InitializeComponent();
        }

        Dictionary<string, string> procsContetentList = new Dictionary<string, string>();
        string selectItemContent = "";
        string selectItemName = "";

        /// <summary>
        /// 页面加载完成后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="refreshList"></param>
        void search(bool refreshList = true)
        {
            Xaml_OnLoad.IsIndeterminate = true;
            TextRange textRanage = new TextRange(Xaml_SPDetails2.Document.ContentStart, Xaml_SPDetails2.Document.ContentEnd);
            textRanage.Text = "";
            var keyword = Xaml_SearchProcContent.Text.ToLower();
            if (refreshList)
            {
                var newProcList = procsContetentList.Where(item => item.Key.ToLower().Contains(Xaml_SearchProcName.Text.Trim().ToLower()));
                List<string> newList = new List<string>();
                foreach (var item in newProcList)
                {
                    if ((item.Value ?? "").ToLower().Contains(keyword))
                    {
                        newList.Add(item.Key);
                    }
                }
                this.Xaml_StoreProducts.ItemsSource = null;
                this.Xaml_StoreProducts.ItemsSource = newList;
            }

            textRanage.Text = selectItemContent;
            TextPointer position = Xaml_SPDetails2.Document.ContentStart;
            while (position != null)
            {
                //向前搜索,需要内容为Text       
                if (position.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
                    //拿出Run的Text        
                    string text = position.GetTextInRun(LogicalDirection.Forward);
                    //可能包含多个keyword,做遍历查找           
                    int index2 = 0;
                    index2 = text.ToLower().IndexOf(keyword, 0);
                    if (index2 != -1)
                    {
                        TextPointer start = position.GetPositionAtOffset(index2);
                        TextPointer end = start.GetPositionAtOffset(keyword.Length);
                        position = selecta(Colors.Red, Xaml_SPDetails2, keyword.Length, start, end);
                    }
                }
                //文字指针向前偏移   
                position = position.GetNextContextPosition(LogicalDirection.Forward);

            }
            Xaml_OnLoad.IsIndeterminate = false;
        }

        public TextPointer selecta(Color l, RichTextBox richTextBox1, int selectLength, TextPointer tpStart, TextPointer tpEnd)
        {
            TextRange range = richTextBox1.Selection;
            range.Select(tpStart, tpEnd);
            //高亮选择         

            range.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(l));
            range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);

            return tpEnd.GetNextContextPosition(LogicalDirection.Forward);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        bool isOnload = false;

        /// <summary>
        /// 刷新数据
        /// </summary>
        async void Reload()
        {
            isOnload = true;
            Xaml_OnLoad.IsIndeterminate = true;
            TextRange textRanage = new TextRange(Xaml_SPDetails2.Document.ContentStart, Xaml_SPDetails2.Document.ContentEnd);
            textRanage.Text = "";

            var procs = new List<string>();
            await Task.Run(() =>
            {
                procs = DbGet.GetProcedures();
            });
            Xaml_OnLoad.IsIndeterminate = false;
            Xaml_OnLoad.Maximum = procs.Count;
            Xaml_OnLoad.Value = 0;
            procs = procs.OrderBy(item => item).ToList();
            int index = 0;
            procsContetentList.Clear();
            foreach (var item in procs)
            {
                await Task.Run(() =>
                {
                    procsContetentList.Add(item, DbGet.GetProceductDetails(item));
                    index++;
                });
                Xaml_OnLoad.Value = index;
            }
            this.Xaml_StoreProducts.ItemsSource = procs;
            search();
            isOnload = false;
        }

        /// <summary>
        /// 保存更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            TextRange textRanage = new TextRange(Xaml_SPDetails2.Document.ContentStart, Xaml_SPDetails2.Document.ContentEnd);
            if (textRanage.Text.Trim().Length < 1)
                return;
            if (MessageBox.Show("此操作无法被撤销，更新存储过程可能会造成一些问题，确认进行此操作？", "请确认", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                if (!Regex.IsMatch(textRanage.Text, "CREATE\\s*PROC", RegexOptions.IgnoreCase))
                {
                    
                    MessageBox.Show("内容无效！");
                }
                else
                {
                    var UpdateSql = Regex.Replace(textRanage.Text, "CREATE\\s*PROC", "ALTER PROC", RegexOptions.IgnoreCase);
                    textRanage.Text.IndexOf("", StringComparison.OrdinalIgnoreCase);
                    //写入备份文件
                    File.WriteAllText(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-hs") + "_log.sql", selectItemContent);
                    try
                    {
                        DbGet.UpdateProc(UpdateSql);
                        Reload();
                    }
                    catch
                    {
                        Window w = new Window();
                        w.Title = selectItemName + "：更新失败，请手动更新！";
                        w.Content = new TextBox() { TextWrapping = TextWrapping.Wrap, Text = UpdateSql };
                    }
                }
            }
        }

        /// <summary>
        /// 备份全部
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BackupAll_Click(object sender, RoutedEventArgs e)
        {
            if(isOnload==true)
            {
                MessageBox.Show("现在正在读取数据，请等待数据读取完毕后再执行此操作！");
                return;
            }
            System.Windows.Forms.FolderBrowserDialog sfd = new System.Windows.Forms.FolderBrowserDialog();
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Xaml_OnLoad.IsIndeterminate = true;
                var folder = sfd.SelectedPath;
                await Task.Run(() =>
                {
                    foreach (var item in procsContetentList)
                    {
                        File.WriteAllText(folder+item.Key+".sql", item.Value);
                    }
                });
                Xaml_OnLoad.IsIndeterminate = false;
            }
        }

        /// <summary>
        /// 更改数据库连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeConn_Click(object sender, RoutedEventArgs e)
        {
            SetConn conn = new SetConn();
            conn.ShowDialog();
        }

        /// <summary>
        /// 鼠标在列表中双击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Xaml_StoreProducts_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.Xaml_StoreProducts.SelectedItem != null)
            {
                Xaml_OnLoad.IsIndeterminate = true;
                var Text = "";
                var procName = Xaml_StoreProducts.SelectedItem.ToString();
                selectItemName = procName;
                await Task.Run(() =>
                {
                    Text = DbGet.GetProceductDetails(procName);
                });
                var textRanage = new TextRange(Xaml_SPDetails2.Document.ContentStart, Xaml_SPDetails2.Document.ContentEnd);
                textRanage.Text = Text;
                selectItemContent = Text; //选中的存储过程内容
                Xaml_OnLoad.IsIndeterminate = false;
                search(false);
            }            
        }

        /// <summary>
        /// 搜索框输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                search();
            }
        }

        private void Window_Close(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Max(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState == System.Windows.WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void Window_Min(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Window_Move(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == System.Windows.WindowState.Maximized)
                {
                    WindowState = System.Windows.WindowState.Normal;
                    return;
                }
                DragMove();
            }
        }

        private void WindowsTitle_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState == System.Windows.WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }
    }
}
