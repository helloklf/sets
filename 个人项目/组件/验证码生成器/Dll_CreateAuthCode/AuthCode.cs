﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dll_CreateAuthCode
{
    public class AuthCode
    {
        public static Image Englis(int length=4)
        {
            System.Drawing.Image img = global::Dll_CreateAuthCode.Properties.Resources._BG;
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(img);

            FontFamily[] fonts = System.Drawing.FontFamily.Families;
            Random r = new Random();
            string  fontname=fonts[r.Next(0, fonts.Length)].Name;

            g.DrawString(CreateEnglish(length), new System.Drawing.Font(fontname, 30), new System.Drawing.SolidBrush(System.Drawing.Color.Red), new System.Drawing.PointF(10, 10));
            
            img.Save(Guid.NewGuid().ToString()+".jpg",System.Drawing.Imaging.ImageFormat.Jpeg);
            //CreateEnglish();
            return img;
        }

        /// <summary>
        /// 创建四位英文字符
        /// </summary>
        /// <returns></returns>
        static string CreateEnglish(int length=5)
        {
            Random rand = new Random();
            rand.Next(67,97);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                if (new Random().Next(0,2) == 0)
                {
                    char c = Convert.ToChar(rand.Next(97, 123));
                    sb.Append(c);
                }
                else
                {
                    char c = Convert.ToChar(rand.Next(65, 91));
                    sb.Append(c);
                }
            }

            return sb.ToString() ;
        }
    }
}
