﻿/*    var dialog = DialogObj('你需要确认此操作', '沙发沙发阿斯顿啊法士大夫阿斯顿发撒盛大', function (r) {
        alert(r);
        dialog = null;
    });
dialog.ShowDialog(); */

/*var dialog = DialogObj( '标题', '内容文本', function (result) {  alert(result); dialog = null; }); //初始化窗口
dialog.ShowDialog(); //打开窗口*/
function DialogObj(titleText, bodyText, postBack) {
    function randomChar(l) { //随机字符串创建器
        var x = "123456789poiuytrewqasdfghjklmnbvcxzQWERTYUIPLKJHGFDSAZXCVBNM";
        var tmp = "";
        for (var i = 0; i < l; i++) {
            tmp += x.charAt(Math.ceil(Math.random() * 100000000) % x.length);
        }
        return tmp;
    }
    var RootPanelID = randomChar(20);//随机创建一个id

    var obj = new Object();
    obj.DiaGID = function (id) { return document.getElementById(id); }
    obj.DiaGCss = function (classname) { return document.getElementsByClassName(classname); }

    obj.ShowDialog = function () {
        document.write(obj._sys_CodeData)
        obj.DialogState.Open();
    };
    //菜单操作功能
    obj.Menu =
    {
        Copy: function (panel)//复制
        {
            window.clipboardData.setData('text', obj.DiaGCss('DialogArticle')[0].innerText);
        },
        Setting: function () //设置页
        {
            obj.Menu._sysPageToogle('DialogSetting');
        },
        About: function () //关于页
        {
            obj.Menu._sysPageToogle('DialogAbout');
        },
        Message: function () //返回消息显示页面
        {
            obj.Menu._sysPageToogle('DialogArticle');
        },
        _sysPageToogle: function (blockClass) //页面切换
        {
            obj.DiaGCss('DialogSetting')[0].style.display = 'none';
            obj.DiaGCss('DialogAbout')[0].style.display = 'none';
            obj.DiaGCss('DialogArticle')[0].style.display = 'none';
            obj.DiaGCss(blockClass)[0].style.display = 'block';
        }
    };
    //窗口移动功能
    obj.Move =
    {
        MoveDialog: function () {
            if (obj.Move._IsCanMove()) {
                var box = obj.GetDialogBox();
                box.style.left = (event.clientX - obj.Move._sys_dialogStartOffsetX) + 'px';
                box.style.top = (event.clientY - obj.Move._sys_dialogStartOffsetY) + 'px';
            }
            return obj.Move._sys_cancelBubble(event);
        },
        MoveIcon_Drag: function () {
            obj.Move._sys_dialogStartOffsetX = obj.GetDialogBox().clientWidth - 52;
            obj.Move._sys_dialogStartOffsetY = 15;
            obj.Move._ToogleMove();
        }, //点击拖动按钮（切换拖动）
        Title_Drag: function () {
            obj.Move._sys_dialogStartOffsetX = 150;
            obj.Move._sys_dialogStartOffsetY = 20;
            obj.Move._EnableMove();
        }, //点击标题栏（打开拖动）
        _ToogleMove: function () {
            obj.Move._sys_canMove = !obj.Move._sys_canMove;
        }, //切换移动
        _EnableMove: function () {
            obj.Move._sys_canMove = true;
        }, //启用移动
        _DisableMove: function () {
            obj.Move._sys_canMove = false;
        },//禁用移动
        _IsCanMove: function () //判断是否允许拖动窗口
        {
            return obj._sys_AutoMoveSelectBox().checked == true || obj.Move._sys_canMove;
        }, //获取窗口是否可以拖动
        _sys_canMove: false, //指定是否可以拖动
        _sys_dialogStartOffsetX: 0, //窗口移动起始偏移量
        _sys_dialogStartOffsetY: 0, //窗口移动起始偏移量
        _sys_cancelBubble: function (e) //取消系统默认事件和事件冒泡
        {
            //如果提供了事件对象，则这是一个非IE浏览器
            if (e && e.preventDefault) {
                //阻止默认浏览器动作(W3C)
                e.preventDefault();
            }
            else {
                //IE中阻止函数器默认动作的方式
                e.returnValue = false;
            }
            if (e.cancelBubble)
                e.cancelBubble = true;
            return false;
        }
    };
    //窗口状态操作
    obj.DialogState =
    {
        DialogYes: function () { obj.DialogResult = true; obj.DialogState.Close(); },
        DialogNo: function () { obj.DialogResult = false; obj.DialogState.Close(); },
        Open: function () {
            obj.GetDialogWindow().onmousemove = obj.Move.MoveDialog;
            obj.DiaGCss('menuDialogAbout')[0].onclick = obj.Menu.About;
            obj.DiaGCss('menuDialogSetting')[0].onclick = obj.Menu.Setting;
            obj.DiaGCss('menuMessageCopy')[0].onclick = obj.Menu.Copy;

            var header = obj.DiaGCss('header')[0];
            header.onmousedown = obj.Move.Title_Drag;
            header.onmouseup = obj.Move._DisableMove;
            header.onclick = obj.Menu.Message;
            if (titleText) header.innerText = titleText;

            if (bodyText) obj.DiaGCss('DialogArticle')[0].innerText = bodyText;

            var MoveIcon = obj.DiaGCss('DialogBoxMove')[0];
            MoveIcon.onmousedown = obj.Move.MoveIcon_Drag;

            //设置热键
            window.addEventListener('keydown', function () {
                if (event.ctrlKey) {
                    if (window.event.keyCode == 89) obj.DialogState.DialogYes; //Ctrl+Y
                    if (window.event.keyCode == 78) obj.DialogState.DialogNo; //Ctrl+N
                }
            });
            //关闭按钮事件
            obj.DiaGCss('DialogBoxClose')[0].onclick = obj.DialogState.DialogNo;

            obj.DiaGCss('DialogBoxNo')[0].onclick = obj.DialogState.DialogNo; //否
            obj.DiaGCss('DialogBoxYes')[0].onclick = obj.DialogState.DialogYes; //是

            //设置时间显示
            obj.Other._sys_dt = setInterval(function () {
                obj.DiaGCss('DialogDateTime')[0].innerText = new Date().toLocaleString();
            }, 1000);


            obj.Other._sys_AutoPostion();
            obj.GetDialogBox().style.left = (window.innerWidth - obj.GetDialogBox().clientWidth) * 50.0 / window.innerWidth + '%';
            obj.GetDialogBox().style.top = (window.innerHeight - obj.GetDialogBox().clientHeight) * 50.0 / window.innerHeight + '%';

            window.addEventListener('resize', obj.Other._sys_AutoPostion);
        },
        Close: function () {
            if (postBack) postBack(obj.DialogResult); //执行回送方法
            window.removeEventListener('resize', obj.Other._sys_AutoPostion);
            clearInterval(obj.Other._sys_dt);
            obj.GetDialogWindow().removeNode(true);
            obj = null;
        }
    };
    obj.Other = {
        _sys_AutoPostion: function () {
            obj.GetDialogBox().style.left = (window.innerWidth - obj.GetDialogBox().clientWidth) * 50.0 / window.innerWidth + '%';
            obj.GetDialogBox().style.top = (window.innerHeight - obj.GetDialogBox().clientHeight) * 50.0 / window.innerHeight + '%';
        },
        _sys_dt: '定时器'
    }
    obj.GetDialogWindow = function () { return obj.DiaGID(RootPanelID); };
    obj.GetDialogBox = function () { return obj.DiaGID(RootPanelID + '_Box'); };
    obj._sys_AutoMoveSelectBox = function () { return obj.DiaGID(RootPanelID + '_Move'); };
    obj._sys_CodeData = "<div class='DialogBoxBackGround' id='" + RootPanelID + "'><link rel='stylesheet' href='http://fontawesome.io/assets/font-awesome/css/font-awesome.css' /><div class='DialogDateTime'></div><div class='DialogBox' id='" + RootPanelID + "_Box' oncontextmenu='return false' ondragstart='return false' onselectstart='return false' onselect='document.selection.empty()'><dropdown class='DialogBoxdropdown'><input id='toggle1' type='checkbox' /><label for='toggle1' class='animate'>菜单<i class='fa fa-bars float-right'></i></label><ul class='animate'><li class='animate menuDialogAbout'>关于<i class='fa fa-lightbulb-o float-right'></i></li><li class='animate menuDialogSetting'>设置<i class='fa fa-cog float-right fa-spin'></i></li><li class='animate menuMessageCopy'>复制<i class='fa fa-copy float-right'></i></li></ul></dropdown><label class='header' id='header'>消息无标题</label><label class='fa fa-close float-right DialogBoxTopBtn DialogBoxClose'></label><span><input type='checkbox' hidden='hidden' style='display: none;' id='" + RootPanelID + "_Move' /><label for='" + RootPanelID + "_Move' class='fa fa-arrows float-right DialogBoxTopBtn DialogBoxMove'></label></span><hr class='DialogBoxHr DialogBoxHrTop' /><article class='DialogArticle'>未提供消息内容...</article><div class='DialogAbout' title='点击小窗口标题回到消息显示页'><div class='DialogAboutline'>我们是谁：蓝龙人力资源公司</div><div class='DialogAboutline'>公司地址：珠海前山<a href='http://map.qq.com/?what=%E8%93%9D%E9%BE%99%E4%BA%BA%E5%8A%9B%E8%B5%84%E6%BA%90&city=%E4%B8%AD%E5%9B%BD&type=poi&ref=tengxun'>【地图】</a></div><div class='DialogAboutline'>联系方式：</div></div><div class='DialogSetting' title='点击小窗口标题回到消息显示页' ><div class='DialogSettingLine'><h4>使用快捷键<label style='float: right; font-size: 1.2em' class='fa fa-toggle-on'></label></h4><div style='margin-top: 10px;'>                        Ctrl+Y=确认；Ctrl+N=取消</div></div><div class='DialogSettingLine'></div></div><hr class='DialogBoxHr' /><div style='padding: 5px 0;'><label class='DialogBoxWatermark'>OmArea.Com</label><i class='fa fa-close DialogBoxNo' accesskey='n'></i><i class='fa fa-check DialogBoxYes' accesskey='y'></i><br style='display: block; clear: both;' /></div></div></div>";
    //返回值（true / false）
    obj.DialogResult = false;
    return obj;
}