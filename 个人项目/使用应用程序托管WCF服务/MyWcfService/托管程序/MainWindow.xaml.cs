﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 托管程序
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        System.ServiceModel.ServiceHost host = null;

        /// <summary>
        /// 启动托管服务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Host_Start_Click(object sender, RoutedEventArgs e)
        {
            host = new System.ServiceModel.ServiceHost(typeof(MyWcfService.Service1));
            host.Open();//启动

            MessageBox.Show(" 托管状态：" + host.State.ToString() );
        }


        /// <summary>
        /// 关闭托管服务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Host_Stop_Click(object sender, RoutedEventArgs e)
        {
            if (host != null && host.State == System.ServiceModel.CommunicationState.Opened)//判断对象是否已经创建且启动
                host.Close();//停止服务

            MessageBox.Show(" 托管状态：" + host.State.ToString());
        }
    }
}
