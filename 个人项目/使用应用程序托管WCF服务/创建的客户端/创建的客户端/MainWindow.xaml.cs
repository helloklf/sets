﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 创建的客户端
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 点击发起一个请求
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WCFRequest_Click(object sender, RoutedEventArgs e)
        {
            MyServicesName.Service1Client sclient = new MyServicesName.Service1Client();
            string content =  sclient.ShowTime();//添加服务引用和发起请求前一定要先启动托管程序

            MessageBox.Show("获取到的服务器时间为："+content);
            this.ContentList.Items.Add( content );
        }
    }
}
