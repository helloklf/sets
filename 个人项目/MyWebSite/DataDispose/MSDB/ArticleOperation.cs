using Dll_DataBaseSelect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataOperate
{
    public class ArticleOperation : BaseClass
    {
        public ArticleOperation(SqlConnection conn) : base(conn) { }

        /// <summary>
        /// 获取文章
        /// </summary>
        /// <returns></returns>
        public List<DBModel.ArticleModel> GetArticle(long StartIndex)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from ( select ROW_NUMBER() over (Order by  [DateTime] desc) as OrderID,* from Article where PDataIndex is null ) as Article where OrderID between @startIndex and @startIndex+10", new SqlParameter[] { new SqlParameter("@startIndex", StartIndex) }))
                {
                    return ReaderAricle(dr);
                }
            }
        }

        /// <summary>
        /// 获取文章评论
        /// </summary>
        /// <returns></returns>
        public List<DBModel.ArticleModel> GetArticleComments(string articleId)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from Article where PDataIndex = @articleId Order by  [DateTime] desc", new SqlParameter[] { new SqlParameter("@articleId", articleId) }))
                {
                    return ReaderAricle(dr);
                }
            }
        }


        /// <summary>
        /// 获取文章评论
        /// </summary>
        /// <returns></returns>
        public List<DBModel.ArticleModel> GetWorksComments(string worksId)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from Works where PDataIndex = @articleId Order by  [DateTime] desc", new SqlParameter[] { new SqlParameter("@articleId", worksId) }))
                {
                    return ReaderAricle(dr);
                }
            }
        }

        /// <summary>
        /// 获取某人从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public List<DBModel.ArticleModel> GetArticle_OneUser(long StartIndex, string IndexID)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from ( select ROW_NUMBER() over (Order by  [DateTime] desc) as OrderID,* from Article where PDataIndex is null and IndexID=@IndexID) as Article where OrderID between @startIndex and @startIndex+10", 
                new SqlParameter[] 
                {
                    SqlDBTools.Parameter("@startIndex",StartIndex),
                    SqlDBTools.Parameter("@IndexID",IndexID),
                }))
                {
                    return ReaderAricle(dr);
                }
            }
        }
        /// <summary>
        /// 获取指定索引号的文章内容
        /// </summary>
        /// <param name="DataIndex">文章索引号</param>
        /// <returns></returns>
        public List<DBModel.ArticleModel> GetArticle(string DataIndex)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from Article where DataIndex =@DataIndex", new SqlParameter[] { new SqlParameter("@DataIndex", DataIndex) }))
                {
                    return ReaderAricle(dr);
                }
            }
        }

        /// <summary>
        /// 更新文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public int UpdateArticle(DBModel.ArticleModel article)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                return dbh.ExecuteNonQuery("update Article set DocTitle=@DocTitle,DocData=@DocData where DataIndex=@DataIndex", new SqlParameter[]{
                    SqlDBTools.Parameter("@DataIndex",article.DataIndex,SqlDbType.VarChar),
                    SqlDBTools.Parameter("@DocTitle",article.DocTitle,SqlDbType.NVarChar),
                    SqlDBTools.Parameter("@DocData",article.DocData,SqlDbType.NVarChar)
                });
            }
        }

        /// <summary>
        /// 添加文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public int InsertArticle(DBModel.ArticleModel article)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                try
                {
                    return dbh.ExecuteNonQuery("insert into Article(IndexID,DocTitle,DocData) values(@IndexID,@DocTitle,@DocData)", new SqlParameter[]{
                    SqlDBTools.Parameter("@IndexID",article.IndexID,SqlDbType.VarChar),
                    SqlDBTools.Parameter("@DocTitle",article.DocTitle,SqlDbType.NVarChar),
                    SqlDBTools.Parameter("@DocData",article.DocData,SqlDbType.NVarChar)
                });
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 删除文章
        /// </summary>
        /// <param name="article">文章信息，其中必须内容为DataIndex</param>
        /// <returns></returns>
        public int DeleteArticle(DBModel.ArticleModel article)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                try
                {
                    return dbh.ExecuteNonQuery("delete Article where DataIndex = @DataIndex and IndexID = @IndexID", new SqlParameter[]{
                    SqlDBTools.Parameter("@IndexID",article.IndexID,SqlDbType.VarChar),
                    SqlDBTools.Parameter("@DataIndex",article.DataIndex,SqlDbType.VarChar),
                });
                }
                catch
                {
                    return 0;
                }
            }
        }

        List<DBModel.ArticleModel> ReaderAricle(DbDataReader dr)
        {
            var list = new List<DBModel.ArticleModel>();
            while (dr.Read())
            {
                list.Add(new DBModel.ArticleModel()
                {
                    DataIndex = dr["DataIndex"] as string,
                    DateTime = (DateTime?)dr["DateTime"],
                    DocData = dr["DocData"] as string,
                    DocTitle = dr["DocTitle"] as string,
                    IndexID = dr["IndexID"] as string,
                    IsValid = dr["IsValid"] as string,
                    PDataIndex = dr["PDataIndex"] as string
                });
            }
            return list;
        }


        /// <summary>
        /// 验证是否是本人文章
        /// </summary>
        /// <param name="DataIndex">文章编号</param>
        /// <param name="IndexID">用户名</param>
        /// <returns></returns>
        public bool AricleValid(string DataIndex, string IndexID)
        {
            return base.Valid("Article", "DataIndex", DataIndex, "IndexID", IndexID);
        }
    }
}