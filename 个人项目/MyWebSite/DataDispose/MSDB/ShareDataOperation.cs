﻿using DBModel;
using Dll_DataBaseSelect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOperate
{
    public class ShareDataOperation : BaseClass
    {
        public ShareDataOperation(SqlConnection conn) : base(conn) { }

        /// <summary>
        /// 查询共享资源
        /// </summary>
        /// <returns></returns>
        public List<ShareDataModel> GetShareData()
        {
            try
            {
                using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
                {
                    using (DbDataReader dr = dbh.ExecuteReader("select top 20 * from ShareData"))
                    {
                        List<ShareDataModel> sdml = ShareDataRead(dr);
                        return sdml;
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// 获得共享资源
        /// </summary>
        /// <param name="startindex">当前正在浏览的最后一条记录</param>
        /// <returns></returns>
        public List<ShareDataModel> GetShareData(long startindex)
        {
            try
            {
                using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
                {
                    SqlParameter[] sqlps = new SqlParameter[] { new SqlParameter("@startId", startindex) };
                    using (DbDataReader dr = dbh.ExecuteReader("select * from ( select ROW_NUMBER() over (order by  [datetime] desc) as OrderID,* from ShareData )  as ShareData where OrderID between  @startId and @startId+20", sqlps))
                    {
                        return ShareDataRead(dr);
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// 根据索引号获取相关的数据
        /// </summary>
        /// <param name="DataIndex"></param>
        /// <returns></returns>
        public List<ShareDataModel> GetShareDataByIndex(string DataIndex)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                SqlParameter[] sqlps = new SqlParameter[] { new SqlParameter("@DataIndex", DataIndex) };
                using (DbDataReader dr = dbh.ExecuteReader("select  * from ShareData where DataIndex = @DataIndex", sqlps))
                {
                    return ShareDataRead(dr);
                }
            }
        }

        /// <summary>
        /// 从DbDataReader中读取数据生成集合，适用于ShareData表
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public List<ShareDataModel> ShareDataRead(DbDataReader dr)
        {
            List<ShareDataModel> sdml = new List<ShareDataModel>();
            ShareDataModel sdm;
            while (dr.Read())
            {
                sdm = new ShareDataModel();
                sdm.DataIndex = dr["DataIndex"].ToString();
                sdm.PDataIndex = dr["PDataIndex"] as string;
                sdm.IndexID = dr["IndexID"] as string;
                sdm.SourceTitle = dr["SourceTitle"].ToString();
                sdm.DateTime = (DateTime)dr["DateTime"];
                sdm.DocData = dr["DocData"].ToString();
                sdm.SourceURL = dr["SourceURL"].ToString();
                sdml.Add(sdm);
            }
            return sdml;
        
        }

        /// <summary>
        /// 添加新的共享资源
        /// </summary>
        /// <param name="model">共享资源模型</param>
        /// <returns></returns>
        public int InsertShareData(ShareDataModel model)
        {
            using (DBHelper dbh = new SQLDBHelper(DBConn))
            {
                SqlParameter[] sqlp = new SqlParameter[] 
                {
                    new SqlParameter("@title",model.SourceTitle){ SqlDbType = SqlDbType.NVarChar},
                    new SqlParameter("@text",model.DocData){ SqlDbType = SqlDbType.NText},
                    new SqlParameter("@url",model.SourceURL){ SqlDbType = SqlDbType.NVarChar}
                };
                return dbh.ExecuteNonQuery("insert into ShareData(SourceTitle,DocData,SourceURL) values (@title,@text,@url)", sqlp);
            }
        }

        
        /// <summary>
        /// 修改共享的资源
        /// </summary>
        /// <param name="model">共享的资源模型</param>
        /// <returns></returns>
        public int EditShareData(ShareDataModel model)
        {
            using (DBHelper dbh = new SQLDBHelper(DBConn))
            {
                SqlParameter[] sqlp = new SqlParameter[] 
                {
                    new SqlParameter("@index",model.DataIndex),
                    new SqlParameter("@title",model.SourceTitle){ SqlDbType = SqlDbType.NVarChar},
                    new SqlParameter("@text",model.DocData){ SqlDbType = SqlDbType.NText},
                    new SqlParameter("@url",model.SourceURL){ SqlDbType = SqlDbType.NVarChar}
                };
                return dbh.ExecuteNonQuery("update ShareData set SourceTitle=@title,DocData =@text ,SourceURL=@url where DataIndex = @index", sqlp);
            }
        }

        /// <summary>
        /// 删除共享的资源
        /// </summary>
        /// <param name="dataIndex">共享资源编号</param>
        /// <returns></returns>
        public int DeleteShareData(string dataIndex)
        {
            
            using (DBHelper dbh = new SQLDBHelper(DBConn))
            {
                SqlParameter[] sqlp = new SqlParameter[] 
                {
                    new SqlParameter("@index",dataIndex)
                };
                return dbh.ExecuteNonQuery("delete ShareData where DataIndex=@index", sqlp);
            }
        }
    }
}
