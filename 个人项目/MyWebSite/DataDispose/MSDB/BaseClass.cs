﻿using Dll_DataBaseSelect;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOperate
{
    public class BaseClass
    {
        public BaseClass(SqlConnection conn) 
        {
            DBConn = conn;
        }
        protected SqlConnection DBConn;

        /// <summary>
        /// 验证某张表中某条数据的某个列是否为指定的值
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="indexcolName">主键列名</param>
        /// <param name="indexcolValue">主键值</param>
        /// <param name="identitycolName">需要验证列的列名</param>
        /// <param name="identityKey">用于验证是否相等的‘值’</param>
        /// <returns></returns>
        public bool Valid(string tableName, string indexcolName, string indexcolValue, string identitycolName, string identitycolValue)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new Exception("未指定有效的表名：表名为空");
            else if (string.IsNullOrWhiteSpace(indexcolName) || string.IsNullOrWhiteSpace(identitycolName))
                throw new Exception("未指定有效的列名：表名为空");
            else
            {
                using (Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn))
                {
                    var count =dbh.ExecuteScalar(
                        string.Format("select count(*) from {0} where {1} =@key and {2}=@value", tableName, indexcolName, identitycolName)
                        , new SqlParameter("@key", indexcolValue), new SqlParameter("@value", identitycolValue));
                    return ( (int)count) > 0;
                }
            }
        }
    }
}
