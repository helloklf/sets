﻿using DBModel;
using Dll_DataBaseSelect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOperate
{
    public class AccountOperation:BaseClass
    {
        public AccountOperation(SqlConnection conn):base(conn){　}

        /// <summary>
        /// 验证账号是否存在 
        /// </summary>
        /// <returns>当指定登录名存在是返回true</returns>
        public bool ValidLoginID(string LoginID)
        {
            Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn);
            var count = (int)dbh.ExecuteScalar("select COUNT(*) from IDBind where LoginID =@LoginID",new SqlParameter[]{
                SqlDBTools.Parameter("@LoginID",LoginID,SqlDbType.VarChar)
            });
            return count > 0;
        }

        /// <summary>
        /// 用户登陆
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public int Login(string uid, string pwd)
        {
            Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn);
            DbParameter[] pars = new DbParameter[] { new SqlParameter("@uid", uid), new SqlParameter("@pwd", pwd) };
            return (int)dbh.ExecuteScalar("select COUNT(*) from IDList where IndexID = (select IndexID from dbo.IDBind where LoginID = @uid) and IndexPwd =@pwd", pars);
        }

        /// <summary>
        /// 获得登录账号的主账号
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public string GetIDByLoginID(string loginId) 
        {
            Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn);
            return dbh.ExecuteScalar("select IndexID from IDBind where loginid = @loginid", new SqlParameter[] { new SqlParameter("@loginid", loginId) }) as string;
        }

        /// <summary>
        /// 根据用户编号获取用户的相关信息
        /// </summary>
        /// <param name="userId">用户编号</param>
        /// <returns></returns>
        public List<UserDataModel> GetUserDataByID(string userId)
        {
            using (Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from UserData where IndexID = @index", new SqlParameter[] { new SqlParameter("@index", userId) }))
                {
                    return ReadUserData(dr);
                }
            }
        }

        /// <summary>
        /// 从DataReader读取用户数据信息
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        List<UserDataModel> ReadUserData(DbDataReader dr)
        {
            var list = new List<UserDataModel>();
            while (dr.Read())
            {
                list.Add(new UserDataModel()
                {
                    Address = dr["Address"] as string,
                    Birthday = dr["Birthday"] as DateTime?,
                    Constellation = dr["Constellation"] as string,
                    Dream = dr["Dream"] as string,
                    Email = dr["Email"] as string,
                    HeadPic = dr["HeadPic"] as byte[],
                    Idol = dr["Idol"] as string,
                    IndexID = dr["IndexID"] as string,
                    Love = dr["Love"] as string,
                    Maxim = dr["Maxim"] as string,
                    MeRemark = dr["MeRemark"] as string,
                    MobilePhone = dr["MobilePhone"] as string,
                    Nickname = dr["Nickname"] as string,
                    Sex = dr["Sex"] as bool?,
                    Signature = dr["Signature"] as string,
                    TelPhone = dr["TelPhone"] as string,
                    WebSite = dr["WebSite"] as string,
                    Word = dr["Word"] as string,
                });
            }
            return list;
        }


        string run1 = "if((select count(*) from UserData where IndexID=@IndexID)<1) begin insert into UserData(IndexID) values(@IndexID) end ";
        /// <summary>
        /// 修改用户资料
        /// </summary>
        /// <param name="model">用户资料模型</param>
        /// <returns></returns>
        public int UpdateUserData(DBModel.UserDataModel model)
        {
            using (Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn))
            {
                try
                {
                    List<SqlParameter> paras = new List<SqlParameter>
                    {
                        new SqlParameter("@Address",model.Address){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Constellation",model.Constellation){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Dream",model.Dream){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Email",model.Email){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Idol",model.Idol){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@IndexID",model.IndexID){ SqlDbType = System.Data.SqlDbType.VarChar },
                        new SqlParameter("@Love",model.Love){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Maxim",model.Maxim){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@MeRemark",model.MeRemark){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@MobilePhone",model.MobilePhone){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Nickname",model.Nickname){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        SqlDBTools.Parameter("@Sex",model.Sex,SqlDbType.Bit),
                        SqlDBTools.Parameter("@Birthday",model.Birthday,SqlDbType.DateTime),
                        SqlDBTools.Parameter("@HeadPic",model.HeadPic,SqlDbType.Image),
                        new SqlParameter("@Signature",model.Signature){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@TelPhone",model.TelPhone){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@WebSite",model.WebSite){ SqlDbType = System.Data.SqlDbType.NVarChar },
                        new SqlParameter("@Word",model.Word){ SqlDbType = System.Data.SqlDbType.NVarChar },
                    };
                    //HeadPic=@HeadPic,
                    return dbh.ExecuteNonQuery("if((select count(*) from UserData where IndexID=@IndexID)<1) begin insert into UserData(IndexID) values(@IndexID) end  update UserData set Address=@Address, Birthday=@Birthday,Constellation= @Constellation,Dream=@Dream,Email=@Email,  Idol = @Idol,Love=@Love,Maxim=@Maxim,MeRemark=@MeRemark,MobilePhone=@MobilePhone,Nickname=@Nickname,Sex=@Sex,Signature=@Signature,TelPhone=@TelPhone,WebSite=@WebSite,Word=@Word where IndexID = @IndexID", paras.ToArray());
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 查询所有用户数量
        /// </summary>
        /// <returns></returns>
        public int GetAllUser()
        {
            Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn);
            return (int)dbh.ExecuteScalar("select COUNT(*) from IDList");
        }

        /// <summary>
        /// 输入账号检测相似的登陆账号
        /// </summary>
        /// <returns></returns>
        public List<UserDataModel> GetLikeUser(string loginid) 
        {
            using (Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn)) 
            {
                List<UserDataModel> udml = new List<UserDataModel>();
                UserDataModel udm;
                DbDataReader dr = dbh.ExecuteReader("select top 50 a.IndexID,LoginID,'Nickname'= case when Nickname is null then LoginID else Nickname end ,HeadPic,[Signature],Sex,birthday,MobilePhone,TelPhone from ( select *from IDBind where LoginID like @loginId) as a left join (select * from UserData where IndexID in (select top 1 IndexID from IDBind where  LoginID like @loginId)) as b on a.IndexID = b.IndexID", new SqlParameter[] { new SqlParameter("@loginId", loginid + "%") });
                while (dr.Read()) 
                {
                    udm = new UserDataModel() { IndexID = dr["IndexID"] as string, LoginID = dr["LoginID"] as string, Nickname = dr["Nickname"] as string, Signature = dr["Signature"] as string, MobilePhone = dr["MobilePhone"] as string, TelPhone = dr["TelPhone"] as string, HeadPic = dr["HeadPic"] as byte[], Sex = dr["Sex"] as bool?, Birthday = dr["Birthday"] as DateTime? };
                    udml.Add(udm);
                }
                return udml;
            }
        }

        /// <summary>
        /// 添加用户的好友
        /// </summary>
        /// <param name="indexid"></param>
        /// <param name="friendid"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public int AddFriend(string indexid,string friendid,string remark) 
        {
            Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn);
            return dbh.ExecuteNonQuery("insert into UserFriend(IndexID,FriendID,FriendRemark) values( @indexid,@friendid,@remark )",
                new SqlParameter("@indexid", indexid), new SqlParameter("@friendid", friendid), new SqlParameter("@remark",remark));
        }

        /// <summary>
        /// 注册新用户
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public int RegisterSQL(string uid, string pwd)
        {
            Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn);
            DbParameter[] pars = new DbParameter[] { new SqlParameter("@uid", uid), new SqlParameter("@pwd", pwd) };
            int count = dbh.ExecuteNonQueryProc("Proc_UserRegister", pars);
            return count > 0 ? 1 : 0;
        }

        /// <summary>
        /// 查询指定账号的所有好友
        /// </summary>
        /// <param name="indexid"></param>
        /// <returns></returns>
        public List<UserFriendModel> GetUserFriend(string indexid)
        {
            using (Dll_DataBaseSelect.DBHelper dbh = new SQLDBHelper(DBConn))
            {
                DbDataReader dr = dbh.ExecuteReader("select * from UserFriend where IndexID = @IndexID", new SqlParameter[] { new SqlParameter("@IndexID", indexid) });
                List<UserFriendModel> ufml = new List<UserFriendModel>();
                UserFriendModel ufm;
                while (dr.Read())
                {
                    ufm = new UserFriendModel() { DataIndex = dr["DataIndex"].ToString(), FriendID = dr["FriendID"].ToString(), FriendRemark = dr["FriendRemark"].ToString(), IndexID = dr["IndexID"].ToString() };
                    ufml.Add(ufm);
                }
                return ufml;
            }
        }

    }
}
