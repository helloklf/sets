﻿using Dll_DataBaseSelect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOperate
{
    public class DynamicInfoOperations:BaseClass
    {
        public DynamicInfoOperations(SqlConnection conn) : base(conn) { }

        /// <summary>
        /// 获取文章
        /// </summary>
        /// <returns></returns>
        public List<DBModel.DynamicInfoModel> GetDynamicInfo(long StartIndex)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from ( select ROW_NUMBER() over (Order by  [DateTime] desc) as OrderID,* from DynamicInfo where PDataIndex is null ) as DynamicInfo where OrderID between @startIndex and @startIndex+10", new SqlParameter[] { new SqlParameter("@startIndex", StartIndex) }))
                {
                    return ReaderToModel(dr);
                }
            }
        }

        /// <summary>
        /// 获取某人从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public List<DBModel.DynamicInfoModel> GetDynamicInfo_OneUser(long StartIndex, string IndexID)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from ( select ROW_NUMBER() over (Order by  [DateTime] desc) as OrderID,* from DynamicInfo where PDataIndex is null and IndexID=@IndexID) as DynamicInfo where OrderID between @startIndex and @startIndex+10",
                new SqlParameter[] 
                {
                    SqlDBTools.Parameter("@startIndex",StartIndex),
                    SqlDBTools.Parameter("@IndexID",IndexID),
                }))
                {
                    return ReaderToModel(dr);
                }
            }
        }


        /// <summary>
        /// 获取最后添加的行
        /// </summary>
        /// <returns></returns>
        public List<DBModel.DynamicInfoModel> GetLastInsert()
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select top 1 * from ( select ROW_NUMBER() over (Order by  [DateTime] desc) as OrderID,* from DynamicInfo where PDataIndex is null ) as DynamicInfo"))
                {
                    return ReaderToModel(dr);
                }
            }
        }


        /// <summary>
        /// 获取指定用户最后添加的行 
        /// </summary>
        /// <param name="IndexID">用户账号索引</param>
        /// <returns></returns>
        public List<DBModel.DynamicInfoModel> GetLastInsert(string IndexID)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select top 1 * from ( select ROW_NUMBER() over (Order by  [DateTime] desc) as OrderID,* from DynamicInfo where PDataIndex is null and IndexID=@IndexID) as DynamicInfo",
                new SqlParameter[] 
                {
                    SqlDBTools.Parameter("@IndexID",IndexID),
                }))
                {
                    return ReaderToModel(dr);
                }
            }
        }

        /// <summary>
        /// 获取指定索引号的文章内容
        /// </summary>
        /// <param name="DataIndex">文章索引号</param>
        /// <returns></returns>
        public List<DBModel.DynamicInfoModel> GetDynamicInfo(string DataIndex)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                using (var dr = dbh.ExecuteReader("select * from DynamicInfo where DataIndex =@DataIndex", new SqlParameter[] { new SqlParameter("@DataIndex", DataIndex) }))
                {
                    return ReaderToModel(dr);
                }
            }
        }

        /// <summary>
        /// 更新文章内容
        /// </summary>
        /// <param name="DynamicInfo">文章内容</param>
        /// <returns></returns>
        public int UpdateDynamicInfo(DBModel.DynamicInfoModel DynamicInfo)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                return dbh.ExecuteNonQuery("update DynamicInfo set DocTitle=@DocTitle,DocData=@DocData where DataIndex=@DataIndex", new SqlParameter[]{
                    SqlDBTools.Parameter("@DataIndex",DynamicInfo.DataIndex,SqlDbType.VarChar),
                    SqlDBTools.Parameter("@DocTitle",DynamicInfo.DocTitle,SqlDbType.NVarChar),
                    SqlDBTools.Parameter("@DocData",DynamicInfo.DocData,SqlDbType.NVarChar)
                });
            }
        }

        /// <summary>
        /// 添加文章内容
        /// </summary>
        /// <param name="DynamicInfo">文章内容</param>
        /// <returns></returns>
        public int InsertDynamicInfo(DBModel.DynamicInfoModel DynamicInfo)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                try
                {
                    return dbh.ExecuteNonQuery("insert into DynamicInfo(IndexID,DocTitle,DocData) values(@IndexID,@DocTitle,@DocData)", new SqlParameter[]{
                    SqlDBTools.Parameter("@IndexID",DynamicInfo.IndexID,SqlDbType.VarChar),
                    SqlDBTools.Parameter("@DocTitle",DynamicInfo.DocTitle,SqlDbType.NVarChar),
                    SqlDBTools.Parameter("@DocData",DynamicInfo.DocData,SqlDbType.NVarChar)
                });
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 删除文章
        /// </summary>
        /// <param name="DynamicInfo">文章信息，其中必须内容为DataIndex</param>
        /// <returns></returns>
        public int DeleteDynamicInfo(DBModel.DynamicInfoModel DynamicInfo)
        {
            using (SQLDBHelper dbh = new SQLDBHelper(DBConn))
            {
                try
                {
                    return dbh.ExecuteNonQuery("delete DynamicInfo where DataIndex = @DataIndex and IndexID = @IndexID", new SqlParameter[]{
                    SqlDBTools.Parameter("@IndexID",DynamicInfo.IndexID,SqlDbType.VarChar),
                    SqlDBTools.Parameter("@DataIndex",DynamicInfo.DataIndex,SqlDbType.VarChar),
                });
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 从DbDataReader读取数据到模型列表
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        List<DBModel.DynamicInfoModel> ReaderToModel(DbDataReader dr)
        {
            var list = new List<DBModel.DynamicInfoModel>();
            while (dr.Read())
            {
                list.Add(new DBModel.DynamicInfoModel()
                {
                    DataIndex = dr["DataIndex"] as string,
                    DateTime = (DateTime?)dr["DateTime"],
                    DocData = dr["DocData"] as string,
                    DocTitle = dr["DocTitle"] as string,
                    IndexID = dr["IndexID"] as string,
                    IsValid = dr["IsValid"] as string,
                    PDataIndex = dr["PDataIndex"] as string
                });
            }
            return list;
        }

        /// <summary>
        /// 验证是否是本人文章
        /// </summary>
        /// <param name="DataIndex">文章编号</param>
        /// <param name="IndexID">用户名</param>
        /// <returns></returns>
        public bool DynamicInfoValid(string DataIndex, string IndexID)
        {
            return base.Valid("DynamicInfo", "DataIndex", DataIndex, "IndexID", IndexID);
        }
        
    }
}
