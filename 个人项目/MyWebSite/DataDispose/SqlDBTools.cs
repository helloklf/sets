﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOperate
{
   public static  class SqlDBTools
    {
       /// <summary>
       /// 创建参数
       /// </summary>
       /// <param name="name">参数名，如：@ID</param>
       /// <param name="value">参数值</param>
       /// <param name="type">数据对于数据类型</param>
       /// <returns></returns>
        public static System.Data.SqlClient.SqlParameter Parameter(string name,object value,SqlDbType type)
        {
            if (value == null)
                return new System.Data.SqlClient.SqlParameter(name, DBNull.Value);
            else
                return new System.Data.SqlClient.SqlParameter(name, value) { SqlDbType = type };
        }

        /// <summary>
        /// 创建参数
        /// </summary>
        /// <param name="name">参数名，如：@ID</param>
        /// <param name="value">参数值</param>
        /// <returns></returns>
        public static System.Data.SqlClient.SqlParameter Parameter(string name, object value)
        {
            if (value == null)
                return new System.Data.SqlClient.SqlParameter(name, DBNull.Value);
            else
                return new System.Data.SqlClient.SqlParameter(name, value);
        }
    }
}
