﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Configuration;
using System.Data.Common;

namespace Dll_DataBaseSelect
{
    public abstract class DBHelper:IDisposable
    {
        public DBHelper(){  }

        /// <summary>
        /// 使用连接字符串初始化数据库连接
        /// </summary>
        /// <param name="conn"></param>
        public DBHelper(string conn)
        {
            DBConn = new OleDbConnection(conn);
        }

        /// <summary>
        /// 使用现有的数据库连接
        /// </summary>
        /// <param name="conn"></param>
        public DBHelper(DbConnection conn)
        {
            this.DBConn = conn;
        }

        public DbConnection DBConn{ get;set; } //数据库连接示例
        public virtual DbCommand DBCmd { get; set; } //DBCommand
        
        /// <summary>
        /// 断开数据库连接
        /// </summary>
        public void DBClose()
        {
            if (DBConn != null && DBConn.State == ConnectionState.Open) DBConn.Close();
        }

        /// <summary>
        /// 释放连接
        /// </summary>
        public void DBDispose()
        {
            if (DBConn != null) DBConn.Dispose();
        }

        /// <summary>
        /// 打开数据库连接
        /// </summary>
        public void DBOpen()
        {
            if (DBConn != null && DBConn.State == ConnectionState.Closed) 
            {
                DBConn.Open();
            }
        }

        /// <summary>
        /// 清理DBCommand
        /// </summary>
        public void DBCmdDispose()
        {
            if (DBCmd != null) DBCmd.Dispose();
        }

        /// <summary>
        /// 创建数据库命令 设置其连接方式为默认实例
        /// </summary>
        public virtual void DBCmdCreate()
        {
            if (DBCmd != null) throw new Exception("功能异常：DbCommand已经创建实例");
            DBCmd = new OleDbCommand();
            DBCmd.Connection = DBConn;
        }

        /// <summary>
        /// 将DBCommand初始化并添加参数
        /// </summary>
        /// <param name="dbexec"></param>
        /// <param name="pars"></param>
        public virtual void DBCmdInitialize(string dbexec, DbParameter[] pars)
        {
            DBCmdCreate();
            DBCmd.CommandType = CommandType.Text;
            DBCmd.CommandText = dbexec;
            if (pars == null || pars.Length == 0) { }
            else DBCmd.Parameters.AddRange(pars);
            DBOpen();
        }

        /// <summary>
        /// 将DBCommand初始化为存储过程模式并添加参数
        /// </summary>
        /// <param name="dbexec"></param>
        /// <param name="pars"></param>
        public virtual void DBCmdInitializeProc(string dbexec, DbParameter[] pars)
        {
            DBCmdCreate();
            DBCmd.CommandType = CommandType.StoredProcedure;
            DBCmd.CommandText = dbexec;
            if (pars == null || pars.Length == 0) { }
            else DBCmd.Parameters.AddRange(pars);
            DBOpen();
        }


        /// <summary>
        /// 返回DataReader
        /// </summary>
        /// <param name="dbexec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public virtual DbDataReader ExecuteReader(string dbexec, params DbParameter[] pars)
        {
            DBCmdInitialize(dbexec,pars);
            return DBCmd.ExecuteReader();
        }

        /// <summary>
        /// 返回受影响行数（存储过程）
        /// </summary>
        /// <param name="dbexec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public virtual int ExecuteNonQueryProc(string dbexec, params DbParameter[] pars) 
        {
            try
            {
                DBCmdInitializeProc(dbexec, pars);
                return DBCmd.ExecuteNonQuery();
            }
            finally
            {
                DBClose(); DBCmdDispose();
            }
        }

        /// <summary>
        /// 返回受影响行数
        /// </summary>
        /// <param name="dbexec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public virtual int ExecuteNonQuery(string dbexec, params DbParameter[] pars)
        {
            try
            {
                DBCmdInitialize(dbexec, pars);
                return DBCmd.ExecuteNonQuery();
            }
            finally
            {
                DBClose(); DBCmdDispose();
            }
        }

        /// <summary>
        /// 返回首行首列
        /// </summary>
        /// <param name="dbexec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public virtual object ExecuteScalar(string dbexec, params DbParameter[] pars)
        {
            try
            {
                DBCmdInitialize(dbexec, pars);
                return DBCmd.ExecuteScalar();
            }
            catch { return null; }
            finally
            {
                DBClose(); DBCmdDispose();
            }
        }

        public void Dispose()
        {
            DBClose(); DBCmdDispose();
        }
    }

    /// <summary>
    /// SQL Server
    /// </summary>
    public class SQLDBHelper : DBHelper
    {
        /// <summary>
        /// 根据数据库连接字符串创建连接
        /// </summary>
        /// <param name="conn"></param>
        public SQLDBHelper(string conn)
        {
            DBConn = new SqlConnection(conn);
        }

        /// <summary>
        /// 使用现有的数据库连接
        /// </summary>
        /// <param name="conn"></param>
        public SQLDBHelper(SqlConnection conn)
        {
            DBConn = conn;
        }

        /// <summary>
        /// 重写DBCommand初始化方式
        /// </summary>
        public override void DBCmdCreate()
        {
            if (DBCmd != null) throw new Exception("功能异常：SqlCommand已经创建实例");
            DBCmd = new SqlCommand();
            DBCmd.Connection = DBConn;
        }


    }


    /// <summary>
    /// Access等
    /// </summary>
    public class OleDBHelper
    {
        
    }

    /// <summary>
    /// Oracle
    /// </summary>
    public class ODBCHelper 
    {
        
    }
}
