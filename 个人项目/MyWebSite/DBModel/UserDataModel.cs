﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBModel
{
    public class UserDataModel : IDBindModel //用户资料模型
    {
        public string Email { get; set; } //电子邮件
        public string WebSite { get; set; }//个人网站
        public string Idol { get; set; } //偶像
        public string Maxim { get; set; }//格言
        public string Constellation { get; set; }//星座
        public string Dream { get; set; }//梦想
        public string Word { get; set; } //工作
        public string Address { get; set; }//地址
        public string Love { get; set; }//爱好
        public string MeRemark { get; set; }//个人说明
        public string Nickname { get; set; }//昵称
        public byte[] HeadPic { get; set; }//头像
        public string Signature { get; set; }//签名
        public bool? Sex { get; set; }//性别
        public DateTime? Birthday { get; set; }//生日
        public string MobilePhone { get; set; }//手机
        public string TelPhone { get; set; }//电话
    }
}
