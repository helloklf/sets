﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBModel
{
    public class ShareDataModel
    {
        public ShareDataModel() { }
        public ShareDataModel(string dindex, DateTime dt, string edoc, string surl)
        {
            DataIndex = dindex; DateTime = dt; DocData = edoc; SourceURL = surl;
        }

        string pDataIndex;
        /// <summary>
        /// 引用原文索引
        /// </summary>
        public string PDataIndex
        {
            get { return pDataIndex; }
            set { pDataIndex = value; }
        }

        string dataIndex;
        /// <summary>
        /// 记录索引
        /// </summary>
        public string DataIndex
        {
            get { return dataIndex; }
            set { dataIndex = value; }
        }

        string indexID;
        /// <summary>
        /// 创建者
        /// </summary>
        public string IndexID
        {
            get { return indexID; }
            set { indexID = value; }
        }

        DateTime dateTime;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime DateTime
        {
            get { return dateTime; }
            set { dateTime = value; }
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string DateTimeText
        {
            get { return DateTime.ToString("yyyy-MM-dd HH:mm:ss"); }
        }

        string sourceTitle;
        /// <summary>
        /// 文档标题
        /// </summary>
        public string SourceTitle
        {
            get { return sourceTitle; }
            set { sourceTitle = value; }
        }


        string docData;
        /// <summary>
        /// 说明文档
        /// </summary>
        public string DocData
        {
            get { return docData; }
            set { docData = value; }
        }


        string sourceURL;
        /// <summary>
        /// 资源地址
        /// </summary>
        public string SourceURL
        {
            get { return sourceURL; }
            set { sourceURL = value; }
        }
    }
}
