﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBModel
{
    public class UserFriendModel:UserDataModel　//好友信息模型
    {
        //select UserFriend.DataIndex,FriendRemark,Nickname from UserFriend join UserData on UserFriend.FriendID = UserData.IndexID

        /// <summary>
        /// 记录索引
        /// </summary>
        public string DataIndex { get; set; }

        /// <summary>
        /// 好友账号
        /// </summary>
        public string FriendID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string FriendRemark { get; set; }
    }
}
