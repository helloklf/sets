﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBModel
{
    /// <summary>
    /// 动态信息-与数据库表DynamicInfo对应
    /// </summary>
    public class DynamicInfoModel
    {
        public DynamicInfoModel() { }


        /// <summary>
        /// 文章记录索引
        /// </summary>
        public string DataIndex { get; set; }

        /// <summary>
        /// 引用原文记录索引
        /// </summary>
        public string PDataIndex { get; set; }

        /// <summary>
        /// 文章发表者账号索引
        /// </summary>
        public string IndexID { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime? DateTime { get; set; }

        public string DateTimeText
        {
            get
            {
                return this.DateTime == null ? "" : ((DateTime)DateTime).ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        /// <summary>
        /// 文章标题
        /// </summary>
        public string DocTitle { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        public string DocData { get; set; }

        /// <summary>
        /// 有效状态
        /// </summary>
        public string IsValid { get; set; }

        /// <summary>
        /// 虚拟字段，指定该内容是否可以被编辑
        /// </summary>
        public bool CanEdit { get; set; }
    }
}
