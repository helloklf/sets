using System;

namespace DBModel
{
    public class WorksModel
    {
        //create table Article										--用户发出的Article(文章)
        //DataIndex varchar(36) default(newid()),							--记录索引
        //PDataIndex varchar(36),												--主文引用
        //IndexID varchar(36),		--作者的主账号索引
        //[DateTime] DateTime default(getdate()),						--发表时间
        //DocTitle Nvarchar(200) not null,									--文章标题
        //DocData Nvarchar(max) not null,									--文档内容
        //IsValid bit default(0)														--有效状态

        public WorksModel() { }

        /// <summary>
        /// 文章记录索引
        /// </summary>
        public string DataIndex { get; set; }

        /// <summary>
        /// 引用原文记录索引
        /// </summary>
        public string PDataIndex { get; set; }

        /// <summary>
        /// 文章发表者账号索引
        /// </summary>
        public string IndexID { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime? DateTime { get; set; }

        public string DateTimeText
        {
            get
            {
                return this.DateTime == null ? "" : ((DateTime)DateTime).ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        /// <summary>
        /// 文章标题
        /// </summary>
        public string DocTitle { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        public string DocData { get; set; }

        /// <summary>
        /// 有效状态
        /// </summary>
        public string IsValid { get; set; }

        /// <summary>
        /// 虚拟字段，指定该内容是否可以被编辑
        /// </summary>
        public bool CanEdit { get; set; }
    }
}