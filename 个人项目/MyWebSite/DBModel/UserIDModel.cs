﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBModel
{
    public class IDModel //主账号和密码模型
    {
        string indexID;

        public string IndexID
        {
            get { return indexID; }
            set { indexID = value; }
        }

        string loginPwd;

        public string LoginPwd
        {
            get { return loginPwd; }
            set { loginPwd = value; }
        }
    }


    public class IDBindModel:IDModel //登陆账号模型
    {
        string loginID;

        public string LoginID
        {
            get { return loginID; }
            set { loginID = value; }
        }
    }
}
