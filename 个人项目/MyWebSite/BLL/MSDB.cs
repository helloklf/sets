﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;


namespace MyWebSite
{
    public partial class MSDB
    {
        static SqlConnection SqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyLocalMSDB"].ConnectionString);

        static DataOperate.AccountOperation account = new DataOperate.AccountOperation(SqlConn);
        static DataOperate.ArticleOperation article = new DataOperate.ArticleOperation(SqlConn);
        static DataOperate.AuthorDataOperate AuthorDataOperate = new DataOperate.AuthorDataOperate(SqlConn);
        static DataOperate.DynamicInfoOperations Dynamicinfo = new DataOperate.DynamicInfoOperations(SqlConn);
        static DataOperate.ShareDataOperation sharedata = new DataOperate.ShareDataOperation(SqlConn);
        static DataOperate.WorksOperation works = new DataOperate.WorksOperation(SqlConn);
    }
}