﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;

namespace MyWebSite
{
    public partial class MSDB
    {

        #region 项目相关

        /// <summary>
        /// 获取从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public static List<DBModel.WorksModel> GetWorks(long StartIndex)
        {
            return works.GetWorks(StartIndex);
        }

        /// <summary>
        /// 获取某人从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public static List<DBModel.WorksModel> GetWorks_OneUser(long StartIndex, string IndexID)
        {
            return works.GetWorks_OneUser(StartIndex, IndexID);
        }

        /// <summary>
        /// 获取指定索引号的文章内容
        /// </summary>
        /// <param name="DataIndex">文章索引号</param>
        /// <returns></returns>
        public static List<DBModel.WorksModel> GetWorks(string DataIndex)
        {
            return works.GetWorks(DataIndex);
        }


        /// <summary>
        /// 更新文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public static int UpdateWorks(DBModel.WorksModel worksModel)
        {
            return works.UpdateWorks(worksModel);
        }


        /// <summary>
        /// 添加文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public static int InsertWorks(DBModel.WorksModel worksModel)
        {
            return works.InsertWorks(worksModel);
        }

        /// <summary>
        /// 删除文章
        /// </summary>
        /// <param name="article">文章信息，其中必须内容为DataIndex</param>
        /// <returns></returns>
        public static int DeleteWorks(DBModel.WorksModel worksModel)
        {
            return works.DeleteWorks(worksModel);
        }


        /// <summary>
        /// 验证是否是本人文章
        /// </summary>
        /// <param name="DataIndex">文章编号</param>
        /// <param name="IndexID">用户名</param>
        /// <returns></returns>
        public static bool WorksValid(string DataIndex, string IndexID)
        {
            return works.WorksValid(DataIndex, IndexID);
        }
        #endregion

    }
}
