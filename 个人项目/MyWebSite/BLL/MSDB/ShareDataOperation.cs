﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;

namespace MyWebSite
{
    public partial class MSDB
    {


        #region 共享资源相关
        /// <summary>
        /// 查询共享资源
        /// </summary>
        /// <returns></returns>
        public static List<ShareDataModel> GetShareData()
        {
            return sharedata.GetShareData();
        }


        /// <summary>
        /// 获取共享资料信息
        /// </summary>
        /// <param name="startindex">当前正在浏览的最后一条记录</param>
        /// <returns></returns>
        public static List<ShareDataModel> GetShareData(long startindex)
        {
            return sharedata.GetShareData(startindex);
        }

        /// <summary>
        /// 根据索引号获取相关的数据
        /// </summary>
        /// <param name="DataIndex"></param>
        /// <returns></returns>
        public static List<ShareDataModel> GetShareDataByIndex(string DataIndex)
        {
            return sharedata.GetShareDataByIndex(DataIndex);
        }
        /// <summary>
        /// 添加新的共享资源
        /// </summary>
        /// <param name="model">共享资源模型</param>
        /// <returns></returns>
        public static int InsertShareData(ShareDataModel model)
        {
            return sharedata.InsertShareData(model);
        }

        /// <summary>
        /// 修改共享的资源
        /// </summary>
        /// <param name="model">共享的资源模型</param>
        /// <returns></returns>
        public static int EditShareData(ShareDataModel model)
        {
            return sharedata.EditShareData(model);
        }

        /// <summary>
        /// 删除共享的资源
        /// </summary>
        /// <param name="dataIndex">共享资源编号</param>
        /// <returns></returns>
        public static int DeleteShareData(string dataIndex)
        {
            return sharedata.DeleteShareData(dataIndex);
        }

        #endregion
    }
}
