﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;

namespace MyWebSite
{
    public partial class MSDB
    {


        #region 作者相关

        /// <summary>
        /// 获取作者账号的IndexID
        /// </summary>
        /// <returns></returns>
        public static string GetAuthorIndexID()
        {
            return AuthorDataOperate.GetAuthorIndexID();
        }
        #endregion
    }
}
