﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;

namespace MyWebSite
{
    public partial class MSDB
    {

        #region 文章相关

        /// <summary>
        /// 获取从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public static List<DBModel.ArticleModel> GetArticle(long StartIndex)
        {
            return article.GetArticle(StartIndex);
        }

        
        /// <summary>
        /// 获取文章评论
        /// </summary>
        /// <returns></returns>
        public static List<DBModel.ArticleModel> GetArticleComments(string articleId)
        {
            return article.GetArticleComments(articleId);
        }
        /// <summary>
        /// 获取文章评论
        /// </summary>
        /// <returns></returns>
        public static List<DBModel.ArticleModel> GetWorksComments(string worksId)
        {
            return article.GetWorksComments(worksId);
        }
        

        /// <summary>
        /// 获取某人从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public static List<DBModel.ArticleModel> GetArticle_OneUser(long StartIndex,string IndexID)
        {
            return article.GetArticle_OneUser(StartIndex,IndexID);
        }



        /// <summary>
        /// 获取指定索引号的文章内容
        /// </summary>
        /// <param name="DataIndex">文章索引号</param>
        /// <returns></returns>
        public static List<DBModel.ArticleModel> GetArticle(string DataIndex)
        {
            return article.GetArticle(DataIndex);
        }


        /// <summary>
        /// 更新文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public static int UpdateArticle(DBModel.ArticleModel articleModel)
        {
            return article.UpdateArticle(articleModel);
        }


        /// <summary>
        /// 添加文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public static int InsertArticle(DBModel.ArticleModel articleModel)
        {
            return article.InsertArticle(articleModel);
        }

        /// <summary>
        /// 删除文章
        /// </summary>
        /// <param name="article">文章信息，其中必须内容为DataIndex</param>
        /// <returns></returns>
        public static int DeleteArticle(DBModel.ArticleModel articleModel)
        {
            return article.DeleteArticle(articleModel);
        }

        
        /// <summary>
        /// 验证是否是本人文章
        /// </summary>
        /// <param name="DataIndex">文章编号</param>
        /// <param name="IndexID">用户名</param>
        /// <returns></returns>
        public static bool AricleValid(string DataIndex, string IndexID)
        {
            return article.AricleValid(DataIndex,IndexID);
        }
        #endregion
    }
}
