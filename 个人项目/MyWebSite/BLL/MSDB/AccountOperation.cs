﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;

namespace MyWebSite
{
    public partial class MSDB
    {

        #region 用户相关

        /// <summary>
        /// 验证账号是否存在 
        /// </summary>
        /// <returns>当指定登录名存在是返回true</returns>
        public static bool ValidLoginID(string LoginID)
        {
            return account.ValidLoginID(LoginID);
        }

        /// <summary>
        /// 用户登陆
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public static int Login(string uid, string pwd)
        {
            return account.Login(uid, pwd);
        }

        /// <summary>
        /// 获得登录账号的主账号
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public static string GetIDByLoginID(string loginId)
        {
            return account.GetIDByLoginID(loginId);
        }

        /// <summary>
        /// 查询所有用户数量
        /// </summary>
        /// <returns></returns>
        public static int GetAllUser()
        {
            return account.GetAllUser();
        }

        /// <summary>
        /// 注册新用户
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public static int RegisterSQL(string uid, string pwd)
        {
            return account.RegisterSQL(uid, pwd);
        }

        /// <summary>
        /// 查询指定账号的所有好友
        /// </summary>
        /// <param name="indexid"></param>
        /// <returns></returns>
        public static List<UserFriendModel> GetUserFriend(string indexid)
        {
            return account.GetUserFriend(indexid);
        }


        /// <summary>
        /// 输入账号检测相似的登陆账号
        /// </summary>
        /// <returns></returns>
        public static List<UserDataModel> GetLikeUser(string loginid)
        {
            return account.GetLikeUser(loginid);
        }


        /// <summary>
        /// 添加用户的好友
        /// </summary>
        /// <param name="indexid"></param>
        /// <param name="friendid"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public static int AddFriend(string indexid, string friendid, string remark)
        {
            return account.AddFriend(indexid, friendid, remark);
        }


        /// <summary>
        /// 根据用户编号获取用户的相关信息
        /// </summary>
        /// <param name="userId">用户编号</param>
        /// <returns></returns>
        public static List<UserDataModel> GetUserDataByID(string userId)
        {
            return account.GetUserDataByID(userId);
        }
        /// <summary>
        /// 修改用户资料
        /// </summary>
        /// <param name="model">用户资料模型</param>
        /// <returns></returns>
        public static int UpdateUserData(DBModel.UserDataModel model)
        {
            return account.UpdateUserData(model);
        }

        #endregion
    }
}
