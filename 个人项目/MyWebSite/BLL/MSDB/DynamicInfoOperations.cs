﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DBModel;
using System.Configuration;

namespace MyWebSite
{
    public partial class MSDB
    {

        #region 动态相关

        /// <summary>
        /// 获取从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public static List<DBModel.DynamicInfoModel> GetDynamicInfos(long StartIndex)
        {
            return Dynamicinfo.GetDynamicInfo(StartIndex);
        }
        
        /// <summary>
        /// 获取某人从第N篇开始的20篇文章
        /// </summary>
        /// <returns>开始位置</returns>
        public static List<DBModel.DynamicInfoModel> GetDynamicInfo_OneUser(long StartIndex, string IndexID)
        {
            return Dynamicinfo.GetDynamicInfo_OneUser(StartIndex,IndexID);
        }

        /// <summary>
        /// 获取最后添加的行
        /// </summary>
        /// <returns></returns>
        public static List<DBModel.DynamicInfoModel> GetLastInsert()
        {
            return Dynamicinfo.GetLastInsert();
        }


        /// <summary>
        /// 获取指定用户最后添加的行 
        /// </summary>
        /// <param name="IndexID">用户账号索引</param>
        /// <returns></returns>
        public static List<DBModel.DynamicInfoModel> GetLastInsert(string IndexID)
        {
            return Dynamicinfo.GetLastInsert(IndexID);
        }

        /// <summary>
        /// 获取指定索引号的文章内容
        /// </summary>
        /// <param name="DataIndex">文章索引号</param>
        /// <returns></returns>
        public static List<DBModel.DynamicInfoModel> GetDynamicInfo(string DataIndex)
        {
            return Dynamicinfo.GetDynamicInfo(DataIndex);
        }


        /// <summary>
        /// 更新文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public static int UpdateDynamicInfo(DBModel.DynamicInfoModel DynamicInfo)
        {
            return Dynamicinfo.UpdateDynamicInfo(DynamicInfo);
        }


        /// <summary>
        /// 添加文章内容
        /// </summary>
        /// <param name="article">文章内容</param>
        /// <returns></returns>
        public static int InsertDynamicInfo(DBModel.DynamicInfoModel DynamicInfo)
        {
            return Dynamicinfo.InsertDynamicInfo(DynamicInfo);
        }

        /// <summary>
        /// 删除文章
        /// </summary>
        /// <param name="article">文章信息，其中必须内容为DataIndex</param>
        /// <returns></returns>
        public static int DeleteDynamicInfo(DBModel.DynamicInfoModel DynamicInfo)
        {
            return Dynamicinfo.DeleteDynamicInfo(DynamicInfo);
        }

        /// <summary>
        /// 验证是否是本人文章
        /// </summary>
        /// <param name="DataIndex">文章编号</param>
        /// <param name="IndexID">用户名</param>
        /// <returns></returns>
        public static bool DynamicInfoValid(string DataIndex, string IndexID)
        {
            return Dynamicinfo.DynamicInfoValid(DataIndex, IndexID);
        }
        #endregion
    }
}
