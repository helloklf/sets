﻿
go

--账户密码列表
create table IDList
(
	IndexID varchar(36) default(newid()) Primary key,--账号索引
	IndexPWD varchar(32) not null					--账号密码
)

--账户别名列表
create table IDBind
(
	IndexID varchar(36)  primary key,				--账号索引
	LoginID varchar(36) not null  Unique			--登陆账号
)


--用户资料
go
create table UserData
(
	IndexID varchar(36) Primary key,				--账号索引
	Nickname varchar(100),							--昵称
	HeadPic image,								--头像
	[Signature] varchar(250),						--签名
	Sex	bit,										--性别
	birthday datetime check( birthday>'1900-1-1 '),	--生日
	MobilePhone varchar(18) ,						--手机
	TelPhone varchar(18) ,							--电话

)

go

