﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MyWebSite.Default" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head">OmArea.Com</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <%--<iframe src="Sources_V2.0.html" style="border:none; width:70%; height:85%; float:left; background:none;"></iframe>--%>
    <div style="border: none; width: 70%; height: 85%; position: relative; float: left; background: none;">
<div id="screen">
    <link href="Sources_V2.0/StyleSheet1.css" rel="stylesheet" />
    <script src="Sources_V2.0/JavaScript1.js"></script>
	<div id="box">
        <img src="Sources_V2.0/home.jpg" title="首页" alt="欢迎访问OmArea.Com测试版页面" />
        <img src="Sources_V2.0/author.JPG" title="作者" alt="查看作者最细动态，来看看有什么值得发现的东西吧" />
        <img src="Sources_V2.0/wenzhang.jpg" title="文章" alt="精彩的原创或转载文章，丰富内容，会有你想要找的" />
        <img src="Sources_V2.0/works.jpg" title="作品" alt="来自作者的小聪明，一起来吐吐槽打发一下时间啦" />
        <img src="Sources_V2.0/shareData.jpg" title="资源" alt="来自站长提供的精华资源集，说不定会有一两样喜欢的东西哦！" />
        <img src="Sources_V2.0/about.jpg" title="关于" alt="了解本网站相关信息，如果你对此感兴趣的话" />
		<span id="txt"></span>
		<span id="tit"></span>
		<span id="lnk">
            <a href="/Index.html">首页</a>
            <a href="/AuthorData/AuthorInfo.aspx">作者专区</a>
            <a href="/Messages/ArticleList.aspx">文章列表</a>
            <a href="/Messages/WorksList.aspx">开源项目</a>
            <a href="/AuthorData/ShareData.aspx">共享资源</a>
            <a href="/AboutInfo/Site.aspx">关于网站</a>
		</span>
	</div>
</div>
    </div>

    <div style="font-size:20px;">
        <h2>邝林飞</h2>
        <h4>族群：程序猿</h4>
        <h4>油箱：HelloKlf@outlook.com</h4>
        <h4>爪机：131-7863-8515</h4>
        <h4>职业：学生</h4>
        <h4>公司：战争学院</h4>
    </div>
    <h3 style="clear:both;display:block; padding:10px 0px;">OmArea.Com正在内测中，在此期间产生的数据可能不会被保存</h3>
</asp:Content>
        
