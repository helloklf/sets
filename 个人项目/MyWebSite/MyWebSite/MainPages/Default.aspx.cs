﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["WantTo"] != null) { string url = Session["WantTo"].ToString(); Session["WantTo"] = null; Response.Redirect(url); return; }
            if (Request.QueryString["uid"] != null) 
            {
                Session["UID"] = "null";
                Response.Write("<script>alert('警告：" + "不要参数发送可见参数给页面，处于安全考虑，您已离线..." + "')</script>");
            }
            else if(Request.Form["uid"]!=null)
            {
                Session["UID"] = Request.Form["uid"];
                if (Session["UID"] != null)
                    Response.Write("<script>alert('登陆成功：" + Session["UID"] + "')</script>");
            }
        }
    }
}