﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite
{
    public partial class Default1 : System.Web.UI.MasterPage,System.Web.SessionState.IRequiresSessionState
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UID"] != null)
            {
                this.Aspx_UserCenter.Visible = true;
                this.Aspx_Friend.Visible = false; //this.Aspx_Friend.Visible = true;
                this.Aspx_login.Visible = false;
                this.Aspx_UserData.Visible = false; //this.Aspx_UserData.Visible = true;
                this.Aspx_logOut.Visible = true;

                this.Aspx_SiteMessage.Visible = false; //this.Aspx_SiteMessage.Visible = true;
            }
            else
            {
                this.Aspx_UserCenter.Visible = false;
                this.Aspx_Friend.Visible = false;
                this.Aspx_login.Visible = true;
                this.Aspx_UserData.Visible = false;
                this.Aspx_logOut.Visible = false;

                this.Aspx_SiteMessage.Visible = false;
            }
        }

    }
}