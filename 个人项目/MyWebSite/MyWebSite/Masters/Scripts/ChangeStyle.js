﻿function $$(a) { return document.getElementById(a); }
//获取本页面URL中的参数集合
function GetLocalUrlParameter() {
    //获得地址中包含的参数
    if (document.location.href.lastIndexOf("?") > 0) {
        var p = document.location.href.substring(document.location.href.lastIndexOf("?") + 1, document.location.href.length);
        var ptext = p.split('&');
        var pobj = new Object();
        for (var i in ptext) {
            var obj = { key: ptext[i].substring(0, ptext[i].indexOf('=')), value: ptext[i].substring(ptext[i].indexOf('=') + 1, ptext[i].length) };
            pobj[i] = obj;
            //alert("参数:"+obj.key+"　值:"+obj.value);
        }
        //for (var i in pobj)
        //{
        //    alert(i+".参数:" + pobj[i].key + "　值:" + pobj[i].value);
        //}
        return pobj;
    }
}

//从 {object key,object value} 类型的集合中获取指定key值得元素值
function GetValueByKey(dictionary, pname) {
    for (var i in dictionary) {
        if (dictionary[i].key == pname) {
            return dictionary[i].value;
        }
    }

}

//从本页面URL中获取指定名称的参数值
function GetLocalUrlParameterByKey(pname) {
    return GetValueByKey(GetLocalUrlParameter(), pname);
}



function toggleBgImage() {
    if (document.getElementById("SystemBg"))
    {
        document.getElementsByTagName("head")[0].removeChild(document.getElementById("SystemBg"));
        localStorage.removeItem("SystemBg");
    }
    else {
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", "/Masters/StyleSheet/Bg.css");
        fileref.setAttribute("id", "SystemBg");
        document.getElementsByTagName("head")[0].appendChild(fileref);
        localStorage.setItem("SystemBg", "true");
    }
    //$$("html").style.background = "url( ../0.png )";
    //$$("html").style.backgroundSize = "cover";
    //$$("html").style.backgroundRepeat = "no-repeat";
    //$$("html").style.backgroundAttachment = "fixed";
    //$$("Body").style.background = "none";
}
function SetBgImage()
{
    toggleBgImage();
}
function SetStyle(filename) {

    if (document.getElementById("SystemAddStyle"))
        document.getElementsByTagName("head")[0].removeChild(document.getElementById("SystemAddStyle"));
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename);
    fileref.setAttribute("id", "SystemAddStyle");
    document.getElementsByTagName("head")[0].appendChild(fileref);
}
function AutoLoadStyle() {

    if (new Date().getHours() > 18 || new Date().getHours() < 7) {
        SetStyle("/Masters/StyleSheet/Night.css");
    }
    else {
        SetStyle("/Masters/StyleSheet/Default.css");
    }
}

function ChangedColor(color) {
    SetStyle("/Masters/StyleSheet/" + color);
    localStorage.setItem("StyleName", color);
}

function AutoChangeStyle() {
    var childNodes = document.getElementById("StyleChangeList").getElementsByTagName("li");
    childNodes(parseInt(Math.random() * childNodes.length)).click();
}


var style = GetLocalUrlParameterByKey("style");
if (style != undefined) {
    switch (style.toLowerCase()) {
        case "night": { SetStyle("/Masters/StyleSheet/Night.css"); break; }
        case "default": { SetStyle("/Masters/StyleSheet/Default.css"); break; }
        case "pink": { SetStyle("/Masters/StyleSheet/Pink.css"); break; }
        case "green": { SetStyle("/Masters/StyleSheet/Green.css"); break; }
        case "orange": { SetStyle("/Masters/StyleSheet/Orange.css"); break; }
        default:
            {
                AutoLoadStyle();
            }
    }
}
else if (localStorage.getItem("StyleName") != null) {
    ChangedColor(localStorage.getItem("StyleName").toString());
}
else AutoLoadStyle();

if (localStorage.getItem("SystemBg") != null) {
    toggleBgImage();
}

   