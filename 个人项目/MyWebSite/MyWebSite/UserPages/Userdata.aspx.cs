﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite
{
    public partial class Userdata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<DBModel.UserDataModel> userData;
            if (!string.IsNullOrWhiteSpace(Request.Params["DataIndex"]))//浏览指定用户的资料
            {
                var userId = (Request.Params["DataIndex"] as string).Trim();
                userData = MSDB.GetUserDataByID(userId);
                if (userId == (Session["UID"] as string))
                {
                    Aspx_EditTheData.Visible = true;
                    if (userData == null || userData.Count() < 1)//未找到用户的资料信息
                    {
                        userData = new List<DBModel.UserDataModel>() { new DBModel.UserDataModel() { IndexID = Session["UID"] as string } };
                    }
                }
                else
                {
                    Aspx_EditTheData.Visible = false;
                    if (userData == null || userData.Count() < 1)//未找到用户的资料信息
                    {
                        Response.Redirect("UserCenter.aspx", true);//返回用户中心
                    }
                }
            }
            else if (Session["UID"] != null)//浏览自己的资料
            {
                userData = MSDB.GetUserDataByID((Session["UID"] as string).Trim());
                Aspx_EditTheData.Visible = true; 
                if (userData == null || userData.Count() < 1)//未找到用户的资料信息
                {
                    userData = new List<DBModel.UserDataModel>() { new DBModel.UserDataModel() { IndexID = Session["UID"] as string } };
                }
            }
            else
            {
                Response.Redirect("UserCenter.aspx", true);//返回用户中心
                return;
            }

            this.Email = userData[0].Email;
            this.WebSite = string.IsNullOrWhiteSpace(userData[0].WebSite) ? "未设置个人网站" : userData[0].WebSite;
            this.Idol = userData[0].Idol;
            this.Maxim = userData[0].Maxim;
            this.Constellation = userData[0].Constellation;
            this.Dream = userData[0].Dream;
            this.Word = userData[0].Word;
            this.Address = userData[0].Address;
            this.Love = userData[0].Love;
            this.MeRemark = userData[0].MeRemark;
            this.Nickname = userData[0].Nickname ?? "无名游侠";
            this.HeadPic = userData[0].HeadPic;
            this.Signature = userData[0].Signature ?? "这家伙很懒，什么都没有留下";
            this.Sex = userData[0].Sex;
            this.Birthday = userData[0].Birthday;
            this.MobilePhone = userData[0].MobilePhone;
            this.TelPhone = userData[0].TelPhone;

            this.DataBind();
        }


        public string Email         { get; set; } //电子邮件
        public string WebSite       { get; set; }//个人网站
        public string Idol          { get; set; } //偶像
        public string Maxim         { get; set; }//格言
        public string Constellation { get; set; }//星座
        public string Dream         { get; set; }//梦想
        public string Word          { get; set; } //工作
        public string Address       { get; set; }//地址
        public string Love          { get; set; }//爱好
        public string MeRemark      { get; set; }//个人说明
        public string Nickname      { get; set; }//昵称
        public byte[] HeadPic       { get; set; }//头像
        public string Signature     { get; set; }//签名
        public bool? Sex            { get; set; }//性别
        public DateTime? Birthday   { get; set; }//生日
        public string MobilePhone   { get; set; }//手机
        public string TelPhone      { get; set; }//电话

    }
}