﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="MyWebSite.UserPages.Register" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head">新用户注册</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="script/UserInfoCheck.js"></script>
            <div id="loginPanel">
                <table>
                    <tr class="inputBoxBorder">
                        <td>
                            <img class="SmallIcon" src="BlackImg/user.png" />
                        </td>
                        <td>
                            <input id="uid" class="ShortTextBox" type="text" placeholder="用户名/邮箱" onkeydown="ClerErrorText()"/>
                        </td>
                        <td>
                            <a class="otherlink" href="Login.aspx">已有账号</a>
                        </td>
                    </tr>
                    <tr class="inputBoxBorder">
                        <td>
                            <img class="SmallIcon" src="BlackImg/key.png" />
                        </td>
                        <td>
                            <input id="pwd" class="ShortTextBox" type="password" placeholder="登录密码" onkeydown="ClerErrorText()"/>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr class="inputBoxBorder">
                        <td>
                            <img class="SmallIcon" src="BlackImg/key2.png" />
                        </td>
                        <td>
                            <input id="pwd2" class="ShortTextBox" type="password" placeholder="确认密码" onkeydown="ClerErrorText()"/>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align:left">
                            <span id="loginState">完成输入点‘确定’注册</span>
                        </td>
                        <td>
                            <input class="SmallTextButton" type="button" value="确定" onclick="GoRegister()" />
                        </td>
                    </tr>
                </table>
            </div>
</asp:Content>
