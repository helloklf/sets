﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserCenter.aspx.cs" Inherits="MyWebSite.UserPages.UserCenter1" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head"><%# UserName %>：的主页</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="Dynamic">
        <div class="LeftBlock">
            <div class="BigBlock" runat="server" id="WebSiteQuickLinks" visible="false">
                <h1>功能列表</h1>
                <div class="WebSiteQuickLinks">
                    <a href="/Messages/ArticleAdd.aspx" title="发表新的文章">
                        <img src="../Resource/appbar.page.add.png"/>
                        <br />
                        新的文章
                    </a>
                    
                    <a href="/Messages/WorksAdd.aspx" title="展示新的项目" >
                        <img src="../Resource/appbar.resource.png" />
                        新的项目
                    </a>
                    
                    <%--<a href="/Share/AddShareData.aspx" title="共享新的资源">
                        <img src="../Resource/appbar.social.sharethis.png"/>
                        共享资源
                    </a>--%>
                    
                    <a href="/UserPages/DynamicInfos.aspx?DataIndex=<%# DataIndex %>" title="时光轴">
                        <img src="../Resource/appbar.timer.png"/>
                        记录时间
                    </a>
                </div>
            </div>
            <div class="BigBlock">
                <h1>时光轴</h1>
                <ul id="TimeLines">
                    <%# DynamicInfoHtml %>
                </ul>
                <a class="BottonLink" href="/UserPages/DynamicInfos.aspx?DataIndex=<%# DataIndex %>">更多···</a>
            </div>
        </div>
        <div class="RightBlock">
            <div class="MiniBlock">
                <h2 title="关于本人的一些基本信息">个人档案</h2>
                <div  class="UserInfoBlock">
                    <div style="display:inline-block">
                        <img src="../Resource/DefaultHeadPic.jpg"  class="UserInfoRow_Img"/>
                    </div>
                    <div class="UserInfoBlock">
                        <div class="UserInfoRow" style="font-size:25px"><%# UserName %></div>
                        <div class="UserInfoRow">邮箱：<%# Email %></div>
                        <div class="UserInfoRow">手机：<%# MobilePhone %></div>
                        <div class="UserInfoRow">性别：<%# Sex %></div>
                        <div class="UserInfoRow">星座：<%# Constellation %> </div>
                        <div class="UserInfoRow">签名：<%# Signature %> </div>
                        <div class="UserInfoRow">
                            <a href="/UserPages/UserdataEdit.aspx" runat="server" id="Aspx_EditTheDetails">修改资料</a>
                        </div>
                    </div>
                </div>
                <a class="BottonLink" href="/UserPages/Userdata.aspx?DataIndex=<%# DataIndex %>">查看详细···</a>
            </div>
            <div class="MiniBlock">
                <h2 title="浏览该用户展示的项目">近期项目</h2>
                <ul>
                    <%# WorksHtml %>
                </ul>
                <a class="BottonLink" href="/Messages/WorksList.aspx?DataIndex=<%# DataIndex %>">更多精彩···</a>
            </div>
            <%--<div class="MiniBlock">
                <h2 title="看看该用户最近共享的资源">共享资源</h2>
                <ul>
                    <%# ShareDatasHtml %>
                </ul>
                <a class="BottonLink" href="ShareData.aspx">更多精彩···</a>
            </div>--%>
            <div class="MiniBlock">
                <h2 title="该用户发表的原创或转载文章">最新文章</h2>
                <ul>
                    <%# ArticlesHtml %>
                </ul>
                <a class="BottonLink" href="../Messages/ArticleList.aspx">更多精彩···</a>
            </div>
        </div>
    </div>
</asp:Content>
<%--
<li>
    <div style="float:left;">文章标题</div>
    <div style="float:right">2014-11-12 12:12:12</div>
    <div style="clear:both;"></div>
</li>--%>