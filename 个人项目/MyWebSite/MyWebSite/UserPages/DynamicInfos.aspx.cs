﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.UserPages
{
    public partial class DynamicInfos : System.Web.UI.Page
    {
        public string UserName { get; set; } //用户名

        protected void Page_Load(object sender, EventArgs e) { LoadData(); }

        void LoadData()
        {
            string userId = null;

            if (!string.IsNullOrWhiteSpace(Request.Params["DataIndex"]))
            {
                userId = Request.Params["DataIndex"];
                if (userId == (Session["UID"] as string))
                {
                    Aspx_InsertNewData.Style.Add("display", "block");
                }
                else
                {
                    Aspx_InsertNewData.Style.Add("display", "none");
                }
            }
            else if (Session["UID"] != null)
            {
                userId = (Session["UID"] as string);
                Aspx_InsertNewData.Visible = true;
            }
            else
            {
                Response.Redirect("UserCenter.aspx");
                Aspx_InsertNewData.Visible = false;
            }

            var userData = MSDB.GetUserDataByID(userId);//获取用户资料

            //错误防御：未找到用户的资料信息
            if (userData == null || userData.Count() < 1)
            {
                userData = new List<DBModel.UserDataModel>() { new DBModel.UserDataModel() { IndexID = Session["UID"] as string } };
            }
            //设置默认昵称
            this.UserName = userData[0].Nickname ?? "无名游侠";
        }
    }
}