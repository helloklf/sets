﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="[New]FriendList.aspx.cs" Inherits="MyWebSite.UserPages.FriendList" MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">好友列表</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style>
        #ChatPanel{
            color:#808080; margin:0px; width:100%;padding:0px;
        }
        #ChatPanel li ,#LikeUserList li{
                width:225px; float:left; background:rgba(255,255,255,0.4); margin:3px;color:#000;
        }
        #ChatPanel li:hover{
            color:#fff;
        }
        #ChatPanel li span {
            clear:both;display:block;font-size:11px; padding:6px 10px; font-weight:600;
        }
        #ChatPanel li span:first-child{
            clear:both;display:block; font-size:18px; font-weight:900;
        }
        #ChatPanel li:last-child {
            text-align: center; width: auto; font-size: 25px; list-style: none; float: none; clear: both;margin:10px 0px 0px 0px; border-top:2px solid #000
            }
        .PageTitle {
            float:none; display:block; margin:5px; clear:both; width:76px; padding:0px; text-align:center; font-size:20px;background:#808080
        }
        #FriendListTitle {
            width:100%;
        }
        #FriendListTitle .PageTitle:hover {
            background:#ccc
        }
        #FriendListTitle .PageTitle:active {
            background:#fff
        }
        #FriendListTitle {
            margin:0px; padding:0px; width:100%;
        }
        #LikeUserList li {
            border-bottom:2px solid #ccc; padding:5px; margin:2px; color:#000; vertical-align:middle; font-size:12px;
        }
        #LikeUserList li:hover {
            background:#ccc;
        }
        #LikeUserList li span:first-child {
            font-size:18px; font-weight:900;
        }
        .LikeIdInput {
            font-size:20px; ime-mode:disabled;  background:none; color:#808080; margin-left:5px; border:0px; border-bottom:1px solid #808080;
        }
    </style>
    <script>
        function FriendClick(indexId)
        {
            
        }
        function GetFriendListPostBack(response)
        {
            try{
                var friends = JSON.parse(response);
                var list = document.getElementById("ChatPanel");
                while (list.childNodes.length>2)
                {
                    list.removeChild(list.firstChild);
                }
                for (var i in friends)
                {
                    var li = document.createElement("li"); li.className = "friendItem";
                    var span1 = document.createElement("span"); span1.textContent = friends[i].FriendRemark;
                    var span2 = document.createElement("span"); span2.textContent = friends[i].DataIndex;
                    var span3 = document.createElement("span"); span3.textContent = friends[i].DataIndex;
                    li.appendChild(span1); li.appendChild(span2); li.appendChild(span3);
                    list.insertBefore(li, list.childNodes[list.childNodes.length - 2]);
                }
            }
            catch (e) { document.getElementById("loadstate").textContent = "－－－数据解析错误－－－"; }
        }
        function RefreshData()
        {
            GetFriendList('loadstate', GetFriendListPostBack)
        }
        function SwitchthePanel(_openid, _panel) {
            var list = document.getElementsByClassName("page");
            for (var i in list) {
                list[i].hidden = true;
            }
            document.getElementById(_openid).hidden = false;
        }
        function GetLikeUserPostBack(response) {
            var likelist = JSON.parse(response);
            var panel = document.getElementById("LikeUserList");
            while (panel.childNodes.length > 1) {
                panel.removeChild(panel.childNodes[0]);
            }
            for (var i in likelist) {
                var li = document.createElement("li");
                var span1 = document.createElement("span"); span1.textContent = "昵称：" + likelist[i].Nickname;
                var span2 = document.createElement("span"); span2.textContent = "账号：" + likelist[i].LoginID;
                li.setAttribute("onclick", "AddFriend('" + likelist[i].IndexID + "')");
                li.appendChild(span1);
                li.appendChild(document.createElement("p"));
                li.appendChild(span2);
                panel.appendChild(li);
            }
        }
        function GetLikeUser() {
            var value = event.srcElement.value;
            if (value.length > 5) {
                try {
                    AjaxGetLikeUser(value.trim(), GetLikeUserPostBack);
                }
                catch (e) { alert(e.message); }
            }
        }
        function AddFriendPostBack(response) {
            alert(response);
        }
        function AddFriend(friendid)
        {
            var nick = prompt("请为您的好友设置你备注(显示名称)：", "新好友");
            if (nick) {
                try {
                    AjaxAddUserFriend(friendid, nick, AddFriendPostBack);
                }
                catch (e) {
                    alert(e.message);
                }
            }
        }
    </script>
    <script src="../JavaScripts/Account/GetFriendList.js"></script>
    <script src="../JavaScripts/Account/AjaxGetLikeUser.js"></script>
    <script src="../JavaScripts/Account/AjaxAddUserFriend.js"></script>
    <div>
        <div id="FriendListTitle" style="float:left; width:90px;">
            <span class="PageTitle" id="T_message" onclick="SwitchthePanel('P1','pages')"><img src="../Resource/message.png"/></span>
            <span class="PageTitle" id="T_friend" onclick="SwitchthePanel('P2','pages')"><img src="../Resource/group.png"/></span>
            <span class="PageTitle" id="T_addfriend" onclick="SwitchthePanel('P3','pages')"><img src="../Resource/group.add.png"/></span>
        </div>
        <div id="pages" style="background:rgba(255,255,255,0.2);width:100%;">

            <div id="P1" class="page">
                <ul>
                    <li>

                    </li>
                </ul>
                <br style="clear:both"/>
            </div>

            <div id="P2" hidden="hidden" class="page">
                <ul id="ChatPanel">
                    <li id="loadstate" onclick="RefreshData()">···点我刷新···</li>
                </ul>
            </div>

            <div id="P3" hidden="hidden" class="page">
                <p style="padding:20px">
                    搜索账号<input type="url" maxlength="36" class="LikeIdInput" onkeyup="GetLikeUser()" onkeydown="if(event.keyCode==13){ GetLikeUser() }"/>
                </p>
                <ul id="LikeUserList">
                    <li style="clear:both; display:none;"></li>
                </ul>
                <br style="clear:both"/>
            </div>


        </div>
    </div>
</asp:Content>