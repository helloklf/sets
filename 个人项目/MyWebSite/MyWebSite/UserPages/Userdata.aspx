﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Userdata.aspx.cs" Inherits="MyWebSite.Userdata" MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">用户资料</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="UserDataDetails" >
        <div class="UserdataTop">
            <img class="headerImgStyle" src="../Resource/DefaultHeadPic.jpg" />
            <div class="One">
                <span style="font-size: 35px"><%# Nickname %></span>
                <hr style="opacity: 0.3" />
                <span style="font-size: 20px"><%# WebSite %></span>
                <hr style="opacity: 0.05; height: 1px" />
                <span style="font-size: 20px">性别：<%# Sex==true?"♂":Sex==false?"♀":"未知" %></span>
                <hr style="opacity: 0.05; height: 1px" />
                <span style="font-size: 18px; width: 100px;"><%# Signature %></span>
            </div>
        </div>
        <br />
        <div id="dataDetailsRows">
            <span>电子邮件：<%# Email %></span>
            <hr/>
            <span>固定电话：<%# TelPhone %></span>
            <hr/>
            <span>手机号码：<%# MobilePhone %></span>
            <hr/>
            <span>生　　日：<%# Birthday %></span>
            <hr/>
            <span>偶　　像：<%# Idol %></span>
            <hr/>
            <span>格　　言：<%# Maxim %></span>
            <hr/>
            <span>星　　座：<%# Constellation %></span>
            <hr/>
            <span>梦　　想：<%# Dream %></span>
            <hr/>
            <span>工　　作：<%# Word %></span>
            <hr/>
            <span>地　　址：<%# Address %></span>
            <hr/>
            <span>爱　　好：<%# Love %></span>
            <hr/>
            <span>个人说明：<%# MeRemark %></span>
            <hr/>
        </div>
        <br />
        <div class="Detailsfooter">
            <a runat="server" id="Aspx_EditTheData" href="UserdataEdit.aspx">修改资料</a>
        </div>
    </div>
</asp:Content>