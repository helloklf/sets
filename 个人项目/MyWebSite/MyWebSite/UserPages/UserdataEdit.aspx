﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserdataEdit.aspx.cs" Inherits="MyWebSite.UserPages.UserdataEdit" MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">修改资料</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <form runat="server">
        <div id="UserDataDetailsEdit">
            <div class="UserdataTop">
                <img class="headerImgStyle" src="../Resource/DefaultHeadPic.jpg" />
                <div class="One">
                    <span style="font-size: 35px">
                        <asp:TextBox runat="server" ID="Aspx_Nickname" placeholder="个性称谓"  style="font-size: 35px;" Text='<%# Nickname %>' ></asp:TextBox></span>
                    <hr style="opacity: 0" />
                    <span style="font-size: 20px">
                        <input type="text" runat="server" id="Aspx_WebSite" placeholder="你的个人网站" value='<%# WebSite %>' /></span>
                    <hr style="opacity: 0.05; height: 1px" />
                    <span style="font-size: 20px">
                        <asp:RadioButton runat="server" ID="Asp_Sex1" GroupName="Sex" Checked='<%# Sex==true %>' Text="男"/>
                        <asp:RadioButton runat="server" ID="Asp_Sex2" GroupName="Sex" Checked='<%# Sex==false %>' Text="女" style="margin-left:15px"/>
                    </span>
                    <hr style="opacity: 0.05; height: 1px" />
                    <span style="font-size: 18px; width: 100px;">
                        <input type="text" runat="server" placeholder="个性签名让大家更容易了解你的Style" id="Aspx_Signature" value='<%# Signature %>' /></span>
                </div>
            </div>
            <br />
            <div id="dataDetailsRows">
                <span>电子邮件：<asp:TextBox ID="Aspx_Email" TextMode="Email" runat="server" placeholder="格式：xxxx@xx.xx" type="email" Text='<%# Email %>' /></span>
                <hr />
                <span>固定电话：<input runat="server" id="Aspx_TelPhone" type="tel" value='<%# TelPhone %>' /></span>
                <hr />
                <span>手机号码：<input runat="server" id="Aspx_MobilePhone"  type="tel" value='<%# MobilePhone %>' /></span>
                <hr />
                <span>生　　日：<input runat="server" id="Aspx_Birthday" placeholder="如：2000-01-01" type="date" value='<%# Birthday %>' /></span>
                <hr />
                <span>偶　　像：<input runat="server" id="Aspx_Idol" type="text" value='<%# Idol %>' /></span>
                <hr />
                <span>格　　言：<input runat="server" id="Aspx_Maxim" type="text" value='<%# Maxim %>' /></span>
                <hr />
                <span>星　　座：<input runat="server" id="Aspx_Constellation" type="text" list="ConstellationDlist" value='<%# Constellation %>' /></span>
                <datalist id="ConstellationDlist">
                    <option label="白羊座-03.21-04.19" value="白羊座"></option>
                    <option label="金牛座-04.20-05.20" value="金牛座"></option>
                    <option label="双子座-05.21-06.21" value="双子座"></option>
                    <option label="巨蟹座-06.22-07.22" value="巨蟹座"></option>
                    <option label="狮子座-07.23-08.22" value="狮子座"></option>
                    <option label="处女座-08.23-09.22" value="处女座"></option>
                    <option label="天秤座-09.23-10.23" value="天秤座"></option>
                    <option label="天蝎座-10.24-11.22" value="天蝎座"></option>
                    <option label="射手座-11.23-12.21" value="射手座"></option>
                    <option label="摩羯座-12.22-01.19" value="摩羯座"></option>
                    <option label="水瓶座-01.20-02.18" value="水瓶座"></option>
                    <option label="双鱼座-02.19-03.20" value="双鱼座"></option>
                </datalist>
                <hr />
                <span>梦　　想：<input runat="server" id="Aspx_Dream" type="text" value='<%# Dream %>' /></span>
                <hr />
                <span>工　　作：<input runat="server" id="Aspx_Word" type="text" value='<%# Word %>' /></span>
                <hr />
                <span>地　　址：<input runat="server" id="Aspx_Address" type="text" value='<%# Address %>' /></span>
                <hr />
                <span>爱　　好：<input runat="server" id="Aspx_Love" type="text" value='<%# Love %>' /></span>
                <hr />
                <span>个人说明：<input type="text" runat="server" id="Aspx_MeRemark" value='<%# MeRemark %>' /></span>
                <hr/>
            </div>
            <br />
            <div class="Detailsfooter">
                <input type="submit" runat="server" id="Aspx_EditTheData" value="保存修改" />
            </div>
        </div>
    </form>
</asp:Content>
