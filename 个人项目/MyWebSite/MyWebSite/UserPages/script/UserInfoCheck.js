﻿//显示请求状态文本（登陆和注册共用）
function setLoginState(text) {
    $id("loginState").textContent = text;
}

//清理错误文本（登陆和注册共用）
function ClerErrorText()
{
    setLoginState("");
}

//验证账号密码格式（登陆和注册共用）
function checkUidPwd(uid,pwd)
{
    if (uid.match(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/) == null && uid.match(/^[\w]{5,17}$/) == null) {
        alert("账号格式不符合规范（仅支持 邮箱地址 或 6-18数字、字母下划线和点组成的账号"); return false;
    }
    if (pwd.match(/^\w{5,18}$/) == null)
    {
        alert("密码长度不符合规范（仅支持6-32位字母、数字、下划线组成的账号"); return false;
    }
    return true;
}

//登陆
function GoLogin()
{
    var uid = $id("uid").value.trim();
    var pwd = $id("pwd").value.trim();
    if (checkUidPwd(uid, pwd))
    {
        setLoginState("正在为您效劳,请稍等...");
        var request = new XMLHttpRequest();
        request.onreadystatechange =
            function () {
                if (request.readyState == 4) {
                    if (request.status == 200)
                    {
                        var response = request.responseText;
                        if (response == "pass")
                        {
                            window.location.href = "/MainPages/Default.aspx"; 
                        }
                        else if (response == "error") {
                            setLoginState("糟糕,账号或者密码错了！");
                        }
                        else
                        {
                            setLoginState("对不起,服务器出现错误,需要维护...");
                        }
                    }
                    else {
                        setLoginState("欸也,又开小差了...");
                    }
                }
            };

        var fd = new FormData();
        fd.append("uid", uid);
        fd.append("pwd", pwd);

        request.open("POST", "/Processing/Account/LoginAshx.ashx", true);
        request.send(fd);
    }
}

//注册
function GoRegister()
{
    var uid = $id("uid").value.trim();
    var pwd = $id("pwd").value.trim();
    var pwd2 = $id("pwd2").value.trim();
    if (checkUidPwd(uid, pwd))//验证输入是否有效
    {
        if (pwd != pwd2)
        {
            setLoginState("两次输入的密码不一致哦！");
        }
        else
        {
            setLoginState("正在为您效劳,请稍等..");
            var request = new XMLHttpRequest();
            request.onreadystatechange =
                function ()
                {
                    if (request.readyState == 4)
                    {
                        if (request.status == 200)
                        {
                            var response = request.responseText;
                            if (response == "pass")
                            {
                                setLoginState("注册成功,正转到登陆页...");
                                window.location.href="../UserPages/Login.aspx";
                            }
                            else if (response == "accountisexist") {
                                setLoginState("糟糕,这个账号已经被占用了");
                            }
                            else if (response == "error") {
                                setLoginState("由于未知错误,注册失败!");
                            }
                            else if (response == "servererror") {
                                setLoginState("对不起,服务器出现错误,需要维护");
                            }
                        }
                        else
                        {
                            setLoginState("欸也,又开小差了...");
                        }
                    }
                };
            var fd = new FormData();
            fd.append("uid", uid);
            fd.append("pwd", pwd);

            request.open("POST", "/Processing/Account/RegisterAshx.ashx", true);
            request.send(fd);
        }
    }
}