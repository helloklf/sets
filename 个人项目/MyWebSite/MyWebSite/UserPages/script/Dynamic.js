﻿var fd = new FormData();
fd.append("DataIndex", Tools_ParamByKey("DataIndex"));
function GetMoreTimeLines() {
    Tools_AjaxRequest("/Processing/DynamicInfo/GetMoreData.ashx?StartIndex=" + ($id("TimeLines").childElementCount), fd, "POST",
        function (c) {
            var jData = JSON.parse(c);
            if (jData.length < 1)
                $id("GetMoreDataBtn").textContent = "没有更多了！";
            for (var i in jData)
            {
                var li = ConvertToLiItem(jData[i]);
                TimeLines.appendChild(li);
            }
        },
        function (e) {
            alert(e);
        });
}

function ConvertToLiItem(item) {
    var li = document.createElement("li");
    li.id = item.DataIndex;
    li.innerHTML = "<div class='LineLeftBlock'><div class='LineTitle'>【" + item.DocTitle + "】</div><div class='LineContent'><blockquote>【" + item.DocData + "】</blockquote></div></div><div class='LineDateTime'>" + item.DateTimeText + "<a href='#' onclick='DeleteTheItem(\"" + item.DataIndex + "\")' style='margin-left:10px;vertical-align:middle'>✖</a></div><div class='clear'></div>";
    return li;
}

function DeleteTheItem(indexId) {
    if (confirm("确定要删除这条记录吗？（无法恢复）")) {
        Tools_AjaxRequest("/Processing/DynamicInfo/Delete.ashx?DataIndex=" + indexId, null, "GET",
            function (c) {
                $id("TimeLines").removeChild($id(indexId));
            },
            function (e) {
                alert(e);
            });
    }
}

function GetLastInsert() {
    Tools_AjaxRequest("/Processing/DynamicInfo/GetLastInsert.ashx", null, "GET",
                function (c) {
                    $id("TimeLines").insertBefore(ConvertToLiItem(JSON.parse(c)), $id("TimeLines").childNodes[2]);
                },
                function (e) {
                    alert(e);
                });
}

function InsertItem() {
    var input_content = $id("Input_Content").value;
    var input_title = $id("Input_Title").value;
    if (input_content == "") {
        return;
    }
    var fd = new FormData();
    fd.append("content", input_content);
    fd.append("title", input_title);

    Tools_AjaxRequest("/Processing/DynamicInfo/Insert.ashx", fd, "POST",
        function (c) {
            GetLastInsert();
            $id("Input_Content").value = "";
            $id("Input_Title").value = "";
        },
        function (e) {
            alert(e);
        });
}