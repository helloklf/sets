﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.UserPages
{
    public partial class UserCenter1 : System.Web.UI.Page
    {
        public StringBuilder ArticlesHtml = new StringBuilder();//文章
        public StringBuilder ShareDatasHtml = new StringBuilder();//共享资源
        public StringBuilder WorksHtml = new StringBuilder();//项目
        public StringBuilder DynamicInfoHtml = new StringBuilder();//动态
        public StringBuilder AchievementsHtml = new StringBuilder();//成就

        #region 

        string userName = "无名大虾";
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get
            {
                return userName;
            }
            set { userName = value; }
        }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 账号索引
        /// </summary>
        public string DataIndex { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 星座
        /// </summary>
        public string Constellation { get; set; }

        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            string userId = null;
            if (!string.IsNullOrWhiteSpace(Request.Params["DataIndex"]))
            {
                userId = Request.Params["DataIndex"];
                if (userId==(Session["UID"] as string))
                    Aspx_EditTheDetails.Visible = true;
                else
                    Aspx_EditTheDetails.Visible = false;
            }
            else if (Session["UID"] != null)
            {
                userId = (Session["UID"] as string);
                Aspx_EditTheDetails.Visible = true;
                WebSiteQuickLinks.Visible = true;
            }
            else
            {
                Aspx_EditTheDetails.Visible = false;
            }


            if (!string.IsNullOrWhiteSpace(userId))
            {
                
                List<DBModel.ArticleModel> Articles = MSDB.GetArticle_OneUser(0,userId);//文章
                List<DBModel.ShareDataModel> ShareDatas = MSDB.GetShareData();//共享资源
                List<DBModel.WorksModel> Works = MSDB.GetWorks_OneUser(0, userId);//项目
                List<DBModel.DynamicInfoModel> DynamicInfo = MSDB.GetDynamicInfo_OneUser(0, userId);//动态

                var userData = MSDB.GetUserDataByID(userId);
                if (userData == null || userData.Count() < 1)//未找到用户的资料信息
                {
                    userData = new List<DBModel.UserDataModel>() { new DBModel.UserDataModel() { IndexID = Session["UID"] as string } };
                }
                this.Sex = userData[0].Sex == true ? "男" : userData[0].Sex == false ? "女" : "未知";
                this.UserName = userData[0].Nickname ?? "无名游侠";
                this.MobilePhone = userData[0].MobilePhone ?? "保密";
                this.DataIndex = userData[0].IndexID;
                this.Constellation = userData[0].Constellation ?? "未设置";
                this.Email = userData[0].Email ?? "未设置";
                this.Signature = userData[0].Signature ?? "这家伙很懒，什么都没有留下！";


                //最近文章
                for (int i = 0; i < 5 && i < Articles.Count; i++)
                {
                    var item = Articles[i];
                    ArticlesHtml.Append(
                                    string.Format(
                                   "<li id='{0}' title='点击查看详细' onclick='window.open(\"/Messages/ArticleDetails.aspx?DataIndex={0}\")'><div style='float:left;'>{1}</div><div style='float:right'>{2}</div><div style='clear:both;'></div></li>", item.DataIndex, item.DocTitle, item.DateTimeText));
                }

                //最近共享的资源
                for (int i = 0; i < 5 && i < ShareDatas.Count; i++)
                {
                    var item = ShareDatas[i];
                    ShareDatasHtml.Append(
                                    string.Format(
                                   "<li id='{0}' title='点击查看详细' onclick='window.open(\"{1}\")'><div style='float:left;'>{2}</div><div style='float:right'>{3}</div><div style='clear:both;'></div></li>", item.DataIndex, item.SourceURL, item.SourceTitle, item.DateTimeText));
                }

                //最近发布的项目
                for (int i = 0; i < 5 && i < Works.Count; i++)
                {
                    var item = Works[i];
                    WorksHtml.Append(
                                    string.Format(
                                   "<li id='{0}' title='点击查看详细' onclick='window.open(\"/Messages/WorksDetails.aspx?DataIndex={0}\")'><div style='float:left;'>{1}</div><div style='float:right'>{2}</div><div style='clear:both;'></div></li>", item.DataIndex, item.DocTitle, item.DateTimeText));
                }

                //动态
                for (int i = 0; i < 20 && i < DynamicInfo.Count; i++)
                {
                    var item = DynamicInfo[i];
                    DynamicInfoHtml.Append(
                                    string.Format(
                                    "<li id='{0}'><div class='LineLeftBlock'><div class='LineTitle'>【{1}】</div><div class='LineContent'><blockquote>【{2}】</blockquote></div></div><div class='LineDateTime'>{3}</div><div class='clear'></div></li>", item.DataIndex, item.DocTitle, item.DocData, item.DateTime == null ? "" : item.DateTimeText));
                }
            }
            else
            {
                Session["WantTo"] = "/UserPages/UserCenter.aspx";
                Response.Redirect("/UserPages/Login.aspx", true);
            }
            DataBind();
        }


    }
}