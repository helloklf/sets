﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MyWebSite.UserPages.Login" MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">用户登陆</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="script/UserInfoCheck.js"></script>
    <div id="loginPanel">
        <table>
            <tr class="inputBoxBorder">
                <td>
                    <img class="SmallIcon" src="BlackImg/user.png" />
                </td>
                <td>
                    <input id="uid" name="uid" class="ShortTextBox" type="text" placeholder="用户名/邮箱" onkeydown="ClerErrorText()" />
                </td>
                <td>
                    <a class="otherlink" href="Register.aspx">注册账号</a>
                </td>
            </tr>
            <tr class="inputBoxBorder">
                <td>
                    <img class="SmallIcon" src="BlackImg/key.png" />
                </td>
                <td>
                    <input id="pwd" name="pwd" class="ShortTextBox" type="password" placeholder="登录密码" onkeydown="ClerErrorText()" />
                </td>
                <td>
                    <a class="otherlink" href="PassReset.aspx">忘记密码</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: left">
                    <span id="loginState">完成输入点‘确定’登陆</span>
                </td>
                <td>
                    <input class="SmallTextButton" type="button" value="确定" onclick="GoLogin()" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>