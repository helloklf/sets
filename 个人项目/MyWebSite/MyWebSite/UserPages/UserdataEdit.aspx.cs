﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.UserPages
{
    public partial class UserdataEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UID"] == null)
            {
                Session["WantTo"] = "/UserPages/UserdataEdit.aspx";
                Response.Redirect("Login.aspx");
                return;
            }
            if (IsPostBack)
                SubMit();
            else
                Loaded();
            
        }

        /// <summary>
        /// 进入页面时执行
        /// </summary>
        void Loaded()
        {
            List<DBModel.UserDataModel> userData;
            userData = MSDB.GetUserDataByID((Session["UID"] as string).Trim());

            if (userData == null || userData.Count() < 1)//未找到用户的资料信息
            {
                userData = new List<DBModel.UserDataModel>() { new DBModel.UserDataModel() { IndexID = Session["UID"] as string } };
            }
            this.Email = userData[0].Email;
            this.WebSite = userData[0].WebSite;
            this.Idol = userData[0].Idol;
            this.Maxim = userData[0].Maxim;
            this.Constellation = userData[0].Constellation;
            this.Dream = userData[0].Dream;
            this.Word = userData[0].Word;
            this.Address = userData[0].Address;
            this.Love = userData[0].Love;
            this.MeRemark = userData[0].MeRemark;
            this.Nickname = userData[0].Nickname;
            this.HeadPic = userData[0].HeadPic;
            this.Signature = userData[0].Signature;
            this.Sex = userData[0].Sex;
            this.Birthday = userData[0].Birthday;
            this.MobilePhone = userData[0].MobilePhone;
            this.TelPhone = userData[0].TelPhone;
            this.DataBind();
        }

        /// <summary>
        /// 页面回送时执行
        /// </summary>
        void SubMit()
        {
           var model = new  DBModel.UserDataModel(){
           Address = Aspx_Address.Value ?? "",
           //Birthday = ,
           Constellation = Aspx_Constellation.Value??"",
           Dream = Aspx_Dream.Value ?? "",
           Email = Aspx_Email.Text ?? "",
           //HeadPic 
           Idol = Aspx_Idol.Value ?? "",
           IndexID = Session["UID"] as string,
           Love = Aspx_Love.Value ?? "",
           Maxim = Aspx_Maxim.Value ?? "",
           MeRemark = Aspx_MeRemark.Value ?? "",
           MobilePhone = Aspx_MobilePhone.Value ?? "",
           Nickname = Aspx_Nickname.Text ?? "",
           //Sex = Aspx_Word
           Signature = Aspx_Signature.Value ?? "",
           TelPhone = Aspx_TelPhone.Value ?? "",
           WebSite = Aspx_WebSite.Value ?? "",
           Word = Aspx_Word.Value ?? ""
           };
            DateTime dt ;
           
            //转换日期文本
            if(DateTime.TryParse(Aspx_Birthday.Value,out dt)){;}
            model.Birthday =dt;
            //取得性别
            if(Asp_Sex1.Checked||Asp_Sex2.Checked)
                model.Sex = Asp_Sex1.Checked;

            //修正日期有效性
            if (model.Birthday != null && ((DateTime)model.Birthday).Year < 1800)
                model.Birthday = null;
            
            int  count =  MSDB.UpdateUserData(model);
            if(count>0)
            {
                Response.Redirect("UserCenter.aspx",true);
            }
            else
            {
                Response.Write("<script>alert('修改失败！')</script>");
            }
        }

        public string Email         { get; set; } //电子邮件
        public string WebSite       { get; set; }//个人网站
        public string Idol          { get; set; } //偶像
        public string Maxim { get; set; }//格言
        public string Constellation { get; set; }//星座
        public string Dream         { get; set; }//梦想
        public string Word          { get; set; } //工作
        public string Address       { get; set; }//地址
        public string Love          { get; set; }//爱好
        public string MeRemark      { get; set; }//个人说明
        public string Nickname      { get; set; }//昵称
        public byte[] HeadPic       { get; set; }//头像
        public string Signature     { get; set; }//签名
        public bool? Sex            { get; set; }//性别
        public DateTime? Birthday   { get; set; }//生日
        public string MobilePhone   { get; set; }//手机
        public string TelPhone      { get; set; }//电话
    }


    

}