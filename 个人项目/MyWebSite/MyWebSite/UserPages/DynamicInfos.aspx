﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DynamicInfos.aspx.cs" Inherits="MyWebSite.UserPages.DynamicInfos" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head"><%# UserName %>的动态</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script src="script/Dynamic.js"></script>
  
    <div id="Dynamic">
        <div class="BigBlock">
                <h1><%# UserName %></h1>
                <ul id="TimeLines">
                    <li runat="server" id="Aspx_InsertNewData" style="display:none">
                        <div class="NewDynamicHeader">发表新的内容</div>
                        <input id="Input_Content" placeholder="请输入内容（*必填）" class="NewDynamicContent"/>
                        <div style="margin-top:5px;">
                            <input id="Input_Title" placeholder="请输入标题（ 可空）" class="NewDynamicTitle"/>
                            <input type="button" class="NewDynamicSubMit" value="发表" onclick="InsertItem()"/>
                        </div>
                        <div class='clear'></div>
                    </li>
                </ul>
                <a class="BottonLink" id="GetMoreDataBtn" href="javascript:GetMoreTimeLines()">查看更多···</a>
            </div>
    </div>
    <script>
        GetMoreTimeLines();
    </script>
</asp:Content>
