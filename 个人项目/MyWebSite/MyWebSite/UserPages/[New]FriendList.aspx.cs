﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.UserPages
{
    public partial class FriendList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("UserCenter.aspx",true);

            if (Session["UID"] == null) 
            { 
                Session["WantTo"] = Request.RawUrl; 
                Response.Redirect("Login.aspx", true); 
            }
        }
    }
}