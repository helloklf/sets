﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// GetWorksComments 的摘要说明
    /// </summary>
    public class GetWorksComments : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Request.Params["DataIndex"] != null)
            {
                System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                context.Response.Write(jss.Serialize(MSDB.GetArticleComments(context.Request.Params["DataIndex"])));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}