﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// DeleteArticle 的摘要说明
    /// </summary>
    public class DeleteArticle : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session["UID"] == null)
            {
                context.Response.Write("notlogin");
                return;
            }
            
            var dataIndex = context.Request.Params["DataIndex"];
            if (dataIndex != null)
            {
                try
                {
                    if (!MSDB.AricleValid(context.Request.Params["DataIndex"], context.Session["UID"] as string))//验证是否是本人的文章
                    {
                        context.Response.Write("permissionslow");
                    }
                    else if (MSDB.DeleteArticle(new DBModel.ArticleModel()
                    {
                        DataIndex = dataIndex,
                        IndexID = context.Session["UID"] as string
                    }) < 1)
                    {
                        context.Response.Write("error");
                    }
                    else
                    {
                        context.Response.Write("complete");
                    }
                }
                catch
                {
                    context.Response.Write("servererror");
                }
            }
            else
            {
                context.Response.Write("requesterror");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}