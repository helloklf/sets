﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// UpdateArticleDetails 的摘要说明
    /// </summary>
    public class UpdateArticleDetails : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session["UID"] == null)
            {
                context.Response.Write("notlogin");
                return;
            }

            var title = context.Request.Params["Title"];
            var document = context.Request.Params["Document"];
            var dataIndex = context.Request.Params["DataIndex"];
            if (title != null && document != null && dataIndex != null)
            {
                if (!MSDB.AricleValid(context.Request.Params["DataIndex"], context.Session["UID"] as string))//验证是否是本人的文章
                {
                    context.Response.Write("permissionslow");
                }
                else if (MSDB.UpdateArticle(new DBModel.ArticleModel() 
                {
                    DocTitle =context.Server.UrlDecode( title ), 
                    DocData = context.Server.UrlDecode(document),
                    DataIndex = context.Server.UrlDecode(dataIndex)
                }) > 0)
                {
                    context.Response.Write("complete");
                }
                else
                {
                    context.Response.Write("servererror");
                }
            }
            else
            {
                context.Response.Write("requesterror");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}