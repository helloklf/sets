﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// InsertArticleDetails 的摘要说明
    /// </summary>
    public class InsertArticleDetails : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session["UID"] == null)
            {
                context.Response.Write("notlogin");
                return;
            }
            
            var title = context.Request.Params["Title"];
            var document = context.Request.Params["Document"];
            if (title != null && document != null)
            {
                try
                {
                    if (MSDB.InsertArticle(new DBModel.ArticleModel()
                    {
                        DocTitle = context.Server.UrlDecode(title),
                        DocData = context.Server.UrlDecode(document),
                        IndexID = context.Session["UID"] as string,
                    }) > 0)
                    {
                        context.Response.Write("complete");
                    }
                    else
                    {
                        context.Response.Write("servererror");
                    }
                }
                catch
                {
                    context.Response.Write("servererror");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}