﻿using DBModel;
using MyWebSite.AuthorData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// GetShareData ：获取共享的文档
    /// </summary>
    public class GetShareData : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Request.QueryString["StartIndex"] != null)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                List<ShareDataModel> sml = MSDB.GetShareData(long.Parse(context.Request.Params["StartIndex"]));
                if (sml.Count == 0)
                {
                    context.Response.Write("notmore");
                }
                else context.Response.Write(jss.Serialize(sml));
            }
            else
            {
                List<ShareDataModel> sml = MSDB.GetShareData();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                context.Response.Write(jss.Serialize(sml));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}