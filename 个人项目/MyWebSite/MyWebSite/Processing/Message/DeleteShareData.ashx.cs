﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// DeleteShareData 的摘要说明
    /// </summary>
    public class DeleteShareData : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
        
            if (context.Session["UID"] == null)
            {
                context.Response.Write("notlogin");
                return;
            }
            else
            {
                try
                {
                    var dataIndex = context.Request.Params["DataIndex"];
                    if (dataIndex != null && dataIndex.Trim().Length > 0 && MSDB.DeleteShareData(dataIndex.Trim())>0)
                    {
                        context.Response.Write("complete");
                    }
                    else {
                        context.Response.Write("error");
                    }
                }
                catch
                {
                    context.Response.Write("servererror");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}