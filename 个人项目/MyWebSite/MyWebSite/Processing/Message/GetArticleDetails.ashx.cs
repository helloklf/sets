﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    
    /// <summary>
    /// 获取文章详细内容
    /// </summary>
    public class GetArticleDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Request.Params["DataIndex"] != null)
            {
                var datas = MSDB.GetArticle(context.Request.Params["DataIndex"]);
                var jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                context.Response.Write(jss.Serialize(datas));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}