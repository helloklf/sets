﻿using DBModel;
using MyWebSite.AuthorData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Message
{
    /// <summary>
    /// AddShareData 的摘要说明
    /// </summary>
    public class AddShareData : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session["UID"] == null)
            {
                context.Response.Write("notlogin");
                return;
            }
            //取得参数
            string title = context.Request.Params["Title"];
            string text = context.Request.Params["Text"];
            string resource = context.Request.Params["Url"];
            //检查参数
            if (title == null || resource == null || text == null)
            {
                context.Response.Write("lackparameter"); //缺少参数
            }
            else 
            {
                //解码
                ShareDataModel sdm = new ShareDataModel() { SourceTitle = context.Server.UrlDecode(title), DocData = context.Server.UrlDecode(text), SourceURL = context.Server.UrlDecode(resource) };
                try
                {
                    int count = MSDB.InsertShareData(sdm);
                    if (count == 1)
                    {
                        context.Response.Write("complete");
                    }
                    else if (count == 0)
                    {
                        context.Response.Write("servererror");
                    }
                }
                catch { context.Response.Write("servererror"); }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}