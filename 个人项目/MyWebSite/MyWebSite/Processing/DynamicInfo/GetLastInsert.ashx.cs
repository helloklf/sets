﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace MyWebSite.Processing.DynamicInfo
{
    /// <summary>
    /// 获取最后添加的一条数据，适用于新增内容后确定操作结果时请求
    /// </summary>
    public class GetLastInsert : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            if (context.Session["UID"] != null)
            {
                List<DynamicInfoModel> sml = MSDB.GetLastInsert(context.Session["UID"] as string);
                if (sml==null||sml.Count == 0)
                {
                    context.Response.Write("notmore");
                }
                else 
                    context.Response.Write(jss.Serialize(sml[0]));
            }
            else
            {
                context.Response.Write("notlogin");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}