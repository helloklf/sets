﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.DynamicInfo
{
    /// <summary>
    /// Insert 的摘要说明
    /// </summary>
    public class Insert : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            if (context.Session["UID"] == null)
            {
                context.Response.Write("notlogin");
                return;
            }

            var content = context.Request.Params["content"];
            var title = context.Request.Params["title"];
            var uid = context.Session["UID"] as string;

            if (title != null && content != null)
            {
                try
                {
                    if (MSDB.InsertDynamicInfo(new DBModel.DynamicInfoModel() { IndexID = uid, DocTitle = title, DocData = content }) > 0)
                    {
                        context.Response.Write("complete");
                    }
                    else
                    {
                        context.Response.Write("error");
                    }
                }
                catch
                {
                    context.Response.Write("servererror");
                }
            }
            else
            {
                context.Response.Write("requesterror");
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}