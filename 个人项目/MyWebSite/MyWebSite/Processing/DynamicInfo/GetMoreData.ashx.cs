﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace MyWebSite.Processing.DynamicInfo
{
    /// <summary>
    /// GetMoreData 的摘要说明
    /// </summary>
    public class GetMoreData : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            List<DBModel.DynamicInfoModel> DynamicInfo = null;
            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            var index = context.Request.Params["StartIndex"] ?? "0";
            var requestType = context.Request.Params["Type"] == null ? "" : context.Request.Params["Type"].ToLower();

            try
            {
                if (!string.IsNullOrWhiteSpace(context.Request.Params["DataIndex"]))//指定查看某人的内容
                {
                    DynamicInfo = MSDB.GetDynamicInfo_OneUser(long.Parse(index), context.Request.Params["DataIndex"]);
                    if ((context.Session["UID"] as string) == context.Request.Params["DataIndex"])
                        foreach (var item in DynamicInfo)
                        {
                            item.CanEdit = true;
                        }
                }
                else if (requestType == "public")//查看公众内容
                {
                    DynamicInfo = MSDB.GetDynamicInfos(long.Parse(index));
                }
                else if (context.Session["UID"] != null)//查看本人内容
                {
                    DynamicInfo = MSDB.GetDynamicInfo_OneUser(long.Parse(index), context.Session["UID"] as string);
                    foreach (var item in DynamicInfo)
                    {
                        item.CanEdit = true;
                    }
                }
                else//默认的请求
                {
                    DynamicInfo = new List<DBModel.DynamicInfoModel>();
                }

                context.Response.Write(jss.Serialize(DynamicInfo));
            }
            catch
            {
                context.Response.Write("servererror");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}