﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Account
{
    /// <summary>
    /// RegisterAshx 的摘要说明
    /// </summary>
    public class RegisterAshx : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string uid = context.Request.Params["uid"];
            string pwd = context.Request.Params["pwd"];

            if (uid == null || pwd == null) //账号和密码空值判断
            {
                context.Response.Write("requesterror");
                return;
            }

            try
            {
                if (MSDB.ValidLoginID(uid))//验证账号是否存在
                {
                    context.Response.Write("accountisexist");
                    return;
                }

                int status = MSDB.RegisterSQL(uid, MD5Helper.ComputeHash( pwd));
                if (status >0)
                {
                    context.Response.Write("pass");
                }
                else if (status == 0)
                {
                    context.Response.Write("error");
                }
            }
            catch
            {
                context.Response.Write("servererror");
            }
            context.Response.ContentType = "text/plain";
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}