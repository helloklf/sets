﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Account
{
    /// <summary>
    /// GetLikeUserList 用于用户模糊查找
    /// </summary>
    public class GetLikeUserList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Request.Params["like"]!=null) 
            {
                try
                {
                    List<DBModel.UserDataModel> userdata = MSDB.GetLikeUser(context.Request.Params["like"]);
                    System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                    context.Response.Write(jss.Serialize(userdata));
                }
                catch { context.Response.Write("servererror"); }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}