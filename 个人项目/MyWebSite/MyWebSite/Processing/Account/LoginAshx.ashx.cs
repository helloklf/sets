﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MyWebSite.Processing.Account
{
    /// <summary>
    /// LoginAshx 的摘要说明
    /// </summary>
    public class LoginAshx : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string uid = context.Request.Params["uid"];
            string pwd = context.Request.Params["pwd"];

            if (uid == null || pwd == null) 
            {
                context.Response.Write("requesterror");
                return; 
            }
            try
            {
                int status = MSDB.Login(uid, MD5Helper.ComputeHash( pwd));
                if (status == 1)
                {
                    try
                    {
                        context.Session["UID"] = MSDB.GetIDByLoginID(uid);//获取用户名的账号索引
                        context.Session["LOGIN"] = uid;
                        context.Response.Write("pass");
                    }
                    catch { context.Response.Write("servererror"); }
                }
                else if (status == 0)
                {
                    context.Response.Write("error");
                }
                else 
                {
                    throw new Exception("存在多个相同的登陆名！");
                }
            }
            catch
            {
                context.Response.Write("servererror");
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}