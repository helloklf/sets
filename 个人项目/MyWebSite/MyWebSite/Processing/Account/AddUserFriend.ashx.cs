﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Account
{
    /// <summary>
    /// AddUserFriend 的摘要说明
    /// </summary>
    public class AddUserFriend : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session["UID"] != null)
            {
                string friendid = context.Request.Params["friendid"];
                string remark = context.Request.Params["remark"];
                if (friendid != null && remark != null)
                {
                    try
                    {
                        MSDB.AddFriend(context.Session["UID"].ToString(), friendid, remark);
                        context.Response.Write("complete");
                        //context.Response.Write(context.Session["UID"].ToString()+"　　"+ friendid+ "　　" +remark);
                    }
                    catch
                    {
                        context.Response.Write("servererror");
                    }
                }
            }
            else
            {
                context.Response.Write("notlogin");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}