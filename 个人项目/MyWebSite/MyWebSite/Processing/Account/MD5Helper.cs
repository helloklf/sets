﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MyWebSite.Processing.Account
{
    /// <summary>
    /// 自定义类：提供加密方法
    /// </summary>
    public static class MD5Helper
    {
        public static string ComputeHash(string text)
        {
            var md5 =MD5.Create();
            return BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(text.Trim())));
        }
    }
}