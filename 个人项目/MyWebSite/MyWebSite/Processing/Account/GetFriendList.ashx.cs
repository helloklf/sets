﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MyWebSite.Processing.Account
{
    /// <summary>
    /// GetFriendList 的摘要说明
    /// </summary>
    public class GetFriendList : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Session["UID"] == null) { context.Response.Write("notlogin"); }
            else 
            {
                try
                {
                    List<DBModel.UserFriendModel> ufml = MSDB.GetUserFriend(context.Session["UID"].ToString());
                    System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                    context.Response.Write(jss.Serialize(ufml));
                }
                catch { context.Response.Write("servererror"); }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}