﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace MyWebSite.Processing
{
    public static class GetConfiguration
    {
        public static string GetAppSettingString(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        public static string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }
    }
}