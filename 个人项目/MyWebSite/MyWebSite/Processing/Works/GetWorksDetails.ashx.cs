﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebSite.Processing.Works
{
    /// <summary>
    /// GetWorksDetails 的摘要说明
    /// </summary>
    public class GetWorksDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Request.Params["DataIndex"] != null)
            {
                var datas = MSDB.GetWorks(context.Request.Params["DataIndex"]);
                var jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                context.Response.Write(jss.Serialize(datas));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}