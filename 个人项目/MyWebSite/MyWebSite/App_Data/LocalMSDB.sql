﻿create table IDList										--账户密码列表
(
	IndexID varchar(36) Primary key,						--账号索引
	IndexPWD varchar(47) not null							--账号密码
)
go

create table IDBind										--账户别名列表
(
	IndexID varchar(36) ,											--账号索引
	LoginID varchar(36) not null  Unique					--登陆账号
)
go

create table UserData										--用户资料
(
	IndexID varchar(36) check(len(IndexID)=36) Primary key,	--账号索引
	Email nvarchar(100), --电子邮件
	WebSite nvarchar(100),--个人网站
	Idol nvarchar(100), --偶像
	Maxim nvarchar(200),--//格言
	Constellation nvarchar(100),--//星座
	Dream nvarchar(200),--//梦想
	Word nvarchar(100) ,--工作
	[Address] nvarchar(200),--地址
	Love nvarchar(200), --爱好
	MeRemark nvarchar(500),--个人说明
	Nickname Nvarchar(100),								--昵称
	HeadPic image,										--头像
	[Signature] Nvarchar(250),								--签名
	Sex	bit,												--性别
	birthday datetime check( birthday>'1800-1-1 '),			--生日
	MobilePhone varchar(18) ,								--手机
	TelPhone varchar(18) ,									--电话

)
go


create table UserFriend									--用户的好友
(
	DataIndex varchar(36) default(newid()),					--记录索引
	IndexID varchar(36) not null check(len(IndexID)=36),		--主账号索引
	FriendID varchar(36) not null check(len(FriendID)=36) ,	--好友主账号索引
	FriendRemark Nvarchar(80)								--备注
	primary key(IndexID,FriendID)							--联合主键：好友不能重复添加
)

go


create table Article										--用户发出的Article(文章)
(
	DataIndex varchar(36) default(newid()),							--记录索引
	PDataIndex varchar(36),												--主文引用
	IndexID varchar(36),		--作者的主账号索引
	[DateTime] DateTime default(getdate()),						--发表时间
	DocTitle Nvarchar(200) not null,									--文章标题
	DocData Nvarchar(max) not null,									--文档内容
	IsValid bit default(0)														--有效状态
)

go

--动态信息
create table DynamicInfo(
	DataIndex varchar(36) default(newid()),							--记录索引
	PDataIndex varchar(36),												--主文引用
	IndexID varchar(36),		--作者的主账号索引
	[DateTime] DateTime default(getdate()),						--发表时间
	DocTitle Nvarchar(200) not null,									--文章标题
	DocData Nvarchar(max) not null,									--文档内容
	IsValid bit default(0)	
)

go

create table Works									--项目、策划
(
	DataIndex varchar(36) default(newid()),							--记录索引
	PDataIndex varchar(36),												--主文引用
	IndexID varchar(36),		--作者的主账号索引
	[DateTime] DateTime default(getdate()),						--发表时间
	DocTitle Nvarchar(200) not null,									--文章标题
	DocData Nvarchar(max) not null,									--文档内容
	IsValid bit default(0)														--有效状态
)

create table ShareData								--共享资源
(
	DataIndex varchar(36) default(newid()),					--记录索引
	PDataIndex varchar(36),												--主文引用
	IndexID varchar(36),--创建者
	[DateTime] DateTime default(getdate()),					--发布时间
	SourceTitle Nvarchar(100) not null,						--文档说明
	DocData Ntext not null,								--说明文档
	SourceURL Nvarchar(200)								--资源链接
)
go

--根据登陆账号获取其个人信息：适用于好友搜索
	select a.IndexID,LoginID,'Nickname'= case
		when Nickname is null then LoginID
		else Nickname
	end
	,HeadPic,[Signature],Sex,birthday,MobilePhone,TelPhone
	from ( select *from IDBind where LoginID like '112233') as a left join
	 (select * from UserData where IndexID in (select top 1 IndexID from IDBind where  LoginID like '112233')) as b on a.IndexID = b.IndexID

go


create proc Proc_UserRegister								--新用户注册
@uid varchar(36),@pwd varchar(47)
as
	begin tran
	 	declare @newuser varchar(36) = newid() 
		insert into IDList values( @newuser,@pwd )			--创建登陆密码记录
		if(@@ERROR!=0)
			begin
				raiserror('RegisterError',16,0) with nowait rollback return
			end
		else
		begin
			insert into IDBind values( @newuser,@uid )		--绑定登陆账号
			if(@@ERROR!=0)
				begin
					raiserror('RegisterIDBindError',16,0) with nowait rollback return
				end
			else commit tran
		end
go

select * from IDList

delete IDList
delete IDBind

select * from Article