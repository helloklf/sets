﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.AuthorData
{
    public partial class AuthorTrends1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("/UserPages/UserCenter.aspx?DataIndex=" + MSDB.GetAuthorIndexID(), true);
        }
    }
}