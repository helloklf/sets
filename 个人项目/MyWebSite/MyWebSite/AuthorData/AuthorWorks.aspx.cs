﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.AuthorData
{
    public partial class AuthorWorks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("/Messages/WorksList.aspx?DataIndex="+MSDB.GetAuthorIndexID(), true);
        }
    }
}