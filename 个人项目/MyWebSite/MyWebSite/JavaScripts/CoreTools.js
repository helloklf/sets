﻿//根据ID获取Dom对象 = document.getElementById(id);
function $id(id) {
    return document.getElementById(id);
}

//根据ClassName获取Dom对象 = document.getElementsByClassName(classname);
function $class(classname) {
    return document.getElementsByClassName(classname);
}

//根据Name获取Dom对象 = document.getElementsByName(name);
function $name(name) {
    return document.getElementsByName(name);
}

//根据TagName获取Dom对象 = document.getElementsByTagName(tagName);
function $tagName(tagName) {
    return document.getElementsByTagName(tagName);
}

//获取本页面URL中的参数集合
function GetLocalUrlParameter() {
    //获得地址中包含的参数
    if (document.location.href.lastIndexOf("?") > 0) {
        var p = document.location.href.substring(document.location.href.lastIndexOf("?") + 1, document.location.href.length);
        var ptext = p.split('&');
        var pobj = new Object();
        for (var i in ptext) {
            var obj = { key:ptext[i].substring(0, ptext[i].indexOf('=')), value:ptext[i].substring(ptext[i].indexOf('=') + 1, ptext[i].length) };
            pobj[i] = obj;
        }
        return pobj;
    }
}

//从 {object key,object value} 类型的集合中获取指定key值得元素值
function GetValueByKey(dictionary, pname)
{
    for (var i in dictionary)
    {
        if (dictionary[i].key.toLowerCase() == pname.toLowerCase())
        {
            return dictionary[i].value;
        }
    }
    
}

//从本页面URL中获取指定名称的参数值
function GetLocalUrlParameterByKey(pname) {
    return GetValueByKey(GetLocalUrlParameter(), pname);
}


//根据参数名 从 "本页面Url中包含的请求参数" 中取值(新方法)
function Request_ParamByKey(key) {
    return GetValueByKey(GetLocalUrlParameter(), key);
}

//根据参数名 从 "本页面Url中包含的请求参数" 中取值(新方法)
function Tools_ParamByKey(key) {
    return GetValueByKey(GetLocalUrlParameter(), key);
}




//打开快速登陆窗口
function Tools_Login() {
    if ($id("QuickLoginBlock")!=null) {
        $id("QuickLoginBlock").hidden = false;
    }
    else
    {
        var loginPage = "/UserPages/Login.aspx";
        window.open(loginPage);
    }
}
//打开快速登陆模式化窗口(logined参数：登陆完成时执行的无参方法，cancel参数：登陆取消时执行的无参方法)
function Tools_LoginDialog(logined, cancel) {
    var loginPage = "/UserPages/QuickLogin.html";
    var isLogined = window.showModelessDialog(loginPage, 'window', "dialogWidth:450px;dialogHeight:250px;center:yes");

    if (isLogined) {
        if (logined)
            logined();
    }
    else {
        if (cancel)
            cancel();
    }
}

//在页面上显示文本消息 text参数：内容，title：标题
function Tools_ShowMessage(text,title) {
    alert(text);
}

/*使用原生JavaScript发起Ajax请求 
    url:请求路径，formData:表单数据，requestType:请求类型（"GET"或"POST"）completed:完成时执行(带1参数接收结果)，error:出错时执行(带1参数接收错误信息)*/
function Tools_AjaxRequest(url,formData,requestType,completed,error) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                Tools_AjaxResponse(xml.response, completed, error);
            }
            else if (xml.status == 404) {
                Tools_AjaxResponse("error", null, error);
            }
            else if(xml.status==500) {
                Tools_AjaxResponse("servererror",null,error);
            }
        }
    }
    //如果未设置请求类型，默认为POST请求
    if (requestType == null || requestType=="")
        requestType = "POST";

    xml.open(requestType, url, true);
    xml.send(formData);
}

/*检查Ajax请求返回结果进行特定值处理返回
response:内容；isValid:数据有效时执行的方法，带一个参数；notValid:数据无效执行的方法，带一个参数
中特定值>>
  　　"complete":完成；"error":失败；"servererror":服务器错误；"notmore":服务器错误；"notlogin":未登陆；""或null:空内容；
判定方式>>
  　　"complete"或json对象：执行isValid方法，将原数据作为参数，并返回true
  　　其它值                 ：执行notValid方法，将错误信息作为参数，并返回false
*/
function Tools_AjaxResponse(response, isValid, notValid) {
    var dataIsValid = false;
    var MessageData = response;
    switch (response) {
        case "complete": {
            dataIsValid = true
            break;
        }
        case "isexist": {
            MessageData = "已存在同名项";
            break;
        }
        case "permissionslow":{
            MessageData = "权限不足";
            break;
        }
        case "accountisexist": {
            MessageData = "该账号已被注册！";
            break;
        } 
        case "error": {
            MessageData ="操作失败，请重试";
            break;
        }
        case "servererror": {
            MessageData = "！！！服务器内部错误！！！";
            break;
        }
        case "notlogin": {
            MessageData = "请完成登陆后重试！";
            Tools_Login();
            break;
        }
        case "notmore": {
            MessageData = "^^^已是最后一条数据^^^";
            break;
        }
        case "requesterror": {
            MessageData = "请求内容错误";
            break;
        }
        case "": {
            MessageData = "未获得内容";
            break;
        }
        case null: {
            MessageData = "未获得内容";
            break;
        }
        default: {
            try {
                JSON.parse(response);
                dataIsValid = true
            }
            catch (e) { MessageData = "未知的内容:" + response; }

            break;
        }
    }
    if (dataIsValid) {
        if (isValid)
            isValid(MessageData);
    }
    else {
        if (notValid)
            notValid(MessageData);
    }
    return dataIsValid;
}