﻿
function AjaxAddUserFriend(friendid, remark, _postback) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                alert(xml.response);
                var r = xml.response;
                if (r == "servererror" || r == "notlogin") { throw Error("服务器错误"); }
                else if (r == "complete") {
                    _postback(r);
                }
                else { throw Error("error") }
            }
            else {
                throw Error(xml.status);
            }
        }
    }
    var fd = new FormData();
    fd.append("friendid", friendid); fd.append("remark", remark);
    xml.open("POST", "../Processing/Account/AddUserFriend.ashx", true);
    xml.send(fd);
}