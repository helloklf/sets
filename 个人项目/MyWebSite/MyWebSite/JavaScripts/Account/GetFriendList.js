﻿function GetFriendList(_statespan,_postback)
{
    try {
        var xml = new XMLHttpRequest();
        xml.onreadystatechange = function () {
            if (xml.readyState == 4)
            {
                if (xml.status == 200)
                {
                    var re = xml.response;
                    if (re == "notlogin") {
                        document.getElementById(_statespan).textContent = "请先完成登陆...";
                        if (window.showModalDialog("../UserPages/QuickLogin.html", 'window', "dialogWidth:450px;dialogHeight:250px;center:yes")) {
                            GetFriendList();
                        }
                        else {
                            document.getElementById(_statespan).textContent = "暂未登陆,点我登陆...";
                        }
                    }
                    else if (re == "servererror") {
                        document.getElementById(_statespan).textContent = "服务器故障...";
                    }
                    else {
                        document.getElementById(_statespan).textContent = "加载完成";
                        if (_postback) { _postback(xml.response); }
                    }
                }
                else
                {
                    document.getElementById(_statespan).textContent = "好友获取失败，点我重试...";
                    //alert(xml.status);
                }
            }
        }
        document.getElementById(_statespan).textContent = "连接网络...";
        xml.open("POST", "../Processing/Account/GetFriendList.ashx", true);
        xml.send(null);
    }
    catch (e) { alert(e.message); }
}