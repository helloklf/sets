﻿function AjaxGetLikeUser(like, _postbakc) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                var r = xml.response;
                if (r == "servererror") {
                    throw Error("服务器错误");
                }
                else _postbakc(r);
            }
        }
    }
    xml.open("GET", "../Processing/Account/GetLikeUserList.ashx?like=" + like, true);
    xml.send(null);
}