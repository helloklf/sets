﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Technology.aspx.cs" Inherits="MyWebSite.AboutInfo.Technology" MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">关于技术</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div style="text-align: center; margin: 0px auto">
        <span style="font-size: 64px">关于此站点所使用的技术信息</span>
        <br />
        <span style="font-size: 64px">OmArea.COM</span>
        <p />
        <h1>项目类型：Asp.Net WebForm Project</h1>
        <h3>运行框架：.NetFramework 4.5</h3>
        <h3>相关语言：C#5.0、JavaScript、HTML、CSS、T-SQL</h3>
        <h3>数 据 库：Microsoft Sql Server Express</h3>
        <h3>相关工具：Visual Studio 2013 Update4、SQL Server 2014 Management Studio</h3>
        <h3>插件语言：无</h3>
    </div>
</asp:Content>

