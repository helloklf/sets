﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Site.aspx.cs" Inherits="MyWebSite.AboutInfo.Site"  MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">关于站点</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div style="text-align: center; margin: 0px auto">
        <span style="font-size: 64px">关于此站点的描述</span>
        <br />
        <span style="font-size: 64px">OmArea.COM</span>
        <p />
        <span style="font-size: 25px">您当前正在访问的页面是由站长本人自主设计和建设的个人网站，其中所使用到部分功能来源于互联网络开源代码。
                     　　omarea.com站点页面构建起始于2014年12月，用于实践网页开发技术和个人作品展示。
        </span>
        <p />
        <div>
            <h2>域名：omarea.com</h2>
            <h3>所有者：邝林飞</h3>
            <h3>站点性质：博客</h3>
        </div>
    </div>
</asp:Content>

