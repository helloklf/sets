﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeviceInfo.aspx.cs" Inherits="MyWebSite.AboutInfo.DeviceInfo" MasterPageFile="~/Masters/Default.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">关于作者</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style>
        .user_b td{
            padding:5px 10px;
        }
    </style>
    <div style="text-align: center;">
        <span style="font-size: 64px; margin-top: 60px;">关于您的浏览器和服务器</span>
        <br />
        <span style="font-size: 64px">OmArea.COM</span>
        <p />

        <table class="user_b" width="435" border="1" cellspacing="0" style="margin: 0px auto; text-align: left;" cellpadding="0">
            <tr>
                <td>浏览器类型</td>
                <td>
                    <script>document.write(navigator.appName)</script>
                </td>
            </tr>
            <tr>
                <td>浏览器版本</td>
                <td>
                    <script>document.write(navigator.appVersion)</script>
                </td>
            </tr>
            <tr>
                <td>浏览器语言</td>
                <td>
                    <script>document.write(navigator.browserLanguage)</script>
                </td>
            </tr>
            <tr>
                <td>CPU类型</td>
                <td>
                    <script>document.write(typeof (navigator.cpuClass) == "undefined" ? "" : navigator.cpuClass)</script>
                </td>
            </tr>
            <tr>
                <td>操作系统</td>
                <td>
                    <script>document.write(typeof (navigator.platform) == "undefined" ? "" : navigator.platform)</script>
                </td>
            </tr>
            <tr>
                <td>系统语言</td>
                <td>
                    <script>document.write(navigator.systemLanguage)</script>
                </td>
            </tr>
            <tr>
                <td>用户语言;</td>
                <td>
                    <script>document.write(navigator.userLanguage)</script>
                </td>
            </tr>
            <tr>
                <td>在线情况</td>
                <td>
                    <script>document.write(navigator.onLine)</script>
                </td>
            </tr>
            <tr>
                <td>屏幕分辨率</td>
                <td>
                    <script>document.write(window.screen.width + "x" + window.screen.height)</script>
                </td>
            </tr>
            <tr>
                <td>颜色</td>
                <td>
                    <script>document.write(window.screen.colorDepth + "位")</script>
                </td>
            </tr>
            <tr>
                <td>字体平滑</td>
                <td>
                    <script>document.write(window.screen.fontSmoothingEnabled)</script>
                </td>
            </tr>
            <tr>
                <td>appMinorVersion</td>
                <td>
                    <script>document.write(navigator.appMinorVersion)</script>
                </td>
            </tr>
            <tr>
                <td>appCodeName</td>
                <td>
                    <script>document.write(navigator.appCodeName)</script>
                </td>
            </tr>
            <tr>
                <td>cookieEnabled</td>
                <td>
                    <script>document.write(navigator.cookieEnabled)</script>
                </td>
            </tr>
            <tr>
                <td>userAgent</td>
                <td>
                    <script>document.write(navigator.userAgent)</script>
                </td>
            </tr>
            <tr>
                <td>javaEnabled</td>
                <td>
                    <script>document.write(navigator.javaEnabled())</script>
                </td>
            </tr>
            <%--<tr>
                <td>taintEnabled</td>
                <td>
                    <script>document.write(navigator.taintEnabled())</script>
                </td>
            </tr>--%>
            <!--<tr>
                <td>connectionType</td>
                <td>
                    <script>document.write(oClientCaps.connectionType)</script>
                </td>
                </tr>-->
        </table>
        <div>
            <h2>服务器配置信息</h2>
            <h3>托管程序：Internet Information Services 7.0 （Asp.Net 4.0）</h3>
            <h3>IP：</h3>
            <h3>CPU：</h3>
            <h3>RAM：</h3>
            <h3>磁盘：</h3>
        </div>
    </div>
</asp:Content>
