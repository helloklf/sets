﻿function DownLoad(url) //点击下载
{
    window.open(url);
}
//参数1：记录索引、参数2：标题、参数3：注解(正文)、参数4：时间、参数5：下载地址
function CreateSharedLi(DataModel) //创建新的项
{
    var li = document.createElement("li"); li.title = DataModel.DataIndex; li.className = "sharedataitem";
    li.id = DataModel.DataIndex;
    var htitle = document.createElement("h1"); htitle.textContent = DataModel.SourceTitle;
    var hdescribe = document.createElement("span");
    hdescribe.textContent = DataModel.DocData;
    var br = document.createElement("br");
    var time = document.createElement("label"); time.textContent = "时间：" + DataModel.DateTimeText;
    var download = document.createElement("img"); download.src = "Img/download.png"; download.className = "download"; 
    download.title = "下载"; download.alt = "下载";
    download.setAttribute("onclick", "DownLoad('" + DataModel.SourceURL + "')");

    li.appendChild(htitle);
    li.appendChild(hdescribe);
    li.appendChild(br);
    li.appendChild(time);
    li.appendChild(download);

    return li;
}

function GetShareDataToli(_statespanel, listpanel, startDataid) {
    var statepanel = document.getElementById(_statespanel);
    statepanel.textContent = "正在获取······";
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                statepanel.textContent = "＞＞＞正在解析数据＜＜＜"
                var r = xml.response;
                if (r == "notmore") {
                    statepanel.textContent = "－－－已经没有更多数据－－－";
                }
                else {
                    try {
                        var datajson = JSON.parse(r);
                        if (datajson)
                        {
                            var list = document.getElementById(listpanel);
                            for (var i in datajson) {
                                list.insertBefore(CreateSharedLi(datajson[i]), list.childNodes[list.childNodes.length - 2]);
                            }
                        }
                        statepanel.textContent = "···加载更多数据···";
                    }
                    catch (e) {
                        statepanel.textContent = "－－－数据解析错误－－－";
                    }
                }
            }
            else {
                statepanel.textContent = "！！！加载失败,请重试！！！";
            }
        }
    }
    if (startDataid) {
        xml.open("GET", "../Processing/Message/GetShareData.ashx?StartIndex=" + startDataid, true);
    }
    else {
        xml.open("GET", "../Processing/Message/GetShareData.ashx", true);
    }
    xml.send(null);
}