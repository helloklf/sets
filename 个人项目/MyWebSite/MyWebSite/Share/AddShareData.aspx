﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddShareData.aspx.cs" Inherits="MyWebSite.AuthorData.AddShareData" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head">添加共享</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style>
        .bigImgBtn 
        {
            border:2px solid #fff; border-radius:50%;
        }
        .bigImgBtn:hover
        {
            background:#fff
        }
        .bigImgBtn:active
        {
            background:#fff; border:2px solid #000
        }
    </style>
    <div>
        <table style="font-size: 15px; color: #000; width:100%; border:1px solid #888;">
            <tr>
                <td style="width: 95px; text-align: center; padding: 5px; background: #888">
                    <img src="Img/share.png" style="vertical-align: middle; border: 2px solid #888; border-radius: 50%" /></td>
                <td style="background: #888; padding: 5px 10px;">
                    <input id="title" type="text" placeholder="请输入资源名称" style="background: none; border: 0px; border-bottom: 2px solid #000; width: 100%; overflow: visible; font-size: 20px; padding: 5px 0px; color: #000; font-weight: 900;" maxlength="50" />
                    <input id="url" type="url" style="background: none; border: 0px; width: 100%; border-bottom: 1px solid #666; font-size: 18px; padding: 5px 0px; color: #000;" maxlength="200"  placeholder="该资源的下载地址" />
                    <%--<div style="vertical-align: middle; padding:2px 0px;">点击<img src="Img/download.png" style="height: 15px; border-radius: 50%; background: #fff" />按钮时转到的页面</div>--%>
                </td>
                <td style="width: 95px; padding: 5px; text-align: center; background: #888">
                    <img src="Img/sumit.png" class="bigImgBtn" onclick="SubmitShareData()" /></td>
            </tr>
        </table>
        <textarea id="text" maxlength="4096" style="overflow:auto; width: 100%; height: 350px; font-size: 18px; border: 1px solid #888; background: none; color: #888">简单描述一下你共享的资源...</textarea>
        <br />
    </div>
    <script>
        function SubmitShareData()
        {
            var data = new FormData();
            data.append("Title", encodeURIComponent($id("title").value));
            data.append("Text", encodeURIComponent($id("text").textContent));
            data.append("Url", encodeURIComponent($id("url").value));

            Tools_AjaxRequest("../Processing/Message/AddShareData.ashx", data, "POST",
                    function (response)
                    {
                        location.href = "ShareData.aspx";
                    },
                    function (errorText)
                    {
                        alert(errorText);
                    }
                );
        }
    </script>
</asp:Content>
