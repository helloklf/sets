﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditShareData.aspx.cs" Inherits="MyWebSite.AuthorData.EditShareData" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head">添加共享</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style>
        .bigImgBtn {
            border: 2px solid #fff;
            border-radius: 50%;
        }

            .bigImgBtn:hover {
                background: #fff;
            }

            .bigImgBtn:active {
                background: #fff;
                border: 2px solid #000;
            }
    </style>
    <form runat="server">
        <div>
            <input runat="server" id="Aspx_DataIndex" hidden="hidden" />
            <table style="font-size: 15px; color: #000; width: 100%; border: 1px solid #888;">
                <tr>
                    <td style="width: 95px; text-align: center; padding: 5px; background: #888">
                        <img src="Img/share.png" style="vertical-align: middle; border: 2px solid #888; border-radius: 50%" /></td>
                    <td style="background: #888; padding: 5px 10px;">
                        <input runat="server" id="Aspx_title" style="background: none; border: 0px; border-bottom: 2px solid #000; width: 100%; overflow: visible; font-size: 20px; padding: 5px 0px; color: #000; font-weight: 900;" maxlength="50" value="标题：为你共享的资源起一个名字" />
                        <input runat="server" id="Aspx_url" style="background: none; border: 0px; width: 100%; border-bottom: 1px solid #666; font-size: 18px; padding: 5px 0px; color: #000;" maxlength="200" value="http://" />
                        <div style="vertical-align: middle; padding: 2px 0px;">点击<img src="Img/download.png" style="height: 15px; border-radius: 50%; background: #fff" />按钮时转到的页面</div>
                    </td>
                    <td style="width: 95px; padding: 5px; text-align: center; background: #888">
                        <input type="image" runat="server" src="Img/sumit.png" class="bigImgBtn" />
                    </td>
                </tr>
            </table>
            <textarea runat="server" id="Aspx_text" maxlength="4096" style="overflow: auto; width: 100%; height: 350px; font-size: 18px; border: 1px solid #888; background: none; color: #888">简单描述一下你共享的资源...</textarea>
            <br />
        </div>
    </form>
</asp:Content>