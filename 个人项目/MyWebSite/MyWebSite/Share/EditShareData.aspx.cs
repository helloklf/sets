﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.AuthorData
{
    public partial class EditShareData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                SubMit();
            else
                Loaded();
        }

        /// <summary>
        /// 进入编辑窗口时
        /// </summary>
        private void Loaded()
        {
            if (Request.Params["DataIndex"] != null)
            {
                var data = Request.Params["DataIndex"].Trim();
                var datas = MSDB.GetShareDataByIndex(data);
                if (datas == null || datas.Count < 1 || datas[0] == null)
                {
                    Response.Redirect("ShareData.aspx", true);
                }
                else
                {
                    this.Aspx_url.Value = datas[0].SourceURL;
                    this.Aspx_title.Value = datas[0].SourceTitle;
                    Aspx_DataIndex.Value = datas[0].DataIndex;
                    Aspx_text.Value = datas[0].DocData;
                }
            }
            else
            {
                Response.Redirect("ShareData.aspx", true);
            }
        }

        /// <summary>
        /// 提交时
        /// </summary>
        private void SubMit()
        {
            if (Session["UID"] == null)
            {
                var t =" <script>window.showModalDialog(\"../UserPages/QuickLogin.html\", 'window', \"dialogWidth:450px;dialogHeight:250px;center:yes\")</script>";
                Response.Write(t);
                return;
            }
            //取得参数
            string dataIndex = Aspx_DataIndex.Value;
            string title = Aspx_title.Value;
            string text = Aspx_text.Value;
            string resource = Aspx_url.Value;
            
            //解码
            ShareDataModel sdm = new ShareDataModel() { SourceTitle = Server.UrlDecode(title), DocData = Server.UrlDecode(text), SourceURL = Server.UrlDecode(resource), DataIndex = dataIndex };
            try
            {
                int count = MSDB.EditShareData(sdm);
                Response.Redirect("ShareData.aspx");
            }
            catch 
            {
                Response.Write("<script>alert('修改失败!');location.href='ShareData.aspx';</script>");
            }
        }
    }
}