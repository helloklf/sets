﻿
    function JsonToList(jsonData) {
        var statePanel = document.getElementById("DatGetState");
        try {
            var json = JSON.parse(jsonData);
            if (json.length < 1) {
                statePanel.textContent = "－－－已经没有更多数据－－－";
                statePanel.isDisabled = false;
            }
            else {
                for (var i in json) {
                    ItemToList(json[i]);
                }
                statePanel.textContent = "···加载更多数据···";
                statePanel.isDisabled = false;
            }
        }
        catch (e) {
            statePanel.textContent = "－－－数据解析错误－－－";
            statePanel.isDisabled = false;
        }
    }

function ItemToList(item) {
    var panel = document.getElementById("WorkDataList");
    var li = document.createElement("li");
    if (item.CanEdit) {
        li.innerHTML = "<img src='null'/><div title = '点击查看详细' class='RightBlock' onclick='window.open(\"WorksDetails.aspx?DataIndex=" + item.DataIndex + "\")'><div class='TileTitle'>" + item.DocTitle + "</div><div class='TileText'>发表于：" + item.DateTimeText + "</div></div><div class='DeleteBlock'><a href='WorksEdit.aspx?DataIndex=" + item.DataIndex + "' target='_blank'>✂</a><a href='javascript:DeleteData(\"" + item.DataIndex + "\")'>✖</a></div>";
    }
    else {
        li.title = "点击查看详细";
        li.setAttribute("onclick", "window.open('WorksDetails.aspx?DataIndex=" + item.DataIndex + "')");
        li.innerHTML = "<img src='null'/><div class='RightBlock'><div class='TileTitle'>" + item.DocTitle + "</div><div class='TileText'>发表于：" + item.DateTimeText + "</div></div>";
    }
    panel.insertBefore(li, panel.childNodes[panel.childElementCount - 1]);
}

function DeleteData(dindex) {
    if (dindex == null)
        alert("页面上的内容出现错误，请向管理员反馈！");
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                if (xml.response == "error") {
                    alert("删除失败 ！您可能没有删除该内容的权限。如果这篇文章是您发布的，请向管理员报告此错误的！");
                }
                else if (xml.response == "notlogin") {
                    Tools_Login();
                    return;
                }
                else if (xml.response == "complete") {
                    var item = document.getElementById(dindex);
                    item.parentElement.removeChild(item);
                }
                else if (xml.response == "servererror") {
                    alert("服务器出现错误，暂时无法执行此操作，请稍后再试！");
                }
                else {
                    alert(xml.response);
                }
            }
        }
    }
    xml.open("GET", "/Processing/Works/DeleteWorks.ashx?DataIndex=" + dindex, true);
    xml.send();
}

var DataIndex = Tools_ParamByKey("DataIndex");
var RequestType = Tools_ParamByKey("Type");

function GetMoreData() {
    var statePanel = document.getElementById("DatGetState");
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                statePanel.textContent = "＞＞＞正在解析数据＜＜＜";
                JsonToList(xml.response);
            }
            else if(xml.status!="500"){
                statePanel.textContent = "！！！加载失败,请重试！！！";
                statePanel.isDisabled = false;
            }
            else {
                statePanel.textContent = "！！！服务出现错误，请稍后再试！！！";
                statePanel.isDisabled = false;
            }
        }
    }
    var fd = new FormData();
    fd.append("Index", document.getElementById("WorkDataList").childElementCount);
    if (DataIndex != null)
        fd.append("DataIndex", DataIndex);
    if (RequestType != null)
        fd.append("Type", RequestType);
    xml.open("POST", "/Processing/Works/GetWorks.ashx");
    xml.send(fd);
}