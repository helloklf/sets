﻿function JsonToList(jsonData) {
    var statePanel = document.getElementById("DatGetState");
    try {
        var json = JSON.parse(jsonData);
        if (json.length < 1) {
            statePanel.textContent = "－－－已经没有更多数据－－－";
            statePanel.isDisabled = false;
        }
        else {
            for (var i in json) {
                ItemToList(json[i]);
            }
            statePanel.textContent = "···加载更多数据···";
            statePanel.isDisabled = false;
        }
    }
    catch (e) {
        statePanel.textContent = "－－－数据解析错误－－－";
        statePanel.isDisabled = false;
    }
}
function ItemToList(item) {
    var pnode = document.getElementById("DocDataList");
    var li = document.createElement("li");
    li.id = item.DataIndex;
    if (item.CanEdit) {
        li.innerHTML = "<div class='RightPanel'><a href='ArticleEdit.aspx?DataIndex=" + item.DataIndex + "'>✂</a><br/><a href='#' onclick='DeleteData(\"" + item.DataIndex + "\")'>✖</a></div><a href='ArticleDetails.aspx?DataIndex=" + item.DataIndex + "'><div class='TitleText'>" + item.DocTitle + "</div><br /><div class='DocText'>" + item.DocData + "</div></a>";
    }
    else {
         li.innerHTML = "<div class='TitleText'>" + item.DocTitle + "</div><br /><div class='DocText'>发表于：" + item.DateTimeText + "</div></a>";
    }
    pnode.insertBefore(li, pnode.childNodes[pnode.childElementCount - 1]);
}

var DataIndex = Tools_ParamByKey("DataIndex");
var RequestType = Tools_ParamByKey("Type");

function GetMoreData() {
    var statePanel = document.getElementById("DatGetState");
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                statePanel.textContent = "＞＞＞正在解析数据＜＜＜";
                JsonToList(xml.response);
            }
            else {
                statePanel.textContent = "！！！加载失败,请重试！！！";
                statePanel.isDisabled = false;
            }
        }
    }
    var fd = new FormData();
    fd.append("Index", document.getElementById("DocDataList").childElementCount);
    if (DataIndex != null)
        fd.append("DataIndex", DataIndex);
    if (RequestType!=null)
        fd.append("Type", RequestType);
    xml.open("POST", "/Processing/Message/GetArticle.ashx?");
    xml.send(fd);
    statePanel.textContent = "正在获取······";
    statePanel.isDisabled = true;
}

function DeleteData(dindex) {
    if (dindex == null)
        alert("页面上的内容出现错误，请向管理员反馈！");
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                if (xml.response == "error") {
                    alert("删除失败 ！您可能没有删除该内容的权限。如果这篇文章是您发布的，请向管理员报告此错误的！");
                }
                else if (xml.response == "notlogin") {
                    Tools_Login();
                    return;
                }
                else if (xml.response == "complete") {
                    var item = document.getElementById(dindex);
                    item.parentElement.removeChild(item);
                }
                else if (xml.response == "servererror") {
                    alert("服务器出现错误，暂时无法执行此操作，请稍后再试！");
                }
                else {
                    alert(xml.response);
                }
            }
        }
    }
    xml.open("GET", "/Processing/Message/DeleteArticle.ashx?DataIndex=" + dindex, true);
    xml.send();
}