﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.Messages
{
    public partial class ArticleList : System.Web.UI.Page
    {
        public System.Text.StringBuilder Htmls = new System.Text.StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            //请求类型参数
            var requestType = Request.Params["Type"] == null ? "" : Request.Params["Type"].ToLower();
            //用户的账号索引
            var indexID = Request.Params["DataIndex"];

            if (!string.IsNullOrWhiteSpace(indexID))//指定查看某人的内容
            {
                GetOneUserData();
            }
            else if (requestType == "public")//查看公众内容
            {
                GetPublicData();
            }
            else if (Session["UID"] != null)//查看本人内容
            {
                GetWeData();
            }
            else//默认
            {
               GetPublicData();
            }
            DataBind();
        }

        /// <summary>
        /// 获取公众内容
        /// </summary>
        void GetPublicData()
        {
            List<DBModel.ArticleModel> Articles = MSDB.GetArticle(0);
            foreach (var item in Articles)
            {
                item.DocData = "发表于：" + item.DateTime;
                Htmls.Append(
                                string.Format(
                               "<li id='{0}'><a href='ArticleDetails.aspx?DataIndex={0}'><div class='TitleText'>{1}</div><br /><div class='DocText'>{2}</div></a></li>", item.DataIndex, item.DocTitle, item.DocData));
            }
        }

        /// <summary>
        /// 指定用户的内容
        /// </summary>
        void GetOneUserData()
        {
            if ((Session["UID"] as string) == Request.Params["DataIndex"])
            {
                GetWeData();
                return;
            }
            List<DBModel.ArticleModel> Articles = MSDB.GetArticle_OneUser(0, Request.Params["DataIndex"]);
            foreach (var item in Articles)
            {
                item.DocData = "发表于：" + item.DateTime;
                Htmls.Append(
                                string.Format(
                               "<li id='{0}'><a href='ArticleDetails.aspx?DataIndex={0}'><div class='TitleText'>{1}</div><br /><div class='DocText'>{2}</div></a></li>", item.DataIndex, item.DocTitle, item.DocData));

            } 
        }

        /// <summary>
        /// 本人的内容
        /// </summary>
        void GetWeData()
        {
            List<DBModel.ArticleModel> Articles = MSDB.GetArticle_OneUser(0, Session["UID"] as string);
            foreach (var item in Articles)
            {
                item.CanEdit = true;
            }
            foreach (var item in Articles)
            {
                item.DocData = "发表于：" + item.DateTime;
                Htmls.Append(
                                string.Format(
                               "<li id='{0}'><div class='RightPanel'><a href='ArticleEdit.aspx?DataIndex={0}'>✂</a><br/><a  href='#' onclick='DeleteData(\"{0}\")'>✖</a></div><a href='ArticleDetails.aspx?DataIndex={0}'><div class='TitleText'>{1}</div><br /><div class='DocText'>{2}</div></a></li>", item.DataIndex, item.DocTitle, item.DocData));

            }
        }
    }
}