﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>富文本编辑器使用示范</title>

    
    <!--相关组建，请复制这部分-->
    <link href="REFiles/Style.css" rel="stylesheet" />
    <script src="REFiles/RichObject.js"></script>
    <script src="REFiles/AjaxFileUpload.js"></script>
    <script src="REFiles/Control.js"></script>
    <script src="REFiles/SaveContentToServer.js"></script>

    <!--获取富文本编辑器中的HTML文档请复制下面这段：-->
    <%--var text = RichProject.$Doc().body.innerHTML;--%>


</head>
<body>
    <form id="form1" runat="server">

        <!--编辑器，请复制这部分-->
        <div id="RichEditPanel">
            <!--顶部栏-->
            <div id="REPtopbar">

                <img alt="缓存当前状态" title="缓存当前状态" src="REFiles/pics/anchor.png" onmousedown="InTemp()" />
                <img alt="返回上一缓存点" title="返回上一缓存点" src="REFiles/pics/left.png" onmousedown="OutTemp()" />
                <img alt="转到下一缓存点" title="转到下一缓存点" src="REFiles/pics/right.png" onmousedown="Advance()" />
                <img alt="撤销样式更改" title="撤销样式更改" src="REFiles/pics/undo.png" onmousedown="doClick('Undo')" />
                <img alt="重做样式更改" title="重做样式更改" src="REFiles/pics/redo.png" onmousedown="doClick('Redo')" />
                <img alt="复制" title="复制" src="REFiles/pics/copy.png" onmousedown="doClick('Copy')" />
                <img alt="剪切" title="剪切" src="REFiles/pics/cut.png" onmousedown="doClick('Cut')" />
                <img alt="粘贴" title="粘贴" src="REFiles/pics/paste.png" onmousedown="doClick('Paste')" />
                <img alt="删除" title="删除" src="REFiles/pics/delete.png" onmousedown="doClick('Delete')" />
                <img alt="插入html标记" title="插入html标记" src="REFiles/pics/xml.png" onmousedown="SwitchPanel('InserHTMLSetPanel')" />
                <img alt="手写html标记" title="手写html标记" src="REFiles/pics/dw.png" onmousedown="SwitchPanel('HTMLModeSetPanel')" />
                <img alt="本地上载" title="本地上载" src="REFiles/pics/upload.png" onmousedown="SwitchPanel('UploadSetPanel')" />
                <img alt="添加图片" title="添加图片" src="REFiles/pics/image.png" onmousedown="SwitchPanel('ImageSetPanel')" />
                <img alt="保存" title="保存" src="REFiles/pics/disk.png" onmousedown="SwitchPanel('SaveSetPanel')" />

                <img alt="展开/收起" title="展开/收起" src="REFiles/pics/up.png" onmousedown="SwitchPanel('REPtoolbar'); src = document.getElementById('REPtoolbar').hidden ? 'REFiles/pics/down.png' : 'REFiles/pics/up.png'" style="float: right" />
                <img alt="显示帮助" title="显示帮助" src="REFiles/pics/question.png" onmousedown="window.open('REFiles/help.html') " style="float: right" />

                <div id="REPOperateState">
                    <span onclick="textContent=''" id="localState"></span>
                    <%--<span onclick="textContent=''" id="fileDownLoad">没有下载</span>--%>
                    <span onclick="textContent=''" id="imgUploadState"></span>
                    <span onclick="textContent=''" id="htmlUploadState"></span>
                    <span onclick="textContent=''" id="docSubmitState"></span>
                    <span onclick="textContent=''" id="tempState">缓存状态：0/0</span>
                </div>

            </div>
            <!--工具栏-->
            <div id="REPtoolbar">
                <img alt="粗体" title="粗体" src="REFiles/pics/text.bold.png" onmousedown="doClick('Bold')" />
                <img alt="斜体" title="斜体" src="REFiles/pics/text.italic.png" onmousedown="doClick('Italic')" />
                <img alt="删除线" title="删除线" src="REFiles/pics/strikethrough.png" onmousedown="doClick('StrikeThrough')" />
                <img alt="下划线" title="下划线" src="REFiles/pics/underline.png" onmousedown="doClick('Underline')" />
                <img alt="居左" title="居左" src="REFiles/pics/align.left.png" onmousedown="doClick('JustifyLeft')" />
                <img alt="居中" title="居中" src="REFiles/pics/align.center.png" onmousedown="doClick('JustifyCenter')" />
                <img alt="居右" title="居右" src="REFiles/pics/align.right.png" onmousedown="doClick('JustifyRight')" />
                <img alt="-缩进" title="-缩进" src="REFiles/pics/ToLeft.png" onmousedown="doClick('outdent')" />
                <img alt="+缩进" title="+缩进" src="REFiles/pics/ToRight.png" onmousedown="doClick('indent')" />
                <img alt="超链" title="超链" src="REFiles/pics/link.png" onmousedown="doClick('createlink')" />
                <img alt="序列" title="序列" src="REFiles/pics/list.png" onmousedown="doClick('insertorderedlist')" />
                <img alt="列表" title="列表" src="REFiles/pics/list2.png" onmousedown="doClick('insertunorderedlist')" />
                <img alt="清除格式" title="清除格式" src="REFiles/pics/clean.png" onmousedown="doClick('removeformat')" />
                <img alt="前景/背景颜色" title="前景/背景颜色" src="REFiles/pics/color.png" onmousedown="SwitchPanel('ColorSetPanel')" />
                <img alt="字体" title="字体" src="REFiles/pics/font.png" onmousedown="SwitchPanel('FontSetPanel')" />
                <img alt="字体大小" title="字体大小" src="REFiles/pics/font-size.png" onmousedown="SwitchPanel('FontSizeSetPanel')" />
                <img alt="表情" title="表情" src="REFiles/pics/face.png" onmousedown="SwitchPanel('FaceSetPanel')" />
            </div>
            <div class="REPSetPanlList">
                <!--颜色选择面板-->
                <div id="ColorSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">颜色设置</span>
                    <div class="Content">
                        <span style="float: left;">字体</span>
                        <hr />
                        <div id="fcslist"></div>
                        <p style="clear: both" />
                        <span style="float: left;">背景</span>
                        <hr />
                        <div id="bgcslist"></div>
                    </div>
                </div>
                <!--字体选择面板-->
                <div id="FontSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">字体选择</span>
                    <div class="Content">
                        <select id="fontlist" onchange='doClick("fontname",value)'></select>
                    </div>
                </div>
                <!--字体大小选择面板-->
                <div id="FontSizeSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">字体大小</span>
                    <div class="Content">
                        <select id="fontsizelist" onchange='doClick("fontsize",value)'>
                            <option value="1" style="font-size: 9px">1号</option>
                            <option value="2" style="font-size: 11px">2号</option>
                            <option value="3" style="font-size: 15px">3号</option>
                            <option value="4" style="font-size: 18px">4号</option>
                            <option value="5" style="font-size: 25px">5号</option>
                            <option value="6" style="font-size: 30px">6号</option>
                            <option value="7" style="font-size: 48px">7号</option>
                        </select>
                    </div>
                </div>
                <!--表情插入-->
                <div id="FaceSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">添加表情</span>
                    <div class="Content" id="faceListPanel">
                    </div>
                </div>
                <!--图片选择面板-->
                <div id="ImageSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">图片选择</span>
                    <div class="Content">
                        <div id="imgfilelist">
                            <img src="REFiles/pics/add.png" class="ImgButton" title="本地上传" alt="本地上传" onclick="$$('imgfiledata').click()" />
                            <img src="REFiles/pics/online.png" class="ImgButton" title="在线图片" alt="在线图片"
                                onclick='var url = prompt("请输入网络图片路径", "http://"); if (url.indexOf("http://") != 0) alert("无效的路径！"); else AddOnlineImgToList(url)' />
                        </div>
                        <div>
                            文件名称：<span id="imgfilename"></span><br />
                        </div>
                        <input type="file" id="imgfiledata" onchange="ImgFileSelected()" />
                    </div>
                </div>
                <!--HTML标记插入面板-->
                <div id="InserHTMLSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">插入HTML标记<img src="REFiles/pics/check.png" title="插入" alt="插入" style="vertical-align: middle; margin: 3px 10px 5px 10px" class="ImgButton" onclick='RichProject.$InserHTML($$("inserthtmltext").textContent)' /></span>
                    <div class="Content">
                        <textarea id="inserthtmltext"></textarea>
                        <br />
                    </div>
                </div>
                <!--HTML编辑模式面板-->
                <div id="HTMLModeSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">HTML编辑模式<img src="REFiles/pics/sync.png" style="vertical-align: middle; margin: 3px 10px 5px 10px" class="ImgButton" onclick="RefreshText()" title="刷新" alt="刷新" /></span>
                    <div class="Content">
                        <textarea id="htmltext" onkeyup="RefreshHtml()" onkeydown='if(event.keyCode==9){var s=selectionStart;textContent = textContent.slice(0,s)+ "   " +textContent.slice(selectionEnd,textContent.length);setSelectionRange(s,s);return false;}'></textarea>
                        <br />
                    </div>
                </div>
                <!--文档存储-->
                <div id="SaveSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">文档存储</span>
                    <div class="Content">
                        <img src="REFiles/pics/save.png" style="margin: 5px; height: 64px; width: 64px;" title="本地保存" alt="本地存储" class="ImgButton" onclick="doClick('saveas', 'NewDoc.htm')" />
                        <img src="REFiles/pics/netserver.png" style="margin: 5px; height: 64px; width: 64px;" title="上载至服务器" alt="服务器" class="ImgButton" onclick="SaveHtmlDoc()" />
                    </div>
                </div>
                <!--HTML文档上载面板-->
                <div id="UploadSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">加载本地HTML</span>
                    <div class="Content">
                        <input type="file" id="htmlfiledata" onchange="HtmlFileSelected()" />
                        <img src="REFiles/pics/open.png" style="margin: 5px; height: 64px; width: 64px;" class="ImgButton" onclick="$$('htmlfiledata').click()" />
                        <br />
                        文件名称：<span id="htmlfilename"></span>
                    </div>
                </div>
            </div>
            <!--编辑区域-->
            <div id="IFrameEditPanel">
                <iframe onload="RichEditInitialize('REPdoc')" id="REPdoc"></iframe>
            </div>
        </div>


    </form>
</body>
</html>
