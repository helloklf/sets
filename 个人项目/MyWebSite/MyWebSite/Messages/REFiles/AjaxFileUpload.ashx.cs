﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MyWebSite.Messages.REFiles
{
    /// <summary>
    /// AjaxFileUpload 的摘要说明
    /// </summary>
    public class AjaxFileUpload : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //设置上载的图片保存路径
                string folder = "/ImgSources/NewImage/";
                //设置文件名
                string filename = folder + Guid.NewGuid().ToString() + context.Request.Params["type"];
                //创建相对于应用程序根目录的路径
                if (!System.IO.Directory.Exists(context.Server.MapPath("~"+folder)))
                    //保存文件
                    context.Request.Files[0].SaveAs(context.Server.MapPath("~"+filename));
                //返回Html格式的路径
                context.Response.Write("../.."+filename);
            }
            catch(Exception ex)
            {
                context.Response.Write(ex.Message);
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}