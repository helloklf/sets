﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MyWebSite.Messages.REFiles
{
    /// <summary>
    /// AjaxUploadHTML 的摘要说明
    /// </summary>
    public class AjaxUploadHTML : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            System.IO.Stream st = context.Request.Files[0].InputStream;
            byte[] bytes = new byte[st.Length];
            st.Read(bytes, 0, (int)st.Length);
            context.Response.Write(Encoding.UTF8.GetString(bytes));
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}