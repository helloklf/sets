﻿//参数1：fileupload.files[0]; 参数2：后缀名; 参数3：提交位置; 参数4：获得返回值的回送方法
function FileUpload(file, filetyp, posturl, postback) {
    var xmlup = new XMLHttpRequest();
    var f = new FormData();
    f.append('file', file);
    xmlup.onreadystatechange = function u() {
        if (xmlup.readyState == 4) {
            if (xmlup.status == 200)
            {
                if (postback) {
                    postback(xmlup.response);
                }
                else {
                    alert("没有设定一个取得结果的方法" + xmlup.response)
                }
            }
        }
    }
    f.append("type", filetyp)
    xmlup.open("POST", posturl, true);
    xmlup.send(f);
}

//检查文本文档文件
function TextFileCheck(fileitem, filetyp) {
    if (filetyp != ".html" && filetyp != ".htm" && filetyp != ".txt") {
        throw new Error("非有效HTML文档文件");
    }
    else if (fileitem.size > 2097152) {
        throw new Error("HTML文档文件超过2M限制");
    }
    else {
        return true;
    }
}

//检查图片文件
function ImgFileCheck(fileitem, filetyp) {
    if (filetyp != ".gif" && filetyp != ".jpeg" && filetyp != ".jpg" && filetyp != ".png" && filetyp != ".bmp") {
        throw new Error("非有效图片文件！");
    }
    else if (fileitem.size > 2097152) {
        throw new Error("图片文件超过2M限制！");
    }
    else {
        return true;
    }
}