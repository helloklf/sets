﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.Messages
{
    public partial class EditArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var DataIndex = Request.Params["DataIndex"];//要编辑的文章的索引
            if (Session["UID"] == null)
            {
                Response.Redirect("/UserPages/Login.aspx");
            }
            else if (string.IsNullOrWhiteSpace(DataIndex))
                Response.Redirect("ArticleList.aspx",true);
            else
            {
                if (!MSDB.AricleValid(Request.Params["DataIndex"], Session["UID"] as string))//验证是否是本人的文章
                {
                    Response.Redirect("ArticleList.aspx", true);
                }
            }
        }
    }
}