﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArticleList.aspx.cs" Inherits="MyWebSite.Messages.ArticleList" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head">文章列表</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script src="script/AjaxGetArticleList.js"></script>
    <ul id="DocDataList">
        <%# Htmls %>
        <li id="DatGetState" class="loadMore" onclick="GetMoreData()">···加载更多数据···</li>
    </ul>
</asp:Content>