﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWebSite.Messages
{
    public partial class WorksList : System.Web.UI.Page
    {
        public System.Text.StringBuilder Htmls = new System.Text.StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {

            //请求类型参数
            var requestType = Request.Params["Type"] == null ? "" : Request.Params["Type"].ToLower();
            //用户的账号索引
            var indexID = Request.Params["DataIndex"];

            if (!string.IsNullOrWhiteSpace(indexID))//指定查看某人的内容
            {
                GetOneUserData();
            }
            else if (requestType == "public")//查看公众内容
            {
                GetPublicData();
            }
            else if (Session["UID"] != null)//查看本人内容
            {
                GetWeData();
            }
            else//默认
            {
                GetPublicData();
            }
            DataBind();
        }



        /// <summary>
        /// 获取公众内容
        /// </summary>
        void GetPublicData()
        {
            List<DBModel.WorksModel> Articles = MSDB.GetWorks(0);
            foreach (var item in Articles)
            {
                item.DocData = "发表于：" + item.DateTime;
                Htmls.Append(
                                string.Format(
                               "<li><img src='null'/><div title = '点击查看详细' class='RightBlock' onclick='window.open(\"WorksDetails.aspx?DataIndex={0}\")'><div class='TileTitle'>{1}</div><div class='TileText'>发表于：{2}</div></div></li>", item.DataIndex, item.DocTitle, item.DocData));
            }
        }

        /// <summary>
        /// 指定用户的内容
        /// </summary>
        void GetOneUserData()
        {
            if ((Session["UID"] as string) == Request.Params["DataIndex"])
            {
                GetWeData();
                return;
            }
            List<DBModel.WorksModel> Articles = MSDB.GetWorks_OneUser(0, Request.Params["DataIndex"]);
            foreach (var item in Articles)
            {
                item.DocData = "发表于：" + item.DateTime;
                Htmls.Append(
                                string.Format(
                               "<li><img src='null'/><div title = '点击查看详细' class='RightBlock' onclick='window.open(\"WorksDetails.aspx?DataIndex={0}\")'><div class='TileTitle'>{1}</div><div class='TileText'>发表于：{2}</div></div></li>", item.DataIndex, item.DocTitle, item.DocData));

            }
        }

        /// <summary>
        /// 本人的内容
        /// </summary>
        void GetWeData()
        {
            List<DBModel.WorksModel> Articles = MSDB.GetWorks_OneUser(0, Session["UID"] as string);
            foreach (var item in Articles)
            {
                item.CanEdit = true;
            }
            foreach (var item in Articles)
            {
                item.DocData = "发表于：" + item.DateTime;
                Htmls.Append(
                                string.Format("<li><img src='null'/><div title = '点击查看详细' class='RightBlock' onclick='window.open(\"WorksDetails.aspx?DataIndex={0}\")'><div class='TileTitle'>{1}</div><div class='TileText'>发表于：{2}</div></div><div class='DeleteBlock'><a href='WorksEdit.aspx?DataIndex={0}' target='_blank'>✂</a><a href='javascript:DeleteData(\"{0}\")'>✖</a></div></li>", item.DataIndex, item.DocTitle, item.DocData));

            }
        }
    }
}