﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArticleDetails.aspx.cs" Inherits="MyWebSite.Messages.ArticleDetails" MasterPageFile="~/Masters/Default.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="head">文章详细内容</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div>
        <style>
            #DocTitle{
                font-size:25px; font-weight:800; text-align:center; padding:10px;
            }
            #DocDocument{
                padding:10px;
             }
            #DocDateTime{
                text-align:center; font-size:12px;
            }
        </style>
        <div id="DocTitle">文章标题</div>
        <div id="DocDateTime">发布时间：</div><div id="leamain">
     </div>

        <iframe id="DocDocument"  src="#" name="DocDocument" style="border-style: none; border-color: inherit; border-width: medium; padding: 0px; width:100%; background:; margin:0px; display:block; overflow:visible;">
            正在加载文章内容，请稍后...
        </iframe>
        <style>
            .ArticleComments{
                list-style:none; padding-left:0px;
                color:#000;
            }
            .ArticleComments li{
                margin-top:25px; padding-left:10px;
                background-color:rgba(255,255,255,0.2);
                border-bottom:1px solid rgba(255,255,255,0.5);
                font-size:13px; line-height:18px;
            }
            .ArticleComments li:hover:not(:last-child){
                background:rgba(255,255,255,0.8);
                color:#000;
            }
            .ArticleComments li:hover:last-child{
                color:#000;
            }
            .ArticleComments li:hover *{
                color:#000;
            }
            .ArticleComments li>span:first-child{
                font-weight:600; display:inline-block; width:280px; font-size:18px;
            }
            .ArticleComments li>blockquote{
                font-size:14px; line-height:18px; font-weight:500;
             }
            .InputNewArticleComments{
                width:100%;
                padding:2px 5px; line-height:18px;border:0px; border-bottom:1px solid rgba(255,255,255,0.5); background:rgba(255,255,255,0.2); display:block; text-align:left; height:85px;
            }
            .SaveNewArticleComments{
                padding:2px 5px; min-width:75px;border:1px solid rgba(255,255,255,0.5); background:rgba(255,255,255,0.2)
            }
        </style>
        <ul class="ArticleComments" id="ArticleComments">
            <li>
                <div id="ArticleCommentsIsLoad" style="text-align:center">正在加载评论，请稍等...</div>
            </li>

            <li>
                <span>🙋&nbsp;我有话要说</span>&nbsp;&nbsp;&nbsp;&nbsp;⏰&nbsp;<detetime>发表时自动获取</detetime><br />
                <style>
                </style>
                <div style="text-align:right">
                    <br />
                    <textarea class="InputNewArticleComments" placeholder="在这里输入要发表的内容..."></textarea>
                    <br />
                    <input type="button" value="✔ 发表" class="SaveNewArticleComments"/>
                </div>
            </li>
        </ul>
    </div>
    <script src="../JavaScripts/Tools.js"></script>
    <script>
        function iFrameHeight() {
            var ifm = document.getElementById("DocDocument");
            var subWeb = document.frames ? document.frames["DocDocument"].document :
            ifm.contentDocument;
            if (ifm != null && subWeb != null) {
                ifm.height = subWeb.body.scrollHeight;
            }
        }
        function GetArticleComments() {
            var xml = new XMLHttpRequest();
            xml.onreadystatechange = function () {
                if (xml.readyState == 4) {
                    if (xml.status == 200) {
                        try
                        {
                            var jd = JSON.parse(xml.response);
                            var ArticleComments = document.getElementById("ArticleComments");
                            if (ArticleComments.childElementCount > 1)
                                ArticleComments.removeChild(ArticleComments.firstChild);
                            if (jd.length < 1)
                            {
                                var loadState = document.getElementById("ArticleCommentsIsLoad");
                                if (loadState)
                                    loadState.textContent = "还没有发表评论，赶紧抢沙发吧！";
                                return;
                            }
                            for (var i in xml.response)
                            {
                                var li = document.createElement("li");
                                li.innerHTML = "<span>🙈&nbsp;" + jd[0].DocTitle + "</span>&nbsp;&nbsp;&nbsp;&nbsp;⏰&nbsp;<detetime>" + jd[0].DateTimeText + "</detetime><br /><blockquote>" + jd[0].DocData + "</blockquote>";
                                ArticleComments.insertBefore(ArticleComments.firstChild,li);
                            }
                        }
                        catch (e) 
                        {
                            alert(e.message);
                        }
                    }
                }
            }
            xml.open("GET", "/Processing/Message/GetArticleComments.ashx?DataIndex=" + dataIndex, true);
            xml.send();
        }

        function GetTheDataToDetails(response) {
            var json = JSON.parse(response);
            for (var i in json) {
                document.getElementById("DocTitle").innerHTML = json[0].DocTitle;
                if (json[0].DateTime != "null" && json[0].DateTime != null)
                    document.getElementById("DocDateTime").innerHTML = "发表于：" + json[0].DateTimeText;
                else
                    document.getElementById("DocDateTime").innerHTML = "发表于：未知时间";
                    var docu = document.getElementById("DocDocument").contentWindow.document;
                    docu.write(json[0].DocData);
                    iFrameHeight();
                //document.getElementById("DocDocument").innerHTML = json[0].DocData;
                GetArticleComments();
            }
        }

        //获取内容
        function GetTheData(dataIndex, back) {
            var xml = new XMLHttpRequest();
            xml.onreadystatechange = function () {
                if (xml.readyState == 4) {
                    if (xml.status == 200) {
                        back(xml.response);
                    }
                }
            }
            xml.open("GET", "/Processing/Message/GetArticleDetails.ashx?DataIndex=" + dataIndex, true);
            xml.send();
        }

        var dataIndex  = GetLocalUrlParameterByKey("DataIndex");
        if (dataIndex == null || dataIndex == "")
        {
            location.href = "ArticleList.aspx";
        }
        else
            GetTheData(dataIndex, GetTheDataToDetails);

    </script>
</asp:Content>
