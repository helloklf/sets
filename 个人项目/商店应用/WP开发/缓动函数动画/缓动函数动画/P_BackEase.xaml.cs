﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace 缓动函数动画
{
    public partial class P_BackEase : UserControl
    {
        public P_BackEase()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Out.Begin();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.In.Begin();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.InOut.Begin();
        }
    }
}
