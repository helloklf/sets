﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using 联系人操作_SilverLight.Resources;
using Microsoft.Phone.Tasks;
using Windows.Phone.PersonalInformation;

using System.IO;
using Windows.Storage.Streams;

namespace 联系人操作_SilverLight
{
    public partial class MainPage : PhoneApplicationPage
    {
        // 构造函数
        public MainPage()
        {
            InitializeComponent();

            // 用于本地化 ApplicationBar 的示例代码
            //BuildLocalizedApplicationBar();
        }

        // 用于生成本地化 ApplicationBar 的示例代码
        //private void BuildLocalizedApplicationBar()
        //{
        //    // 将页面的 ApplicationBar 设置为 ApplicationBar 的新实例。
        //    ApplicationBar = new ApplicationBar();

        //    // 创建新按钮并将文本值设置为 AppResources 中的本地化字符串。
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // 使用 AppResources 中的本地化字符串创建新菜单项。
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
        /// <summary>
        /// 添加联系人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Add_Click(object sender, RoutedEventArgs e)
        {
            ContactStore cs = await ContactStore.CreateOrOpenAsync(ContactStoreSystemAccessMode.ReadWrite, ContactStoreApplicationAccessMode.ReadOnly);
            ContactInformation ci = new ContactInformation();
            var pr = await ci.GetPropertiesAsync();

            pr.Add(KnownContactProperties.FamilyName, this.Name.Text);
            pr.Add(KnownContactProperties.Telephone, this.Tel.Text);
            pr.Add(KnownContactProperties.WorkEmail, this.Email.Text);
            StoredContact store = new StoredContact(cs, ci);
            if (saveSt != null) 
            {
                await store.SetDisplayPictureAsync(saveSt);
            }
            await store.SaveAsync();

            this.Name.Text = "";
            this.Tel.Text = "";
            this.Email.Text = "";
        }

        IInputStream saveSt;
        void btc_Completed(object sender, PhotoResult e)
        {
            if (e.ChosenPhoto != null)
            {
                saveSt = e.ChosenPhoto.AsInputStream();
            }
        }

        private void HeaderImg_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var btc = new PhotoChooserTask();
            btc.PixelHeight = 170;
            btc.PixelWidth = 170;
            btc.Completed += btc_Completed;
            btc.Show();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            ContactStore cs = await ContactStore.CreateOrOpenAsync(ContactStoreSystemAccessMode.ReadWrite, ContactStoreApplicationAccessMode.ReadOnly);
            ContactQueryResult cqr = cs.CreateContactQuery();
            IReadOnlyList<StoredContact> c = await cqr.GetContactsAsync();

            ConList.ItemsSource = c.ToList();
        }
    }
}