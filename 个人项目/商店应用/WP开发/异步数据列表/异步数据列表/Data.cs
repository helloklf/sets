﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace 异步数据列表
{
    class Data
    {
        public string Name { get; set; }
        Uri imageUri;

        public Uri ImageUri
        {
            get { return imageUri; }
            set { imageUri = value; }
        }

        /// <summary>
        /// 一个弱引用的对象
        /// </summary>
        WeakReference bitmapImage;

        public ImageSource ImageSource
        {
            get 
            {
                if (bitmapImage != null)
                {
                    if (bitmapImage.IsAlive)
                    {
                        return (ImageSource)bitmapImage.Target;
                    }
                    else 
                    {
                        Debug.WriteLine("该对象可能已经被垃圾回收器回收！");
                        return null;
                    }
                }
                else 
                {
                    if (imageUri != null)
                    {
                        //Windows.System.Threading.ThreadPool.RunAsync(); 
                        Task.Run(() => { DownLoad(imageUri); });
                        return null;
                    }
                    else
                        return null;   
                }
            }
        }

        public void DownLoad(object o) 
        {
            HttpWebRequest requeset = WebRequest.CreateHttp(o as Uri);
            requeset.BeginGetResponse(DownLoadComplate,requeset);

        }

        public void DownLoadComplate(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            HttpWebResponse response = (HttpWebResponse) request.EndGetResponse(result);
            Stream stream = response.GetResponseStream();
            Stream streamForUI = new MemoryStream((int)stream.Length);
            byte[] buffer = new byte[(int)stream.Length];
            int read = 0;
            do
            {
                read= stream.Read(buffer,0,(int)stream.Length);
                streamForUI.Write(buffer,0,read);
            }
            while( read== (int)stream.Length );
            streamForUI.Seek(0,SeekOrigin.Begin);
            //! 
        }
    }
}
