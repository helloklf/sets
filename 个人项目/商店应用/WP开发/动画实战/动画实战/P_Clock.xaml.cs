﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;

namespace 动画实战
{
    public partial class P_Clock : UserControl
    {
        public P_Clock()
        {
            InitializeComponent(); S1.Begin();
            DispatcherTimer time = new DispatcherTimer();
            time.Tick += time_Tick;
            time.Interval = TimeSpan.FromMilliseconds(1000);
            time.Start();
        }

        void time_Tick(object sender, EventArgs e)
        {
            this.T_H.Text = System.DateTime.Now.Hour.ToString();
            this.T_M.Text = System.DateTime.Now.Minute.ToString();
            this.T_S.Text = System.DateTime.Now.Second.ToString();
            //throw new NotImplementedException();
        }
    }
}
