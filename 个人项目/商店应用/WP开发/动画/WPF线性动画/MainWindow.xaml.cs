﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF线性动画
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            foreach (Button item in ButList.Children)
            {
                item.Click += item_Click;
            }
        }

        void item_Click(object sender, RoutedEventArgs e)
        {
            TabList.SelectedIndex = int.Parse((sender as Button).Tag.ToString());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DoubleAnimation doub = new DoubleAnimation() { From = 320, To = 30, Duration = TimeSpan.FromMilliseconds(250) };
            Storyboard story = new Storyboard() {  };
            story.Children.Add(doub);
            Storyboard.SetTarget( doub,B1);
            Storyboard.SetTargetProperty(doub,new PropertyPath("Height"));
            story.Begin();
            CloseBtn.Visibility = Visibility.Collapsed; OpenBtn.Visibility = Visibility.Visible;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DoubleAnimation doub = new DoubleAnimation() { From = 30, To = 320, Duration = TimeSpan.FromMilliseconds(250) };
            Storyboard story = new Storyboard() { };
            story.Children.Add(doub);
            Storyboard.SetTarget(doub, B1);
            Storyboard.SetTargetProperty(doub, new PropertyPath("Height"));
            story.Begin();

            CloseBtn.Visibility = Visibility.Visible; OpenBtn.Visibility = Visibility.Collapsed;
        }

        private void TabList_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) 
            {
                
            }
        }

        /// <summary>
        /// 鼠标起始位置
        /// </summary>
        double x = 0; double y = 0;
        /// <summary>
        /// 鼠标按下时获取鼠标位置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            x = e.GetPosition(TabList).X;
            y = e.GetPosition(TabList).Y;
        }

        /// <summary>
        /// 释放鼠标时获取新坐标计算移动大小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabList_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            double c = e.GetPosition(TabList).X - x;
            if (Math.Abs(c)> 90 && Math.Abs(e.GetPosition(TabList).Y - y) < 30) 
            {
                if (c > 0)
                {
                    if (TabList.SelectedIndex != 0)
                        TabList.SelectedIndex = TabList.SelectedIndex - 1;
                }
                else 
                {
                    if(TabList.SelectedIndex!=TabList.Items.Count-1)
                    TabList.SelectedIndex = TabList.SelectedIndex + 1;
                }
            }
        }

    }
}
