﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using 基于帧的动画.Resources;
using System.Windows.Media;

namespace 基于帧的动画
{
    public partial class MainPage : PhoneApplicationPage
    {
        Point index;
        // 构造函数
        public MainPage()
        {
            InitializeComponent();
            CompositionTarget.Rendering += CompositionTarget_Rendering;
            r1.RenderTransform = ts;
        }
        DateTime dt = DateTime.Now;
        TranslateTransform ts = new TranslateTransform();
        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            DateTime d = DateTime.Now;
            double span = (d - dt).TotalSeconds;
            dt = d;
            if (index != null) 
            {
                ts.X += index.X * span;
                ts.Y += index.Y * span;
            }
        }

        private void C1_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.index = e.GetPosition(C1);
        }

        private void C1_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.index = e.GetPosition(C1);
        }
    }
}