﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using System.ComponentModel;

namespace TimeHelp.MyPages
{
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();
            DI.Load(); dt.Interval = new TimeSpan(0, 0, 1); dt.Start(); dt.Tick += dt_Tick; L.DataContext = DI;
        }
        DateInfo DI = DateInfo.DI;
        void dt_Tick(object sender, object e)
        {
            this.MyDate.Text = DateTime.Now.ToString("yyyy-MM-dd ") + Other_Function.GetWeek(DateTime.Now.DayOfWeek);
            this.MyTime.Text = DateTime.Now.ToString("HH:mm:ss");
            DI.Load();
        }
        DispatcherTimer dt = new DispatcherTimer();

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            MpreInfo.Visibility= MpreInfo.Visibility==Visibility.Collapsed?Visibility.Visible:Visibility.Collapsed;
            MoreInfoLink.Content = MpreInfo.Visibility == Visibility.Collapsed ? "详细内容" : "基本信息";
        }
    }

    public class DateInfo : INotifyPropertyChanged
    {
        public TimeHelp.Other_Function.ChineseCalendar cc = new TimeHelp.Other_Function.ChineseCalendar(System.DateTime.Now);
        public static DateInfo DI = new DateInfo();
        public void Load()
        {
            DI.dateString = cc.DateString;
            DI.animalString = cc.AnimalString;
            DI.chineseDateString = cc.ChineseDateString;
            DI.chineseHour = cc.ChineseHour;
            DI.chineseTwentyFourDay = cc.ChineseTwentyFourDay;
            DI.dateHoliday = cc.DateHoliday;
            DI.chineseTwentyFourPrevDay = cc.ChineseTwentyFourPrevDay;
            DI.chineseTwentyFourNextDay = cc.ChineseTwentyFourNextDay;
            DI.ganZhiDateString = cc.GanZhiDateString;
            DI.weekDayStr = cc.WeekDayStr;
            DI.chineseConstellation = cc.ChineseConstellation;
            DI.constellation = cc.Constellation;

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("DateString"));
                PropertyChanged(this, new PropertyChangedEventArgs("AnimalString"));
                PropertyChanged(this, new PropertyChangedEventArgs("ChineseDateString"));
                PropertyChanged(this, new PropertyChangedEventArgs("ChineseHour"));
                PropertyChanged(this, new PropertyChangedEventArgs("ChineseTwentyFourDay"));
                PropertyChanged(this, new PropertyChangedEventArgs("DateHoliday"));
                PropertyChanged(this, new PropertyChangedEventArgs("ChineseTwentyFourPrevDay"));
                PropertyChanged(this, new PropertyChangedEventArgs("ChineseTwentyFourNextDay"));
                PropertyChanged(this, new PropertyChangedEventArgs("GanZhiDateString"));
                PropertyChanged(this, new PropertyChangedEventArgs("WeekDayStr"));
                PropertyChanged(this, new PropertyChangedEventArgs("ChineseConstellation"));
                PropertyChanged(this, new PropertyChangedEventArgs("Constellation"));
            }
        }


        string dateString;
        /// <summary>
        /// 阳历
        /// </summary>
        public string DateString
        {
            get { return "阳历:" + dateString; }
            //set { dateString = value; }
        }

        string animalString;
        /// <summary>
        /// 属相
        /// </summary>
        public string AnimalString
        {
            get { return "属相:" + animalString; }
            //set { animalString = value; }
        }

        string chineseDateString;
        /// <summary>
        /// 农历
        /// </summary>
        public string ChineseDateString
        {
            get { return "农历:" + chineseDateString; }
            //set { chineseDateString = value; }
        }

        string chineseHour;
        /// <summary>
        /// 时辰
        /// </summary>
        public string ChineseHour
        {
            get { return "时辰:" + chineseHour; }
            //set { chineseHour = value; }
        }

        string chineseTwentyFourDay;
        /// <summary>
        /// 节气
        /// </summary>
        public string ChineseTwentyFourDay
        {
            get { return "节气:" + chineseTwentyFourDay; }
            //set { chineseTwentyFourDay = value; }
        }

        string dateHoliday;
        /// <summary>
        /// 节日
        /// </summary>
        public string DateHoliday
        {
            get { return "节日:" + dateHoliday; }
            //set { dateHoliday = value; }
        }

        string chineseTwentyFourPrevDay;
        /// <summary>
        /// 前一个节气
        /// </summary>
        public string ChineseTwentyFourPrevDay
        {
            get { return "前一节气:" + chineseTwentyFourPrevDay; }
            //set { chineseTwentyFourPrevDay = value; }
        }

        string chineseTwentyFourNextDay;
        /// <summary>
        /// 后一个节气
        /// </summary>
        public string ChineseTwentyFourNextDay
        {
            get { return "后一个节气:" + chineseTwentyFourNextDay; }
            //set { chineseTwentyFourNextDay = value; }
        }

        string weekDayStr;
        /// <summary>
        /// 星期
        /// </summary>
        public string WeekDayStr
        {
            get { return "星期:" + weekDayStr; }
            //set { weekDayStr = value; }
        }

        string ganZhiDateString;
        /// <summary>
        /// 干支
        /// </summary>
        public string GanZhiDateString
        {
            get { return "干支:" + ganZhiDateString; }
            //set { ganZhiDateString = value; }
        }

        string chineseConstellation;
        /// <summary>
        /// 星宿
        /// </summary>
        public string ChineseConstellation
        {
            get { return "星宿:" + chineseConstellation; }
            //set { chineseConstellation = value; }
        }

        string constellation;
        /// <summary>
        /// 星座
        /// </summary>
        public string Constellation
        {
            get { return "星座:" + constellation; }
            //set { constellation = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
