﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TimeHelp.MyPages
{
    public partial class ImageList : UserControl
    {
        public ImageList()
        {
            SettingSave.Load();
            InitializeComponent();
            LB.DataContext = Images;
        }
        static List<ImageInfo> Images = new List<ImageInfo>()
        {
            new ImageInfo(){ Title="酷黑经典",Uri= "/Images/i (1).jpg",MoreInfo="经典的褐色与蓝色多边形搭配，彰显硬朗气息，低调不低挡。"},
            new ImageInfo(){ Title="清新蓝色",Uri= "/Images/i (2).jpg",MoreInfo="白色折纸小飞机、小清新的蓝色，"},
            new ImageInfo(){ Title="简约",Uri= "/Images/i (3).jpg",MoreInfo="简单清净"},
            new ImageInfo(){ Title="闪耀光斑",Uri= "/Images/i (4).jpg"},
            new ImageInfo(){ Title="卡通图片",Uri= "/Images/i (5).jpg"},
            new ImageInfo(){ Title="太阳花",Uri= "/Images/i (6).jpg"},
            new ImageInfo(){ Title="海绵宝宝",Uri= "/Images/i (7).jpg"},
            new ImageInfo(){ Title="美国国旗",Uri= "/Images/i (8).jpg"},
            new ImageInfo(){ Title="小小瓢虫",Uri= "/Images/i (9).jpg"},
            new ImageInfo(){ Title="海的声音",Uri= "/Images/i (10).jpg"},
            new ImageInfo(){ Title="沙滩贝壳",Uri= "/Images/i (11).jpg"},
            new ImageInfo(){ Title="色彩缤纷",Uri= "/Images/i (12).jpg"},
            new ImageInfo(){ Title="天然绿色",Uri= "/Images/i (13).jpg"},
            new ImageInfo(){ Title="天空之蓝",Uri= "/Images/i (14).jpg"},
            new ImageInfo(){ Title="思想幻境",Uri= "/Images/i (15).jpg"},
            new ImageInfo(){ Title="活力橙色",Uri= "/Images/i (16).jpg"}
        };

        private void ListBox_DoubleTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Image image = (sender as Grid).Children[0] as Image;
            Setting.Surface.BackImage = new ImageBrush() { ImageSource = image.Source };
            this.Background = new ImageBrush() { ImageSource = image.Source };
            Setting.Surface.ImageUri = image.Tag.ToString();
            SettingSave.Save();
            //Image image = sender as Image;
            //Setting.Surface.BackImage = new ImageBrush() { ImageSource = image.Source };
        }

    }

    public class ImageInfo
    {
        string title;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        string uri;

        public string Uri
        {
            get { return uri; }
            set { uri = value; }
        }

        string moreInfo;

        public string MoreInfo
        {
            get { return moreInfo; }
            set { moreInfo = value; }
        }
    }
}
