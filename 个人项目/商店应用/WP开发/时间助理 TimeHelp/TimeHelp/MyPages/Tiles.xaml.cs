﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace TimeHelp.MyPages
{
    public partial class Tiles : UserControl
    {
        static List<TileInfo> TL = new List<TileInfo>()
        {
            new TileInfo() { Title = "标题", Content = "内容" },
            new TileInfo() { Title = "标题", Content = "内容" },
            new TileInfo() { Title = "标题", Content = "内容" }
        };
        public Tiles()
        {
            InitializeComponent(); TileList.DataContext = TL;
        }
    }
    public class TileInfo
    {
        string title;
        /// <summary>
        /// 磁贴标题
        /// </summary>
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        string content;
        /// <summary>
        /// 显示内容
        /// </summary>
        public string Content
        {
            get { return content; }
            set { content = value; }
        }
    }
}
