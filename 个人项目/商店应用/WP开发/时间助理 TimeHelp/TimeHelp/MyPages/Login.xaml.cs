﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace TimeHelp.MyPages
{
    public partial class Login : UserControl
    {
        public Login()
        {
            InitializeComponent();
        }

        private void InputNum(object sender, RoutedEventArgs e)
        {
            Button but = sender as Button;
            if(PassWord.Text.Length<16)
            PassWord.Text += but.Content.ToString();
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            if (PassWord.Text.Length != 0) { PassWord.Text = PassWord.Text.Substring(0, PassWord.Text.Length - 1); }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
