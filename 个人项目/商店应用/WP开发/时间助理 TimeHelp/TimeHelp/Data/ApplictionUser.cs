﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeHelp.Data
{
    public class ApplictionUser
    {
        string userName;
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        string userPass;
        /// <summary>
        /// 用户密码
        /// </summary>
        public string UserPass
        {
            get { return userPass; }
            set { userPass = value; }
        }
    }
}
