﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage.Streams;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Xml;
using System.Diagnostics;

namespace TimeHelp
{
    public class Setting:INotifyPropertyChanged
    {
        public static Setting Surface = new Setting();
        private string imageUri = "/Images/i (1).jpg";

        public string ImageUri
        {
            get { return imageUri; }
            set
            {
                imageUri = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("BackImage")); image.Stretch = Stretch.UniformToFill;
                }
            }
        }
        ImageBrush image;
        public ImageBrush BackImage 
        {
            get {
                if (image == null) image = new ImageBrush() { ImageSource = new BitmapImage(new Uri(ImageUri, UriKind.Relative)), Stretch = Stretch.UniformToFill }; 
                return image; }
            set
            {
                image = value; 
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("BackImage")); image.Stretch = Stretch.UniformToFill;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class SettingSave
    {
        public static void Load()
        {
            try
            {
                if (File.Exists("BgSetting.dat"))
                {
                    System.Xml.Linq.XElement xe = XElement.Load("BgSetting.dat");
                    Setting.Surface.ImageUri = xe.Element("Setting").Element("ImageUri").Value.ToString();
                }
                else { CreateNew(); Load(); }
            }
            catch (Exception ex)
            {
                CreateNew();
                Debug.WriteLine(ex.Message);
            }
        }
        public static void Save() 
        {
            XElement xe = new XElement("RootNode", new XElement[]
            { 
                new XElement("Setting", new XElement[]
                    { new XElement("ImageUri", Setting.Surface.ImageUri == null ? "/Images/i (1).jpg":Setting.Surface.ImageUri) })
            });
            using (FileStream fs = File.Create("BgSetting.dat"))
            {
                xe.Save(fs);
            }
        }

        /// <summary>
        /// 创建新的设置
        /// </summary>
        public static void CreateNew() 
        {
            XElement xe = new XElement("RootNode", new XElement[] { new XElement("Setting", new XElement[] { new XElement("ImageUri", "/Images/i (1).jpg") }) });
            using (FileStream fs = File.Create("BgSetting.dat"))
            {
                xe.Save(fs);
            }
        }
    }
}
