﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Windows.Storage;

namespace TimeHelp.Data
{
    public class DataRead:Stream
    {
        public static void GetData()
        {
            //System.IO.BinaryReader br = new System.IO.BinaryReader();
            //FileStream fs = new FileStream("temp.xml", FileMode.Create, FileAccess.Write, FileShare.Read);
            //XmlWriter write = XmlWriter.Create(fs);
            //write.WriteStartDocument();
            //write.WriteStartElement("Movies");
            //write.WriteStartElement("movie");
            //write.WriteAttributeString("id", "1");
            //write.WriteComment(" this is a movie name");

            //write.WriteStartElement("name");
            //write.WriteString("暗战");
            //write.WriteEndElement();//name end

            //write.WriteStartElement("price");
            //write.WriteString("10");
            //write.WriteEndElement();

            //write.WriteEndElement();//movie end
            //write.WriteEndElement();//Movies end
            //write.WriteEndDocument();
            //write.Close();
            //LB.Items.Clear();
            //ListBoxItem lb = new ListBoxItem(); lb.Content = "写入完成";
            //LB.Items.Add(lb);

            //write.Dispose();
            //fs.Close(); fs.Dispose();
            
        }

        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanWrite
        {
            get { throw new NotImplementedException(); }
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// 闹钟
    /// </summary>
    public class AlarmClock
    {
        string title;
        /// <summary>
        /// 内容标题
        /// </summary>
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        string allInfo;
        /// <summary>
        /// 全部信息
        /// </summary>
        public string AllInfo
        {
            get { return allInfo; }
            set { allInfo = value; }
        }

        private int AlertCount;
        /// <summary>
        /// 提醒次数
        /// </summary>
        public int AlertCount1
        {
            get { return AlertCount; }
            set { AlertCount = value; }
        }

        DateTime alertTime;
        /// <summary>
        /// 提醒时间
        /// </summary>
        public DateTime AlertTime
        {
            get { return alertTime; }
            set { alertTime = value; }
        }

        bool isValid;
        /// <summary>
        /// 是否有效
        /// </summary>
        public bool IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }
    }
}
