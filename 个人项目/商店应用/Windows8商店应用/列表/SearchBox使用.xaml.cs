﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace 列表
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class SearchBox使用 : Page
    {
        public SearchBox使用()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 提交内容
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private async void Xaml_SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            var md = new Windows.UI.Popups.MessageDialog("输入的内容："+args.QueryText);
            await md.ShowAsync();
        }
    }
}
