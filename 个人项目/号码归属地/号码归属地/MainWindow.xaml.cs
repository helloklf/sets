﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace 号码归属地
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string[] text = File.ReadAllLines("手机号段归属地数据库20120814.txt",Encoding.Default);
            foreach (var item in text)
            {
               string t= item.Replace("\"\t\"","\"");
                string[] txt = t.Split('"');
                LB.Items.Add("号段："+txt[0]+"\t\t归属地："+txt[1]+"\t\t公司："+txt[2]+"\t\t区号："+txt[3]);
            }
        }
    }
}
