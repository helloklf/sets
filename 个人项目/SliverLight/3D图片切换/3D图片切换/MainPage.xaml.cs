﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace _3D图片切换
{
    public partial class MainPage : UserControl
    {
        private Point pt;
        public MainPage()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.MouseMove += new MouseEventHandler(LayoutRoot_MouseMove);
            CompositionTarget.Rendering += new EventHandler(CompositionTarget_Rendering);
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            Image1Projection.RotationY += ((pt.X - (LayoutRoot.ActualWidth / 2)) / LayoutRoot.ActualWidth) * 10;
            Image2Projection.RotationY += ((pt.X - (LayoutRoot.ActualWidth / 2)) / LayoutRoot.ActualWidth) * 10;
            Image3Projection.RotationY += ((pt.X - (LayoutRoot.ActualWidth / 2)) / LayoutRoot.ActualWidth) * 10;
            Image4Projection.RotationY += ((pt.X - (LayoutRoot.ActualWidth / 2)) / LayoutRoot.ActualWidth) * 10;
            Image5Projection.RotationY += ((pt.X - (LayoutRoot.ActualWidth / 2)) / LayoutRoot.ActualWidth) * 10;
            Image6Projection.RotationY += ((pt.X - (LayoutRoot.ActualWidth / 2)) / LayoutRoot.ActualWidth) * 10;
            image1.Opacity = (pt.Y / LayoutRoot.ActualHeight) * 0.5 + 0.5;
            image2.Opacity = (pt.Y / LayoutRoot.ActualHeight) * 0.5 + 0.5;
            image3.Opacity = (pt.Y / LayoutRoot.ActualHeight) * 0.5 + 0.5;
            image4.Opacity = (pt.Y / LayoutRoot.ActualHeight) * 0.5 + 0.5;
            image5.Opacity = (pt.Y / LayoutRoot.ActualHeight) * 0.5 + 0.5;
            image6.Opacity = (pt.Y / LayoutRoot.ActualHeight) * 0.5 + 0.5;
        }

        void LayoutRoot_MouseMove(object sender, MouseEventArgs e)
        {
            pt = e.GetPosition(LayoutRoot);
            
        }
    }
}
