﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=391641 上有介绍

namespace 淘WEB
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            
        }

        bool canexit = false;
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        { 
            /*if (Frame.CanGoBack) 
            {
                e.Handled = true; 
                Frame.GoBack(); 
            }*/
            if (WebPage.CanGoBack)
            {
                e.Handled = true;
                WebPage.GoBack();
            }
            else if(canexit==false)
            {
                DispatcherTimer dt = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(2000) };
                
                dt.Tick += (a, b) =>
                {
                    canexit = false;
                };
                dt.Start();
                e.Handled = true;
                canexit = true;
            }
        }

        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。
        /// 此参数通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: 准备此处显示的页面。

            // TODO: 如果您的应用程序包含多个页面，请确保
            // 通过注册以下事件来处理硬件“后退”按钮:
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed 事件。
            // 如果使用由某些模板提供的 NavigationHelper，
            // 则系统会为您处理该事件。
        }


        /// <summary>
        /// 网页加载完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WebPage_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            Xaml_PageLoadState.Visibility = Visibility.Collapsed;

            Xaml_PageLoadState.IsActive = false;
            
            //Xaml_PageLoadState.IsIndeterminate = false; 
            SetToggleState();
        }

        /// <summary>
        /// 页面加载失败
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void WebPage_NavigationFailed(object sender, WebViewNavigationFailedEventArgs e)
        {
            await new Windows.UI.Popups.MessageDialog("页面加载失败,请检查网络连接！").ShowAsync();
            Xaml_PageLoadState.Visibility = Visibility.Collapsed;

            Xaml_PageLoadState.IsActive = false;

            //Xaml_PageLoadState.IsIndeterminate = false;
        }

        /// <summary>
        /// 页面开始加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void WebPage_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            Xaml_PageLoadState.Visibility = Visibility.Visible;

            Xaml_PageLoadState.IsActive = true;
            
            //Xaml_PageLoadState.IsIndeterminate = true;
            SetToggleState();
        }


        void SetToggleState()
        {
            var uri = WebPage.Source.ToString();
            System.Diagnostics.Debug.WriteLine(uri);

            Navigate_Home.IsChecked = false;
            Navigate_Cart.IsChecked = false;
            Navigate_User.IsChecked = false;
            Navigate_Star.IsChecked = false;

            if (uri.IndexOf("http://m.taobao.com/")>-1 || uri.IndexOf("http://h5.m.taobao.com/index-ie.html") > -1)
            {
                Navigate_Home.IsChecked = true;
            }
            else if (uri.IndexOf("http://h5.m.taobao.com/awp/mtb/mtb.htm") > -1)
            {
                Navigate_User.IsChecked = true;
            }
            else if(uri.IndexOf("http://login.m.taobao.com/login.htm") > -1)
            {
                Navigate_User.IsChecked = true;
            }
            else if (uri.IndexOf("http://h5.m.taobao.com/fav/index.htm") > -1)
            {
                Navigate_Star.IsChecked = true;
            }
            else if (uri.IndexOf("cart.htm")>-1)
            {
                Navigate_Cart.IsChecked = true;
            }
        }


        /// <summary>
        /// 关于应用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_About(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(About));
        }

        /// <summary>
        /// 切换导航栏显示状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TopNavigateBarState_Click(object sender, RoutedEventArgs e)
        {
            Xaml_TopBar.Visibility = Xaml_TopBar.Visibility == Visibility.Collapsed? Visibility.Visible : Visibility.Collapsed;
        }

    }


    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {

        /// <summary>
        /// 导航到首页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Home(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/index-ie.html"));

            (sender as AppBarToggleButton).IsChecked = false;
        }

        /// <summary>
        /// 查看商品浏览记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_History(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/awp/mtb/mtb.htm?#!/awp/mtb/views.htm"));
        }

        /// <summary>
        /// 跳转到登陆页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Login(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://login.m.taobao.com/login.htm"));
        }

        /// <summary>
        /// 跳转到充值页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Cost(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/app/cz/cost.html"));
        }

        /// <summary>
        /// 跳转到分类页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Category(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/category/index.html"));
        }

        /// <summary>
        /// 转到购物车
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Cart(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/awp/base/cart.htm"));
        }

        /// <summary>
        /// 转到用户页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_User(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/awp/mtb/mtb.htm"));
        }

        /// <summary>
        /// 转到收藏页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Fav(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/fav/index.htm"));
        }

        /// <summary>
        /// 转到收藏店铺
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_FavStore(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/fav/index.htm?#!shop"));
        }

        /// <summary>
        /// 阿里旺旺
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_WW(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://h5.m.taobao.com/ww/index.htm"));
        }

        /// <summary>
        /// 支付宝
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Alipay(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://my.m.taobao.com/myAlipay.htm"));
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Search(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://m.taobao.com/channel/act/sale/searchlist.html"));
        }

        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Navigate_Refresh(object sender, RoutedEventArgs e)
        {
            WebPage.Refresh();
        }


        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_Register(object sender, RoutedEventArgs e)
        {
            WebPage.Navigate(new Uri("http://reg.taobao.com/member/new_register.jhtml"));
        }
    }
}
