using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace 淘WEB
{
    /// <summary>
    /// 订单导航
    /// </summary>
    public sealed partial class MainPage : Page
    {
        void OrderNavigate(int state)
        {
            WebPage.Navigate(new Uri(string.Format("http://h5.m.taobao.com/awp/mtb/mtb.htm?spm=0.0.0.0#!/awp/mtb/olist.htm?sta={0}", state)));
        }

        /// <summary>
        /// 代付款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_DFK(object sender, RoutedEventArgs e)
        {
            OrderNavigate(0);
        }


        /// <summary>
        /// 待确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_DQR(object sender, RoutedEventArgs e)
        {
            OrderNavigate(1);
        }

        /// <summary>
        /// 交易成功
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_CG(object sender, RoutedEventArgs e)
        {
            OrderNavigate(2);
        }

        /// <summary>
        /// 退款中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_TKZ(object sender, RoutedEventArgs e)
        {
            OrderNavigate(3);
        }
        /// <summary>
        /// 全部订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_All(object sender, RoutedEventArgs e)
        {
            OrderNavigate(4);
        }

        /// <summary>
        /// 查物流
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_WL(object sender, RoutedEventArgs e)
        {
            OrderNavigate(5);
        }

        /// <summary>
        /// 待发货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_DFH(object sender, RoutedEventArgs e)
        {
            OrderNavigate(6);
        }

        /// <summary>
        /// 待评价
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_DPJ(object sender, RoutedEventArgs e)
        {
            OrderNavigate(7);
        }

        /// <summary>
        /// 三个月前所有订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateTo_All_More(object sender, RoutedEventArgs e)
        {
            OrderNavigate(8);
        }
    }
}