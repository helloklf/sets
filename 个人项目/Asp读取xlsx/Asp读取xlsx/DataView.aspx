﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataView.aspx.cs" Inherits="Asp读取xlsx.DataView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        table {
            color:#fff; width:100%;
        }
        th {
            font-size:18px; padding:12px 25px; text-align:center; font-weight:800; color:#fff; background:#b70000;
        }
        td {
            font-size:12px;padding:8px 15px; text-align:center;color:#333; border-color:none;background:#eee;
        }
    </style>
</head>
<body style="left:30px; right:30px; position:absolute; top:30px; bottom:30px;">
    <form id="form1" runat="server">
    <div>
        <h1 style="color:#b70000">表1的数据：</h1>
        <asp:GridView runat="server" ID="girdview1" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="组长" HeaderText="组长"/>
                <asp:BoundField DataField="成员1" HeaderText="成员1"/>
                <asp:BoundField DataField="成员2" HeaderText="成员2"/>
                <asp:BoundField DataField="成员3" HeaderText="成员3"/>
                <asp:BoundField DataField="成员4" HeaderText="成员4"/>
                <asp:BoundField DataField="成员5" HeaderText="成员5"/>
                <asp:BoundField DataField="成员6" HeaderText="成员6"/>
                <asp:BoundField DataField="成员7" HeaderText="成员7"/>
                <asp:BoundField DataField="成员8" HeaderText="成员8"/>
                <asp:BoundField DataField="成员9" HeaderText="成员9"/>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
