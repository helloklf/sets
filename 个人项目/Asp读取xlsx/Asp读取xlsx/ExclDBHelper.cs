﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.OleDb;
using System.IO;

namespace Asp读取xlsx
{
    public class ExclDBHelper:IDisposable
    {
        /// <summary>
        /// 参数1：文件路径，可选参数：是否将第一行作为标题
        /// </summary>
        /// <param name="pathName"></param>
        /// <param name="firstRowIsTitle"></param>
        public ExclDBHelper(string pathName,bool firstRowIsTitle=false) 
        {
            Conn = GetConn(pathName,firstRowIsTitle);
        }

        OleDbConnection Conn;
        
        /// <summary>
        /// 返回DataTable
        /// </summary>
        /// <param name="exec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public DataTable FillTable(string exec, List<OleDbParameter> pars=null)
        {
            DataTable dt = new DataTable();
            Conn.Open();
            using (OleDbDataAdapter da = new OleDbDataAdapter(exec,Conn))
            {
                if (pars != null)
                da.SelectCommand.Parameters.AddRange(pars.ToArray());
                try
                {
                    da.Fill(dt);
                    return dt;
                }
                catch { return null; }
                finally
                {
                    da.Dispose(); dt.Dispose();
                    Conn.Close();
                }
            }
        }

        /// <summary>
        /// 返回受影响行数
        /// </summary>
        /// <param name="exec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public int ExecQuery(string exec, List<OleDbParameter> pars = null) 
        {
            OleDbCommand cmd = new OleDbCommand(exec, Conn);
            if (pars != null)
            cmd.Parameters.AddRange(pars.ToArray());
            int count = cmd.ExecuteNonQuery();
            cmd.Dispose();
            return count;
        }

        /// <summary>
        /// 返回查询结果的首行首列
        /// </summary>
        /// <param name="exec"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public object ExecScalar(string exec, List<OleDbParameter> pars = null)
        {
            OleDbCommand cmd = new OleDbCommand(exec, Conn);
            if(pars!=null)
            cmd.Parameters.AddRange(pars.ToArray());
            object obj = cmd.ExecuteScalar();
            cmd.Dispose();
            return obj;
        }

        OleDbConnection GetConn(string pathName, bool firstRowIsTitle=false)
        {
            FileInfo file = new FileInfo(pathName);
            if (!file.Exists)
            {
                throw new Exception("指定的Excl文件不存在");
            }
            string extension = file.Extension;
            string strConn;
            switch (extension)
            {
                case ".xls":
                    {
                        if (firstRowIsTitle) strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2;'";
                        else
                            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathName + ";Extended Properties='Excel 8.0;HDR=No;IMEX=2;'";
                        break;
                    }
                case ".xlsx":
                    {
                        if (firstRowIsTitle)
                            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathName + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=2;'";
                        else
                            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathName + ";Extended Properties='Excel 12.0;HDR=No;IMEX=2;'";
                        break;
                    }
                default:
                    {
                        if (firstRowIsTitle)
                            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2;'";
                        else
                            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathName + ";Extended Properties='Excel 8.0;HDR=No;IMEX=2;'";
                        break;
                    }
            }
            OleDbConnection myConn = new OleDbConnection(strConn);
            return myConn;
        }

        public void Dispose()
        {
            Conn.Dispose();
            //throw new NotImplementedException();
        }
    }
}