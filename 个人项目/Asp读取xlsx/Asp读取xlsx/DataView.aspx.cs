﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asp读取xlsx
{
    public partial class DataView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string file = Server.UrlDecode(Request.QueryString["path"]);
            if (file != null)
            {
                ExclDBHelper dbh = new ExclDBHelper(file, true); 
                DataTable dt = dbh.FillTable("SELECT * FROM [Sheet1$]");
                girdview1.DataSource = dt;
                girdview1.DataBind();
            }
        }
    }
}