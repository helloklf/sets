﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows;

//using Excel = Microsoft.Office.Interop.Excel; 


namespace EXCL
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 打开文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog d = new System.Windows.Forms.OpenFileDialog();
            d.Filter = "Excel 2003表格|*.xls|Excel 2007-*表格|*.xlsx|所有电子表格|*.xls;*.xlsx";
            if (d.ShowDialog()==System.Windows.Forms.DialogResult.OK) 
            {
                OpenExcelFile(d.FileName);
            }
        }

        //Provider=Microsoft.ACE.OLEDB.12.0;Data Source = 1.xlsx;Extended Properties=Excel 12.0 //新版
        //Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = 1.xls;Extended Properties=Excel 8.0 //旧版

        /// <summary>
        /// 读取文件
        /// </summary>
        /// <param name="pathName"></param>
        public void OpenExcelFile(string pathName)
        {
            FileInfo file = new FileInfo(pathName);
            if (!file.Exists)
            {
                throw new Exception("文件不存在");
            }
            string extension = file.Extension;
            string strConn;
            switch (extension)
            {
                case ".xls":
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2;'";
                    break;
                case ".xlsx":
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pathName + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=2;'";
                    break;
                default:
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathName + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2;'";
                    break;
            }
            OleDbConnection myConn = new OleDbConnection(strConn); string strCom = " SELECT * FROM [Sheet1$] ";
            myConn.Open();
            OleDbDataAdapter myCommand = new OleDbDataAdapter(strCom, myConn);
            DataSet myDataSet = new DataSet();
            myCommand.Fill(myDataSet, "[Sheet1$]");
            myConn.Close();
            G1.DataContext = myDataSet;
        }

        #region 测试
        /// <summary>
        /// 测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string connstr = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Excel 8.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes'","1.xls");  
            OleDbConnection oleConn = new OleDbConnection(connstr);         
            try
            {
                oleConn.Open();
                string sqlStr;
                DataTable dt = oleConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables_Info, null);
                bool existTable = false;
                foreach (DataRow dr in dt.Rows)//检查是否有信息表  
                {                    
                    if(dr["TABLE_NAME"].ToString()=="信息表$")//要加个$号      
                    existTable = true;              
                }                
                if (!existTable)  
                {
                        OleDbCommand oleCmd = new OleDbCommand(@"create table 信息表(手机 char(15),姓名 nvarchar(10))", oleConn);  
                        oleCmd.ExecuteNonQuery();           
                }
                string phone = "020-654322";  string name = "王小曼";     
                sqlStr = "insert into 信息表 values('"+phone+"','"+name+"')";
                OleDbCommand Cmd = new OleDbCommand(sqlStr, oleConn);          
                Cmd.ExecuteNonQuery();          
            }            
            catch (Exception te)      
            {           
                MessageBox.Show(te.Message);
            }            
            finally 
            {        
                oleConn.Close();       
            }
        }
        #endregion

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var ds = (this.G1.DataContext as DataSet);
            ds.WriteXml("sava.xml", XmlWriteMode.IgnoreSchema);
        }
    }
}
