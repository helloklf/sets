﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 本地小型数据库的读取
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (OleDbConnection od = new OleDbConnection("Provider= Microsoft.Jet.OLEDB.4.0;Data Source=AreaInfo.mdb"))
            {
                od.Open();
                OleDbCommand ole = new OleDbCommand("select * from AreaInfo where AreaPId<1", od);
                OleDbDataReader dr = ole.ExecuteReader(); comboBox1.Items.Clear();
                List<AreaInfo> ar = new List<AreaInfo>();
                while (dr.Read())
                {
                    AreaInfo ai = new AreaInfo((int)dr[0], (string)dr[1], (int)dr[2]);
                    ar.Add(ai);
                }
                comboBox1.DataSource = ar;
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked) ToComBox(comboBox1, comboBox2);
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            this.textBox1.Text = (comboBox3.SelectedValue as AreaInfo).AreaId.ToString();
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked) ToComBox(comboBox2, comboBox3);
            else 
            {
                using (OleDbConnection Conn = new OleDbConnection("Provider= Microsoft.Jet.OLEDB.4.0;Data Source=AreaInfo.mdb"))
                {
                    Conn.Open();
                    if (comboBox2.Text == null) return;
                    string text = comboBox2.Text;
                    OleDbCommand ole = new OleDbCommand
                        (" select * from AreaInfo where AreaId in (select AreaPid from AreaInfo where AreaInfo.AreaName='" + text + "')", Conn);
                    OleDbDataReader dr = ole.ExecuteReader();
                    List<AreaInfo> AL = new List<AreaInfo>();
                    while (dr.Read())
                    {
                        AreaInfo ai = new AreaInfo((int)dr[0], (string)dr[1], (int)dr[2]); AL.Add(ai);
                        OleDbCommand Ole = new OleDbCommand("select * from AreaInfo where AreaId=" + ai.AreaPid, Conn);
                        OleDbDataReader Dr = Ole.ExecuteReader();
                    }
                    comboBox1.DataSource = AL;
                }
            }
        }

        private void comboBox3_TextChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked) return;
            using (OleDbConnection Conn = new OleDbConnection("Provider= Microsoft.Jet.OLEDB.4.0;Data Source=AreaInfo.mdb"))
            {
                Conn.Open();
                if (comboBox3.Text == null) return;
                string text = comboBox3.Text;
                OleDbCommand ole = new OleDbCommand
                    (" select * from AreaInfo where AreaId in (select AreaPid from AreaInfo where AreaInfo.AreaName='" + text + "')", Conn);
                OleDbDataReader dr = ole.ExecuteReader();
                List<AreaInfo> AL = new List<AreaInfo>();
                List<AreaInfo> AL2 = new List<AreaInfo>();
                while (dr.Read())
                {
                    AreaInfo ai = new AreaInfo((int)dr[0], (string)dr[1], (int)dr[2]); AL.Add(ai);
                    OleDbCommand Ole = new OleDbCommand("select * from AreaInfo where AreaId="+ai.AreaPid, Conn);
                    OleDbDataReader Dr = Ole.ExecuteReader();
                    while (Dr.Read())
                    {
                        AreaInfo Ai = new AreaInfo((int)Dr[0], (string)Dr[1], (int)Dr[2]); AL2.Add(Ai);
                    }
                }
                comboBox1.DataSource = AL2;
                comboBox2.DataSource = AL;
                
            }
        }

        public void ToComBox( ComboBox c1,ComboBox c2)
        {
            using (OleDbConnection od = new OleDbConnection("Provider= Microsoft.Jet.OLEDB.4.0;Data Source=AreaInfo.mdb"))
            {
                od.Open();
                if (c1.SelectedValue == null) return;
                AreaInfo a = (AreaInfo)c1.SelectedValue;
                OleDbCommand ole = new OleDbCommand("select * from AreaInfo where AreaPId=" + a.AreaId, od);
                OleDbDataReader dr = ole.ExecuteReader();
                List<AreaInfo> ar = new List<AreaInfo>();
                while (dr.Read())
                {
                    AreaInfo ai = new AreaInfo((int)dr[0], (string)dr[1], (int)dr[2]);
                    ar.Add(ai);
                }
                c2.DataSource = ar; c2.SelectedIndex = 0;
            }
        }
    }
}
