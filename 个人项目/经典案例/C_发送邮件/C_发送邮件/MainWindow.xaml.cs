﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Net.Mail;

using Microsoft.Exchange.WebServices;
using Microsoft.Exchange.WebServices.Data;
using System.Net;

namespace C_发送邮件
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private void SendEmail() 
        {
            ExchangeService service = new ExchangeService();
            service.Credentials = new NetworkCredential("klf1191634433.@outlook.com", "KlfNotbook", "");
            //service.Credentials = new NetworkCredential("user", "password", "domain");
            service.TraceEnabled = true;
            service.AutodiscoverUrl("klf1191634433.@live.com");//发件地址
            EmailMessage em = new EmailMessage( service );
            em.Subject = "默认主题"; em.Body = "正文："+"我叫刘先生";
            em.ToRecipients.Add("1191634433@qq.com");//收件人
            em.Save();
            em.SendAndSaveCopy();
        }
        
        public MainWindow()
        {
            InitializeComponent();
            System.Net.Mail.MailAddress ma = new MailAddress("klf1191634433@live.com");
            
        }

        public void SendSMTPEMail(string strSmtpServer, string strFrom, string strFromPass, string strto, string strSubject, string strBody)
        {
            System.Net.Mail.SmtpClient client = new SmtpClient(strSmtpServer);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(strFrom, strFromPass);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            System.Net.Mail.MailMessage message = new MailMessage(strFrom, strto, strSubject, strBody);

            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;
            client.Send(message);
        }
        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Email email = new Email();
            email.mailFrom = "979359486@qq.com";//你的邮箱地址
            email.mailPwd = "song?011321520";//你的邮箱密码
            email.mailSubject = "邮件主题";
            email.mailBody = "邮件内容:22222222";
            email.isbodyHtml = true;    //是否是HTML
            email.host = "smtp.qq.com";//如果是QQ邮箱则：smtp:qq.com,依次类推
            email.mailToArray = new string[] { "1159650790@qq.com" };//接收者邮件集合
            email.mailCcArray = new string[] { "979359486@qq.com" };//抄送者邮件集合
            if (email.Send())
            {
                //MessageBox.Show("11");
                ///Response.Write("<script type='text/javascript'>alert('发送成功！');history.go(-1)</script>");//发送成功则提示返回当前页面；
            }
            else
            {
                //MessageBox.Show("22");
                ///Response.Write("<script type='text/javascript'>alert('发送失败！');history.go(-1)</script>");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SendEmail();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SendSMTPEMail("smtp.qq.com", "1191634433@qq.com", "KlfNotbok", "979359486@qq.com", "标题", "内容3242423432");//"服务器地址","发件人账号","发件人密码","收件人账号","标题","内容"
        }
    }


    public class Email
    {
        /// <summary>
        /// 发送者
        /// </summary>
        public string mailFrom { get; set; }

        /// <summary>
        /// 收件人
        /// </summary>
        public string[] mailToArray { get; set; }

        /// <summary>
        /// 抄送
        /// </summary>
        public string[] mailCcArray { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string mailSubject { get; set; }

        /// <summary>
        /// 正文
        /// </summary>
        public string mailBody { get; set; }

        /// <summary>
        /// 发件人密码
        /// </summary>
        public string mailPwd { get; set; }

        /// <summary>
        /// SMTP邮件服务器
        /// </summary>
        public string host { get; set; }

        /// <summary>
        /// 正文是否是html格式
        /// </summary>
        public bool isbodyHtml { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        public string[] attachmentsPath { get; set; }

        public bool Send()
        {
            //使用指定的邮件地址初始化MailAddress实例
            MailAddress maddr = new MailAddress(mailFrom);
            //初始化MailMessage实例
            MailMessage myMail = new MailMessage();


            //向收件人地址集合添加邮件地址
            if (mailToArray != null)
            {
                for (int i = 0; i < mailToArray.Length; i++)
                {
                    myMail.To.Add(mailToArray[i].ToString());
                }
            }

            //向抄送收件人地址集合添加邮件地址
            if (mailCcArray != null)
            {
                for (int i = 0; i < mailCcArray.Length; i++)
                {
                    myMail.CC.Add(mailCcArray[i].ToString());
                }
            }
            //发件人地址
            myMail.From = maddr;

            //电子邮件的标题
            myMail.Subject = mailSubject;

            //电子邮件的主题内容使用的编码
            myMail.SubjectEncoding = Encoding.UTF8;

            //电子邮件正文
            myMail.Body = mailBody;

            //电子邮件正文的编码
            myMail.BodyEncoding = Encoding.Default;

            myMail.Priority = MailPriority.High;

            myMail.IsBodyHtml = isbodyHtml;

            //在有附件的情况下添加附件
            try
            {
                if (attachmentsPath != null && attachmentsPath.Length > 0)
                {
                    System.Net.Mail.Attachment attachFile = null;
                    foreach (string path in attachmentsPath)
                    {
                        attachFile = new System.Net.Mail.Attachment(path);
                        myMail.Attachments.Add(attachFile);
                    }
                }
            }
            catch (Exception err)
            {
                throw new Exception("在添加附件时有错误:" + err);
            }

            SmtpClient smtp = new SmtpClient();
            //指定发件人的邮件地址和密码以验证发件人身份
            smtp.Credentials = new System.Net.NetworkCredential(mailFrom, mailPwd);


            //设置SMTP邮件服务器
            smtp.Host = host;

            try
            {
                //将邮件发送到SMTP邮件服务器
                smtp.Send(myMail);
                return true;

            }
            catch (System.Net.Mail.SmtpException ex)
            {
                return false;
            }

        }
    }
}
