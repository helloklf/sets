﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace 改后缀
{
    class Program
    {
        [STAThreadAttribute]
        static void Main(string[] args)
        {
            System.Windows.Forms.FolderBrowserDialog fb = new System.Windows.Forms.FolderBrowserDialog();
            fb.Description = "请选择源文件夹";
            if (fb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string folder = fb.SelectedPath;
                string[] files = Directory.GetFiles(folder);
                string filetype = InputType();

                FileInfo fi;
                string newfilepath;
                foreach (var item in files)
                {
                    fi = new FileInfo(item);
                    if (fi.Extension == "")
                        newfilepath = item + filetype;
                    else
                        newfilepath = item.Substring(0, item.LastIndexOf('.')) + filetype;
                    if (item.ToLower() == newfilepath.ToLower())
                        continue;
                    if (File.Exists(newfilepath))
                    {
                        if (MessageBox.Show(newfilepath + "已经存在，是否覆盖此文件？（否：跳过此文件，是：覆盖旧文件）", "是否覆盖文件？", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            File.Delete(newfilepath);
                        else
                            continue;
                    }
                    File.Move(item, newfilepath);
                }
                Console.WriteLine("文件复制完成,任意键结束...");
                Console.ReadKey();
            }
        }

        //输入后缀
        static string InputType()
        {
            Console.Write("请输入后缀名（如：Mp4）：");
            string filetype = Console.ReadLine();
            if (filetype.Contains("\\") || filetype.Contains("//"))
            {
                Console.WriteLine("包含无效的字符，请重新输入后缀名（如：Mp4）：");
                return InputType();
            }
            else
            {
                if (filetype.IndexOf('.') != 0)
                    filetype = "." + filetype;
                return filetype;
            }
        }
    }
}
