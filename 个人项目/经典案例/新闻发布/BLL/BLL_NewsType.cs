﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    /// <summary>
    /// 查询NewsType(新闻类型)表的对应BLL类
    /// </summary>
    public static class BLL_NewsType
    {
        static DAL.DAL_NewsType dNewsType = new DAL.DAL_NewsType(); //DAL层中的对应操作类
        public static List<Model.M_NewsType> GetNewsType(bool? IsValid = null)
        {
            return dNewsType.GetNewsType(IsValid);
        }


        /// <summary>
        /// 添加一种新闻类型
        /// </summary>
        /// <param name="newstype"></param>
        /// <returns></returns>
        public static int AddtNewsType(Model.M_NewsType newstype)
        {
            return dNewsType.AddtNewsType(newstype);
        }

        /// <summary>
        /// 修改一种新闻类型
        /// </summary>
        /// <param name="newstype"></param>
        /// <returns></returns>
        public static int UpdatetNewsType(Model.M_NewsType newstype)
        {
            return dNewsType.UpdatetNewsType(newstype);
        }

        /// <summary>
        /// 删除一种新闻类型
        /// </summary>
        /// <param name="newstype"></param>
        /// <returns></returns>
        public static int DeleteNewsType(Model.M_NewsType newstype)
        {
            return dNewsType.DeleteNewsType(newstype);
        }
    }
}
