﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class M_News:M_NewsType
    {
        int indexId;

        public int IndexId
        {
            get { return indexId; }
            set { indexId = value; }
        }
        //int TypeId  {get;set;} //从父类继承
        //string TypeName {get;set;} //从父类继承

        int newsType;
        /// <summary>
        /// 新闻类型编号
        /// </summary>
        public int NewsType
        {
            get { return newsType; }
            set { newsType = value; }
        }

        string newsFrom;
        /// <summary>
        /// 新闻来源
        /// </summary>
        public string NewsFrom
        {
            get { return newsFrom; }
            set { newsFrom = value; }
        }


        string newsAuthor;
        /// <summary>
        /// 作者
        /// </summary>
        public string NewsAuthor
        {
            get { return newsAuthor; }
            set { newsAuthor = value; }
        }


        string newsTitle;
        /// <summary>
        /// 标题
        /// </summary>
        public string NewsTitle
        {
            get { return newsTitle; }
            set { newsTitle = value; }
        }

        DateTime? newsDate;
        /// <summary>
        /// 新闻时间
        /// </summary>
        public DateTime? NewsDate
        {
            get { return newsDate; }
            set { newsDate = value; }
        }

        /// <summary>
        /// 文本化的时间
        /// </summary>
        public string NewsDateText 
        {
            get 
            {
                if (NewsDate != null) 
                {
                    DateTime dt = (DateTime)NewsDate;
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
                }
                return "未知时间";
            }
        }


        string generalizeTitle;
        /// <summary>
        /// 推广标题
        /// </summary>
        public string GeneralizeTitle
        {
            get { return generalizeTitle; }
            set { generalizeTitle = value; }
        }


        string newsContent;
        /// <summary>
        /// 内容
        /// </summary>
        public string NewsContent
        {
            get { return newsContent; }
            set { newsContent = value; }
        }

        // bool? IsValid { get; set; } //有效性 继承自父类
    }
}
