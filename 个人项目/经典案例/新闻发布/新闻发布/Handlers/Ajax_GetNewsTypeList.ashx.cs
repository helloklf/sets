﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace kehui.Handlers
{
    /// <summary>
    /// 专用于Ajax方式取得新闻类型集合的处理程序
    /// </summary>
    public class Ajax_GetNewsTypeList : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                JavaScriptSerializer jss = new JavaScriptSerializer();
                context.Response.Write(jss.Serialize(BLL.BLL_NewsType.GetNewsType()));
            }
            catch { context.Response.Write("servererror"); }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}