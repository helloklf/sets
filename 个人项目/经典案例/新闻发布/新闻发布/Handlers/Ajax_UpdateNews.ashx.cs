﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kehui.Handlers
{
    /// <summary>
    /// 专用于Ajax方式更新新闻的处理程序
    /// </summary>
    public class Ajax_UpdateNews : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                var r = context.Request;
                if (r.Params["NewsTitle"] != null && r.Params["NewsContent"] != null)
                {
                    Model.M_News news = new Model.M_News();
                    news.IndexId = Convert.ToInt32(r.Params["IndexId"]); //新闻索引ID
                    news.NewsType = Convert.ToInt32(r.Params["NewsType"]); //新闻类型ID
                    news.NewsTitle = context.Server.UrlDecode(r.Params["NewsTitle"]); //新闻标题
                    news.NewsFrom = context.Server.UrlDecode(r.Params["NewsFrom"]); //新闻来源
                    news.NewsAuthor = context.Server.UrlDecode(r.Params["NewsAuthor"]); //新闻作者
                    news.GeneralizeTitle = context.Server.UrlDecode(r.Params["GeneralizeTitle"]); //新闻简述
                    news.NewsContent = context.Server.UrlDecode(r.Params["NewsContent"]); //新闻正文
                    if (BLL.BLL_News.UpdateNews(news) > 0)
                    {
                        context.Response.Write("complete");
                    }
                    else context.Response.Write("servererror");
                }
            }
            catch { context.Response.Write("servererror"); }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}