﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kehui.Handlers
{
    /// <summary>
    /// 专用于Ajax方式取得所有新闻的处理程序
    /// </summary>
    public class Ajax_GetNewsList : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                List<Model.M_News> news = BLL.BLL_News.GetNews();
                System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                context.Response.Write(jss.Serialize(news));
            }
            catch { context.Response.Write("servererror"); }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}