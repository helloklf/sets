﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Frameworks/Admin.Master" AutoEventWireup="true" CodeBehind="NewsManagement.aspx.cs" Inherits="kehui.Admin.NewsManagement" %>
<asp:Content runat="server" ContentPlaceHolderID="head">新闻管理</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="nicEdit.css" rel="stylesheet" />
    <style>
        #newsmanagepanel td {
            padding:2px 5px; text-align:center; font-size:13px; margin:2px; background:#f2f2f2
        }
        #newsmanagepanel th {
            padding:7px; text-align:center; font-size:18px; margin:2px; font-weight:900;background:url(../ImgSources/FrameWorkimages/bg_02.gif);color:#fff;
        }
        #newsmanagepanel table {
            color: #555; width: 100%; border:1px solid #000;
        }
    </style>
    <div id="newsmanagepanel">
        <h2 style="text-align:left">新闻管理</h2>
        <asp:ListView runat="server" ID="NewsDataList" OnItemDeleting="Unnamed_ItemDeleting" DataKeyNames="IndexId">
            <LayoutTemplate>
                <table>
                    <tr>
                        <th>编号</th><th>标题</th><th>类型</th><th>来源</th>
                        <th>作者</th><th>时间</th><th>简述</th><th>操作</th>
                    </tr>
                    <tr>
                        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("IndexId") %></td>
                    <td><%# Eval("NewsTitle") %></td>
                    <td><%# Eval("TypeName") %></td>
                    <td><%# Eval("NewsFrom") %></td>
                    <td><%# Eval("NewsAuthor") %></td>
                    <td><%# Eval("NewsDateText") %></td>
                    <td><%# Eval("GeneralizeTitle") %></td>
                    <td>
                        <input type="button" runat="server" title='<%# Eval("IndexId") %>' onclick="window.open('News/Admin_EditNews.aspx?newsid=' + title)" value="修改" />
                        <asp:Button runat="server" CommandName="delete" Text="删除" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>
