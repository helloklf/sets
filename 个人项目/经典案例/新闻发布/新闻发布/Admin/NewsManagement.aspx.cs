﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kehui.Admin
{
    public partial class NewsManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                ReLoad();
            }
        }

        protected void Unnamed_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            BLL.BLL_News.DeleteNews(new Model.M_News() { IndexId = (int)e.Keys[0] });
            ReLoad();
        }

        void ReLoad() 
        {
            NewsDataList.DataSource = BLL.BLL_News.GetNews();
            NewsDataList.DataBind();
        }
    }
}