﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kehui.Admin.News.REFiles
{
    /// <summary>
    /// GetFaceServerPath 的摘要说明
    /// </summary>
    public class GetFaceServerPath : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                if (context.Request.Params["file"] != null)
                {
                    context.Response.Write( "../../ImgSources/"+context.Server.UrlDecode( context.Request.Params["file"]));
                }
                else 
                {
                    context.Response.Write("servererror"); 
                }
            }
            catch { context.Response.Write("servererror"); }
            //context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}