﻿function $$(_id) { return document.getElementById(_id); }

function getHtmlText() { $$("htmltext").textContent = RichProject.$Doc().body.innerHTML; }
function setHtmlView() { RichProject.$Doc().body.innerHTML = $$("htmltext").textContent; }

var RichProject;
function IFrameEnableEdit(_id)
{
    var docu = $$(_id).contentWindow.document;
    $$(_id).contentEditable = true;
    docu.designMode = "on"; docu.write("<html><body><div>请在此输入内容...</div></body></html>");
    docu.onkeydown = function (e) {
        if (e.keyCode == 9)//Tab 按钮
        {
            RichProject.$ExeCmd("indent");//增加缩进
            return false;
        }
    };
    docu.onkeyup = getHtmlText;
}


//执行命令
function doClick(cmd, value) {
    try {
        RichProject.$ExeCmd(cmd, value); 
        return false;
    }
    catch (e) {
        //alert(e.message);
        $$("localState").textContent = "操作无效";
    }
}
//开关设置面板
function SwitchPanel(PanelName)
{
    try
    {
        var d = document.getElementsByClassName('SetPanel');
        for (var i in d)
        {
            if (d[i].id != PanelName) d[i].hidden = true;
        }
    }
    catch(e){ }
    $$(PanelName).hidden = $$(PanelName).hidden == false ? true : false; return false;
}
function HtmlFileSelected()
{
    try {
        var fu = $$('htmlfiledata');
        if (fu.value == "") return;
        var fileitem = fu.files[0];
        var filetyp = fileitem.name.substring(fileitem.name.lastIndexOf('.'), fileitem.name.length);
        $$('htmlfilename').textContent = fileitem.name; 
        $$('htmlUploadState').textContent = "1个文档正在上传...";
        if (TextFileCheck(fileitem, filetyp)) {
            FileUpload(fileitem, filetyp, "REFiles/AjaxUploadHTML.ashx", function (responseText) {
                RichProject.$Doc().body.innerHTML = responseText;
                SwitchPanel('UploadSetPanel');
            }); //上传后返回文档内容
            $$('htmlUploadState').textContent = "1个文档上传完成";
        }
        else {
            $$('htmlfiledata').value = "";
        }
    }
    catch (e) {
        $$('htmlUploadState').textContent = "1个文档上传失败";
        $$('imgUploadState').textContent = "不是有效的图片";
        $$('imgfilename').textContent = "不是有效的图片";
    }
}
function AddImgToDoc() {
    RichProject.$InserHTML('<img src="' + event.srcElement.title + '"/>');
}

function AddOnlineImgToList(_src)
{
    var newimg = document.createElement("img");
    newimg.onmousedown = AddImgToDoc; newimg.title = _src;
    newimg.src = _src;
    $$("imgfilelist").appendChild(newimg);
}
function ImgFileSelected() {
    var fu = $$('imgfiledata');
    if (fu.value == "") return;
    var fileitem = fu.files[0];
    var filetyp = fileitem.name.substring(fileitem.name.lastIndexOf('.'), fileitem.name.length);
    $$('imgfilename').textContent = fileitem.name;
    try{
        ImgFileCheck(fileitem, filetyp);
        $$('imgUploadState').textContent = "1张图片正在上传...";
        try
        {
            FileUpload(fileitem, filetyp, "REFiles/AjaxFileUpload.ashx", function (responseText) { AddOnlineImgToList(responseText);});
            $$('imgUploadState').textContent = "1张图片上传完成";
            $$('imgfilename').textContent = "1张图片上传完成";
        }
        catch (e)
        {
            $$('imgUploadState').textContent = "1张图片上传失败";
        }; //上传后返回路径
    }
    catch (e)
    {
        $$('imgfiledata').value = "";
        $$('imgUploadState').textContent = "不是有效的图片";
        $$('imgfilename').textContent = "不是有效的图片";
    }
}

//参数1：前景色列表容器id ； 参数2：背景色列表容器id ； 参数3：色块class属性名
function Color(_fclist,_bglist,_class) {
    try {
        var colors = ["black", "#333333", "#666666", "#999999", "#bbbbbb", "white", "#ffbbbb", "#ff9999", "#ff6666", "#ff3333", "red", "#bb0000", "#ff4600", "#fe5500", "orange", "yellow", "#d1e900", "#7deb00", "#80ba02", "#0cb405", "#0d8e03", "#00fee6", "#00c0c6", "#0092ff", "#0058e2", "#0302a1", "#030271", "#f500ff", "#c500bb", "#8f0069", "#666666"];

        var fclist = $$(_fclist);
        for (var i in colors)
        {
            var newcolor = document.createElement("span"); newcolor.setAttribute("class", _class); newcolor.style.background = colors[i];
            newcolor.onmousedown = function(){doClick("forecolor",event.srcElement.style.background);return false; };
            fclist.appendChild(newcolor);
        }
        var bglist = $$(_bglist);
        for (var i in colors)
        {
            var newcolor = document.createElement("span"); newcolor.setAttribute("class",_class); newcolor.style.background = colors[i];
            newcolor.onmousedown = function(){doClick("backcolor",event.srcElement.style.background);return false;};
            bglist.appendChild(newcolor);
        }
    }
    catch (e) { alert(e.message); }
}

//获得表情在服务器中的路径
function AjaxGetImgPath(_file, _postback) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                if (xml.response != "servererror")  _postback(xml.response);
                else alert("无法获得指定文件路径！");
            }
        }
    }
    xml.open("GET", "REFiles/GetFaceServerPath.ashx?file=" + _file, true);
    xml.send();
}

//参数1：表情容器的id ； 参数2：文件夹中包含的png图片数量 ；参数3：文件夹中包含的gif图片数量 ；
//（请确保图片资源相对路径未被改变，且文件夹内资源遵循Window批量命名规则>>例如：" (1).png"、" (1).gif"。）
function Face(_faceListPanel,_pngcount,_gifcount) {
    try {
        function InsertFace()
        {
            AjaxGetImgPath(encodeURI(event.srcElement.title), function (r) {
                RichProject.$InserHTML("<img src='" + r + "'></img>");
            });
        }
        var folder = "REFiles/face/f(";
        var flp = $$(_faceListPanel);
        var imgpath;
        if (!_gifcount)
        for (var i = 1; i < 106; i++) {
            var imageitem = document.createElement('img');
            imgpath = folder + i + ").gif"
            imageitem.src = imgpath; imageitem.title = imgpath;
            imageitem.onmousedown = InsertFace;
            flp.appendChild(imageitem);
        }
        if (!_pngcount)
        for (var j = 1; j < 37; j++)
        {
            var imageitem = document.createElement('img');
            imgpath = folder + j + ").gif"
            imageitem.src = imgpath; imageitem.title = imgpath; imageitem.onmousedown = InsertFace;
            flp.appendChild(imageitem);
        }
    }
    catch (e) { alert(e.message); }
}

//参数1：字体列表容器（select）的ID
function FontList(_fontlist) {
    try {
        var fonts = ["Default", "微软雅黑", "宋体", "华文行楷", "幼圆", "隶书", "Arial", "Arial Black", "Impact"];

        var fontlist = $$(_fontlist);
        for (var i in fonts) {
            var newfont = document.createElement("option");
            newfont.setAttribute("value", fonts[i]);newfont.textContent = fonts[i]; newfont.style.fontFamily = fonts[i];
            fontlist.appendChild(newfont);
        }
    }
    catch (e) { alert(e.message); }
}


//HTML相关初始化
function RichEditInitialize(_id)
{
    RichProject = CreateRichProject(_id, getHtmlText)
    IFrameEnableEdit(_id);
    Color('fcslist', 'bgcslist', 'ColorBlock');
    Face('faceListPanel');
    FontList('fontlist');
}

function InTemp() {
    $$("tempState").textContent = "缓存状态：" + RichProject.InTemp();
}
function OutTemp() {
    $$("tempState").textContent = "缓存状态：" + RichProject.OutTemp();
    
}
function Advance() {
    $$("tempState").textContent = "缓存状态：" + RichProject.Advance();
}