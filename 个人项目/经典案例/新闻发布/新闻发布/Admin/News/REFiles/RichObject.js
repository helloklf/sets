﻿function CreateRichProject(newitemname,onchanged)
{
    var obj = 
    {
        itemName: newitemname, //IFrame框架名称
        OnChanged:onchanged,//内容改变
        //获取指定iframe中的document对象
        $Doc: function $Doc() 
        {
            return document.getElementById(this.itemName).contentWindow.document;
        },
        //html内容
        $InnerHTML: function () { return this.$Doc().body.innerHTML; },
        //纯文本内容
        $InnerText: function () { },
        //选中的内容
        $DocSelection: function $DocSelection()
        {
            return this.$Doc(this.itemName).getSelection();
            //var _thedoc = document.getElementById(this.itemName);
            //if (_thedoc.document.getSelection) {
            //    return _thedoc.document.getSelection();
            //}
            //else if (_thedoc.document.selection) {
            //    return _thedoc.document.selection.createRange()
            //}
            //if (_thedoc.getRangeAt) {
            //    return _thedoc.getRangeAt(0);
            //}
        },

        $ExeCmd: function $ExeCmd(cmdName, value) {
            this.$Doc(this.itemName).execCommand(cmdName, true, value);
            if (this.OnChanged) this.OnChanged();
        },

        $InserHTML: function $InserHTML(_html)
        {
            if (this.$Doc().getSelection().focusNode.parentNode == null) return;
            else {
                var doc = this.$DocSelection().focusNode.parentNode.innerHTML;
                var ofset = this.$DocSelection().focusOffset;
                var left = doc.substring(0, ofset);
                var right = doc.substring(ofset, doc.length);
                if (left.lastIndexOf(">") < left.lastIndexOf("<"))
                {
                    //如果出现了‘<’在此之后没有‘>’，则在此位置插入内容会截断html标记，需要进行处理 
                    //alert("不进行处理会出错！"+"Left："+left+"　right："+right);
                    var temp = left.substring(left.lastIndexOf("<"), left.length); right = temp + right;
                    left = left.substring(0,left.lastIndexOf("<"));
                }
                this.$DocSelection().focusNode.parentNode.innerHTML = left +_html + right;
            }
            return false;
        },

        Temp:new Array(),
        TempIndex:-1,

        InTemp:function InTemp() //写入缓存
        {
            while (this.TempIndex != -1 && this.TempIndex < this.Temp.length - 1) {
                this.Temp.pop();
            }
            this.Temp.push( this.$Doc().body.innerHTML );
            this.CanAdvance = false;
            this.CanFallBack = true;
            if (this.Temp.length > 20) this.Temp.shift();
            this.TempIndex = this.Temp.length - 1;
            return this.TempIndex + 1 + "／" + this.Temp.length;
        },
        OutTemp:function OutTemp() //输出缓存
        {
            if (this.$Doc().body.innerHTML != this.Temp[this.TempIndex]) this.InTemp();

            if (this.TempIndex > 0)
            {
                this.CanAdvance = true;
                this.TempIndex--;
                this.$Doc().body.innerHTML = this.Temp[this.TempIndex];
            }
            return this.TempIndex + 1 + "／" + this.Temp.length;
        },
        Advance: function Advance() { //缓存索引向后移位
            if (this.TempIndex < this.Temp.length - 1)
            {
                this.TempIndex++;
                this.$Doc().body.innerHTML = this.Temp[this.TempIndex];
            }
            return this.TempIndex+1 + "／" + this.Temp.length;
        },
        CanAdvance: false,  //可以前进，与超级缓存的撤销层次相关
        CanFallBack: false, //可以后退，与超级缓存的撤销层次相关

        FontSize: function FontSize(_fontsize) { //设置字体大小
            this.$ExeCmd("fontsize", _fontsize);
        },
        FontName: function FontName(_fontname) { //设置字体名
            this.$ExeCmd("fontname", _fontname);
        },
        ForeColor: function ForeColor(_rgbcolor) { //前景色
            this.$ExeCmd("forecolor", _rgbcolor);
        },
        BackColor:function BackColor(_rgbcolor) { //背景色
            this.$ExeCmd("backcolor", _rgbcolor);
        },
        Bold:function Bold() {//加粗
            this.$ExeCmd("Bold");
        },
        Italic:function Italic() { //斜体
            this.$ExeCmd("Italic");
        },
        Underline:function Underline() { //下划线
            this.$ExeCmd("Underline");
        },
        StrikeThrough:function StrikeThrough() { //删除线
            this.$ExeCmd("StrikeThrough");
        },
        JustifyLeft:function JustifyLeft() { //居左
            this.$ExeCmd("JustifyLeft");
        },
        JustifyRight:function JustifyRight() { //居右
            this.$ExeCmd("JustifyRight");
        },
        JustifyCenter:function JustifyCenter() { //居中
            this.$ExeCmd("JustifyCenter");
        },
        //Postition:function Postition()//定位方式
        //{
        //    this.$ExeCmd("postition", "absolute");
        //},
        Cut:function Cut() { //剪切
            this.$ExeCmd("Cut");
        },
        Copy:function Copy() { //复制
            this.$ExeCmd("Copy");
        },
        Paste:function Paste() { //粘贴
            this.$ExeCmd("Paste");
        },
        Delete:function Delete() { //删除
            this.$ExeCmd("Delete");
        },
        Undo:function Undo() { //撤销
            this.$ExeCmd("undo");
        },
        Redo:function Redo() { //重做
            this.$ExeCmd("redo");
        },
        CreateLink:function CreateLink() { //超链:调用时弹出窗口输入Url
            this.$ExeCmd("createlink");
        },
        //function FormatBlock() { //格式化块
        //    this.$ExeCmd("formatblock");
        //},
        InsertButton:function InsertButton() {//插入按钮
            this.$ExeCmd("insertbutton");
        },
        InsertFieldset:function InsertFieldset() {//插入方框
            this.$ExeCmd("insertfieldset");
        },
        InsertHorizontalRule:function InsertHorizontalRule() {//插入水平线
            this.$ExeCmd("inserthorizontalrule");
        },
        InsertIFrame:function InsertIFrame() {//插入内联框架
            this.$ExeCmd("insertiframe");
        },
        InsertInputButton:function InsertInputButton() { //插入按钮
            this.$ExeCmd("insertinputbutton");
        },
        InsertInputCheckbox:function InsertInputCheckbox() { //插入复选框
            this.$ExeCmd("insertinputcheckbox");
        },
        InsertInputFileUpload:function InsertInputFileUpload() { //插入文件上传
            this.$ExeCmd("insertinputfileupload");
        },
        InsertInputHidden:function InsertInputHidden() { //插入隐藏控件
            this.$ExeCmd("insertinputhidden");
        },
        InsertInputImage:function InsertInputImage() { //插入图片按钮
            this.$ExeCmd("insertinputimage");
        },
        InsertInputPassword:function InsertInputPassword() { //插入密码
            this.$ExeCmd("insertinputpassword");
        },
        InsertInputRadio:function InsertInputRadio() { //插入单选按钮
            this.$ExeCmd("insertinputradio");
        },
        InsertInputReset:function InsertInputReset() { //插入重置按钮
            this.$ExeCmd("insertinputreset");
        },
        InsertInputSubmit:function InsertInputSubmit() { //插入提交按钮
            this.$ExeCmd("insertinputsubmit");
        },
        InsertInputText:function InsertInputText() { //插入文本框
            this.$ExeCmd("insertinputtext");
        },
        InsertMarquee:function InsertMarquee() { //插入空字幕
            this.$ExeCmd("insertmarquee");
        },
        InsertOrderedList:function InsertOrderedList() { //切换当前区域是编号列表还是常规格式
            this.$ExeCmd("insertorderedlist");
        },
        InsertUnorderedList:function InsertUnorderedList() { //切换当前区域是符号列表还是常规格式
            this.$ExeCmd("insertunorderedlist");
        },
        InsertParagraph:function InsertParagraph() { //插入换行
            this.$ExeCmd("insertparagraph");
        },
        InsertSelectDropdown:function InsertSelectDropdown() { //插入下拉框
            this.$ExeCmd("insertselectdropdown");
        },
        InsertSelectListbox:function InsertSelectListbox() { //插入列表框
            this.$ExeCmd("insertselectlistbox");
        },
        //InsertTextArea:function InsertTextArea() { //插入多行文本
        //    this.$ExeCmd("inserttextarea");
        //},
        //LiveResize:function LiveResize() { //强制持续刷新
        //    this.$ExeCmd("liveresize");
        //},
        //MultipleSelection:function MultipleSelection() { //允许多选
        //    this.$ExeCmd("multipleselection");
        //},
        Open:function Open() { //打开（调用时输入URL，确定后在当前页面打开）
            this.$ExeCmd("open");
        },
        Outdent:function Outdent() { //减少缩进
            this.$ExeCmd("outdent");
        },
        Indent:function Indent() { //加缩进
            this.$ExeCmd("indent");
        },
        //Print:function Print() { //打印
        //    this.$ExeCmd("print");
        //},
        //Refresh:function Refresh() { //刷新
        //    this.$ExeCmd("refresh");
        //},
        RemoveFormat:function RemoveFormat() { //从当前选中区删除格式化标签
            this.$ExeCmd("removeformat");
        },
        SaveAs:function SaveAs() {//保存正在编辑的文档
            this.$ExeCmd("saveas");
        },
        SelectAll:function SelectAll() { //全选
            this.$ExeCmd("selectall");
        },
        //Stop:function Stop() { //停止
        //    this.$ExeCmd("stop");
        //},
        UnBookmark:function UnBookmark() { //从选中区域删除全部书签
            this.$ExeCmd("unbookmark");
        },
        Unlink:function Unlink() { //删除全部超链
            this.$ExeCmd("unlink");
        },
        Unselect:function Unselect() { //取消选中
            this.$ExeCmd("unselect");
        },

    }

    return obj;

}
