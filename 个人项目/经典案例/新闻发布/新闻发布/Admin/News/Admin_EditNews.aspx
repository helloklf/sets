﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin_EditNews.aspx.cs" Inherits="kehui.Admin.News.Admin_EditNews" MasterPageFile="~/Frameworks/AdminSubpage.master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">修改新闻</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <link href="REFiles/Style.css" rel="stylesheet" />
    <script src="REFiles/RichObject.js"></script>
    <script src="REFiles/AjaxFileUpload.js"></script>
    <script src="REFiles/Control.js"></script>

    <link href="NewsEdit.css" rel="stylesheet" />
    
    <div style="clear: both; float: none;">
        <ul id="newsaddForm">
            <li>
                <input id="IndexId" readonly="true" style="display: none; visibility: collapse;" /></li>
            <li>标题：<input type="text" id="NewsTitle" value="请输入新闻标题" />
            </li>
            <li>类型：<select onchange='document.getElementById("NewsType").value= value' id="NewsTypeSelect"></select>
                <input type="text" id="NewsType" value="" readonly="true" hidden="hidden" />
            </li>
            <li>来源：<input type="text" id="NewsFrom" value="科瑞网络" />
            </li>
            <li>作者：<input type="text" id="NewsAuthor" value="科瑞网络编辑" />
            </li>
            <li>简介：<textarea id="GeneralizeTitle" maxlength="200">请在这里书写新闻的简要信息</textarea>
            </li>
        </ul>

        <div id="RichEditPanel">
            <!--顶部栏-->
            <div id="REPtopbar">

                <img alt="缓存当前状态" title="缓存当前状态" src="REFiles/pics/anchor.png" onmousedown="InTemp()" />
                <img alt="返回上一缓存点" title="返回上一缓存点" src="REFiles/pics/left.png" onmousedown="OutTemp()" />
                <img alt="转到下一缓存点" title="转到下一缓存点" src="REFiles/pics/right.png" onmousedown="Advance()" />
                <img alt="撤销样式更改" title="撤销样式更改" src="REFiles/pics/undo.png" onmousedown="doClick('Undo')" />
                <img alt="重做样式更改" title="重做样式更改" src="REFiles/pics/redo.png" onmousedown="doClick('Redo')" />
                <img alt="复制" title="复制" src="REFiles/pics/copy.png" onmousedown="doClick('Copy')" />
                <img alt="剪切" title="剪切" src="REFiles/pics/cut.png" onmousedown="doClick('Cut')" />
                <img alt="粘贴" title="粘贴" src="REFiles/pics/paste.png" onmousedown="doClick('Paste')" />
                <img alt="删除" title="删除" src="REFiles/pics/delete.png" onmousedown="doClick('Delete')" />
                <img alt="插入html标记" title="插入html标记" src="REFiles/pics/xml.png" onmousedown="SwitchPanel('InserHTMLSetPanel')" />
                <img alt="手写html标记" title="手写html标记" src="REFiles/pics/dw.png" onmousedown="SwitchPanel('HTMLModeSetPanel')" />
                <img alt="本地上载" title="本地上载" src="REFiles/pics/upload.png" onmousedown="SwitchPanel('UploadSetPanel')" />
                <img alt="添加图片" title="添加图片" src="REFiles/pics/image.png" onmousedown="SwitchPanel('ImageSetPanel')" />
                <img alt="保存" title="保存" src="REFiles/pics/disk.png" onmousedown="SwitchPanel('SaveSetPanel')" />

                <img alt="展开/收起" title="展开/收起" src="REFiles/pics/up.png" onmousedown="SwitchPanel('REPtoolbar'); src = document.getElementById('REPtoolbar').hidden ? 'REFiles/pics/down.png' : 'REFiles/pics/up.png'" style="float: right" />
                <img alt="显示帮助" title="显示帮助" src="REFiles/pics/question.png" onmousedown="window.open('REFiles/help.html') " style="float: right" />

                <div id="REPOperateState">
                    <span onclick="textContent=''" id="localState"></span>
                    <%--<span onclick="textContent=''" id="fileDownLoad">没有下载</span>--%>
                    <span onclick="textContent=''" id="imgUploadState"></span>
                    <span onclick="textContent=''" id="htmlUploadState"></span>
                    <span onclick="textContent=''" id="docSubmitState"></span>
                    <span onclick="textContent=''" id="tempState">缓存状态：0/0</span>
                </div>

            </div>
            <!--工具栏-->
            <div id="REPtoolbar">
                <img alt="粗体" title="粗体" src="REFiles/pics/text.bold.png" onmousedown="doClick('Bold')" />
                <img alt="斜体" title="斜体" src="REFiles/pics/text.italic.png" onmousedown="doClick('Italic')" />
                <img alt="删除线" title="删除线" src="REFiles/pics/strikethrough.png" onmousedown="doClick('StrikeThrough')" />
                <img alt="下划线" title="下划线" src="REFiles/pics/underline.png" onmousedown="doClick('Underline')" />
                <img alt="居左" title="居左" src="REFiles/pics/align.left.png" onmousedown="doClick('JustifyLeft')" />
                <img alt="居中" title="居中" src="REFiles/pics/align.center.png" onmousedown="doClick('JustifyCenter')" />
                <img alt="居右" title="居右" src="REFiles/pics/align.right.png" onmousedown="doClick('JustifyRight')" />
                <img alt="-缩进" title="-缩进" src="REFiles/pics/ToLeft.png" onmousedown="doClick('outdent')" />
                <img alt="+缩进" title="+缩进" src="REFiles/pics/ToRight.png" onmousedown="doClick('indent')" />
                <img alt="超链" title="超链" src="REFiles/pics/link.png" onmousedown="doClick('createlink')" />
                <img alt="序列" title="序列" src="REFiles/pics/list.png" onmousedown="doClick('insertorderedlist')" />
                <img alt="列表" title="列表" src="REFiles/pics/list2.png" onmousedown="doClick('insertunorderedlist')" />
                <img alt="清除格式" title="清除格式" src="REFiles/pics/clean.png" onmousedown="doClick('removeformat')" />
                <img alt="前景/背景颜色" title="前景/背景颜色" src="REFiles/pics/color.png" onmousedown="SwitchPanel('ColorSetPanel')" />
                <img alt="字体" title="字体" src="REFiles/pics/font.png" onmousedown="SwitchPanel('FontSetPanel')" />
                <img alt="字体大小" title="字体大小" src="REFiles/pics/font-size.png" onmousedown="SwitchPanel('FontSizeSetPanel')" />
                <img alt="表情" title="表情" src="REFiles/pics/face.png" onmousedown="SwitchPanel('FaceSetPanel')" />
            </div>
            <div class="REPSetPanlList">
                <!--颜色选择面板-->
                <div id="ColorSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">颜色设置</span>
                    <div class="Content">
                        <span style="float: left;">字体</span>
                        <hr />
                        <div id="fcslist"></div>
                        <p style="clear: both" />
                        <span style="float: left;">背景</span>
                        <hr />
                        <div id="bgcslist"></div>
                    </div>
                </div>
                <!--字体选择面板-->
                <div id="FontSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">字体选择</span>
                    <div class="Content">
                        <select id="fontlist" onchange='doClick("fontname",value)'></select>
                    </div>
                </div>
                <!--字体大小选择面板-->
                <div id="FontSizeSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">字体大小</span>
                    <div class="Content">
                        <select id="fontsizelist" onchange='doClick("fontsize",value)'>
                            <option value="1" style="font-size: 9px">1号</option>
                            <option value="2" style="font-size: 11px">2号</option>
                            <option value="3" style="font-size: 15px">3号</option>
                            <option value="4" style="font-size: 18px">4号</option>
                            <option value="5" style="font-size: 25px">5号</option>
                            <option value="6" style="font-size: 30px">6号</option>
                            <option value="7" style="font-size: 48px">7号</option>
                        </select>
                    </div>
                </div>
                <!--表情插入-->
                <div id="FaceSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">添加表情</span>
                    <div class="Content" id="faceListPanel">
                    </div>
                </div>
                <!--图片选择面板-->
                <div id="ImageSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">图片选择</span>
                    <div class="Content">
                        <div id="imgfilelist">
                            <img src="REFiles/pics/add.png" class="ImgButton" title="本地上传" alt="本地上传" onclick="$$('imgfiledata').click()" />
                            <img src="REFiles/pics/online.png" class="ImgButton" title="在线图片" alt="在线图片"
                                onclick='var url = prompt("请输入网络图片路径", "http://"); if (url.indexOf("http://") != 0) alert("无效的路径！"); else AddOnlineImgToList(url)' />
                        </div>
                        <div>
                            文件名称：<span id="imgfilename"></span><br />
                        </div>
                        <input type="file" id="imgfiledata" onchange="ImgFileSelected()" />
                    </div>
                </div>
                <!--HTML标记插入面板-->
                <div id="InserHTMLSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">插入HTML标记<img src="REFiles/pics/check.png" title="插入" alt="插入" style="vertical-align: middle; margin: 3px 10px 5px 10px" class="ImgButton" onclick='RichProject.$InserHTML($$("inserthtmltext").textContent)' /></span>
                    <div class="Content">
                        <textarea id="inserthtmltext"></textarea>
                        <br />
                    </div>
                </div>
                <!--HTML编辑模式面板-->
                <div id="HTMLModeSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">HTML编辑模式<img src="REFiles/pics/sync.png" style="vertical-align: middle; margin: 3px 10px 5px 10px" class="ImgButton" onclick="getHtmlText()" title="刷新" alt="刷新" /></span>
                    <div class="Content">
                        <textarea id="htmltext" onkeyup="setHtmlView()" onkeydown='if(event.keyCode==9){var s=selectionStart;textContent = textContent.slice(0,s)+ "   " +textContent.slice(selectionEnd,textContent.length);setSelectionRange(s,s);return false;}'></textarea>
                        <br />
                    </div>
                </div>
                <!--文档存储-->
                <div id="SaveSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">文档存储</span>
                    <div class="Content">
                        <img src="REFiles/pics/save.png" style="margin: 5px; height: 64px; width: 64px;" title="本地保存" alt="本地存储" class="ImgButton" onclick="doClick('saveas', 'NewDoc.htm')" />
                        <img src="REFiles/pics/netserver.png" style="margin: 5px; height: 64px; width: 64px;" title="上载至服务器" alt="服务器" class="ImgButton" onclick="alert('未实现此功能')" />
                    </div>
                </div>
                <!--HTML文档上载面板-->
                <div id="UploadSetPanel" class="SetPanel" hidden="hidden">
                    <span class="Title">加载本地HTML</span>
                    <div class="Content">
                        <input type="file" id="htmlfiledata" onchange="HtmlFileSelected()" />
                        <img src="REFiles/pics/open.png" style="margin: 5px; height: 64px; width: 64px;" class="ImgButton" onclick="$$('htmlfiledata').click()" />
                        <br />
                        文件名称：<span id="htmlfilename"></span>
                    </div>
                </div>
            </div>
            <!--编辑区域-->
            <div id="IFrameEditPanel">
                <iframe onload="RichEditInitialize('REPdoc')" id="REPdoc"></iframe>
            </div>
        </div>


        <input type="button" value="完成" onclick="AjaxUpdateNews()" />
    </div>

    
<script>


    function $Id(id) { return document.getElementById(id); }
    function AjaxGet(url, _postback) {
        var xml = new XMLHttpRequest();
        xml.onreadystatechange = function () {
            if (xml.readyState == 4) {
                if (xml.status == 200) {
                    _postback(xml.response);
                }
            }
        }
        xml.open("GET", url, true);
        xml.send(null);
    }
    function AjaxPost(url, data, _postback) {
        var xml = new XMLHttpRequest();
        xml.onreadystatechange = function () {
            if (xml.readyState == 4) {
                if (xml.status == 200) {
                    _postback(xml.response);
                }
            }
        }
        xml.open("POST", url, true);
        xml.send(data);
    }



    function $$(id) { return document.getElementById(id); }
    var newstype = 1; //全局变量，用户保存原始新闻id
    var newsid = 0; //全局变量，用户保存原始新闻id
    var NewsVessel = "REPdoc"; //显示新闻的内联框架id
    //处理新闻内容
    function GetNewsPostBack(r) {
        try {
            var js = JSON.parse(r);
            for (var i in js) {
                newstype = js[i].NewsType;
                $$("NewsTitle").value = js[i].NewsTitle;
                $$("IndexId").value = js[i].IndexId;
                $$("NewsAuthor").value = js[i].NewsAuthor;
                $$("NewsFrom").value = js[i].NewsFrom;
                $$("GeneralizeTitle").textContent = js[i].GeneralizeTitle;
                RichProject.$Doc().body.innerHTML = js[i].NewsContent;
                //IFrameEnableEdit(js[i].NewsContent, NewsVessel);
            }
            AjaxGetNewsType();
        }
        catch (e) { IFrameEnableEdit("数据解析错误！", NewsVessel); }
    }

    //获取新闻内容
    function AjaxGetNewsContent(newsid, _postback) {
        var xml = new XMLHttpRequest();
        xml.onreadystatechange = function () {
            if (xml.readyState == 4) {
                if (xml.status == 200) {
                    var r = xml.response;
                    if (r != "servererror") _postback(r);
                    else IFrameEnableEdit("没有找到相关新闻！", NewsVessel);
                }
                else { IFrameEnableEdit("您的网络不给力！", NewsVessel); }
            }
        }
        var f = new FormData(); f.append("newsid", newsid);
        xml.open("POST", "../../Handlers/Ajax_GetNewsContent.ashx", true);
        xml.send(f);
    }
    //Ajax获取新闻类型的回送
    function GetNewsTypePostBack(response) 
    {
        if (response == "serverror") { throw Error("服务器出现错误"); }
        else {
            var json = JSON.parse(response);
            if (json)
            {
                AddNewsTypeToSelect(json, 'NewsTypeSelect');
                document.getElementById("NewsType").value = document.getElementById("NewsTypeSelect").value;
            }
            else { alert("新闻类型数据转换失败，请联系开发者!"); }
        }
    }
    //将新闻类型添加到Select中 需要提供json集合对象和Select容器ID
    function AddNewsTypeToSelect(json, _selectId) {
        var select = document.getElementById(_selectId);
        for (var i in json) {
            var op = document.createElement("option");
            op.value = json[i].TypeId;
            op.textContent = json[i].TypeName;
            select.appendChild(op);
            if (json[i].TypeId == newstype)
            {
                document.getElementById("NewsType").value = newstype; op.selected = true;
            }
        }
    }
    //将新闻类型添加到Ul中 需要提供json集合对象和Ul容器ID
    function AddNewsTypeToUl(json, _ulId) {
        var ul=document.getElementById(_ulId);
        for (var i in json) {
            var li = document.createElement("li");
            li.textContent = json[i].TypeName; li.value = json[i].TypeId;
            ul.appendChild(li);
        }
    }
    //Ajax 获取新闻类型
    function AjaxGetNewsType() {
        AjaxGet("../../Handlers/Ajax_GetNewsTypeList.ashx", GetNewsTypePostBack);
    }

    //添加新闻操作回送
    function AddNewsPostBack(response) {
        if (response == "complete") { window.location.href = "../NewsManagement.aspx"; }
        else if (response == "servererror") { alert("服务器错误，无法发布新闻，请联系开发者！"); }
    }
    //生成可以用Ajax提交的表单数据
    function CreateFormData() {
        var fd = new FormData();
        fd.append("NewsContent", encodeURI( RichProject.$Doc().body.innerHTML ) );
        fd.append("NewsTitle", encodeURI($Id("NewsTitle").value));
        fd.append("NewsType", $Id("NewsType").value);
        fd.append("NewsAuthor", encodeURI($Id("NewsAuthor").value));
        fd.append("NewsFrom", encodeURI($Id("NewsFrom").value));
        fd.append("GeneralizeTitle", encodeURI($Id("GeneralizeTitle").textContent));
        return fd;
    }

    function AjaxUpdateNews() {
        var fd =  CreateFormData(); fd.append("IndexId",newsid);
        AjaxPost("../../Handlers/Ajax_UpdateNews.ashx", fd, AddNewsPostBack)
    }

    //截取newsid(新闻ID)参数
    if (location.href.indexOf('?') > 0) {
        var href = location.href;
        href = href.substring(href.indexOf('?newsid=') + 8, href.length);
        if (href.indexOf('&') > 0) href = href.substring(0, href.indexOf('&')); newsid = href;
        AjaxGetNewsContent(href, GetNewsPostBack); 
    }
</script>


</asp:Content>