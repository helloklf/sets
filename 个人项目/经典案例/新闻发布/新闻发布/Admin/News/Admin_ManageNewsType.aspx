﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin_ManageNewsType.aspx.cs" Inherits="kehui.Admin.Admin_ManageNewsType" MasterPageFile="~/Frameworks/AdminSubpage.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="head">管理新闻类型</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style>
        th {
            padding:8px; font-size:18px; font-weight:900;background-color: #0094ff;color: #333333;
        }
        td {
             background-color: #FFFBD6;color: #333333;
        }
    </style>
    <asp:ListView runat="server" ID="NewsTypeList" OnItemEditing="NewsTypeList_ItemEditing" OnItemUpdating="NewsTypeList_ItemUpdating" OnItemInserting="NewsTypeList_ItemInserting" OnItemDeleting="NewsTypeList_ItemDeleting" InsertItemPosition="FirstItem" DataKeyNames="TypeId">
        <LayoutTemplate>
            <table runat="server" style="width:100%;text-align:center">
                <tr runat="server">
                    <th runat="server">类型编号</th>
                    <th runat="server">类型名称</th>
                    <th runat="server">可以使用</th>
                    <th runat="server">操作</th>
                </tr>
                <tr id="itemPlaceholder" runat="server"></tr>
            </table>
        </LayoutTemplate>
        <EditItemTemplate>
            <tr style="">
                <td>
                    自动<asp:TextBox runat="server" ID="TypeIdTextBox" style="visibility:collapse; display:none" Text='<%# Eval("TypeId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TypeNameTextBox" runat="server" Text='<%# Bind("TypeName") %>' />
                </td>
                <td>
                    不能修改<input type="checkbox" disabled="disabled" style="visibility:collapse; display:none" checked='<%# Eval("IsValid") %>'/>
                </td>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Update" Text="完成修改" />
                </td>
            </tr>
        </EditItemTemplate>
        <InsertItemTemplate>
            <tr style="">
                <td>
                    自动
                </td>
                <td>
                    <asp:TextBox ID="TypeNameTextBox" runat="server" Text='<%# Bind("TypeName") %>' />
                </td>
                <td>
                    <input type="checkbox" disabled="disabled" checked="checked"/>
                </td>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="完成添加" />
                </td>
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="TypeIdLabel" runat="server" Text='<%# Eval("TypeId") %>' />
                </td>
                <td>
                    <asp:Label ID="TypeNameLabel" runat="server" Text='<%# Eval("TypeName") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="IsValidCheckBox" runat="server" Checked='<%# Eval("IsValid") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="禁用/启用" />
                    <asp:Button ID="Button1" runat="server" CommandName="Edit" Text="修改" />
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>
