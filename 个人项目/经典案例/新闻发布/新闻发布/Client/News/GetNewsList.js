﻿
//将消息组添加到指定的容器
function GroupingAddTolist(_list, _panel) {
    var li = document.createElement("li"); li.textContent = _list.title; _panel.appendChild(li);
    for (var i in _list.values) {
        var l = document.createElement("li");
        var aItem = document.createElement("a"); aItem.href = "News/NewsDetails.aspx?newsid=" + _list.values[i].IndexId;
        aItem.textContent = _list.values[i].NewsTitle;
        l.appendChild(aItem);
        _panel.appendChild(l);
    }
}

//创建组
function CreateGroup(_value) {
    var type = { key: _value.NewsType, title: _value.TypeName, values: new Array() };
    type.values.push(_value);
    return type;
}
//根据新闻类型对数据进行分组
function JsonGrouping(Json) {
    var groupData = new Array();
    for (var i in Json) {
        var ishas = false;
        for (var j in groupData) {
            if (groupData[j].key == Json[i].NewsType) {
                groupData[j].values.push(Json[i]); ishas = true; break;
            }
        }
        if (!ishas) {
            groupData.push(CreateGroup(Json[i]));
        }
    }
    for (var k in groupData) {
        var ul = document.createElement("ul"); // ul.id = "N_" + groupData[k].NewsType;
        document.getElementById("NewsListPanel").appendChild(ul);;
        GroupingAddTolist(groupData[k], ul);

    }
}

//处理从服务器取得的结果
function DisposeData(response) {
    if (response == "servererror") { alert("服务器暂时无法提供数据！"); }
    else
    {
        JsonGrouping(JSON.parse(response));
    }
}

//从服务器取得结果
function AjaxGetNews(serverUrl, _postback) {
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4) {
            if (xml.status == 200) {
                _postback(xml.response);
            }
        }
    }
    xml.open("GET", serverUrl, true);
    xml.send(null);
}
//新闻获取操作
function GetNews() {
    AjaxGetNews("../Handlers/Ajax_GetNewsList.ashx", DisposeData);
}