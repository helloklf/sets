﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsDetails.aspx.cs" Inherits="kehui.NewsDetails" MasterPageFile="~/Frameworks/ClientSubpage.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div style="text-align:center;">
        <h1 id="NewsTitle" style="text-align: center">正在从服务器获取内容...</h1>
        <span id="NewsAuthor"></span>
        <span id="NewsFrom"></span>
        <span id="NewsDate"></span>
        <span id="TypeName"></span>
        <p />
        <span id="GeneralizeTitle" style="font-size:20px;"></span>
        <p />
        <p />
        <iframe id="REPdoc" style="width:100%; min-height:500px; border: 0px; padding: 0px; margin: 0px; background: none;"></iframe>
    </div>
    <script>
        function $$(id) { return document.getElementById(id); }
        var NewsVessel = "REPdoc"; //显示新闻的内联框架id
        var newstype = 1; //全局变量，用户保存原始新闻id

        //处理新闻内容
        function GetNewsPostBack(r) {
            try {
                var js = JSON.parse(r);
                for (var i in js) {
                    $$("NewsTitle").textContent = "新闻标题：" + js[i].NewsTitle;
                    $$("NewsAuthor").textContent = "新闻作者：" + js[i].NewsAuthor;
                    $$("NewsFrom").textContent = "新闻来源：" + js[i].NewsFrom;
                    $$("NewsDate").textContent = "发布时间：" + js[i].NewsDateText;
                    $$("TypeName").textContent = "类型名称：" + js[i].TypeName;
                    $$("GeneralizeTitle").textContent = js[i].GeneralizeTitle;
                    IFrameEnableEdit(js[i].NewsContent, NewsVessel);
                }
            }
            catch (e) { IFrameEnableEdit("数据解析错误！", NewsVessel); }
        }

        //截取newsid(新闻ID)参数
        if (location.href.indexOf('?') > 0) {
            var href = location.href;
            href = href.substring(href.indexOf('?newsid=') + 8, href.length);
            if (href.indexOf('&') > 0) href = href.substring(0, href.indexOf('&'));
            AjaxGetNewsContent(href, GetNewsPostBack);
        }

        //获取新闻内容
        function AjaxGetNewsContent(newsid, _postback) {
            var xml = new XMLHttpRequest();
            xml.onreadystatechange = function () {
                if (xml.readyState == 4) {
                    if (xml.status == 200) {
                        var r = xml.response;
                        if (r != "servererror") _postback(r);
                        else IFrameEnableEdit("没有找到相关新闻！", NewsVessel);
                    }
                    else { IFrameEnableEdit("您的网络不给力！", NewsVessel); }
                }
            }
            var f = new FormData(); f.append("newsid", newsid);
            xml.open("POST", "../../Handlers/Ajax_GetNewsContent.ashx", true);
            xml.send(f);
        }

        function setHeight() {
            document.getElementById("REPdoc").contentWindow.document.readyState;
            document.getElementById("REPdoc").height = document.getElementById("REPdoc").contentWindow.document.body.scrollHeight;
        };
        //HTML编辑器相关初始化
        function IFrameEnableEdit(t, _id, edit) {
            var docu = $$(_id).contentWindow.document;
            $$(_id).contentEditable = true;
            if (edit) docu.designMode = "on";
            docu.write(t);
            document.getElementById("REPdoc").contentWindow.onscroll = setHeight;
            //alert(document.getElementById("REPdoc").contentWindow.frameElement.scrollHeight);
        }
    </script>

</asp:Content>
