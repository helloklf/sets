﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kehui.Frameworks
{
    public partial class ClientSubpage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UID"] != null)
            {
                tbtn_Home.Visible = true;
                tbtn_Login.Visible = false;
                tbtn_Register.Visible = false;
                tbtn_LogOut.Visible = true;
            }
            else
            {
                tbtn_Home.Visible = false;
                tbtn_Login.Visible = true;
                tbtn_Register.Visible = true;
                tbtn_LogOut.Visible = false;
            }
        }
    }
}