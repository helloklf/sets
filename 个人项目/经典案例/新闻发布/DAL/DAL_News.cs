﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;

namespace DAL
{
    /// <summary>
    /// 查询News(新闻条目)表的对应DAL类
    /// </summary>
    public class DAL_News
    {

        /// <summary>
        /// 查询所有新闻 可选参数typeid ,默认为无参数查询
        /// </summary>
        /// <param name="typeid"></param>
        /// <returns></returns>
        public List<Model.M_News> GetNews(int typeid=0)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    DbDataReader dr;
                    if (typeid != 0)
                    {
                        dr = db.ExecuteReader("select * from News join NewsType on News.NewsType = NewsType.TypeId where News.IsValid = 1 and NewsType=@type", new SqlParameter[]{ new SqlParameter("@type", typeid) });
                    }
                    else { dr = db.ExecuteReader("select * from News join NewsType on News.NewsType = NewsType.TypeId where News.IsValid = 1"); }
                    return ReadNewsData(dr);
                }
            }
            catch { throw new Exception("查找新闻出错"); }
        }


        /// <summary>
        /// 更新新闻
        /// </summary>
        /// <param name="news"></param>
        /// <returns></returns>
        public int UpdateNews(Model.M_News news) 
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    return db.ExecuteNonQuery("update news set NewsAuthor=@NewsAuthor,NewsType=@NewsType,NewsFrom = @NewsFrom,NewsTitle=@NewsTitle,GeneralizeTitle=@GeneralizeTitle,NewsContent=@NewsContent where indexid= @IndexId", new SqlParameter[]{
                        new SqlParameter("@IndexId",news.IndexId),
                        new SqlParameter("@NewsAuthor",news.NewsAuthor),
                        new SqlParameter("@NewsType",news.NewsType),
                        new SqlParameter("@NewsFrom",news.NewsFrom),
                        new SqlParameter("@NewsTitle",news.NewsTitle),
                        new SqlParameter("@GeneralizeTitle",news.GeneralizeTitle),
                        new SqlParameter("@NewsContent",news.NewsContent),
                    });
                }
            }
            catch{ throw new Exception("修改新闻出错"); }
        }

        /// <summary>
        /// 删除新闻
        /// </summary>
        /// <param name="news"></param>
        /// <returns></returns>
        public int DeleteNews(Model.M_News news)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    return db.ExecuteNonQuery("update news set isvalid =0 where Indexid = @IndexId", new SqlParameter[]{new SqlParameter("@IndexId",news.IndexId)
                    });
                }
            }
            catch { throw new Exception("删除新闻出错"); }
        }
        /// <summary>
        /// 查询某条新闻
        /// </summary>
        /// <param name="typeid"></param>
        /// <returns></returns>
        public List<Model.M_News> GetNewsByID(int IndexId) 
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    DbDataReader dr;
                    dr = db.ExecuteReader("select * from News join NewsType on News.NewsType = NewsType.TypeId where News.IsValid = 1 and IndexId = @IndexId", new SqlParameter[] { new SqlParameter("@IndexId", IndexId) });
                    return ReadNewsData(dr,true);
                }
            }
            catch { throw new Exception("查询新闻详情出错"); }
        }


        /// <summary>
        /// 从DbDataReader读取数据到泛型集合 适用于M_News（新闻条目）,可选参数details（详情），为true时读取新闻详细内容
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public List<Model.M_News> ReadNewsData(DbDataReader dr, bool details = false) 
        {
            try
            {
                List<Model.M_News> newslist = new List<Model.M_News>();
                Model.M_News news;
                while (dr.Read())
                {
                    news = new Model.M_News();
                    news.IndexId = (int)dr["IndexId"];
                    news.IsValid = (bool?)dr["IsValid"];
                    news.NewsAuthor = dr["NewsAuthor"] as string;
                    if (details) news.NewsContent = dr["NewsContent"] as string;
                    news.NewsFrom = dr["NewsFrom"] as string;
                    news.NewsDate = dr["NewsDate"] as DateTime?;
                    news.NewsTitle = dr["NewsTitle"] as string;
                    news.NewsType = (int)dr["NewsType"];
                    news.TypeName = dr["TypeName"] as string;
                    news.GeneralizeTitle = dr["GeneralizeTitle"] as string;
                    newslist.Add(news);
                }
                return newslist;
            }
            catch { throw new Exception("读取新闻条目错误"); }
        }

        
        /// <summary>
        /// 添加新闻
        /// </summary>
        /// <returns></returns>
        public int AddNews(Model.M_News news)
        {
            try
            {
                using (var db = DbConnects.GetDbHelper())
                {
                    return db.ExecuteNonQuery("insert into News( NewsType, NewsTitle, NewsFrom, NewsAuthor,GeneralizeTitle, NewsContent) values(@NewsType,@NewsTitle,@NewsFrom,@NewsAuthor,@GeneralizeTitle,@NewsContent)", new SqlParameter[]{
                        new SqlParameter("@NewsType",news.NewsType),
                        new SqlParameter("@NewsTitle",news.NewsTitle),
                        new SqlParameter("@NewsFrom",news.NewsFrom),
                        new SqlParameter("@NewsAuthor",news.NewsAuthor),
                        new SqlParameter("@GeneralizeTitle",news.GeneralizeTitle),
                        new SqlParameter("@NewsContent",news.NewsContent),
                    });
                }
            }
            catch { throw new Exception("添加新闻失败"); }
        }
    }
}
