﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 中文编码
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            AllChinese();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            byte[] c =new byte[2];

            string text = "";
            Random r = new Random();
            for (int i = 0; i < 20; i++)
            {
                int index = r.Next(176, 216);
                int index1 = 0;
                if (index == 215)
                {
                    index1 = r.Next(161, 250);
                }
                else
                {
                    index1 = r.Next(161, 255);
                }
                c[0] = Convert.ToByte(index.ToString());
                c[1] = Convert.ToByte(index1.ToString());
                text += "\t"+index.ToString()+index1.ToString()+":"+ Encoding.Default.GetString(c);
            }

            OutText.Text = text;
        }

        public async void AllChinese() 
        {
            Dictionary<string, List<byte[]>> Fonts = new Dictionary<string, List<byte[]>>();
            await Task.Run(() =>
            {
                for (int i = 176; i < 216; i++)
                {
                    List<byte[]> FontList = new List<byte[]>();
                    for (int j = 161; j < 255; j++)
                    {
                        if (i == 215 && j > 249)
                        {
                            int ii = i;
                            int jj = j;
                            continue;
                        }
                        else
                        {
                            byte[] bytes = new byte[2];
                            bytes[0] = Convert.ToByte(i.ToString());
                            bytes[1] = Convert.ToByte(j.ToString());
                            FontList.Add(bytes);
                        }
                    }
                    Fonts.Add(i.ToString(), FontList);
                }
            });
            Xaml_FontList.Items.Clear();
            foreach (var item in Fonts)
            {
                TreeViewItem tvi = new TreeViewItem(); tvi.Header = item.Key; tvi.FontSize = 35;
                foreach (var item2 in item.Value)
                {
                    TreeViewItem tvi2 = new TreeViewItem();
                    tvi2.FontSize = 15;
                    tvi2.Header = item2[0].ToString() + item2[1].ToString() + ":" + Encoding.Default.GetString(item2);
                    tvi.Items.Add(tvi2);
                }
                Xaml_FontList.Items.Add(tvi);
            }

        }
    }
}
