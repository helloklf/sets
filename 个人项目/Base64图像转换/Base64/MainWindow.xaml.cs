﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Base64
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        /// <summary>
        /// 图片转为Base64
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "常见图片文件|*.png;*.bmp;*.jpg;*.jpeg;*.gif|矢量图形(IE9+)|*.svg|所有格式|*.*";
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == true)
            {
                ProcState.IsIndeterminate = true;
                foreach (var file in ofd.FileNames)
                {
                    using (var reader = System.IO.File.OpenRead(file))
                    {
                        FileInfo fileinfo = new FileInfo(file);
                        var bytes = new byte[reader.Length];
                        var len = reader.Read(bytes, 0, (int)reader.Length);
                        var fileType = fileinfo.Extension;
                        if (fileType.ToLower() == ".svg")
                            fileType = ".svg+xml";
                        var base64 = "data:image" + fileType.Replace('.', '/') +";base64," + Convert.ToBase64String(bytes);
                        File.WriteAllText(file + ".txt", base64, Encoding.UTF8);
                    }
                }
                ProcState.IsIndeterminate = false;
                MessageBox.Show("操作成功！");
            }
        }

        /// <summary>
        /// Base64转为图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Border_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Base64编码的图片文本|*.txt"; ofd.Title = "UTF8编码存储的Base64标准文本";
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == true)
            {
                ProcState.IsIndeterminate = true;
                foreach (var file in ofd.FileNames)
                {
                    var data = File.ReadAllText( file,Encoding.UTF8 );
                    var filetype = data.Substring(0, data.IndexOf(';')); filetype = filetype.Substring( filetype.IndexOf('/') ).Replace('/','.');
                    if (filetype.ToLower() == ".svg+xml") filetype = ".svg";
                    data = data.Substring( data.IndexOf(',')+1 );
                    File.WriteAllBytes(file+filetype, Convert.FromBase64String(data));
                }
                ProcState.IsIndeterminate = false;
                MessageBox.Show("操作成功！");
            }
        }

        private void Border_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            Close();
        }
    }
}
