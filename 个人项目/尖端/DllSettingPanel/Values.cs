﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.IO;

namespace DllSettingPanel
{
    /// <summary>
    /// 全局共享的属性值
    /// </summary>
    public static class Values
    {
        /// <summary>
        /// 基本外观
        /// </summary>
        public static DllSetting.ApplictionSurface SurfaceSetting = DllSetting.Values.SurfaceSetting;
    }
}
