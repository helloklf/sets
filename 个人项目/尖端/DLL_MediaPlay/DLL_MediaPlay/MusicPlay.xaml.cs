﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;
using System.IO;

namespace DLL_MediaPlay
{
    /// <summary>
    /// MusicPlay.xaml 的交互逻辑
    /// </summary>
    public partial class MusicPlay : UserControl
    {
        PlayState PS;//播放列表及状态信息
        public MusicPlay()
        {
            InitializeComponent(); time.Tick += time_Tick; time.Interval = TimeSpan.FromSeconds(0.2);
        }

        /// <summary>
        /// 播放、暂停
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (IsPlay)
                {
                    Stop();
                }
                else Play();
            }
            catch { }
        }

        #region 打开文件
        /// <summary>
        /// 打开文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenFilePlay(object sender, MouseButtonEventArgs e)
        {
           // ME.Source = new Uri(@"http://sc1.111ttt.cn/2014/1/07/23/231615173.mp3");
           // ME.Source = new Uri("http://avi.mtvxz.cn/%E5%BA%84%E5%BF%83%E5%A6%8D_%E7%A5%81%E9%9A%86%E4%B8%80%E4%B8%87%E4%B8%AA%E8%88%8D%E4%B8%8D%E5%BE%97%5Bmtvxz.cn%5D.avi");
            OpenMedia();
          //  ME.Play();
            
        }
        #endregion

        #region 音量控制
        /// <summary>
        /// 音量控制
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Voice_Click(object sender, MouseButtonEventArgs e)
        {
            if (ME.IsMuted)
            {
                SoundValue.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/音量小.png", UriKind.Relative));
                ME.IsMuted = false;
                ME.Volume = 0.25;
            }
            else 
            {
                if (ME.Volume == 0.25) 
                {
                    SoundValue.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/音量中.png", UriKind.Relative));
                    ME.Volume = 0.5;
                }
                else if (ME.Volume == 0.5)
                {
                    SoundValue.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/音量大.png", UriKind.Relative));
                    ME.Volume = 0.75;
                }
                else if (ME.Volume == 0.75)
                {
                    SoundValue.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/音量最大.png", UriKind.Relative));
                    ME.Volume = 1;
                }
                else
                {
                    SoundValue.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/静音.png", UriKind.Relative));
                    ME.Volume = 0; ME.IsMuted = true;
                }
            }
        }
        #endregion

        #region 点击播放列表
        private void PlayList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PlayListBox.SelectedItem != null)
            {
                PlayList uri = PlayListBox.SelectedValue as PlayList;
                if (uri!=null)
                {
                    ME.Source = new Uri(uri.FullName);
                    Play(); PS.index = PlayListBox.SelectedIndex;
                }
            }
        }
        #endregion

        DispatcherTimer time = new DispatcherTimer();
        void time_Tick(object sender, EventArgs e)
        {
            try
            {
                PRO.Value = ME.Position.TotalMilliseconds;
                double d= ME.DownloadProgress;
                ProText.Text = (int)ME.Position.TotalMinutes + ":" + ME.Position.Seconds + "/" + (int)ME.NaturalDuration.TimeSpan.TotalMinutes + ":" + ME.NaturalDuration.TimeSpan.Seconds;
            }
            catch { }
            //PRO1.Maximum = ME.NaturalDuration.TimeSpan.TotalMilliseconds;
            //PRO1.Value = ME.NaturalDuration.TimeSpan.TotalMilliseconds * d;
            //double s = ME.BufferingProgress;
        }

        private void ME_Loaded(object sender, RoutedEventArgs e)
        {
            time.Start();
            PRO.Maximum = ME.NaturalDuration.TimeSpan.TotalMilliseconds;
            if (ME.HasVideo) Tab.SelectedIndex = 1;
            else Tab.SelectedIndex = 0;
        }

        #region 快进1/35 或30秒
        private void PostionAdd(object sender, MouseButtonEventArgs e)
        {
            int t = (int)ME.NaturalDuration.TimeSpan.TotalSeconds / 35; if (t > 30) t = 30;
            MePostionSet(t);
        }
        #endregion

        #region 后退1/35或1分钟
        private void PostionCut(object sender, MouseButtonEventArgs e)
        {
            int t = (int)ME.NaturalDuration.TimeSpan.TotalSeconds / 35; if (t > 60) t = 60;
            MePostionSet(-t);
        }
        #endregion

        #region 上一曲按下
        private void LastMusic(object sender, MouseButtonEventArgs e)
        {
            PlayLas();
        }
        #endregion

        #region 停止按钮
        private void StopMusic(object sender, MouseButtonEventArgs e)
        {
            Stop();
        }
        #endregion

        /// <summary>
        /// 点击下一曲按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextMusic(object sender, MouseButtonEventArgs e)
        {
            PlayNext();//播放下一个文件
        }

        /// <summary>
        /// 当前文件播放完时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ME_MediaEnded(object sender, RoutedEventArgs e)
        {
            PlayNext();//播放下一个文件
        }
        #region 方法组

        #region 打开音乐
        /// <summary>
        /// 打开播放文件
        /// </summary>
        public void OpenMedia()
        {
            System.Windows.Forms.OpenFileDialog of = new System.Windows.Forms.OpenFileDialog(); of.Multiselect = true;
            of.Filter = "无损音乐|*.Flac;*.Ape|MP3文件|*.Mp3|WAV波形音频|*.Wav|OGG媒体文件|*.Ogg|所有支持的音频|*.Mp3;*.Wav;*.Flac;*.Ape;*.Ogg;*.aac;*.ac3|视频|*.mkv";
            if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] text = of.FileNames;
                PS.PL.Clear();
                foreach (var item in text)
                {
                    PlayList PL = new PlayList();
                    PL.FullName = item;
                    PS.PL.Add(PL);
                }
                PS.index = 0;
                saveList();//保存播放列表
                ME.Source = new Uri(PS.PL[0].FullName); Play();
                PlayListBox.ItemsSource = null; PlayListBox.ItemsSource = PS.PL; 
            }
        }
        #endregion
        #region 播放定位
        /// <summary>
        /// 播放器定位（用一个整数表示要前进或后退的秒数）
        /// </summary>
        /// <param name="I"></param>
        public void MePostionSet(int I)
        {
            try { ME.Position += TimeSpan.FromSeconds(I); }
            catch { }
        }
        #endregion
        #region 播放下个
        /// <summary>
        /// 播放下个文件
        /// </summary>
        public void PlayNext()
        {
            if (PS.index + 1 != PS.Count)
            {
                try
                {
                    ME.Source = new Uri(PS.PL[PS.index + 1].FullName);
                    Play();
                    PS.index += 1; PlayListBox.SelectedIndex += 1;
                }
                catch { Stop(); }
            }
        }
        #endregion
        #region 播放上个
        /// <summary>
        /// 播放上一曲
        /// </summary>
        public void PlayLas() 
        {
            if (PS.index - 1 > 0)
            {
                try
                {
                    ME.Source = new Uri(PS.PL[PS.index - 1].FullName);
                    Play();
                    PS.index -= 1; PlayListBox.SelectedIndex -= 1;
                }
                catch { }
            }
            else { Stop(); }
        }
        #endregion
        #region 保存播放列表
        public bool saveList() 
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(typeof(PlayState));
                xml.Serialize(File.CreateText("PlayList.pls"),PS);
                return true;
            }
            catch { return false; }
        }
        #endregion
        #region 加载播放列表
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(typeof(PlayState));
                PlayState PL = xml.Deserialize(File.OpenRead("PlayList.pls")) as PlayState;
                PS = PL; PlayListBox.ItemsSource = null; PlayListBox.ItemsSource = PL.PL; 
            }
            catch { PS = new PlayState(); }
        }
        #endregion

        public bool IsPlay = false;

        #region 播放
        public void Play()
        {
            try
            {
                PulseImage.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/暂停.png", UriKind.Relative));
                if (ME.Source == null)
                {
                    if (PS.PL.Count == 0) return;
                    try
                    {
                        ME.Source = new Uri(PS.PL[0].FullName); ME.Play();
                    }
                    catch { }
                }
                else ME.Play();
                IsPlay = true;
            }
            catch { }
        }
        #endregion

        #region 暂停
        public void Stop()
        {
            try
            {
                PulseImage.Source = new BitmapImage(new Uri("/DLL_MediaPlay;component/Play/播放.png", UriKind.Relative));
                if (!ME.CanPause) return;
                ME.Pause(); saveList(); //保存播放列表
                IsPlay = false;
            }
            catch { }
        }
        #endregion

        private void List_Play(object sender, MouseButtonEventArgs e)
        {
            Tab.SelectedIndex = Tab.SelectedIndex == 0 ? 1 : 0;
        }

        #endregion
    }

    #region 播放文件信息
    public class PlayList
    {
        public string FullName;//全路径
        public string ShortName//显示名
        {
            get
            {
                if (FullName != null)
                {
                    string name = FullName.Substring(FullName.LastIndexOf('\\')+1);
                    return name;
                }
                return null;
            }
        }
    }
    #endregion

    #region 播放状态
    public class PlayState 
    {
        public int index=0;
        public int Count { get { return PL.Count; } }
        public TimeSpan ts; 
        public List<PlayList> PL = new List<PlayList>();
    }
    #endregion
}
