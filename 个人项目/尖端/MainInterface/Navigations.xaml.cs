﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Term2
{
    /// <summary>
    /// Navigations.xaml 的交互逻辑
    /// </summary>
    public partial class Navigations : UserControl
    {
        public Navigations()
        {
            InitializeComponent();

            Up = NotBug;
            Down = NotBug;
            Left = NotBug;
            Right = NotBug;
        }

        /// <summary>
        /// 委托：上
        /// </summary>
        public Void Up;

        /// <summary>
        /// 委托：下
        /// </summary>
        public Void Down;

        /// <summary>
        /// 委托左
        /// </summary>
        public Void Left;

        /// <summary>
        /// 委托右
        /// </summary>
        public Void Right;

        public void NotBug() 
        {
            
        }
        
    }
    public delegate void Void();
}
