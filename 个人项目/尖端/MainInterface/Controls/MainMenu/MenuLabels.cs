﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.IO;

namespace Project_Term2.MainMenu
{
    public class MenuLabels
    {
        public static ObservableCollection<MenuLabel> MenuLists = new ObservableCollection<MenuLabel>();
        /// <summary>
        /// 序列化
        /// </summary>
        /// <param name="uri"></param>
        public static void MenuSerialization(params string[] uri) 
        {
            using(FileStream fs = File.OpenWrite(uri==null?"MenuList.xml":uri[0]))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<MenuLabelSeri>));
                List<MenuLabelSeri> list = new List<MenuLabelSeri>();
            }
        }
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="uri"></param>
        public static void MenuDeSerialize(params string[] uri) 
        {
            using (FileStream fs = File.OpenRead( uri==null?"MenuList.xml":uri[0]))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<MenuLabelSeri>));
                List<MenuLabelSeri> list = (List<MenuLabelSeri>)xml.Deserialize(fs);
            }
        }
    }
    public class MenuLabel:INotifyPropertyChanged
    {
        string title = "";
        /// <summary>
        /// 文本
        /// </summary>
        public string TitleTxt
        {
            get { return title; }
            set { title = value; Changed("Title"); }
        }

        ImageSource bitImage;
        /// <summary>
        /// 图像
        /// </summary>
        public ImageSource TitleImage
        {
            get { return bitImage; }
            set { bitImage = value; Changed("BitImage"); }
        }

        ImageSource Images;
        /// <summary>
        /// 资源路径
        /// </summary>
        public ImageSource ImageS
        {
            get { return Images; }
            set { Images = value; Changed("ImageS"); }
        }

        Visibility vState = Visibility.Visible;

        public Visibility VState
        {
            get { return vState; }
            set { vState = value; Changed("VState"); }
        }

        SolidColorBrush foreColor = new SolidColorBrush(Colors.Black);
        /// <summary>
        /// 前景颜色
        /// </summary>
        public SolidColorBrush ForeColor
        {
            get { return foreColor; }
            set { foreColor = value; }
        }

        SolidColorBrush bgColor = new SolidColorBrush(Colors.White);
        /// <summary>
        /// 背景颜色
        /// </summary>
        public SolidColorBrush BGColor
        {
            get { return bgColor; }
            set { bgColor = value; }
        }


        public void Changed(string name)
        {
            if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(name)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class MenuLabelSeri 
    {
        
    }
}
