﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Term2.MainMenu
{
    /// <summary>
    /// P_MainMenu.xaml 的交互逻辑
    /// </summary>
    public partial class P_MainMenu : UserControl
    {
        public P_MainMenu()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.C.DataContext = Values.SurfaceSetting.Surface; MainMenuCreate();
        }

        #region 主菜单构造辅助
        private bool MainMenuCreate()
        {
            try
            {
                foreach (object item in MainMenu_Lable_List.Children)
                {
                    if (item is Label)
                    {
                        //item.MouseEnter += Menu_Lable_Border1;
                        //item.MouseLeave += Menu_Lable_Border0;
                        //item.MouseLeftButtonDown += Menu_Lable_Left;
                        //item.MouseRightButtonDown += Menu_Lable_Right;
                    }
                }
                return true;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + "\r\n窗体构造遇到无法纠正的错误，请向开发者获取帮助！"); return false; }
        }

        #endregion

        /// <summary>
        /// 主菜单示范性双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoPage1(object sender, MouseButtonEventArgs e)
        {
            //? this.Page_CorePage_View.ScrollToHorizontalOffset(this.Page_CorePage_View.HorizontalOffset + this.Page_CorePage_View.ActualWidth);
        }

        /// <summary>
        /// 主菜单鼠标离开：隐藏边框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_Lable_Border0(object sender, MouseEventArgs e)
        {
            Label l = sender as Label;
            l.BorderThickness = new Thickness(0);
        }

        /// <summary>
        /// 主菜单鼠标移入：显示边框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_Lable_Border1(object sender, MouseEventArgs e)
        {
            Label l = sender as Label;
            l.BorderThickness = new Thickness(1, 1, 2, 2);
        }


        /// <summary>
        /// 主菜单按钮右键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_Lable_Right(object sender, MouseButtonEventArgs e)
        {
            Label l = sender as Label;
            l.Background = new SolidColorBrush(Colors.Green);
        }

        /// <summary>
        /// 主菜单按钮左键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menu_Lable_Left(object sender, MouseButtonEventArgs e)
        {

            Label l = sender as Label;
            l.Background = new SolidColorBrush(Colors.Yellow);
        }

        private void TabChange_Click(object sender, RoutedEventArgs e)
        {
            Tab.SelectedIndex=(bool)IsCkecked_Hide.IsChecked?1:0;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (object item in MainMenu_Lable_List.Children)
            {
                if (item is Label)
                {
                    Label lb = item as Label;
                    if (lb.IsFocused)
                    {
                        MainMenu_Lable_List.Children.Remove(lb);
                        //item.Visibility = Visibility.Collapsed;
                        MainMenu_Hide.Children.Add(lb);
                        break;
                    }
                }
            }
            //MessageBox.Show("!1");
        }

        private void MenuItem_Click_NoHide(object sender, RoutedEventArgs e)
        {
            foreach (Control item in MainMenu_Hide.Children)
            {
                if (item.IsFocused)
                {
                    MainMenu_Hide.Children.Remove(item);
                    //item.Visibility = Visibility.Collapsed;
                    MainMenu_Lable_List.Children.Add(item);
                    break;
                }
            }
        }

    }
}
