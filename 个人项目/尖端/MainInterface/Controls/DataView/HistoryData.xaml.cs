﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project_Term2.DataView
{
    /// <summary>
    /// HistoryData.xaml 的交互逻辑
    /// </summary>
    public partial class HistoryData : UserControl
    {
        public HistoryData()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.C.DataContext = Values.SurfaceSetting.Surface;
        }

        private void ReturnTo_MainMenu(object sender, MouseButtonEventArgs e)
        {
            if (MainTab.SelectedIndex != 0) { MainTab.SelectedIndex = 0; HeadText.Text = "历史记录"; }
            else Delegates.ToPageIndex(0);
        }

        private void ToHelpPage(object sender, MouseButtonEventArgs e)
        {

        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainTab.SelectedIndex = 1; HeadText.Text = "历史记录-消费历史";
        }

        private void ReturnTo_Tab0(object sender, MouseButtonEventArgs e)
        {
            MainTab.SelectedIndex = 0;
        }

        /// <summary>
        /// 向上一级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StoreyUp_Click(object sender, MouseButtonEventArgs e)
        {
            if(MainTab.SelectedIndex!=0)
            MainTab.SelectedIndex -= 1;
        }

        /// <summary>
        /// 住房历史记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabTo_HousingData(object sender, MouseButtonEventArgs e)
        {
            MainTab.SelectedIndex = 2; HeadText.Text = "历史记录-住房历史";
        }

        private void Navigations_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) 
            {
               
            }
        }
    }
}
