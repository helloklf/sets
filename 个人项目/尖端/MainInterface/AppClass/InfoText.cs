﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Term2.Other
{
    public class InfoText
    {
        /// <summary>
        /// 设置保存
        /// </summary>
        public static string SettingSave = "应用程序外观设定已存储，下次启动时将自动载入当前设定！";
        
        /// <summary>
        /// 设置保存出错
        /// </summary>
        public static string SettringSaveError = "实验性功能出现严重的错误，外观设定无法正常保存，请重启应用程序或联系开发者！";

        /// <summary>
        /// 因为输入错误 无法登陆
        /// </summary>
        public static string UserLoginMessage = "当您看到这个提示，请您认真的检查您输入的信息。比如说账户名或密码为空，已经密码长度少于6位！";

        /// <summary>
        /// 用户登录失败
        /// </summary>
        public static string UserLoginError="用户登陆失败，请检查账号和密码是否输入正确。如果确认无误依然无法正常登陆，请告知开发者！";

        /// <summary>
        /// 账户注销提示
        /// </summary>
        public static string ExitLogionQuestion = "你当前已经在线，继续此操作将注销当前已经登陆的账户，是否要注销？";

        /// <summary>
        /// 关于安全协议的说明
        /// </summary>
        public static string AboutSecurity = "为什么会需要此功能？\r\n因为每种用户具有不同的操作权限，可能无法执行某些关键性操作，即时是管理员也是会具有一定限制的。那么，此时你将需要输入超级管理员密码才能继续操作！";

        /// <summary>
        /// 应用程序严重错误
        /// </summary>
        public static string ApplictionError = "应用程序发生了不可预料的严重错误，请将该错误报告给开发者，以便修正！";

        /// <summary>
        /// 快捷键说明
        /// </summary>
        public static string HelpInfo_HotKey = "\r\nF1显示帮助窗口！\r\nF2切换导航栏显示状态！\r\nF3切换工具栏显示状态！\r\nF4切换状态栏显示状态！\r\nF11切换全屏、窗口显示！";


        /// <summary>
        /// 房间信息读取器参数无效
        /// </summary>
        public static string RoomReadError = "此方法需要一个Int型参数表示操作类型。\r\n\t0：读取所有有效数据，\r\n\t1：按房间类型过滤数据，\r\n\t2：按房间状态过滤数据，3：双重条件过滤，4：按搜索方式";

        /// <summary>
        /// 按房间计费时长搜索输入无效
        /// </summary>
        public static string RoomSeachNotInt = "你输入的不是一个有效的整数。如果你要找日结房，请输入24，钟点房请输入1";

        /// <summary>
        /// 价格搜索帮助信息
        /// </summary>
        public static string RoomSHelpPrice = "价格搜索以输入的文本作为客户可以接受的最高价格，查询结果按价格优惠程度排序";

        /// <summary>
        /// 价格搜索输入无效
        /// </summary>
        public static string RoomSeacheNoPrice = "价格搜索以输入的文本作为客户可以接受的最高价格，查询结果按价格优惠程度排序";
    }
}
