﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Windows.Threading;

using System.Runtime.InteropServices;
using TModel;

namespace Project_Term2
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class StartWindow : Window
    {
        /// <summary>
        /// 窗体构建时
        /// </summary>
        public StartWindow()
        {
            InitializeComponent();
            SearchBox.DataList = new List<string> 
            {
                "adsfdasf","asdgdasg","啊都发生发射点放大发大水","大飒飒法十多个但是","的舒适感差点洒哥才是成功","速度fads发但是","但是撒的吃饭速度帝国时代"
            };

            #region 原始方法
            TabItem t = new TabItem();
            t.Visibility = Visibility.Collapsed;
            t.Content = new DataView.RoomList();
            MainMenuList.Items.Add(t);

            TabItem t2 = new TabItem();
            t2.Visibility = Visibility.Collapsed;
            t2.Content = new DataView.DataGrid1();
            MainMenuList.Items.Add(t2);

            TabItem t3 = new TabItem();
            t3.Visibility = Visibility.Collapsed;
            t3.Content = new Project_Term2_Client.ClientData();
            MainMenuList.Items.Add(t3);


            TabItem t4 = new TabItem();
            t4.Visibility = Visibility.Collapsed;
            t4.Content = new Project_Term2_Staff.StaffData();
            MainMenuList.Items.Add(t4);


            TabItem t5 = new TabItem();
            t5.Visibility = Visibility.Collapsed;
            t5.Content = new Project_Term2_RoomList.RoomData();
            MainMenuList.Items.Add(t5);

            TabItem t6 = new TabItem();
            t6.Visibility = Visibility.Collapsed;
            t6.Content = new Project_Term2_BaseData.BaseData();
            MainMenuList.Items.Add(t6);

            TabItem t7 = new TabItem();
            t7.Visibility = Visibility.Collapsed;
            t7.Content = new Project_Term2.DataView.HistoryData();
            MainMenuList.Items.Add(t7);


            //xmlns:SettingPage="clr-namespace:Project_Term2.Page_Setting"
            //xmlns:ToolBar="clr-namespace:Project_Term2.Top"
            //xmlns:MainMenu="clr-namespace:Project_Term2.MainMenu"
            //xmlns:LeftMenu="clr-namespace:Project_Term2.LeftMenu"
            //xmlns:Other="clr-namespace:Project_Term2_Staff;assembly=Project_Term2_Staff"
            //xmlns:Other2="clr-namespace:Project_Term2_BaseData;assembly=Project_Term2_BaseData"
            //xmlns:Other3="clr-namespace:Project_Term2_RoomList;assembly=Project_Term2_RoomList"
            //xmlns:Other4="clr-namespace:Project_Term2_Client;assembly=Project_Term2_Client"
            //xmlns:Settings="clr-namespace:DllSettingPanel;assembly=DllSettingPanel"
            //<!--房间列表-->
            //<TabItem Visibility="Collapsed">
            //    <view:RoomList></view:RoomList>
            //</TabItem>
            //<!--数据统计-->
            //<TabItem Visibility="Collapsed">
            //    <view:DataGrid1></view:DataGrid1>
            //</TabItem>
            //<!--客户管理-->
            //<TabItem Visibility="Collapsed">
            //    <Other4:ClientData></Other4:ClientData>
            //</TabItem>
            //<!--员工管理-->
            //<TabItem Visibility="Collapsed">
            //    <Other:StaffData></Other:StaffData>
            //</TabItem>
            //<!--房间管理-->
            //<TabItem Visibility="Collapsed">
            //    <Other3:RoomData></Other3:RoomData>
            //</TabItem>
            //<!--基础数据-->
            //<TabItem Visibility="Collapsed">
            //    <Other2:BD></Other2:BD>
            //</TabItem>
            #endregion

            //Binding bd = new Binding() { Source = MainMenuView, Path = new PropertyPath("ActualWidth") };
            //Binding bd2 = new Binding() { Source = MainMenuView, Path = new PropertyPath("ActuaHeight") };

            ////! 房间列表
            //DataView.RoomList rl = new DataView.RoomList(); rl.Name = "RoomList";
            //rl.SetBinding(WidthProperty, bd);rl.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(rl);

            //foreach (Control item in MainMenuList.Children)
            //{
            //    if (item.Name == "RoomList") MessageBox.Show("啊！");
            //}

            ////! 历史数据
            //DataView.DataGrid1 dg = new DataView.DataGrid1(); 
            //dg.SetBinding(WidthProperty, bd);dg.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(dg);

            ////! 客户信息
            //Project_Term2_Client.ClientData pc = new Project_Term2_Client.ClientData(); pc.Name = "ClientData";
            //pc.SetBinding(WidthProperty, bd);pc.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(pc);

            ////! 员工信息
            //Project_Term2_Staff.StaffData ps = new Project_Term2_Staff.StaffData(); ps.Name = "StaffData";
            //ps.SetBinding(WidthProperty, bd);ps.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(ps);

            ////! 房间管理
            //Project_Term2_RoomList.RoomData pr = new Project_Term2_RoomList.RoomData(); pr.Name = "RoomData";
            //pr.SetBinding(WidthProperty, bd);pr.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(pr);

            ////! 基础数据
            //Project_Term2_BaseData.BD pb = new Project_Term2_BaseData.BD(); pb.Name = "BaseData";
            //pb.SetBinding(WidthProperty, bd);pb.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(pb);

            ////! 历史记录
            //Project_Term2.DataView.HistoryData ph = new DataView.HistoryData(); ph.Name = "HistoryData";
            //ph.SetBinding(WidthProperty, bd); ph.SetBinding(HeightProperty, bd2);
            //MainMenuList.Children.Add(ph);


            //显示登陆页
            LoginPage l = new LoginPage(); l.Show();

        }



        DllSetting.AppSurface SET = Values.SurfaceSetting.Surface;

        /// <summary>
        /// 窗口加载时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Collapsed;
                ThemeSetting();//外观初始化
                #region 其它可延迟加载的操作
                await Task.Run(() => DelegateCreate());//委托初始化
                #endregion
                #region 数据库自动结算、取消失效预订功能
                DispatcherTimer time = new DispatcherTimer();
                time.Interval = new TimeSpan(0, 5, 0); time.Start();
                time.Tick += (a, b) => { DbSettleAccounts(); };
                #endregion 
                MessageWindow.Show();//启动消息系统
                Inform.Delegates.AddChildren_("全局消息系统正式提供使用，所有提示消息都可以该形式弹出！", "新本功能");
            }
            catch { MessageBox.Show("在应用程序启动时发生不可预料的错误！"); }
        }

        /// <summary>
        /// 消息系统-消息窗口
        /// </summary>
        Inform.MessageWindow MessageWindow = new Inform.MessageWindow();

        /// <summary>
        /// 自动结算
        /// </summary>
        public async void DbSettleAccounts() 
        {
            try
            {
                bool b = false;
                await Task.Run(() => 
                {
                    try
                    {
                        b = DLL_BLL.AutoRunProcBLL.Proc_AutoSettleAccounts();
                    }
                    catch (Exception ex) { Functions.ShowMessage(ex.Message,"自动结算错误"); }
                });
                if (b)
                {
                    List<View_ClientRoomState> list = DLL_BLL.AutoRunProcBLL.Proc_AccountRemind();
                    foreach (var item in list)
                    {
                        Functions.ShowMessageAndToList("房间“" + item.RoomID + "”客户 " + item.ClientName + "的账户已欠费 " + item.ClientAccount + "元。请及时处理！", "数据库通知", true);
                    }
                }
                //查询失效预订
                await Task.Run(() =>
                {
                    try
                    {
                        List<T_BookRoom> TB = DLL_BLL.AutoRunProcBLL.Proc_AutoDeleteBook();
                        foreach (var item in TB)
                        {
                            Functions.ShowMessageAndToList("客户"+item.ClientName+"预订的房间"+item.RoomID+"超出预定有效期"+item.BookValidTime+"天。系统已自动清除该预订信息！","预订失效", false);
                        }
                    }
                    catch (Exception ex) { Functions.ShowMessage(ex.Message, "自动结算错误"); }
                });
                Functions.ShowMessage("自动结算程序执行(扣除客户住房费用和清理过期预订)。执行时间:"+DateTime.Now.ToShortTimeString(),"自动结算");

            }
            catch (Exception ex)
            {
                Functions.ShowMessage(ex.Message, "出现错误");
            }
        }

        #region 主题加载
        /// <summary>
        /// 主题加载
        /// </summary>
        public void ThemeSetting()
        {
            Page_Setting_Content.DataContext = Values.PageStates;//关联页面显示状态
            Page_UserPage_Content.DataContext = Values.PageStates;//关联页面显示状态
            Grid_.DataContext = Values.SurfaceSetting;//背景图像绑定
            C.DataContext = SET;//外观色彩的绑定
        }
        #endregion

        #region 快捷键
        /// <summary>
        /// 按键按下事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F2) { if (Delegates.LeftMenuState != null) Delegates.LeftMenuState(); }
            else if (e.Key == Key.F3) { Top_ToolBar.Visibility = Top_ToolBar.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible; }
            else if (e.Key == Key.F4) { BottomStateBar.Visibility = BottomStateBar.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible; }
            else if (e.Key == Key.F11) { WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized; }
        }
        #endregion
    }
}
